package prizma.maxiru.testsuite;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import java.net.MalformedURLException;

import prizma.maxiru.framework.common.*;
import prizma.maxiru.tests.*;

@RunWith(Suite.class)
// Specify an array of test classes
@Suite.SuiteClasses({
        WebSDKRunLongTrendingBotAndValidateLimitOfTradingAmountOnExchangeTests.class
})

// The actual class should be empty
public class PrizmaTestSuiteRunLongTrendingBotAndValidateLimitOfTradingAmountOnExchange {

    @BeforeClass
    public static void beforeClass() throws MalformedURLException {
        String platform = "Chrome";
        String link = "https://dev.prizma-sdk.simis.ai/";

        PrizmaDriver.setup(platform, link);
    }

    @AfterClass
    public static void afterClass() {
        PrizmaDriver.quit();
    }
}
package prizma.maxiru.testsuite;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import java.net.MalformedURLException;

import prizma.maxiru.framework.common.*;
import prizma.maxiru.tests.*;

@RunWith(Suite.class)
// Specify an array of test classes
@Suite.SuiteClasses({
        WebSDKRunAllBotsAndValidateBotStopTests.class
})

// The actual class should be empty
public class PrizmaTestSuiteRunAllBotsAndValidateBotStop {

    @BeforeClass
    public static void beforeClass() throws MalformedURLException {
        String platform = "Chrome";
        String link = "http://127.0.0.1:8000";

        //tests works only locally

        PrizmaDriver.setup(platform, link);
    }

    @AfterClass
    public static void afterClass() {
        PrizmaDriver.quit();
    }
}
package prizma.maxiru.tests;

import org.junit.FixMethodOrder;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import prizma.maxiru.framework.common.*;

import static app.PrizmaChartingPage.*;
import static prizma.maxiru.framework.common.PrizmaDriver.*;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)

public class WebSDKChartingIndicatorsConfigurationsTests {
    @Rule
    public PrizmaScreenshotRule screenshotRule = new PrizmaScreenshotRule();

    public Actionwords actionwords = new Actionwords();

    public void testcase001OpenChartingPageAndDisableAllChartingIndicatorsBeforeTests(String link, String message) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.clickOnChartingBtnOnHomePage();
        actionwords.getScreenshot("testcase001OpenChartingPageAndDisableAllChartingIndicatorsBeforeTests_1");
        actionwords.clickOnIndicatorsBtn();
        actionwords.getScreenshot("testcase001OpenChartingPageAndDisableAllChartingIndicatorsBeforeTests_2");
        actionwords.verifyThatIndicatorNoSettingsMessageAppear(message);
        actionwords.disableAllChartingIndicatorsBeforeTests();
        actionwords.verifyThatVolumeCheckboxDisabled();
        actionwords.verifyThatBollingerBandsDisabled();
        actionwords.verifyThatRelativeStrengthIndexCheckboxDisabled();
        actionwords.getScreenshot("testcase001OpenChartingPageAndDisableAllChartingIndicatorsBeforeTests_3");
        actionwords.clickOnCloseIndicatorsBtn();
        actionwords.verifyThatIndicatorsDialogCloseAfterClickOnCloseBtn();
        actionwords.getScreenshot("testcase001OpenChartingPageAndDisableAllChartingIndicatorsBeforeTests_4");
    }

    @Test
    public void testcase001OpenChartingPageAndDisableAllChartingIndicatorsBeforeTestsDataSet1() {
        testcase001OpenChartingPageAndDisableAllChartingIndicatorsBeforeTests(websitelink, INDICATOR_NO_SETTINGS_MESSAGE);
    }

    public void testcase002OpenChartingPageAndClickOnVolumeIndicatorToCheckThatIndicatorNoSettingsMessageAppear(String link, String message) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.clickOnChartingBtnOnHomePage();
        actionwords.getScreenshot("testcase002OpenChartingPageAndClickOnVolumeIndicatorToCheckThatIndicatorNoSettingsMessageAppear_1");
        actionwords.clickOnIndicatorsBtn();
        actionwords.getScreenshot("testcase002OpenChartingPageAndClickOnVolumeIndicatorToCheckThatIndicatorNoSettingsMessageAppear_2");
        actionwords.clickOnVolumeLabel();
        actionwords.verifyThatIndicatorNoSettingsMessageAppear(message);
        actionwords.getScreenshot("testcase002OpenChartingPageAndClickOnVolumeIndicatorToCheckThatIndicatorNoSettingsMessageAppear_3");
    }

    @Test
    public void testcase002OpenChartingPageAndClickOnVolumeIndicatorToCheckThatIndicatorNoSettingsMessageAppearDataSet1() {
        testcase002OpenChartingPageAndClickOnVolumeIndicatorToCheckThatIndicatorNoSettingsMessageAppear(websitelink, INDICATOR_NO_SETTINGS_MESSAGE);
    }

    public void testcase003OpenChartingPageAndClickOnBollingerBandsIndicatorAndUpdateConfigurationsAndResetBackAfter(String link, String message, String length, String standartDeviation, String degreesOfFreedom, String offset) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.clickOnChartingBtnOnHomePage();
        actionwords.getScreenshot("testcase003OpenChartingPageAndClickOnBollingerBandsIndicatorAndUpdateConfigurationsAndResetBackAfter_1");
        actionwords.clickOnIndicatorsBtn();
        actionwords.getScreenshot("testcase003OpenChartingPageAndClickOnBollingerBandsIndicatorAndUpdateConfigurationsAndResetBackAfter_2");
        actionwords.clickOnBollingerBandsLabel();
        actionwords.verifyThatIndicatorNoSettingsMessageNotAppear(message);
        actionwords.saveForFutureTestsValuesFromAllBollingerBandsIndicatorConfigurationsFields();
        actionwords.verifyThatIndicatorsApplyBtnDisabled();
        actionwords.verifyThatIndicatorsResetBtnDisabled();
        actionwords.setLength(length);
        actionwords.setStandartDeviation(standartDeviation);
        actionwords.setDegreesOfFreedom(degreesOfFreedom);
        actionwords.setOffset(offset);
        actionwords.verifyThatIndicatorsApplyBtnEnabled();
        actionwords.verifyThatIndicatorsResetBtnDisabled();
        actionwords.getScreenshot("testcase003OpenChartingPageAndClickOnBollingerBandsIndicatorAndUpdateConfigurationsAndResetBackAfter_3");
        actionwords.clickOnIndicatorsApplyBtn();
        actionwords.verifyThatIndicatorsApplyBtnDisabled();
        actionwords.verifyThatIndicatorsResetBtnEnabled();
        actionwords.getScreenshot("testcase003OpenChartingPageAndClickOnBollingerBandsIndicatorAndUpdateConfigurationsAndResetBackAfter_4");
        actionwords.clickOnIndicatorsResetBtn();
        actionwords.verifyThatIndicatorsApplyBtnDisabled();
        actionwords.verifyThatIndicatorsResetBtnDisabled();
        actionwords.verifyThatAllBollingerBandsIndicatorConfigurationsFieldsRestoreBackAfterClickOnResetBtn();
        actionwords.getScreenshot("testcase003OpenChartingPageAndClickOnBollingerBandsIndicatorAndUpdateConfigurationsAndResetBackAfter_5");
    }

    @Test
    public void testcase003OpenChartingPageAndClickOnBollingerBandsIndicatorAndUpdateConfigurationsAndResetBackAfterDataSet1() {
        testcase003OpenChartingPageAndClickOnBollingerBandsIndicatorAndUpdateConfigurationsAndResetBackAfter(websitelink, INDICATOR_NO_SETTINGS_MESSAGE, "5", "5", "5", "5");
    }

    public void testcase004OpenChartingPageAndClickOnBollingerBandsIndicatorAndCheckThatCanNotApplySettingsWithEmptyLengthInput(String link, String message, String length) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.clickOnChartingBtnOnHomePage();
        actionwords.getScreenshot("testcase004OpenChartingPageAndClickOnBollingerBandsIndicatorAndCheckThatCanNotApplySettingsWithEmptyLengthInput_1");
        actionwords.clickOnIndicatorsBtn();
        actionwords.getScreenshot("testcase004OpenChartingPageAndClickOnBollingerBandsIndicatorAndCheckThatCanNotApplySettingsWithEmptyLengthInput_2");
        actionwords.clickOnBollingerBandsLabel();
        actionwords.verifyThatIndicatorNoSettingsMessageNotAppear(message);
        actionwords.saveForFutureTestsValuesFromAllBollingerBandsIndicatorConfigurationsFields();
        actionwords.verifyThatIndicatorsApplyBtnDisabled();
        actionwords.verifyThatIndicatorsResetBtnDisabled();
        actionwords.setLength(length);
        actionwords.verifyThatIndicatorsApplyBtnDisabled();
        actionwords.verifyThatIndicatorsResetBtnDisabled();
        actionwords.getScreenshot("testcase004OpenChartingPageAndClickOnBollingerBandsIndicatorAndCheckThatCanNotApplySettingsWithEmptyLengthInput_3");
        actionwords.clickOnIndicatorsApplyBtn();
        actionwords.verifyThatIndicatorsApplyBtnDisabled();
        actionwords.verifyThatIndicatorsResetBtnDisabled();
        actionwords.getScreenshot("testcase004OpenChartingPageAndClickOnBollingerBandsIndicatorAndCheckThatCanNotApplySettingsWithEmptyLengthInput_4");
    }

    @Test
    public void testcase004OpenChartingPageAndClickOnBollingerBandsIndicatorAndCheckThatCanNotApplySettingsWithEmptyLengthInputDataSet1() {
        testcase004OpenChartingPageAndClickOnBollingerBandsIndicatorAndCheckThatCanNotApplySettingsWithEmptyLengthInput(websitelink, INDICATOR_NO_SETTINGS_MESSAGE, "");
    }

    public void testcase005OpenChartingPageAndClickOnBollingerBandsIndicatorAndCheckThatCanNotApplySettingsWithEmptyStandartDeviationInput(String link, String message, String standartDeviation) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.clickOnChartingBtnOnHomePage();
        actionwords.getScreenshot("testcase005OpenChartingPageAndClickOnBollingerBandsIndicatorAndCheckThatCanNotApplySettingsWithEmptyStandartDeviationInput_1");
        actionwords.clickOnIndicatorsBtn();
        actionwords.getScreenshot("testcase005OpenChartingPageAndClickOnBollingerBandsIndicatorAndCheckThatCanNotApplySettingsWithEmptyStandartDeviationInput_2");
        actionwords.clickOnBollingerBandsLabel();
        actionwords.verifyThatIndicatorNoSettingsMessageNotAppear(message);
        actionwords.saveForFutureTestsValuesFromAllBollingerBandsIndicatorConfigurationsFields();
        actionwords.verifyThatIndicatorsApplyBtnDisabled();
        actionwords.verifyThatIndicatorsResetBtnDisabled();
        actionwords.setStandartDeviation(standartDeviation);
        actionwords.verifyThatIndicatorsApplyBtnDisabled();
        actionwords.verifyThatIndicatorsResetBtnDisabled();
        actionwords.getScreenshot("testcase005OpenChartingPageAndClickOnBollingerBandsIndicatorAndCheckThatCanNotApplySettingsWithEmptyStandartDeviationInput_3");
        actionwords.clickOnIndicatorsApplyBtn();
        actionwords.verifyThatIndicatorsApplyBtnDisabled();
        actionwords.verifyThatIndicatorsResetBtnDisabled();
        actionwords.getScreenshot("testcase005OpenChartingPageAndClickOnBollingerBandsIndicatorAndCheckThatCanNotApplySettingsWithEmptyStandartDeviationInput_4");
    }

    @Test
    public void testcase005OpenChartingPageAndClickOnBollingerBandsIndicatorAndCheckThatCanNotApplySettingsWithEmptyStandartDeviationInputDataSet1() {
        testcase005OpenChartingPageAndClickOnBollingerBandsIndicatorAndCheckThatCanNotApplySettingsWithEmptyStandartDeviationInput(websitelink, INDICATOR_NO_SETTINGS_MESSAGE, "");
    }

    public void testcase006OpenChartingPageAndClickOnBollingerBandsIndicatorAndCheckThatCanNotApplySettingsWithEmptyDegreesOfFreedomInput(String link, String message, String degreesOfFreedom) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.clickOnChartingBtnOnHomePage();
        actionwords.getScreenshot("testcase006OpenChartingPageAndClickOnBollingerBandsIndicatorAndCheckThatCanNotApplySettingsWithEmptyDegreesOfFreedomInput_1");
        actionwords.clickOnIndicatorsBtn();
        actionwords.getScreenshot("testcase006OpenChartingPageAndClickOnBollingerBandsIndicatorAndCheckThatCanNotApplySettingsWithEmptyDegreesOfFreedomInput_2");
        actionwords.clickOnBollingerBandsLabel();
        actionwords.verifyThatIndicatorNoSettingsMessageNotAppear(message);
        actionwords.saveForFutureTestsValuesFromAllBollingerBandsIndicatorConfigurationsFields();
        actionwords.verifyThatIndicatorsApplyBtnDisabled();
        actionwords.verifyThatIndicatorsResetBtnDisabled();
        actionwords.setDegreesOfFreedom(degreesOfFreedom);
        actionwords.verifyThatIndicatorsApplyBtnDisabled();
        actionwords.verifyThatIndicatorsResetBtnDisabled();
        actionwords.getScreenshot("testcase006OpenChartingPageAndClickOnBollingerBandsIndicatorAndCheckThatCanNotApplySettingsWithEmptyDegreesOfFreedomInput_3");
        actionwords.clickOnIndicatorsApplyBtn();
        actionwords.verifyThatIndicatorsApplyBtnDisabled();
        actionwords.verifyThatIndicatorsResetBtnDisabled();
        actionwords.getScreenshot("testcase006OpenChartingPageAndClickOnBollingerBandsIndicatorAndCheckThatCanNotApplySettingsWithEmptyDegreesOfFreedomInput_4");
    }

    @Test
    public void testcase006OpenChartingPageAndClickOnBollingerBandsIndicatorAndCheckThatCanNotApplySettingsWithEmptyDegreesOfFreedomInputDataSet1() {
        testcase006OpenChartingPageAndClickOnBollingerBandsIndicatorAndCheckThatCanNotApplySettingsWithEmptyDegreesOfFreedomInput(websitelink, INDICATOR_NO_SETTINGS_MESSAGE, "");
    }

    public void testcase007OpenChartingPageAndClickOnBollingerBandsIndicatorAndCheckThatCanNotApplySettingsWithEmptyOffsetInput(String link, String message, String offset) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.clickOnChartingBtnOnHomePage();
        actionwords.getScreenshot("testcase007OpenChartingPageAndClickOnBollingerBandsIndicatorAndCheckThatCanNotApplySettingsWithEmptyOffsetInput_1");
        actionwords.clickOnIndicatorsBtn();
        actionwords.getScreenshot("testcase007OpenChartingPageAndClickOnBollingerBandsIndicatorAndCheckThatCanNotApplySettingsWithEmptyOffsetInput_2");
        actionwords.clickOnBollingerBandsLabel();
        actionwords.verifyThatIndicatorNoSettingsMessageNotAppear(message);
        actionwords.saveForFutureTestsValuesFromAllBollingerBandsIndicatorConfigurationsFields();
        actionwords.verifyThatIndicatorsApplyBtnDisabled();
        actionwords.verifyThatIndicatorsResetBtnDisabled();
        actionwords.setOffset(offset);
        actionwords.verifyThatIndicatorsApplyBtnDisabled();
        actionwords.verifyThatIndicatorsResetBtnDisabled();
        actionwords.getScreenshot("testcase007OpenChartingPageAndClickOnBollingerBandsIndicatorAndCheckThatCanNotApplySettingsWithEmptyOffsetInput_3");
        actionwords.clickOnIndicatorsApplyBtn();
        actionwords.verifyThatIndicatorsApplyBtnDisabled();
        actionwords.verifyThatIndicatorsResetBtnDisabled();
        actionwords.getScreenshot("testcase007OpenChartingPageAndClickOnBollingerBandsIndicatorAndCheckThatCanNotApplySettingsWithEmptyOffsetInput_4");
    }

    @Test
    public void testcase007OpenChartingPageAndClickOnBollingerBandsIndicatorAndCheckThatCanNotApplySettingsWithEmptyOffsetInputDataSet1() {
        testcase007OpenChartingPageAndClickOnBollingerBandsIndicatorAndCheckThatCanNotApplySettingsWithEmptyOffsetInput(websitelink, INDICATOR_NO_SETTINGS_MESSAGE, "");
    }

    public void testcase008OpenChartingPageAndClickOnBollingerBandsIndicatorAndCheckThatApplySettingsStartToBeEnabledWithSomeNewLengthInputValueSet(String link, String message, String length) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.clickOnChartingBtnOnHomePage();
        actionwords.getScreenshot("testcase008OpenChartingPageAndClickOnBollingerBandsIndicatorAndCheckThatApplySettingsStartToBeEnabledWithSomeNewLengthInputValueSet_1");
        actionwords.clickOnIndicatorsBtn();
        actionwords.getScreenshot("testcase008OpenChartingPageAndClickOnBollingerBandsIndicatorAndCheckThatApplySettingsStartToBeEnabledWithSomeNewLengthInputValueSet_2");
        actionwords.clickOnBollingerBandsLabel();
        actionwords.verifyThatIndicatorNoSettingsMessageNotAppear(message);
        actionwords.saveForFutureTestsValuesFromAllBollingerBandsIndicatorConfigurationsFields();
        actionwords.verifyThatIndicatorsApplyBtnDisabled();
        actionwords.verifyThatIndicatorsResetBtnDisabled();
        actionwords.setLength(length);
        actionwords.verifyThatIndicatorsApplyBtnEnabled();
        actionwords.verifyThatIndicatorsResetBtnDisabled();
        actionwords.getScreenshot("testcase008OpenChartingPageAndClickOnBollingerBandsIndicatorAndCheckThatApplySettingsStartToBeEnabledWithSomeNewLengthInputValueSet_3");
    }

    @Test
    public void testcase008OpenChartingPageAndClickOnBollingerBandsIndicatorAndCheckThatApplySettingsStartToBeEnabledWithSomeNewLengthInputValueSetDataSet1() {
        testcase008OpenChartingPageAndClickOnBollingerBandsIndicatorAndCheckThatApplySettingsStartToBeEnabledWithSomeNewLengthInputValueSet(websitelink, INDICATOR_NO_SETTINGS_MESSAGE, "10");
    }

    public void testcase009OpenChartingPageAndClickOnBollingerBandsIndicatorAndCheckThatApplySettingsStartToBeEnabledWithSomeNewStandartDeviationInputValueSet(String link, String message, String standartDeviation) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.clickOnChartingBtnOnHomePage();
        actionwords.getScreenshot("testcase009OpenChartingPageAndClickOnBollingerBandsIndicatorAndCheckThatApplySettingsStartToBeEnabledWithSomeNewStandartDeviationInputValueSet_1");
        actionwords.clickOnIndicatorsBtn();
        actionwords.getScreenshot("testcase009OpenChartingPageAndClickOnBollingerBandsIndicatorAndCheckThatApplySettingsStartToBeEnabledWithSomeNewStandartDeviationInputValueSet_2");
        actionwords.clickOnBollingerBandsLabel();
        actionwords.verifyThatIndicatorNoSettingsMessageNotAppear(message);
        actionwords.saveForFutureTestsValuesFromAllBollingerBandsIndicatorConfigurationsFields();
        actionwords.verifyThatIndicatorsApplyBtnDisabled();
        actionwords.verifyThatIndicatorsResetBtnDisabled();
        actionwords.setStandartDeviation(standartDeviation);
        actionwords.verifyThatIndicatorsApplyBtnEnabled();
        actionwords.verifyThatIndicatorsResetBtnDisabled();
        actionwords.getScreenshot("testcase009OpenChartingPageAndClickOnBollingerBandsIndicatorAndCheckThatApplySettingsStartToBeEnabledWithSomeNewStandartDeviationInputValueSet_3");
    }

    @Test
    public void testcase009OpenChartingPageAndClickOnBollingerBandsIndicatorAndCheckThatApplySettingsStartToBeEnabledWithSomeNewStandartDeviationInputValueSetDataSet1() {
        testcase009OpenChartingPageAndClickOnBollingerBandsIndicatorAndCheckThatApplySettingsStartToBeEnabledWithSomeNewStandartDeviationInputValueSet(websitelink, INDICATOR_NO_SETTINGS_MESSAGE, "10");
    }

    public void testcase010OpenChartingPageAndClickOnBollingerBandsIndicatorAndCheckThatApplySettingsStartToBeEnabledWithSomeNewDegreesOfFreedomInputValueSet(String link, String message, String degreesOfFreedom) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.clickOnChartingBtnOnHomePage();
        actionwords.getScreenshot("testcase010OpenChartingPageAndClickOnBollingerBandsIndicatorAndCheckThatApplySettingsStartToBeEnabledWithSomeNewDegreesOfFreedomInputValueSet_1");
        actionwords.clickOnIndicatorsBtn();
        actionwords.getScreenshot("testcase010OpenChartingPageAndClickOnBollingerBandsIndicatorAndCheckThatApplySettingsStartToBeEnabledWithSomeNewDegreesOfFreedomInputValueSet_2");
        actionwords.clickOnBollingerBandsLabel();
        actionwords.verifyThatIndicatorNoSettingsMessageNotAppear(message);
        actionwords.saveForFutureTestsValuesFromAllBollingerBandsIndicatorConfigurationsFields();
        actionwords.verifyThatIndicatorsApplyBtnDisabled();
        actionwords.verifyThatIndicatorsResetBtnDisabled();
        actionwords.setDegreesOfFreedom(degreesOfFreedom);
        actionwords.verifyThatIndicatorsApplyBtnEnabled();
        actionwords.verifyThatIndicatorsResetBtnDisabled();
        actionwords.getScreenshot("testcase010OpenChartingPageAndClickOnBollingerBandsIndicatorAndCheckThatApplySettingsStartToBeEnabledWithSomeNewDegreesOfFreedomInputValueSet_3");
    }

    @Test
    public void testcase010OpenChartingPageAndClickOnBollingerBandsIndicatorAndCheckThatApplySettingsStartToBeEnabledWithSomeNewDegreesOfFreedomInputValueSetDataSet1() {
        testcase010OpenChartingPageAndClickOnBollingerBandsIndicatorAndCheckThatApplySettingsStartToBeEnabledWithSomeNewDegreesOfFreedomInputValueSet(websitelink, INDICATOR_NO_SETTINGS_MESSAGE, "10");
    }

    public void testcase011OpenChartingPageAndClickOnBollingerBandsIndicatorAndCheckThatApplySettingsStartToBeEnabledWithSomeNewOffsetInputValueSet(String link, String message, String offset) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.clickOnChartingBtnOnHomePage();
        actionwords.getScreenshot("testcase011OpenChartingPageAndClickOnBollingerBandsIndicatorAndCheckThatApplySettingsStartToBeEnabledWithSomeNewOffsetInputValueSet_1");
        actionwords.clickOnIndicatorsBtn();
        actionwords.getScreenshot("testcase011OpenChartingPageAndClickOnBollingerBandsIndicatorAndCheckThatApplySettingsStartToBeEnabledWithSomeNewOffsetInputValueSet_2");
        actionwords.clickOnBollingerBandsLabel();
        actionwords.verifyThatIndicatorNoSettingsMessageNotAppear(message);
        actionwords.saveForFutureTestsValuesFromAllBollingerBandsIndicatorConfigurationsFields();
        actionwords.verifyThatIndicatorsApplyBtnDisabled();
        actionwords.verifyThatIndicatorsResetBtnDisabled();
        actionwords.setOffset(offset);
        actionwords.verifyThatIndicatorsApplyBtnEnabled();
        actionwords.verifyThatIndicatorsResetBtnDisabled();
        actionwords.getScreenshot("testcase011OpenChartingPageAndClickOnBollingerBandsIndicatorAndCheckThatApplySettingsStartToBeEnabledWithSomeNewOffsetInputValueSet_3");
    }

    @Test
    public void testcase011OpenChartingPageAndClickOnBollingerBandsIndicatorAndCheckThatApplySettingsStartToBeEnabledWithSomeNewOffsetInputValueSetDataSet1() {
        testcase011OpenChartingPageAndClickOnBollingerBandsIndicatorAndCheckThatApplySettingsStartToBeEnabledWithSomeNewOffsetInputValueSet(websitelink, INDICATOR_NO_SETTINGS_MESSAGE, "10");
    }

    public void testcase012OpenChartingPageAndClickOnRelativeStrengthIndexIndicatorAndUpdateConfigurationsAndResetBackAfter(String link, String message, String length, String scalar, String differencePeriod, String offset) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.clickOnChartingBtnOnHomePage();
        actionwords.getScreenshot("testcase012OpenChartingPageAndClickOnRelativeStrengthIndexIndicatorAndUpdateConfigurationsAndResetBackAfter_1");
        actionwords.clickOnIndicatorsBtn();
        actionwords.getScreenshot("testcase012OpenChartingPageAndClickOnRelativeStrengthIndexIndicatorAndUpdateConfigurationsAndResetBackAfter_2");
        actionwords.clickOnRelativeStrengthIndexLabel();
        actionwords.verifyThatIndicatorNoSettingsMessageNotAppear(message);
        actionwords.saveForFutureTestsValuesFromAllRelativeStrengthIndexIndicatorConfigurationsFields();
        actionwords.verifyThatIndicatorsApplyBtnDisabled();
        actionwords.verifyThatIndicatorsResetBtnDisabled();
        actionwords.setLength(length);
        actionwords.setScalar(scalar);
        actionwords.setDifferencePeriod(differencePeriod);
        actionwords.setOffset(offset);
        actionwords.verifyThatIndicatorsApplyBtnEnabled();
        actionwords.verifyThatIndicatorsResetBtnDisabled();
        actionwords.getScreenshot("testcase012OpenChartingPageAndClickOnRelativeStrengthIndexIndicatorAndUpdateConfigurationsAndResetBackAfter_3");
        actionwords.clickOnIndicatorsApplyBtn();
        actionwords.verifyThatIndicatorsApplyBtnDisabled();
        actionwords.verifyThatIndicatorsResetBtnEnabled();
        actionwords.getScreenshot("testcase012OpenChartingPageAndClickOnRelativeStrengthIndexIndicatorAndUpdateConfigurationsAndResetBackAfter_4");
        actionwords.clickOnIndicatorsResetBtn();
        actionwords.verifyThatIndicatorsApplyBtnDisabled();
        actionwords.verifyThatIndicatorsResetBtnDisabled();
        actionwords.verifyThatAllRelativeStrengthIndexIndicatorConfigurationsFieldsRestoreBackAfterClickOnResetBtn();
        actionwords.getScreenshot("testcase012OpenChartingPageAndClickOnRelativeStrengthIndexIndicatorAndUpdateConfigurationsAndResetBackAfter_5");
    }

    @Test
    public void testcase012OpenChartingPageAndClickOnRelativeStrengthIndexIndicatorAndUpdateConfigurationsAndResetBackAfterDataSet1() {
        testcase012OpenChartingPageAndClickOnRelativeStrengthIndexIndicatorAndUpdateConfigurationsAndResetBackAfter(websitelink, INDICATOR_NO_SETTINGS_MESSAGE, "5", "5", "5", "5");
    }

    public void testcase013OpenChartingPageAndClickOnRelativeStrengthIndexIndicatorAndCheckThatCanNotApplySettingsWithEmptyLengthInput(String link, String message, String length) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.clickOnChartingBtnOnHomePage();
        actionwords.getScreenshot("testcase013OpenChartingPageAndClickOnRelativeStrengthIndexIndicatorAndCheckThatCanNotApplySettingsWithEmptyLengthInput_1");
        actionwords.clickOnIndicatorsBtn();
        actionwords.getScreenshot("testcase013OpenChartingPageAndClickOnRelativeStrengthIndexIndicatorAndCheckThatCanNotApplySettingsWithEmptyLengthInput_2");
        actionwords.clickOnRelativeStrengthIndexLabel();
        actionwords.verifyThatIndicatorNoSettingsMessageNotAppear(message);
        actionwords.saveForFutureTestsValuesFromAllRelativeStrengthIndexIndicatorConfigurationsFields();
        actionwords.verifyThatIndicatorsApplyBtnDisabled();
        actionwords.verifyThatIndicatorsResetBtnDisabled();
        actionwords.setLength(length);
        actionwords.verifyThatIndicatorsApplyBtnDisabled();
        actionwords.verifyThatIndicatorsResetBtnDisabled();
        actionwords.getScreenshot("testcase013OpenChartingPageAndClickOnRelativeStrengthIndexIndicatorAndCheckThatCanNotApplySettingsWithEmptyLengthInput_3");
        actionwords.clickOnIndicatorsApplyBtn();
        actionwords.verifyThatIndicatorsApplyBtnDisabled();
        actionwords.verifyThatIndicatorsResetBtnDisabled();
        actionwords.getScreenshot("testcase013OpenChartingPageAndClickOnRelativeStrengthIndexIndicatorAndCheckThatCanNotApplySettingsWithEmptyLengthInput_4");
    }

    @Test
    public void testcase013OpenChartingPageAndClickOnRelativeStrengthIndexIndicatorAndCheckThatCanNotApplySettingsWithEmptyLengthInputDataSet1() {
        testcase013OpenChartingPageAndClickOnRelativeStrengthIndexIndicatorAndCheckThatCanNotApplySettingsWithEmptyLengthInput(websitelink, INDICATOR_NO_SETTINGS_MESSAGE, "");
    }

    public void testcase014OpenChartingPageAndClickOnRelativeStrengthIndexIndicatorAndCheckThatCanNotApplySettingsWithEmptyScalarInput(String link, String message, String scalar) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.clickOnChartingBtnOnHomePage();
        actionwords.getScreenshot("testcase014OpenChartingPageAndClickOnRelativeStrengthIndexIndicatorAndCheckThatCanNotApplySettingsWithEmptyScalarInput_1");
        actionwords.clickOnIndicatorsBtn();
        actionwords.getScreenshot("testcase014OpenChartingPageAndClickOnRelativeStrengthIndexIndicatorAndCheckThatCanNotApplySettingsWithEmptyScalarInput_2");
        actionwords.clickOnRelativeStrengthIndexLabel();
        actionwords.verifyThatIndicatorNoSettingsMessageNotAppear(message);
        actionwords.saveForFutureTestsValuesFromAllRelativeStrengthIndexIndicatorConfigurationsFields();
        actionwords.verifyThatIndicatorsApplyBtnDisabled();
        actionwords.verifyThatIndicatorsResetBtnDisabled();
        actionwords.setScalar(scalar);
        actionwords.verifyThatIndicatorsApplyBtnDisabled();
        actionwords.verifyThatIndicatorsResetBtnDisabled();
        actionwords.getScreenshot("testcase014OpenChartingPageAndClickOnRelativeStrengthIndexIndicatorAndCheckThatCanNotApplySettingsWithEmptyScalarInput_3");
        actionwords.clickOnIndicatorsApplyBtn();
        actionwords.verifyThatIndicatorsApplyBtnDisabled();
        actionwords.verifyThatIndicatorsResetBtnDisabled();
        actionwords.getScreenshot("testcase014OpenChartingPageAndClickOnRelativeStrengthIndexIndicatorAndCheckThatCanNotApplySettingsWithEmptyScalarInput_4");
    }

    @Test
    public void testcase014OpenChartingPageAndClickOnRelativeStrengthIndexIndicatorAndCheckThatCanNotApplySettingsWithEmptyScalarInputDataSet1() {
        testcase014OpenChartingPageAndClickOnRelativeStrengthIndexIndicatorAndCheckThatCanNotApplySettingsWithEmptyScalarInput(websitelink, INDICATOR_NO_SETTINGS_MESSAGE, "");
    }

    public void testcase015OpenChartingPageAndClickOnRelativeStrengthIndexIndicatorAndCheckThatCanNotApplySettingsWithEmptyDifferencePeriodInput(String link, String message, String differencePeriod) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.clickOnChartingBtnOnHomePage();
        actionwords.getScreenshot("testcase015OpenChartingPageAndClickOnRelativeStrengthIndexIndicatorAndCheckThatCanNotApplySettingsWithEmptyDifferencePeriodInput_1");
        actionwords.clickOnIndicatorsBtn();
        actionwords.getScreenshot("testcase015OpenChartingPageAndClickOnRelativeStrengthIndexIndicatorAndCheckThatCanNotApplySettingsWithEmptyDifferencePeriodInput_2");
        actionwords.clickOnRelativeStrengthIndexLabel();
        actionwords.verifyThatIndicatorNoSettingsMessageNotAppear(message);
        actionwords.saveForFutureTestsValuesFromAllRelativeStrengthIndexIndicatorConfigurationsFields();
        actionwords.verifyThatIndicatorsApplyBtnDisabled();
        actionwords.verifyThatIndicatorsResetBtnDisabled();
        actionwords.setDifferencePeriod(differencePeriod);
        actionwords.verifyThatIndicatorsApplyBtnDisabled();
        actionwords.verifyThatIndicatorsResetBtnDisabled();
        actionwords.getScreenshot("testcase015OpenChartingPageAndClickOnRelativeStrengthIndexIndicatorAndCheckThatCanNotApplySettingsWithEmptyDifferencePeriodInput_3");
        actionwords.clickOnIndicatorsApplyBtn();
        actionwords.verifyThatIndicatorsApplyBtnDisabled();
        actionwords.verifyThatIndicatorsResetBtnDisabled();
        actionwords.getScreenshot("testcase015OpenChartingPageAndClickOnRelativeStrengthIndexIndicatorAndCheckThatCanNotApplySettingsWithEmptyDifferencePeriodInput_4");
    }

    @Test
    public void testcase015OpenChartingPageAndClickOnRelativeStrengthIndexIndicatorAndCheckThatCanNotApplySettingsWithEmptyDifferencePeriodInputDataSet1() {
        testcase015OpenChartingPageAndClickOnRelativeStrengthIndexIndicatorAndCheckThatCanNotApplySettingsWithEmptyDifferencePeriodInput(websitelink, INDICATOR_NO_SETTINGS_MESSAGE, "");
    }

    public void testcase016OpenChartingPageAndClickOnRelativeStrengthIndexIndicatorAndCheckThatCanNotApplySettingsWithEmptyOffsetInput(String link, String message, String offset) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.clickOnChartingBtnOnHomePage();
        actionwords.getScreenshot("testcase016OpenChartingPageAndClickOnRelativeStrengthIndexIndicatorAndCheckThatCanNotApplySettingsWithEmptyOffsetInput_1");
        actionwords.clickOnIndicatorsBtn();
        actionwords.getScreenshot("testcase016OpenChartingPageAndClickOnRelativeStrengthIndexIndicatorAndCheckThatCanNotApplySettingsWithEmptyOffsetInput_2");
        actionwords.clickOnRelativeStrengthIndexLabel();
        actionwords.verifyThatIndicatorNoSettingsMessageNotAppear(message);
        actionwords.saveForFutureTestsValuesFromAllRelativeStrengthIndexIndicatorConfigurationsFields();
        actionwords.verifyThatIndicatorsApplyBtnDisabled();
        actionwords.verifyThatIndicatorsResetBtnDisabled();
        actionwords.setOffset(offset);
        actionwords.verifyThatIndicatorsApplyBtnDisabled();
        actionwords.verifyThatIndicatorsResetBtnDisabled();
        actionwords.getScreenshot("testcase016OpenChartingPageAndClickOnRelativeStrengthIndexIndicatorAndCheckThatCanNotApplySettingsWithEmptyOffsetInput_3");
        actionwords.clickOnIndicatorsApplyBtn();
        actionwords.verifyThatIndicatorsApplyBtnDisabled();
        actionwords.verifyThatIndicatorsResetBtnDisabled();
        actionwords.getScreenshot("testcase016OpenChartingPageAndClickOnRelativeStrengthIndexIndicatorAndCheckThatCanNotApplySettingsWithEmptyOffsetInput_4");
    }

    @Test
    public void testcase016OpenChartingPageAndClickOnRelativeStrengthIndexIndicatorAndCheckThatCanNotApplySettingsWithEmptyOffsetInputDataSet1() {
        testcase016OpenChartingPageAndClickOnRelativeStrengthIndexIndicatorAndCheckThatCanNotApplySettingsWithEmptyOffsetInput(websitelink, INDICATOR_NO_SETTINGS_MESSAGE, "");
    }

    public void testcase017OpenChartingPageAndClickOnRelativeStrengthIndexIndicatorAndCheckThatApplySettingsStartToBeEnabledWithSomeNewLengthInputValueSet(String link, String message, String length) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.clickOnChartingBtnOnHomePage();
        actionwords.getScreenshot("testcase017OpenChartingPageAndClickOnRelativeStrengthIndexIndicatorAndCheckThatApplySettingsStartToBeEnabledWithSomeNewLengthInputValueSet_1");
        actionwords.clickOnIndicatorsBtn();
        actionwords.getScreenshot("testcase017OpenChartingPageAndClickOnRelativeStrengthIndexIndicatorAndCheckThatApplySettingsStartToBeEnabledWithSomeNewLengthInputValueSet_2");
        actionwords.clickOnRelativeStrengthIndexLabel();
        actionwords.verifyThatIndicatorNoSettingsMessageNotAppear(message);
        actionwords.saveForFutureTestsValuesFromAllRelativeStrengthIndexIndicatorConfigurationsFields();
        actionwords.verifyThatIndicatorsApplyBtnDisabled();
        actionwords.verifyThatIndicatorsResetBtnDisabled();
        actionwords.setLength(length);
        actionwords.verifyThatIndicatorsApplyBtnEnabled();
        actionwords.verifyThatIndicatorsResetBtnDisabled();
        actionwords.getScreenshot("testcase017OpenChartingPageAndClickOnRelativeStrengthIndexIndicatorAndCheckThatApplySettingsStartToBeEnabledWithSomeNewLengthInputValueSet_3");
    }

    @Test
    public void testcase017OpenChartingPageAndClickOnRelativeStrengthIndexIndicatorAndCheckThatApplySettingsStartToBeEnabledWithSomeNewLengthInputValueSetDataSet1() {
        testcase017OpenChartingPageAndClickOnRelativeStrengthIndexIndicatorAndCheckThatApplySettingsStartToBeEnabledWithSomeNewLengthInputValueSet(websitelink, INDICATOR_NO_SETTINGS_MESSAGE, "10");
    }

    public void testcase018OpenChartingPageAndClickOnRelativeStrengthIndexIndicatorAndCheckThatApplySettingsStartToBeEnabledWithSomeNewScalarInputValueSet(String link, String message, String scalar) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.clickOnChartingBtnOnHomePage();
        actionwords.getScreenshot("testcase018OpenChartingPageAndClickOnRelativeStrengthIndexIndicatorAndCheckThatApplySettingsStartToBeEnabledWithSomeNewScalarInputValueSet_1");
        actionwords.clickOnIndicatorsBtn();
        actionwords.getScreenshot("testcase018OpenChartingPageAndClickOnRelativeStrengthIndexIndicatorAndCheckThatApplySettingsStartToBeEnabledWithSomeNewScalarInputValueSet_2");
        actionwords.clickOnRelativeStrengthIndexLabel();
        actionwords.verifyThatIndicatorNoSettingsMessageNotAppear(message);
        actionwords.saveForFutureTestsValuesFromAllRelativeStrengthIndexIndicatorConfigurationsFields();
        actionwords.verifyThatIndicatorsApplyBtnDisabled();
        actionwords.verifyThatIndicatorsResetBtnDisabled();
        actionwords.setScalar(scalar);
        actionwords.verifyThatIndicatorsApplyBtnEnabled();
        actionwords.verifyThatIndicatorsResetBtnDisabled();
        actionwords.getScreenshot("testcase018OpenChartingPageAndClickOnRelativeStrengthIndexIndicatorAndCheckThatApplySettingsStartToBeEnabledWithSomeNewScalarInputValueSet_3");
    }

    @Test
    public void testcase018OpenChartingPageAndClickOnRelativeStrengthIndexIndicatorAndCheckThatApplySettingsStartToBeEnabledWithSomeNewScalarInputValueSetDataSet1() {
        testcase018OpenChartingPageAndClickOnRelativeStrengthIndexIndicatorAndCheckThatApplySettingsStartToBeEnabledWithSomeNewScalarInputValueSet(websitelink, INDICATOR_NO_SETTINGS_MESSAGE, "10");
    }

    public void testcase019OpenChartingPageAndClickOnRelativeStrengthIndexIndicatorAndCheckThatApplySettingsStartToBeEnabledWithSomeNewDifferencePeriodInputValueSet(String link, String message, String differencePeriod) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.clickOnChartingBtnOnHomePage();
        actionwords.getScreenshot("testcase019OpenChartingPageAndClickOnRelativeStrengthIndexIndicatorAndCheckThatApplySettingsStartToBeEnabledWithSomeNewDifferencePeriodInputValueSet_1");
        actionwords.clickOnIndicatorsBtn();
        actionwords.getScreenshot("testcase019OpenChartingPageAndClickOnRelativeStrengthIndexIndicatorAndCheckThatApplySettingsStartToBeEnabledWithSomeNewDifferencePeriodInputValueSet_2");
        actionwords.clickOnRelativeStrengthIndexLabel();
        actionwords.verifyThatIndicatorNoSettingsMessageNotAppear(message);
        actionwords.saveForFutureTestsValuesFromAllRelativeStrengthIndexIndicatorConfigurationsFields();
        actionwords.verifyThatIndicatorsApplyBtnDisabled();
        actionwords.verifyThatIndicatorsResetBtnDisabled();
        actionwords.setDifferencePeriod(differencePeriod);
        actionwords.verifyThatIndicatorsApplyBtnEnabled();
        actionwords.verifyThatIndicatorsResetBtnDisabled();
        actionwords.getScreenshot("testcase019OpenChartingPageAndClickOnRelativeStrengthIndexIndicatorAndCheckThatApplySettingsStartToBeEnabledWithSomeNewDifferencePeriodInputValueSet_3");
    }

    @Test
    public void testcase019OpenChartingPageAndClickOnRelativeStrengthIndexIndicatorAndCheckThatApplySettingsStartToBeEnabledWithSomeNewDifferencePeriodInputValueSetDataSet1() {
        testcase019OpenChartingPageAndClickOnRelativeStrengthIndexIndicatorAndCheckThatApplySettingsStartToBeEnabledWithSomeNewDifferencePeriodInputValueSet(websitelink, INDICATOR_NO_SETTINGS_MESSAGE, "10");
    }

    public void testcase020OpenChartingPageAndClickOnRelativeStrengthIndexIndicatorAndCheckThatApplySettingsStartToBeEnabledWithSomeNewOffsetInputValueSet(String link, String message, String offset) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.clickOnChartingBtnOnHomePage();
        actionwords.getScreenshot("testcase020OpenChartingPageAndClickOnRelativeStrengthIndexIndicatorAndCheckThatApplySettingsStartToBeEnabledWithSomeNewOffsetInputValueSet_1");
        actionwords.clickOnIndicatorsBtn();
        actionwords.getScreenshot("testcase020OpenChartingPageAndClickOnRelativeStrengthIndexIndicatorAndCheckThatApplySettingsStartToBeEnabledWithSomeNewOffsetInputValueSet_2");
        actionwords.clickOnRelativeStrengthIndexLabel();
        actionwords.verifyThatIndicatorNoSettingsMessageNotAppear(message);
        actionwords.saveForFutureTestsValuesFromAllRelativeStrengthIndexIndicatorConfigurationsFields();
        actionwords.verifyThatIndicatorsApplyBtnDisabled();
        actionwords.verifyThatIndicatorsResetBtnDisabled();
        actionwords.setOffset(offset);
        actionwords.verifyThatIndicatorsApplyBtnEnabled();
        actionwords.verifyThatIndicatorsResetBtnDisabled();
        actionwords.getScreenshot("testcase020OpenChartingPageAndClickOnRelativeStrengthIndexIndicatorAndCheckThatApplySettingsStartToBeEnabledWithSomeNewOffsetInputValueSet_3");
    }

    @Test
    public void testcase020OpenChartingPageAndClickOnRelativeStrengthIndexIndicatorAndCheckThatApplySettingsStartToBeEnabledWithSomeNewOffsetInputValueSetDataSet1() {
        testcase020OpenChartingPageAndClickOnRelativeStrengthIndexIndicatorAndCheckThatApplySettingsStartToBeEnabledWithSomeNewOffsetInputValueSet(websitelink, INDICATOR_NO_SETTINGS_MESSAGE, "10");
    }
}
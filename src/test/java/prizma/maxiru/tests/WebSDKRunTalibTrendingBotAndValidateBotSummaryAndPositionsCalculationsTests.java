package prizma.maxiru.tests;

import org.junit.FixMethodOrder;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import prizma.maxiru.framework.common.*;

import static prizma.maxiru.framework.common.PrizmaDriver.*;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)

public class WebSDKRunTalibTrendingBotAndValidateBotSummaryAndPositionsCalculationsTests {
    @Rule
    public PrizmaScreenshotRule screenshotRule = new PrizmaScreenshotRule();

    public Actionwords actionwords = new Actionwords();

    public void testcase001RedirectToBotDetailsPageAndRunBotWithShortPositionTrendingDialogWithValidValuesAndValidateBotLandingPageAfter(String link, String fileName, String startBalance, String runFrom, String runTo, String tradingFee, String tradeLimit, String botName) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.deleteTheLogsFolder();
        actionwords.createTheLogsFolder();
        actionwords.createTheLogsFileForFailedTests(fileName);
        actionwords.clickOnBotsBtnOnHomePage();
        actionwords.getScreenshot("testcase001RedirectToBotDetailsPageAndRunBotWithShortPositionTrendingDialogWithValidValuesAndValidateBotLandingPageAfter_1");
        actionwords.clickOnBotRowContainerToOpenBotDetailPageByBotName(botName);
        actionwords.clickOnBotRunBtnOnBotDetailsPage();
        actionwords.getScreenshot("testcase001RedirectToBotDetailsPageAndRunBotWithShortPositionTrendingDialogWithValidValuesAndValidateBotLandingPageAfter_2");
        actionwords.setStartBalanceOnBotRunDialog(startBalance);
        actionwords.setRunFromOnBotRunPage(runFrom);
        actionwords.setRunToOnBotRunPage(runTo);
        actionwords.setTradingFeeOnBotRunDialog(tradingFee);
        actionwords.setTradeLimitOnBotRunDialog(tradeLimit);
        actionwords.getScreenshot("testcase001RedirectToBotDetailsPageAndRunBotWithShortPositionTrendingDialogWithValidValuesAndValidateBotLandingPageAfter_3");
        actionwords.clickOnBotRunBtnOnBotRunDialog();
        actionwords.getScreenshot("testcase001RedirectToBotDetailsPageAndRunBotWithShortPositionTrendingDialogWithValidValuesAndValidateBotLandingPageAfter_4");
        actionwords.verifyThatRunBotDialogCloseWhenUserTriedToRunBotWithCorrectDataBot();
        actionwords.refreshPage();
        actionwords.openWebsite(link);
        actionwords.clickOnBotsBtnOnHomePage();
        actionwords.getScreenshot("testcase001RedirectToBotDetailsPageAndRunBotWithShortPositionTrendingDialogWithValidValuesAndValidateBotLandingPageAfter_5");
        actionwords.clickOnBotRowContainerToOpenBotDetailPageByBotName(botName);
        actionwords.getAndStoreTotalCountOfAllTrendingPositions();
        actionwords.getAndStoreTotalCountOfOpenAndCloseTrendingPositions();
        actionwords.getAndStoreInOpenTrendingPositionsValue();
        actionwords.getAndCalculateAllProfitCurrencyValues();
        actionwords.getAndCalculateAllProfitCurrencyValuesForClosedPositions();
        actionwords.getAndCalculateAllProfitCurrencyValuesForOpenPositions();
        actionwords.getAndCalculateAllProfitPercentageValues();
        actionwords.getAndCalculateAllProfitPercentageValuesForClosedPositions();
        actionwords.getAndCalculateAllProfitPercentageValuesForOpenPositions();
        actionwords.getAndCalculateTotalBalance();
        actionwords.getAndCalculateAvailableBalance();
        actionwords.getAndCalculateAverageProfitCurrency();
        actionwords.getAndCalculateAverageProfitPercentage();
        actionwords.getAndCalculateTotalProfitPercentageValues();
        actionwords.getAndCalculateBotEarnedValues();
        actionwords.getScreenshot("testcase001RedirectToBotDetailsPageAndRunBotWithShortPositionTrendingDialogWithValidValuesAndValidateBotLandingPageAfter_6");
        actionwords.refreshPage();
        actionwords.openWebsite(link);
        actionwords.clickOnBotsBtnOnHomePage();
        actionwords.getScreenshot("testcase001RedirectToBotDetailsPageAndRunBotWithShortPositionTrendingDialogWithValidValuesAndValidateBotLandingPageAfter_7");
        actionwords.verifyThatBotNameOnPositionFromVariableValueIsCorrect(botName);
        actionwords.verifyThatBotLastRunOnPositionFromVariableValueIsCorrectIfValidateWithVariableValue();
        actionwords.verifyThatBotAverangeProfitCurrencyOnPositionFromVariableValueIsCorrectIfValidateWithVariableValue();
        actionwords.verifyThatBotAverangeProfitPercentageOnPositionFromVariableValueIsCorrectIfValidateWithVariableValue();
        actionwords.verifyThatBotProfitCurrencyOnPositionFromVariableValueIsCorrectIfValidateWithVariableValue();
        actionwords.verifyThatBotProfitPercentageOnPositionFromVariableValueIsCorrectIfValidateWithVariableValue();
        actionwords.getScreenshot("testcase001RedirectToBotDetailsPageAndRunBotWithShortPositionTrendingDialogWithValidValuesAndValidateBotLandingPageAfter_8");
    }

    @Test
    public void testcase001RedirectToBotDetailsPageAndRunBotWithShortPositionTrendingDialogWithValidValuesAndValidateBotLandingPageAfterDataSet1() {
        testcase001RedirectToBotDetailsPageAndRunBotWithShortPositionTrendingDialogWithValidValuesAndValidateBotLandingPageAfter(websitelink, "bot_talib_landing_page.txt", "1000", "10/01/2021", "10/31/2021", "0.5", "10", "test_talib");
    }

    public void testcase002RedirectToBotDetailsPageAndRunBotDialogWithShortPositionTrendingValidValuesAndValidateBotSummaryPageAfter(String link, String fileName, String startBalance, String tradingFee, String tradeLimit, String botName) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.createTheLogsFileForFailedTests(fileName);
        actionwords.clickOnBotsBtnOnHomePage();
        actionwords.getScreenshot("testcase002RedirectToBotDetailsPageAndRunBotDialogWithShortPositionTrendingValidValuesAndValidateBotSummaryPageAfter_1");
        actionwords.clickOnBotRowContainerToOpenBotDetailPageByBotName(botName);
        actionwords.clickOnBotRunBtnOnBotDetailsPage();
        actionwords.getScreenshot("testcase002RedirectToBotDetailsPageAndRunBotDialogWithShortPositionTrendingValidValuesAndValidateBotSummaryPageAfter_2");
        actionwords.setStartBalanceOnBotRunDialog(startBalance);
        actionwords.setTradingFeeOnBotRunDialog(tradingFee);
        actionwords.setTradeLimitOnBotRunDialog(tradeLimit);
        actionwords.getScreenshot("testcase002RedirectToBotDetailsPageAndRunBotDialogWithShortPositionTrendingValidValuesAndValidateBotSummaryPageAfter_3");
        actionwords.clickOnBotRunBtnOnBotRunDialog();
        actionwords.getScreenshot("testcase002RedirectToBotDetailsPageAndRunBotDialogWithShortPositionTrendingValidValuesAndValidateBotSummaryPageAfter_4");
        actionwords.verifyThatRunBotDialogCloseWhenUserTriedToRunBotWithCorrectDataBot();
        actionwords.refreshPage();
        actionwords.openWebsite(link);
        actionwords.clickOnBotsBtnOnHomePage();
        actionwords.getScreenshot("testcase002RedirectToBotDetailsPageAndRunBotDialogWithShortPositionTrendingValidValuesAndValidateBotSummaryPageAfter_5");
        actionwords.clickOnBotRowContainerToOpenBotDetailPageByBotName(botName);
        actionwords.getAndStoreTotalCountOfAllTrendingPositions();
        actionwords.getAndStoreTotalCountOfOpenAndCloseTrendingPositions();
        actionwords.getAndStoreInOpenTrendingPositionsValue();
        actionwords.getAndCalculateAllProfitCurrencyValues();
        actionwords.getAndCalculateAllProfitCurrencyValuesForClosedPositions();
        actionwords.getAndCalculateAllProfitCurrencyValuesForOpenPositions();
        actionwords.getAndCalculateAllProfitPercentageValues();
        actionwords.getAndCalculateAllProfitPercentageValuesForClosedPositions();
        actionwords.getAndCalculateAllProfitPercentageValuesForOpenPositions();
        actionwords.getAndCalculateTotalBalance();
        actionwords.getAndCalculateAvailableBalance();
        actionwords.getAndCalculateAverageProfitCurrency();
        actionwords.getAndCalculateAverageProfitPercentage();
        actionwords.getAndCalculateTotalProfitPercentageValues();
        actionwords.getAndCalculateBotEarnedValues();
        actionwords.getScreenshot("testcase002RedirectToBotDetailsPageAndRunBotDialogWithShortPositionTrendingValidValuesAndValidateBotSummaryPageAfter_6");
        actionwords.refreshPage();
        actionwords.openWebsite(link);
        actionwords.clickOnBotsBtnOnHomePage();
        actionwords.getScreenshot("testcase002RedirectToBotDetailsPageAndRunBotDialogWithShortPositionTrendingValidValuesAndValidateBotSummaryPageAfter_7");
        actionwords.clickOnBotRowContainerToOpenBotDetailPageByBotName(botName);
        actionwords.verifyThatBotLastRunIsCorrectOnBotSummaryTableIfValidateWithVariableValue();
        actionwords.verifyThatBotFromToIsCorrectOnBotSummaryTableStartDateIfValidateWithVariableValue();
        actionwords.verifyThatBotFromToIsCorrectOnBotSummaryTableEndDateIfValidateWithVariableValue();
        actionwords.verifyThatBotClosedPositionsIsCorrectOnBotSummaryTableIfValidateWithVariableValue();
        actionwords.verifyThatBotInitialBalanceIsCorrectOnBotSummaryTableIfValidateWithVariableValue();
        actionwords.verifyThatBotInOpenPositionsBalanceIsCorrectOnBotSummaryTableWithIfValidateVariableValue();
        actionwords.verifyThatBotTotalBalanceIsCorrectOnBotSummaryTableIfValidateWithVariableValue();
        actionwords.verifyThatBotAverangeProfitCurrencyIsCorrectOnBotSummaryTableIfValidateWithVariableValue();
        actionwords.verifyThatBotAverangeProfitPercentageIsCorrectOnBotSummaryTableIfValidateWithVariableValue();
        actionwords.verifyThatBotTotalProfitCurrencyIsCorrectOnBotSummaryTableIfValidateWithVariableValue();
        actionwords.verifyThatBotTotalProfitPercentageIsCorrectOnBotSummaryTableIfValidateWithVariableValue();
        actionwords.verifyThatBotEarnedIsCorrectOnBotSummaryTableIfValidateWithVariableValue();
        actionwords.getScreenshot("testcase002RedirectToBotDetailsPageAndRunBotDialogWithShortPositionTrendingValidValuesAndValidateBotSummaryPageAfter_8");
    }

    @Test
    public void testcase002RedirectToBotDetailsPageAndRunBotDialogWithShortPositionTrendingValidValuesAndValidateBotSummaryPageAfterDataSet1() {
        testcase002RedirectToBotDetailsPageAndRunBotDialogWithShortPositionTrendingValidValuesAndValidateBotSummaryPageAfter(websitelink, "bot_talib_summary_page.txt", "1000", "0.5", "10", "test_talib");
    }

    public void testcase003RedirectToBotDetailsPageAndRunBotWithShortPositionTrendingDialogWithValidValuesAndValidateBotPositionPageAfter(String link, String fileName, String startBalance, String tradingFee, String tradeLimit, String botName) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.createTheLogsFileForFailedTests(fileName);
        actionwords.clickOnBotsBtnOnHomePage();
        actionwords.getScreenshot("testcase003RedirectToBotDetailsPageAndRunBotWithShortPositionTrendingDialogWithValidValuesAndValidateBotPositionPageAfter_1");
        actionwords.clickOnBotRowContainerToOpenBotDetailPageByBotName(botName);
        actionwords.clickOnBotRunBtnOnBotDetailsPage();
        actionwords.getScreenshot("testcase003RedirectToBotDetailsPageAndRunBotWithShortPositionTrendingDialogWithValidValuesAndValidateBotPositionPageAfter_2");
        actionwords.setStartBalanceOnBotRunDialog(startBalance);
        actionwords.setTradingFeeOnBotRunDialog(tradingFee);
        actionwords.setTradeLimitOnBotRunDialog(tradeLimit);
        actionwords.getScreenshot("testcase003RedirectToBotDetailsPageAndRunBotWithShortPositionTrendingDialogWithValidValuesAndValidateBotPositionPageAfter_3");
        actionwords.clickOnBotRunBtnOnBotRunDialog();
        actionwords.getScreenshot("testcase003RedirectToBotDetailsPageAndRunBotWithShortPositionTrendingDialogWithValidValuesAndValidateBotPositionPageAfter_4");
        actionwords.verifyThatRunBotDialogCloseWhenUserTriedToRunBotWithCorrectDataBot();
        actionwords.refreshPage();
        actionwords.openWebsite(link);
        actionwords.clickOnBotsBtnOnHomePage();
        actionwords.getScreenshot("testcase003RedirectToBotDetailsPageAndRunBotWithShortPositionTrendingDialogWithValidValuesAndValidateBotPositionPageAfter_5");
        actionwords.clickOnBotRowContainerToOpenBotDetailPageByBotName(botName);
        actionwords.getAndCalculateAndValidateAllValuesInPositionTableTalibBot("testcase003RedirectToBotDetailsPageAndRunBotWithShortPositionTrendingDialogWithValidValuesAndValidateBotPositionPageAfter_");
    }

    @Test
    public void testcase003RedirectToBotDetailsPageAndRunBotWithShortPositionTrendingDialogWithValidValuesAndValidateBotPositionPageAfterDataSet1() {
        testcase003RedirectToBotDetailsPageAndRunBotWithShortPositionTrendingDialogWithValidValuesAndValidateBotPositionPageAfter(websitelink, "bot_talib_position_page.txt", "1000", "0.5", "10", "test_talib");
    }

    public void testcase004RedirectToBotDetailsPageAndRunBotWithShortPositionTrendingDialogWithValidValuesAndValidateBotPositionPageAfterPlusRedirectToChartingAfterClickOnViewOnChartBtn(String link, String startBalance, String tradingFee, String tradeLimit, int position, String botName) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.clickOnBotsBtnOnHomePage();
        actionwords.getScreenshot("testcase004RedirectToBotDetailsPageAndRunBotWithShortPositionTrendingDialogWithValidValuesAndValidateBotPositionPageAfterPlusRedirectToChartingAfterClickOnViewOnChartBtn_1");
        actionwords.clickOnBotRowContainerToOpenBotDetailPageByBotName(botName);
        actionwords.clickOnBotRunBtnOnBotDetailsPage();
        actionwords.getScreenshot("testcase004RedirectToBotDetailsPageAndRunBotWithShortPositionTrendingDialogWithValidValuesAndValidateBotPositionPageAfterPlusRedirectToChartingAfterClickOnViewOnChartBtn_2");
        actionwords.setStartBalanceOnBotRunDialog(startBalance);
        actionwords.setTradingFeeOnBotRunDialog(tradingFee);
        actionwords.setTradeLimitOnBotRunDialog(tradeLimit);
        actionwords.getScreenshot("testcase004RedirectToBotDetailsPageAndRunBotWithShortPositionTrendingDialogWithValidValuesAndValidateBotPositionPageAfterPlusRedirectToChartingAfterClickOnViewOnChartBtn_3");
        actionwords.clickOnBotRunBtnOnBotRunDialog();
        actionwords.getScreenshot("testcase004RedirectToBotDetailsPageAndRunBotWithShortPositionTrendingDialogWithValidValuesAndValidateBotPositionPageAfterPlusRedirectToChartingAfterClickOnViewOnChartBtn_4");
        actionwords.verifyThatRunBotDialogCloseWhenUserTriedToRunBotWithCorrectDataBot();
        actionwords.refreshPage();
        actionwords.openWebsite(link);
        actionwords.clickOnBotsBtnOnHomePage();
        actionwords.getScreenshot("testcase004RedirectToBotDetailsPageAndRunBotWithShortPositionTrendingDialogWithValidValuesAndValidateBotPositionPageAfterPlusRedirectToChartingAfterClickOnViewOnChartBtn_5");
        actionwords.clickOnBotRowContainerToOpenBotDetailPageByBotName(botName);
        actionwords.getScreenshot("testcase004RedirectToBotDetailsPageAndRunBotWithShortPositionTrendingDialogWithValidValuesAndValidateBotPositionPageAfterPlusRedirectToChartingAfterClickOnViewOnChartBtn_6");
        actionwords.clickOnViewOnChartingButtonInPositionsTableRowByPositionX(position);
        actionwords.getScreenshot("testcase004RedirectToBotDetailsPageAndRunBotWithShortPositionTrendingDialogWithValidValuesAndValidateBotPositionPageAfterPlusRedirectToChartingAfterClickOnViewOnChartBtn_7");
    }

    @Test
    public void testcase004RedirectToBotDetailsPageAndRunBotWithShortPositionTrendingDialogWithValidValuesAndValidateBotPositionPageAfterPlusRedirectToChartingAfterClickOnViewOnChartBtnDataSet1() {
        testcase004RedirectToBotDetailsPageAndRunBotWithShortPositionTrendingDialogWithValidValuesAndValidateBotPositionPageAfterPlusRedirectToChartingAfterClickOnViewOnChartBtn(websitelink, "1000", "0.5", "10", 1, "test_talib");
    }
}
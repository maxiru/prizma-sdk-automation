package prizma.maxiru.tests;

import org.junit.FixMethodOrder;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import prizma.maxiru.framework.common.*;

import static prizma.maxiru.framework.common.PrizmaDriver.*;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)

public class WebSDKRunLongTrendingBotAndValidateLimitOfTradingAmountOnExchangeTests {
    @Rule
    public PrizmaScreenshotRule screenshotRule = new PrizmaScreenshotRule();

    public Actionwords actionwords = new Actionwords();

    public void testcase001RedirectToBotDetailsPageAndRunBotWithLongPositionTrendingDialogWithValidValuesAndValidateBotLandingPageAfter(String link, String fileName, String startBalance, String runFrom, String runTo, String tradingFee, String tradeLimit) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.deleteTheLogsFolder();
        actionwords.createTheLogsFolder();
        actionwords.createTheLogsFileForFailedTests(fileName);
        actionwords.clickOnBotsBtnOnHomePage();
        actionwords.getScreenshot("testcase001RedirectToBotDetailsPageAndRunBotWithLongPositionTrendingDialogWithValidValuesAndValidateBotLandingPageAfter_1");
        actionwords.clickOnBotRowContainerToOpenBotDetailPageByBotNameFromVariableValue();
        actionwords.clickOnBotRunBtnOnBotDetailsPage();
        actionwords.getScreenshot("testcase001RedirectToBotDetailsPageAndRunBotWithLongPositionTrendingDialogWithValidValuesAndValidateBotLandingPageAfter_2");
        actionwords.setStartBalanceOnBotRunDialog(startBalance);
        actionwords.setRunFromOnBotRunPage(runFrom);
        actionwords.setRunToOnBotRunPage(runTo);
        actionwords.setTradingFeeOnBotRunDialog(tradingFee);
        actionwords.setTradeLimitOnBotRunDialog(tradeLimit);
        actionwords.getScreenshot("testcase001RedirectToBotDetailsPageAndRunBotWithLongPositionTrendingDialogWithValidValuesAndValidateBotLandingPageAfter_3");
        actionwords.clickOnBotRunBtnOnBotRunDialog();
        actionwords.getScreenshot("testcase001RedirectToBotDetailsPageAndRunBotWithLongPositionTrendingDialogWithValidValuesAndValidateBotLandingPageAfter_4");
        actionwords.verifyThatRunBotDialogCloseWhenUserTriedToRunBotWithCorrectDataBot();
        actionwords.refreshPage();
        actionwords.openWebsite(link);
        actionwords.clickOnBotsBtnOnHomePage();
        actionwords.getScreenshot("testcase001RedirectToBotDetailsPageAndRunBotWithLongPositionTrendingDialogWithValidValuesAndValidateBotLandingPageAfter_5");
        actionwords.clickOnBotRowContainerToOpenBotDetailPageByBotNameFromVariableValue();
        actionwords.getAndStoreTotalCountOfAllTrendingPositions();
        actionwords.getAndStoreTotalCountOfOpenAndCloseTrendingPositions();
        actionwords.getAndStoreInOpenTrendingPositionsValue();
        actionwords.getAndCalculateAllProfitCurrencyValues();
        actionwords.getAndCalculateAllProfitCurrencyValuesForClosedPositions();
        actionwords.getAndCalculateAllProfitCurrencyValuesForOpenPositions();
        actionwords.getAndCalculateAllProfitPercentageValues();
        actionwords.getAndCalculateAllProfitPercentageValuesForClosedPositions();
        actionwords.getAndCalculateAllProfitPercentageValuesForOpenPositions();
        actionwords.getAndCalculateTotalBalance();
        actionwords.getAndCalculateAvailableBalance();
        actionwords.getAndCalculateAverageProfitCurrency();
        actionwords.getAndCalculateAverageProfitPercentage();
        actionwords.getAndCalculateTotalProfitPercentageValues();
        actionwords.getAndCalculateBotEarnedValues();
        actionwords.getScreenshot("testcase001RedirectToBotDetailsPageAndRunBotWithLongPositionTrendingDialogWithValidValuesAndValidateBotLandingPageAfter_6");
        actionwords.refreshPage();
        actionwords.openWebsite(link);
        actionwords.clickOnBotsBtnOnHomePage();
        actionwords.getScreenshot("testcase001RedirectToBotDetailsPageAndRunBotWithLongPositionTrendingDialogWithValidValuesAndValidateBotLandingPageAfter_7");
        actionwords.verifyThatBotNameOnPositionFromVariableValueIsCorrectIfValidateWithVariableValue();
        actionwords.verifyThatBotLastRunOnPositionFromVariableValueIsCorrectIfValidateWithVariableValue();
        actionwords.getScreenshot("testcase001RedirectToBotDetailsPageAndRunBotWithLongPositionTrendingDialogWithValidValuesAndValidateBotLandingPageAfter_8");
    }

    @Test
    public void testcase001RedirectToBotDetailsPageAndRunBotWithLongPositionTrendingDialogWithValidValuesAndValidateBotLandingPageAfterDataSet1() {
        testcase001RedirectToBotDetailsPageAndRunBotWithLongPositionTrendingDialogWithValidValuesAndValidateBotLandingPageAfter(websitelink, "bot_long_landing_page_1.txt", "100", "10/01/2021", "10/10/2021", "0.5", "10");
    }

    public void testcase002RedirectToBotDetailsPageAndRunBotDialogWithLongPositionTrendingValidValuesAndValidateBotSummaryPageAfter(String link, String fileName, String startBalance, String tradingFee, String tradeLimit) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.createTheLogsFileForFailedTests(fileName);
        actionwords.clickOnBotsBtnOnHomePage();
        actionwords.getScreenshot("testcase002RedirectToBotDetailsPageAndRunBotDialogWithLongPositionTrendingValidValuesAndValidateBotSummaryPageAfter_1");
        actionwords.clickOnBotRowContainerToOpenBotDetailPageByBotNameFromVariableValue();
        actionwords.clickOnBotRunBtnOnBotDetailsPage();
        actionwords.getScreenshot("testcase002RedirectToBotDetailsPageAndRunBotDialogWithLongPositionTrendingValidValuesAndValidateBotSummaryPageAfter_2");
        actionwords.setStartBalanceOnBotRunDialog(startBalance);
        actionwords.setTradingFeeOnBotRunDialog(tradingFee);
        actionwords.setTradeLimitOnBotRunDialog(tradeLimit);
        actionwords.getScreenshot("testcase002RedirectToBotDetailsPageAndRunBotDialogWithLongPositionTrendingValidValuesAndValidateBotSummaryPageAfter_3");
        actionwords.clickOnBotRunBtnOnBotRunDialog();
        actionwords.getScreenshot("testcase002RedirectToBotDetailsPageAndRunBotDialogWithLongPositionTrendingValidValuesAndValidateBotSummaryPageAfter_4");
        actionwords.verifyThatRunBotDialogCloseWhenUserTriedToRunBotWithCorrectDataBot();
        actionwords.refreshPage();
        actionwords.openWebsite(link);
        actionwords.clickOnBotsBtnOnHomePage();
        actionwords.getScreenshot("testcase002RedirectToBotDetailsPageAndRunBotDialogWithLongPositionTrendingValidValuesAndValidateBotSummaryPageAfter_5");
        actionwords.clickOnBotRowContainerToOpenBotDetailPageByBotNameFromVariableValue();
        actionwords.getAndStoreTotalCountOfAllTrendingPositions();
        actionwords.getAndStoreTotalCountOfOpenAndCloseTrendingPositions();
        actionwords.getAndStoreInOpenTrendingPositionsValue();
        actionwords.getAndCalculateAllProfitCurrencyValues();
        actionwords.getAndCalculateAllProfitCurrencyValuesForClosedPositions();
        actionwords.getAndCalculateAllProfitCurrencyValuesForOpenPositions();
        actionwords.getAndCalculateAllProfitPercentageValues();
        actionwords.getAndCalculateAllProfitPercentageValuesForClosedPositions();
        actionwords.getAndCalculateAllProfitPercentageValuesForOpenPositions();
        actionwords.getAndCalculateTotalBalance();
        actionwords.getAndCalculateAvailableBalance();
        actionwords.getAndCalculateAverageProfitCurrency();
        actionwords.getAndCalculateAverageProfitPercentage();
        actionwords.getAndCalculateTotalProfitPercentageValues();
        actionwords.getAndCalculateBotEarnedValues();
        actionwords.getScreenshot("testcase002RedirectToBotDetailsPageAndRunBotDialogWithLongPositionTrendingValidValuesAndValidateBotSummaryPageAfter_6");
        actionwords.refreshPage();
        actionwords.openWebsite(link);
        actionwords.clickOnBotsBtnOnHomePage();
        actionwords.getScreenshot("testcase002RedirectToBotDetailsPageAndRunBotDialogWithLongPositionTrendingValidValuesAndValidateBotSummaryPageAfter_7");
        actionwords.clickOnBotRowContainerToOpenBotDetailPageByBotNameFromVariableValue();
        actionwords.verifyThatBotLastRunIsCorrectOnBotSummaryTableIfValidateWithVariableValue();
        actionwords.verifyThatBotFromToIsCorrectOnBotSummaryTableStartDateIfValidateWithVariableValue();
        actionwords.verifyThatBotFromToIsCorrectOnBotSummaryTableEndDateIfValidateWithVariableValue();
        actionwords.verifyThatBotClosedPositionsIsCorrectOnBotSummaryTableIfValidateWithVariableValue();
        actionwords.verifyThatBotInitialBalanceIsCorrectOnBotSummaryTableIfValidateWithVariableValue();
        actionwords.verifyThatBotInOpenPositionsBalanceIsCorrectOnBotSummaryTableWithIfValidateVariableValue();
        actionwords.verifyThatBotTotalBalanceIsCorrectOnBotSummaryTableIfValidateWithVariableValue();
        actionwords.verifyThatBotAverangeProfitIsCorrectOnBotSummaryTableIfValidateWithVariableValueWhichIsZero();
        actionwords.verifyThatBotTotalProfitIsCorrectOnBotSummaryTableIfValidateWithVariableValueWhichIsZero();
        actionwords.verifyThatBotEarnedIsCorrectOnBotSummaryTableIfValidateWithVariableValueWhichIsZero();
        actionwords.getScreenshot("testcase002RedirectToBotDetailsPageAndRunBotDialogWithLongPositionTrendingValidValuesAndValidateBotSummaryPageAfter_8");
    }

    @Test
    public void testcase002RedirectToBotDetailsPageAndRunBotDialogWithLongPositionTrendingValidValuesAndValidateBotSummaryPageAfterDataSet1() {
        testcase002RedirectToBotDetailsPageAndRunBotDialogWithLongPositionTrendingValidValuesAndValidateBotSummaryPageAfter(websitelink, "bot_long_summary_page_1.txt", "100", "0.5", "10");
    }

    public void testcase003RedirectToBotDetailsPageAndRunBotWithLongPositionTrendingDialogWithValidValuesAndValidateBotPositionPageAfter(String link, String fileName, String startBalance, String tradingFee, String tradeLimit) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.createTheLogsFileForFailedTests(fileName);
        actionwords.clickOnBotsBtnOnHomePage();
        actionwords.getScreenshot("testcase003RedirectToBotDetailsPageAndRunBotWithLongPositionTrendingDialogWithValidValuesAndValidateBotPositionPageAfter_1");
        actionwords.clickOnBotRowContainerToOpenBotDetailPageByBotNameFromVariableValue();
        actionwords.clickOnBotRunBtnOnBotDetailsPage();
        actionwords.getScreenshot("testcase003RedirectToBotDetailsPageAndRunBotWithLongPositionTrendingDialogWithValidValuesAndValidateBotPositionPageAfter_2");
        actionwords.setStartBalanceOnBotRunDialog(startBalance);
        actionwords.setTradingFeeOnBotRunDialog(tradingFee);
        actionwords.setTradeLimitOnBotRunDialog(tradeLimit);
        actionwords.getScreenshot("testcase003RedirectToBotDetailsPageAndRunBotWithLongPositionTrendingDialogWithValidValuesAndValidateBotPositionPageAfter_3");
        actionwords.clickOnBotRunBtnOnBotRunDialog();
        actionwords.getScreenshot("testcase003RedirectToBotDetailsPageAndRunBotWithLongPositionTrendingDialogWithValidValuesAndValidateBotPositionPageAfter_4");
        actionwords.verifyThatRunBotDialogCloseWhenUserTriedToRunBotWithCorrectDataBot();
        actionwords.refreshPage();
        actionwords.openWebsite(link);
        actionwords.clickOnBotsBtnOnHomePage();
        actionwords.getScreenshot("testcase003RedirectToBotDetailsPageAndRunBotWithLongPositionTrendingDialogWithValidValuesAndValidateBotPositionPageAfter_5");
        actionwords.clickOnBotRowContainerToOpenBotDetailPageByBotNameFromVariableValue();
        actionwords.getAndCalculateAndValidateAllValuesInPositionTableWhichShouldBeNotEmpty("testcase003RedirectToBotDetailsPageAndRunBotWithLongPositionTrendingDialogWithValidValuesAndValidateBotPositionPageAfter_6");
    }

    @Test
    public void testcase003RedirectToBotDetailsPageAndRunBotWithLongPositionTrendingDialogWithValidValuesAndValidateBotPositionPageAfterDataSet1() {
        testcase003RedirectToBotDetailsPageAndRunBotWithLongPositionTrendingDialogWithValidValuesAndValidateBotPositionPageAfter(websitelink, "bot_long_position_page_1.txt", "100", "0.5", "10");
    }
}
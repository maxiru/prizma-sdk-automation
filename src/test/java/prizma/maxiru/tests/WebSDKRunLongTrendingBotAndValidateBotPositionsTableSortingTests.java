package prizma.maxiru.tests;

import org.junit.FixMethodOrder;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import prizma.maxiru.framework.common.*;

import static prizma.maxiru.framework.common.PrizmaDriver.*;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)

public class WebSDKRunLongTrendingBotAndValidateBotPositionsTableSortingTests {
    @Rule
    public PrizmaScreenshotRule screenshotRule = new PrizmaScreenshotRule();

    public Actionwords actionwords = new Actionwords();

    public void testcase001RedirectToBotDetailsPageAndRunBotWithLongPositionTrendingDialogWithValidValuesAndValidateBotLandingPageAfter(String link, String fileName, String startBalance, String runFrom, String runTo, String tradingFee, String tradeLimit) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.deleteTheLogsFolder();
        actionwords.createTheLogsFolder();
        actionwords.createTheLogsFileForFailedTests(fileName);
        actionwords.clickOnBotsBtnOnHomePage();
        actionwords.getScreenshot("testcase001RedirectToBotDetailsPageAndRunBotWithLongPositionTrendingDialogWithValidValuesAndValidateBotLandingPageAfter_1");
        actionwords.clickOnBotRowContainerToOpenBotDetailPageByBotNameFromVariableValue();
        actionwords.clickOnBotRunBtnOnBotDetailsPage();
        actionwords.getScreenshot("testcase001RedirectToBotDetailsPageAndRunBotWithLongPositionTrendingDialogWithValidValuesAndValidateBotLandingPageAfter_2");
        actionwords.setStartBalanceOnBotRunDialog(startBalance);
        actionwords.setRunFromOnBotRunPage(runFrom);
        actionwords.setRunToOnBotRunPage(runTo);
        actionwords.setTradingFeeOnBotRunDialog(tradingFee);
        actionwords.setTradeLimitOnBotRunDialog(tradeLimit);
        actionwords.getScreenshot("testcase001RedirectToBotDetailsPageAndRunBotWithLongPositionTrendingDialogWithValidValuesAndValidateBotLandingPageAfter_3");
        actionwords.clickOnBotRunBtnOnBotRunDialog();
        actionwords.getScreenshot("testcase001RedirectToBotDetailsPageAndRunBotWithLongPositionTrendingDialogWithValidValuesAndValidateBotLandingPageAfter_4");
        actionwords.verifyThatRunBotDialogCloseWhenUserTriedToRunBotWithCorrectDataBot();
        actionwords.refreshPage();
        actionwords.openWebsite(link);
        actionwords.clickOnBotsBtnOnHomePage();
        actionwords.getScreenshot("testcase001RedirectToBotDetailsPageAndRunBotWithLongPositionTrendingDialogWithValidValuesAndValidateBotLandingPageAfter_5");
        actionwords.clickOnBotRowContainerToOpenBotDetailPageByBotNameFromVariableValue();
        actionwords.getAndStoreTotalCountOfAllTrendingPositions();
        actionwords.getAndStoreTotalCountOfOpenAndCloseTrendingPositions();
        actionwords.getAndStoreInOpenTrendingPositionsValue();
        actionwords.getAndCalculateAllProfitCurrencyValues();
        actionwords.getAndCalculateAllProfitCurrencyValuesForClosedPositions();
        actionwords.getAndCalculateAllProfitCurrencyValuesForOpenPositions();
        actionwords.getAndCalculateAllProfitPercentageValues();
        actionwords.getAndCalculateAllProfitPercentageValuesForClosedPositions();
        actionwords.getAndCalculateAllProfitPercentageValuesForOpenPositions();
        actionwords.getAndCalculateTotalBalance();
        actionwords.getAndCalculateAvailableBalance();
        actionwords.getAndCalculateAverageProfitCurrency();
        actionwords.getAndCalculateAverageProfitPercentage();
        actionwords.getAndCalculateTotalProfitPercentageValues();
        actionwords.getAndCalculateBotEarnedValues();
        actionwords.getScreenshot("testcase001RedirectToBotDetailsPageAndRunBotWithLongPositionTrendingDialogWithValidValuesAndValidateBotLandingPageAfter_6");
        actionwords.refreshPage();
        actionwords.openWebsite(link);
        actionwords.clickOnBotsBtnOnHomePage();
        actionwords.getScreenshot("testcase001RedirectToBotDetailsPageAndRunBotWithLongPositionTrendingDialogWithValidValuesAndValidateBotLandingPageAfter_7");
        actionwords.verifyThatBotNameOnPositionFromVariableValueIsCorrectIfValidateWithVariableValue();
        actionwords.verifyThatBotLastRunOnPositionFromVariableValueIsCorrectIfValidateWithVariableValue();
        actionwords.verifyThatBotAverangeProfitCurrencyOnPositionFromVariableValueIsCorrectIfValidateWithVariableValue();
        actionwords.verifyThatBotAverangeProfitPercentageOnPositionFromVariableValueIsCorrectIfValidateWithVariableValue();
        actionwords.verifyThatBotProfitCurrencyOnPositionFromVariableValueIsCorrectIfValidateWithVariableValue();
        actionwords.verifyThatBotProfitPercentageOnPositionFromVariableValueIsCorrectIfValidateWithVariableValue();
        actionwords.getScreenshot("testcase001RedirectToBotDetailsPageAndRunBotWithLongPositionTrendingDialogWithValidValuesAndValidateBotLandingPageAfter_8");
    }

    @Test
    public void testcase001RedirectToBotDetailsPageAndRunBotWithLongPositionTrendingDialogWithValidValuesAndValidateBotLandingPageAfterDataSet1() {
        testcase001RedirectToBotDetailsPageAndRunBotWithLongPositionTrendingDialogWithValidValuesAndValidateBotLandingPageAfter(websitelink, "bot_long_landing_page.txt", "1000", "10/01/2021", "10/10/2021", "0.5", "10");
    }

    public void testcase002RedirectToBotDetailsPageAndValidateBotPositionsTableSortingByDate(String link, String startBalance, String tradingFee, String tradeLimit) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.clickOnBotsBtnOnHomePage();
        actionwords.getScreenshot("testcase002RedirectToBotDetailsPageAndValidateBotPositionsTableSortingByDate_1");
        actionwords.clickOnBotRowContainerToOpenBotDetailPageByBotNameFromVariableValue();
        actionwords.clickOnBotRunBtnOnBotDetailsPage();
        actionwords.getScreenshot("testcase002RedirectToBotDetailsPageAndValidateBotPositionsTableSortingByDate_2");
        actionwords.setStartBalanceOnBotRunDialog(startBalance);
        actionwords.setTradingFeeOnBotRunDialog(tradingFee);
        actionwords.setTradeLimitOnBotRunDialog(tradeLimit);
        actionwords.getScreenshot("testcase002RedirectToBotDetailsPageAndValidateBotPositionsTableSortingByDate_3");
        actionwords.clickOnBotRunBtnOnBotRunDialog();
        actionwords.getScreenshot("testcase002RedirectToBotDetailsPageAndValidateBotPositionsTableSortingByDate_4");
        actionwords.verifyThatRunBotDialogCloseWhenUserTriedToRunBotWithCorrectDataBot();
        actionwords.refreshPage();
        actionwords.openWebsite(link);
        actionwords.clickOnBotsBtnOnHomePage();
        actionwords.getScreenshot("testcase002RedirectToBotDetailsPageAndValidateBotPositionsTableSortingByDate_5");
        actionwords.clickOnBotRowContainerToOpenBotDetailPageByBotNameFromVariableValue();
        actionwords.getScreenshot("testcase002RedirectToBotDetailsPageAndValidateBotPositionsTableSortingByDate_6");
        actionwords.clickOnPositionsTableDateSortingToggle();
        actionwords.getScreenshot("testcase002RedirectToBotDetailsPageAndValidateBotPositionsTableSortingByDate_7");
        actionwords.clickOnPositionsTableDateSortingToggle();
        actionwords.getScreenshot("testcase002RedirectToBotDetailsPageAndValidateBotPositionsTableSortingByDate_8");
        actionwords.clickOnPositionsTableDateSortingToggle();
        actionwords.getScreenshot("testcase002RedirectToBotDetailsPageAndValidateBotPositionsTableSortingByDate_9");
    }

    @Test
    public void testcase002RedirectToBotDetailsPageAndValidateBotPositionsTableSortingByDateDataSet1() {
        testcase002RedirectToBotDetailsPageAndValidateBotPositionsTableSortingByDate(websitelink, "1000", "0.5", "10");
    }

    public void testcase003RedirectToBotDetailsPageAndValidateBotPositionsTableSortingByPair(String link, String startBalance, String tradingFee, String tradeLimit) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.clickOnBotsBtnOnHomePage();
        actionwords.getScreenshot("testcase003RedirectToBotDetailsPageAndValidateBotPositionsTableSortingByPair_1");
        actionwords.clickOnBotRowContainerToOpenBotDetailPageByBotNameFromVariableValue();
        actionwords.clickOnBotRunBtnOnBotDetailsPage();
        actionwords.getScreenshot("testcase003RedirectToBotDetailsPageAndValidateBotPositionsTableSortingByPair_2");
        actionwords.setStartBalanceOnBotRunDialog(startBalance);
        actionwords.setTradingFeeOnBotRunDialog(tradingFee);
        actionwords.setTradeLimitOnBotRunDialog(tradeLimit);
        actionwords.getScreenshot("testcase003RedirectToBotDetailsPageAndValidateBotPositionsTableSortingByPair_3");
        actionwords.clickOnBotRunBtnOnBotRunDialog();
        actionwords.getScreenshot("testcase003RedirectToBotDetailsPageAndValidateBotPositionsTableSortingByPair_4");
        actionwords.verifyThatRunBotDialogCloseWhenUserTriedToRunBotWithCorrectDataBot();
        actionwords.refreshPage();
        actionwords.openWebsite(link);
        actionwords.clickOnBotsBtnOnHomePage();
        actionwords.getScreenshot("testcase003RedirectToBotDetailsPageAndValidateBotPositionsTableSortingByPair_5");
        actionwords.clickOnBotRowContainerToOpenBotDetailPageByBotNameFromVariableValue();
        actionwords.getScreenshot("testcase003RedirectToBotDetailsPageAndValidateBotPositionsTableSortingByPair_6");
        actionwords.clickOnPositionsTablePairSortingToggle();
        actionwords.getScreenshot("testcase003RedirectToBotDetailsPageAndValidateBotPositionsTableSortingByPair_7");
        actionwords.clickOnPositionsTablePairSortingToggle();
        actionwords.getScreenshot("testcase003RedirectToBotDetailsPageAndValidateBotPositionsTableSortingByPair_8");
        actionwords.clickOnPositionsTablePairSortingToggle();
        actionwords.getScreenshot("testcase003RedirectToBotDetailsPageAndValidateBotPositionsTableSortingByPair_9");
    }

    @Test
    public void testcase003RedirectToBotDetailsPageAndValidateBotPositionsTableSortingByPairDataSet1() {
        testcase003RedirectToBotDetailsPageAndValidateBotPositionsTableSortingByPair(websitelink, "1000", "0.5", "10");
    }

    public void testcase004RedirectToBotDetailsPageAndValidateBotPositionsTableSortingByType(String link, String startBalance, String tradingFee, String tradeLimit) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.clickOnBotsBtnOnHomePage();
        actionwords.getScreenshot("testcase004RedirectToBotDetailsPageAndValidateBotPositionsTableSortingByType_1");
        actionwords.clickOnBotRowContainerToOpenBotDetailPageByBotNameFromVariableValue();
        actionwords.clickOnBotRunBtnOnBotDetailsPage();
        actionwords.getScreenshot("testcase004RedirectToBotDetailsPageAndValidateBotPositionsTableSortingByType_2");
        actionwords.setStartBalanceOnBotRunDialog(startBalance);
        actionwords.setTradingFeeOnBotRunDialog(tradingFee);
        actionwords.setTradeLimitOnBotRunDialog(tradeLimit);
        actionwords.getScreenshot("testcase004RedirectToBotDetailsPageAndValidateBotPositionsTableSortingByType_3");
        actionwords.clickOnBotRunBtnOnBotRunDialog();
        actionwords.getScreenshot("testcase004RedirectToBotDetailsPageAndValidateBotPositionsTableSortingByType_4");
        actionwords.verifyThatRunBotDialogCloseWhenUserTriedToRunBotWithCorrectDataBot();
        actionwords.refreshPage();
        actionwords.openWebsite(link);
        actionwords.clickOnBotsBtnOnHomePage();
        actionwords.getScreenshot("testcase004RedirectToBotDetailsPageAndValidateBotPositionsTableSortingByType_5");
        actionwords.clickOnBotRowContainerToOpenBotDetailPageByBotNameFromVariableValue();
        actionwords.getScreenshot("testcase004RedirectToBotDetailsPageAndValidateBotPositionsTableSortingByType_6");
        actionwords.clickOnPositionsTableTypeSortingToggle();
        actionwords.getScreenshot("testcase004RedirectToBotDetailsPageAndValidateBotPositionsTableSortingByType_7");
        actionwords.clickOnPositionsTableTypeSortingToggle();
        actionwords.getScreenshot("testcase004RedirectToBotDetailsPageAndValidateBotPositionsTableSortingByType_8");
        actionwords.clickOnPositionsTableTypeSortingToggle();
        actionwords.getScreenshot("testcase004RedirectToBotDetailsPageAndValidateBotPositionsTableSortingByType_9");
    }

    @Test
    public void testcase004RedirectToBotDetailsPageAndValidateBotPositionsTableSortingByTypeDataSet1() {
        testcase004RedirectToBotDetailsPageAndValidateBotPositionsTableSortingByType(websitelink, "1000", "0.5", "10");
    }

    public void testcase005RedirectToBotDetailsPageAndValidateBotPositionsTableSortingByProfit(String link, String startBalance, String tradingFee, String tradeLimit) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.clickOnBotsBtnOnHomePage();
        actionwords.getScreenshot("testcase005RedirectToBotDetailsPageAndValidateBotPositionsTableSortingByProfit_1");
        actionwords.clickOnBotRowContainerToOpenBotDetailPageByBotNameFromVariableValue();
        actionwords.clickOnBotRunBtnOnBotDetailsPage();
        actionwords.getScreenshot("testcase005RedirectToBotDetailsPageAndValidateBotPositionsTableSortingByProfit_2");
        actionwords.setStartBalanceOnBotRunDialog(startBalance);
        actionwords.setTradingFeeOnBotRunDialog(tradingFee);
        actionwords.setTradeLimitOnBotRunDialog(tradeLimit);
        actionwords.getScreenshot("testcase005RedirectToBotDetailsPageAndValidateBotPositionsTableSortingByProfit_3");
        actionwords.clickOnBotRunBtnOnBotRunDialog();
        actionwords.getScreenshot("testcase005RedirectToBotDetailsPageAndValidateBotPositionsTableSortingByProfit_4");
        actionwords.verifyThatRunBotDialogCloseWhenUserTriedToRunBotWithCorrectDataBot();
        actionwords.refreshPage();
        actionwords.openWebsite(link);
        actionwords.clickOnBotsBtnOnHomePage();
        actionwords.getScreenshot("testcase005RedirectToBotDetailsPageAndValidateBotPositionsTableSortingByProfit_5");
        actionwords.clickOnBotRowContainerToOpenBotDetailPageByBotNameFromVariableValue();
        actionwords.getScreenshot("testcase005RedirectToBotDetailsPageAndValidateBotPositionsTableSortingByProfit_6");
        actionwords.clickOnPositionsTableProfitSortingToggle();
        actionwords.getScreenshot("testcase005RedirectToBotDetailsPageAndValidateBotPositionsTableSortingByProfit_7");
        actionwords.clickOnPositionsTableProfitSortingToggle();
        actionwords.getScreenshot("testcase005RedirectToBotDetailsPageAndValidateBotPositionsTableSortingByProfit_8");
        actionwords.clickOnPositionsTableProfitSortingToggle();
        actionwords.getScreenshot("testcase005RedirectToBotDetailsPageAndValidateBotPositionsTableSortingByProfit_9");
    }

    @Test
    public void testcase005RedirectToBotDetailsPageAndValidateBotPositionsTableSortingByProfitDataSet1() {
        testcase005RedirectToBotDetailsPageAndValidateBotPositionsTableSortingByProfit(websitelink, "1000", "0.5", "10");
    }
}
package prizma.maxiru.tests;

import org.junit.FixMethodOrder;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import prizma.maxiru.framework.common.*;

import static prizma.maxiru.framework.common.PrizmaCurrency.*;
import static prizma.maxiru.framework.common.PrizmaDriver.*;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)

public class WebSDKChartingTests {
    @Rule
    public PrizmaScreenshotRule screenshotRule = new PrizmaScreenshotRule();

    public Actionwords actionwords = new Actionwords();

    public void testcase001OpenChartingPageAndSelectTimeRange1Min(String link) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.clickOnChartingBtnOnHomePage();
        actionwords.getScreenshot("testcase001OpenChartingPageAndSelectTimeRange1Min_1");
        actionwords.clickOnOneMinBtnOnChartingPage();
        actionwords.getScreenshot("testcase001OpenChartingPageAndSelectTimeRange1Min_2");
    }

    @Test
    @Ignore
    public void testcase001OpenChartingPageAndSelectTimeRange1MinDataSet1() {
        testcase001OpenChartingPageAndSelectTimeRange1Min(websitelink);
    }

    public void testcase002OpenChartingPageAndSelectTimeRange5Min(String link) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.clickOnChartingBtnOnHomePage();
        actionwords.getScreenshot("testcase002OpenChartingPageAndSelectTimeRange5Min_1");
        actionwords.clickOnFiveMinBtnOnChartingPage();
        actionwords.getScreenshot("testcase002OpenChartingPageAndSelectTimeRange5Min_2");
    }

    @Test
    public void testcase002OpenChartingPageAndSelectTimeRange5MinDataSet1() {
        testcase002OpenChartingPageAndSelectTimeRange5Min(websitelink);
    }

    public void testcase003OpenChartingPageAndSelectTimeRange15Min(String link) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.clickOnChartingBtnOnHomePage();
        actionwords.getScreenshot("testcase003OpenChartingPageAndSelectTimeRange15Min_1");
        actionwords.clickOnFifteenMinBtnOnChartingPage();
        actionwords.getScreenshot("testcase003OpenChartingPageAndSelectTimeRange15Min_2");
    }

    @Test
    public void testcase003OpenChartingPageAndSelectTimeRange15MinDataSet1() {
        testcase003OpenChartingPageAndSelectTimeRange15Min(websitelink);
    }

    public void testcase004OpenChartingPageAndSelectTimeRange60Min(String link) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.clickOnChartingBtnOnHomePage();
        actionwords.getScreenshot("testcase004OpenChartingPageAndSelectTimeRange60Min_1");
        actionwords.clickOnSixtyMinBtnOnChartingPage();
        actionwords.getScreenshot("testcase004OpenChartingPageAndSelectTimeRange60Min_2");
    }

    @Test
    public void testcase004OpenChartingPageAndSelectTimeRange60MinDataSet1() {
        testcase004OpenChartingPageAndSelectTimeRange60Min(websitelink);
    }

    public void testcase005OpenChartingPageAndSelectTimeRange12Hours(String link) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.clickOnChartingBtnOnHomePage();
        actionwords.getScreenshot("testcase005OpenChartingPageAndSelectTimeRange12Hours_1");
        actionwords.clickOnTwelveHoursBtnOnChartingPage();
        actionwords.getScreenshot("testcase005OpenChartingPageAndSelectTimeRange12Hours_2");
    }

    @Test
    public void testcase005OpenChartingPageAndSelectTimeRange12HoursDataSet1() {
        testcase005OpenChartingPageAndSelectTimeRange12Hours(websitelink);
    }

    public void testcase006OpenChartingPageAndSelectTimeRange1Day(String link) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.clickOnChartingBtnOnHomePage();
        actionwords.getScreenshot("testcase006OpenChartingPageAndSelectTimeRange1Day_1");
        actionwords.clickOnOneDayBtnOnChartingPage();
        actionwords.getScreenshot("testcase006OpenChartingPageAndSelectTimeRange1Day_2");
    }

    @Test
    public void testcase006OpenChartingPageAndSelectTimeRange1DayDataSet1() {
        testcase006OpenChartingPageAndSelectTimeRange1Day(websitelink);
    }

    public void testcase007OpenChartingPageAndSelectCurrencyPairCurrency1_USD(String link, String currency) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.clickOnChartingBtnOnHomePage();
        actionwords.getScreenshot("testcase007OpenChartingPageAndSelectCurrencyPairCurrency1_USD_1");
        actionwords.clickOnCurrencyDropDownAndSelectProperOption(currency);
        actionwords.getScreenshot("testcase007OpenChartingPageAndSelectCurrencyPairCurrency1_USD_2");
    }

    @Test
    public void testcase007OpenChartingPageAndSelectCurrencyPairCurrency1_USDDataSet1() {
        testcase007OpenChartingPageAndSelectCurrencyPairCurrency1_USD(websitelink, CURRENCY_1);
    }

    public void testcase008OpenChartingPageAndSelectCurrencyPairCurrency2_USD(String link, String currency) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.clickOnChartingBtnOnHomePage();
        actionwords.getScreenshot("testcase008OpenChartingPageAndSelectCurrencyPairCurrency2_USD_1");
        actionwords.clickOnCurrencyDropDownAndSelectProperOption(currency);
        actionwords.getScreenshot("testcase008OpenChartingPageAndSelectCurrencyPairCurrency2_USD_2");
    }

    @Test
    public void testcase008OpenChartingPageAndSelectCurrencyPairCurrency2_USDDataSet1() {
        testcase008OpenChartingPageAndSelectCurrencyPairCurrency2_USD(websitelink, CURRENCY_2);
    }

    public void testcase009OpenChartingPageAndSelectCurrencyPairCurrency3_USD(String link, String currency) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.clickOnChartingBtnOnHomePage();
        actionwords.getScreenshot("testcase009OpenChartingPageAndSelectCurrencyPairCurrency3_USD_1");
        actionwords.clickOnCurrencyDropDownAndSelectProperOption(currency);
        actionwords.getScreenshot("testcase009OpenChartingPageAndSelectCurrencyPairCurrency3_USD_2");
    }

    @Test
    public void testcase009OpenChartingPageAndSelectCurrencyPairCurrency3_USDDataSet1() {
        testcase009OpenChartingPageAndSelectCurrencyPairCurrency3_USD(websitelink, CURRENCY_3);
    }

    public void testcase010OpenChartingPageAndSelectCurrencyPairCurrency4_USD(String link, String currency) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.clickOnChartingBtnOnHomePage();
        actionwords.getScreenshot("testcase010OpenChartingPageAndSelectCurrencyPairCurrency4_USD_1");
        actionwords.clickOnCurrencyDropDownAndSelectProperOption(currency);
        actionwords.getScreenshot("testcase010OpenChartingPageAndSelectCurrencyPairCurrency4_USD_2");
    }

    @Test
    public void testcase010OpenChartingPageAndSelectCurrencyPairCurrency4_USDDataSet1() {
        testcase010OpenChartingPageAndSelectCurrencyPairCurrency4_USD(websitelink, CURRENCY_4);
    }

    public void testcase011OpenChartingPageAndSelectCurrencyPairCurrency5_USD(String link, String currency) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.clickOnChartingBtnOnHomePage();
        actionwords.getScreenshot("testcase011OpenChartingPageAndSelectCurrencyPairCurrency5_USD_1");
        actionwords.clickOnCurrencyDropDownAndSelectProperOption(currency);
        actionwords.getScreenshot("testcase011OpenChartingPageAndSelectCurrencyPairCurrency5_USD_2");
    }

    @Test
    public void testcase011OpenChartingPageAndSelectCurrencyPairCurrency5_USDDataSet1() {
        testcase011OpenChartingPageAndSelectCurrencyPairCurrency5_USD(websitelink, CURRENCY_5);
    }

    public void testcase012OpenChartingPageAndSelectCurrencyPairCurrency6_USD(String link, String currency) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.clickOnChartingBtnOnHomePage();
        actionwords.getScreenshot("testcase012OpenChartingPageAndSelectCurrencyPairCurrency6_USD_1");
        actionwords.clickOnCurrencyDropDownAndSelectProperOption(currency);
        actionwords.getScreenshot("testcase012OpenChartingPageAndSelectCurrencyPairCurrency6_USD_2");
    }

    @Test
    public void testcase012OpenChartingPageAndSelectCurrencyPairCurrency6_USDDataSet1() {
        testcase012OpenChartingPageAndSelectCurrencyPairCurrency6_USD(websitelink, CURRENCY_6);
    }

    public void testcase013OpenChartingPageAndSelectCurrencyPairCurrency7_USD(String link, String currency) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.clickOnChartingBtnOnHomePage();
        actionwords.getScreenshot("testcase013OpenChartingPageAndSelectCurrencyPairCurrency7_USD_1");
        actionwords.clickOnCurrencyDropDownAndSelectProperOption(currency);
        actionwords.getScreenshot("testcase013OpenChartingPageAndSelectCurrencyPairCurrency7_USD_2");
    }

    @Test
    public void testcase013OpenChartingPageAndSelectCurrencyPairCurrency7_USDDataSet1() {
        testcase013OpenChartingPageAndSelectCurrencyPairCurrency7_USD(websitelink, CURRENCY_7);
    }

    public void testcase014OpenChartingPageAndSelectCurrencyPairCurrency8_USD(String link, String currency) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.clickOnChartingBtnOnHomePage();
        actionwords.getScreenshot("testcase014OpenChartingPageAndSelectCurrencyPairCurrency8_USD_1");
        actionwords.clickOnCurrencyDropDownAndSelectProperOption(currency);
        actionwords.getScreenshot("testcase014OpenChartingPageAndSelectCurrencyPairCurrency8_USD_2");
    }

    @Test
    public void testcase014OpenChartingPageAndSelectCurrencyPairCurrency8_USDDataSet1() {
        testcase014OpenChartingPageAndSelectCurrencyPairCurrency8_USD(websitelink, CURRENCY_8);
    }

    public void testcase015OpenChartingPageAndSelectCurrencyPairCurrency1_USDAndTimeRange1Min(String link, String currency) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.clickOnChartingBtnOnHomePage();
        actionwords.getScreenshot("testcase015OpenChartingPageAndSelectCurrencyPairCurrency1_USDAndTimeRange1Min_1");
        actionwords.clickOnOneMinBtnOnChartingPage();
        actionwords.clickOnCurrencyDropDownAndSelectProperOption(currency);
        actionwords.getScreenshot("testcase015OpenChartingPageAndSelectCurrencyPairCurrency1_USDAndTimeRange1Min_2");
    }

    @Test
    @Ignore
    public void testcase015OpenChartingPageAndSelectCurrencyPairCurrency1_USDAndTimeRange1MinDataSet1() {
        testcase015OpenChartingPageAndSelectCurrencyPairCurrency1_USDAndTimeRange1Min(websitelink, CURRENCY_1);
    }

    public void testcase016OpenChartingPageAndSelectCurrencyPairCurrency1_USDAndTimeRange5Min(String link, String currency) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.clickOnChartingBtnOnHomePage();
        actionwords.getScreenshot("testcase016OpenChartingPageAndSelectCurrencyPairCurrency1_USDAndTimeRange5Min_1");
        actionwords.clickOnFiveMinBtnOnChartingPage();
        actionwords.clickOnCurrencyDropDownAndSelectProperOption(currency);
        actionwords.getScreenshot("testcase016OpenChartingPageAndSelectCurrencyPairCurrency1_USDAndTimeRange5Min_2");
    }

    @Test
    public void testcase016OpenChartingPageAndSelectCurrencyPairCurrency1_USDAndTimeRange5MinDataSet1() {
        testcase016OpenChartingPageAndSelectCurrencyPairCurrency1_USDAndTimeRange5Min(websitelink, CURRENCY_1);
    }

    public void testcase017OpenChartingPageAndSelectCurrencyPairCurrency1_USDAndTimeRange15Min(String link, String currency) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.clickOnChartingBtnOnHomePage();
        actionwords.getScreenshot("testcase017OpenChartingPageAndSelectCurrencyPairCurrency1_USDAndTimeRange15Min_1");
        actionwords.clickOnFifteenMinBtnOnChartingPage();
        actionwords.clickOnCurrencyDropDownAndSelectProperOption(currency);
        actionwords.getScreenshot("testcase017OpenChartingPageAndSelectCurrencyPairCurrency1_USDAndTimeRange15Min_2");
    }

    @Test
    public void testcase017OpenChartingPageAndSelectCurrencyPairCurrency1_USDAndTimeRange15MinDataSet1() {
        testcase017OpenChartingPageAndSelectCurrencyPairCurrency1_USDAndTimeRange15Min(websitelink, CURRENCY_1);
    }

    public void testcase018OpenChartingPageAndSelectCurrencyPairCurrency1_USDAndTimeRange60Min(String link, String currency) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.clickOnChartingBtnOnHomePage();
        actionwords.getScreenshot("testcase018OpenChartingPageAndSelectCurrencyPairCurrency1_USDAndTimeRange60Min_1");
        actionwords.clickOnSixtyMinBtnOnChartingPage();
        actionwords.clickOnCurrencyDropDownAndSelectProperOption(currency);
        actionwords.getScreenshot("testcase018OpenChartingPageAndSelectCurrencyPairCurrency1_USDAndTimeRange60Min_2");
    }

    @Test
    public void testcase018OpenChartingPageAndSelectCurrencyPairCurrency1_USDAndTimeRange60MinDataSet1() {
        testcase018OpenChartingPageAndSelectCurrencyPairCurrency1_USDAndTimeRange60Min(websitelink, CURRENCY_1);
    }

    public void testcase019OpenChartingPageAndSelectCurrencyPairCurrency1_USDAndTimeRange12Hours(String link, String currency) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.clickOnChartingBtnOnHomePage();
        actionwords.getScreenshot("testcase019OpenChartingPageAndSelectCurrencyPairCurrency1_USDAndTimeRange12Hours_1");
        actionwords.clickOnTwelveHoursBtnOnChartingPage();
        actionwords.clickOnCurrencyDropDownAndSelectProperOption(currency);
        actionwords.getScreenshot("testcase019OpenChartingPageAndSelectCurrencyPairCurrency1_USDAndTimeRange12Hours_2");
    }

    @Test
    public void testcase019OpenChartingPageAndSelectCurrencyPairCurrency1_USDAndTimeRange12HoursDataSet1() {
        testcase019OpenChartingPageAndSelectCurrencyPairCurrency1_USDAndTimeRange12Hours(websitelink, CURRENCY_1);
    }

    public void testcase020OpenChartingPageAndSelectCurrencyPairCurrency1_USDAndTimeRange1Day(String link, String currency) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.clickOnChartingBtnOnHomePage();
        actionwords.getScreenshot("testcase020OpenChartingPageAndSelectCurrencyPairCurrency1_USDAndTimeRange1Day_1");
        actionwords.clickOnOneDayBtnOnChartingPage();
        actionwords.clickOnCurrencyDropDownAndSelectProperOption(currency);
        actionwords.getScreenshot("testcase020OpenChartingPageAndSelectCurrencyPairCurrency1_USDAndTimeRange1Day_2");
    }

    @Test
    public void testcase020OpenChartingPageAndSelectCurrencyPairCurrency1_USDAndTimeRange1DayDataSet1() {
        testcase020OpenChartingPageAndSelectCurrencyPairCurrency1_USDAndTimeRange1Day(websitelink, CURRENCY_1);
    }
}
package prizma.maxiru.tests;

import app.*;
import prizma.maxiru.framework.common.*;

public class Actionwords {

    public void setFocusInBrowser() {
        PrizmaUtils.setFocusInBrowser();
    }

    public void openWebsite(String link) {
        PrizmaUtils.openWebsite(link);
    }

    public void refreshPage() {
        PrizmaUtils.refreshPage();
    }

    public void getScreenshot(String fileName) {
        PrizmaDriver.getScreenshot(fileName);
    }

    public void deleteTheDownloadFolder() {
        PrizmaUtils.deleteTheDownloadFolder();
    }

    public void createTheDownloadFolder() {
        PrizmaUtils.createTheDownloadFolder();
    }

    public void deleteTheLogsFolder() {
        PrizmaUtils.deleteTheLogsFolder();
    }

    public void createTheLogsFolder() {
        PrizmaUtils.createTheLogsFolder();
    }

    public void createTheLogsFileForFailedTests(String fileName) {
        PrizmaUtils.createTheLogsFileForFailedTests(fileName);
    }

    public void logFailedTestResultsToTheLogsFile(String errorMessage) {
        PrizmaUtils.logFailedTestResultsToTheLogsFile(errorMessage);
    }

    public void checkThatFolderIsNotEmptyAfterDownloadSomeFilesFromPrizmaSDK() {
        PrizmaUtils.checkThatFolderIsNotEmptyAfterDownloadSomeFilesFromPrizmaSDK();
    }

    public void clickOnBotsBtnOnHomePage() {
        PrizmaHomePage.clickOnBotsBtnOnHomePage();
    }

    public void clickOnChartingBtnOnHomePage() {
        PrizmaHomePage.clickOnChartingBtnOnHomePage();
    }

    public void clickOnBotRowContainerToOpenBotDetailPageByPositionX(int position) {
        PrizmaBotsLandingPage.clickOnBotRowContainerToOpenBotDetailPageByPositionX(position);
    }

    public void clickOnBotRowContainerToOpenBotDetailPageByPositionFromVariableValue() {
        PrizmaBotsLandingPage.clickOnBotRowContainerToOpenBotDetailPageByPositionFromVariableValue();
    }

    public void clickOnBotRowContainerToOpenBotDetailPageByBotName(String botName) {
        PrizmaBotsLandingPage.clickOnBotRowContainerToOpenBotDetailPageByBotName(botName);
    }

    public void clickOnBotRowContainerToOpenBotDetailPageByBotNameFromVariableValue() {
        PrizmaBotsLandingPage.clickOnBotRowContainerToOpenBotDetailPageByBotNameFromVariableValue();
    }

    public void verifyThatBotNameOnPositionIsCorrectOnBotsLandingPage(String botName, int position) {
        PrizmaBotsLandingPage.verifyThatBotNameOnPositionIsCorrect(botName, position);
    }

    public void verifyThatBotNameOnPositionFromVariableValueIsCorrectIfValidateWithVariableValue() {
        PrizmaBotsLandingPage.verifyThatBotNameOnPositionFromVariableValueIsCorrectIfValidateWithVariableValue();
    }

    public void verifyThatBotNameOnPositionFromVariableValueIsCorrect(String botName) {
        PrizmaBotsLandingPage.verifyThatBotNameOnPositionFromVariableValueIsCorrect(botName);
    }

    public void verifyThatBotLastRunOnPositionIsCorrectOnBotsLandingPage(String botRun, int position) {
        PrizmaBotsLandingPage.verifyThatBotLastRunOnPositionIsCorrect(botRun, position);
    }

    public void verifyThatBotLastRunOnPositionFromVariableValueIsCorrectIfValidateWithVariableValue() {
        PrizmaBotsLandingPage.verifyThatBotLastRunOnPositionFromVariableValueIsCorrectIfValidateWithVariableValue();
    }

    public void verifyThatBotAverangeProfitCurrencyOnPositionIsCorrectOnBotsLandingPage(String averangeProfitCurrency, int position) {
        PrizmaBotsLandingPage.verifyThatBotAverangeProfitCurrencyOnPositionIsCorrect(averangeProfitCurrency, position);
    }

    public void verifyThatBotAverangeProfitCurrencyOnPositionFromVariableValueIsCorrectIfValidateWithVariableValue() {
        PrizmaBotsLandingPage.verifyThatBotAverangeProfitCurrencyOnPositionFromVariableValueIsCorrectIfValidateWithVariableValue();
    }

    public void verifyThatBotAverangeProfitOnPositionFromVariableValueIsCorrectIfValidateWithVariableValueWhichIsZero() {
        PrizmaBotsLandingPage.verifyThatBotAverangeProfitOnPositionFromVariableValueIsCorrectIfValidateWithVariableValueWhichIsZero();
    }

    public void verifyThatBotAverangeProfitPercentageOnPositionIsCorrectOnBotsLandingPage(String averangeProfitPercentage, int position) {
        PrizmaBotsLandingPage.verifyThatBotAverangeProfitPercentageOnPositionIsCorrect(averangeProfitPercentage, position);
    }

    public void verifyThatBotAverangeProfitPercentageOnPositionFromVariableValueIsCorrectIfValidateWithVariableValue() {
        PrizmaBotsLandingPage.verifyThatBotAverangeProfitPercentageOnPositionFromVariableValueIsCorrectIfValidateWithVariableValue();
    }

    public void verifyThatBotProfitCurrencyOnPositionIsCorrectOnBotsLandingPage(String profitCurrency, int position) {
        PrizmaBotsLandingPage.verifyThatBotProfitCurrencyOnPositionIsCorrect(profitCurrency, position);
    }

    public void verifyThatBotProfitCurrencyOnPositionFromVariableValueIsCorrectIfValidateWithVariableValue() {
        PrizmaBotsLandingPage.verifyThatBotProfitCurrencyOnPositionFromVariableValueIsCorrectIfValidateWithVariableValue();
    }

    public void verifyThatBotProfitOnPositionFromVariableValueIsCorrectIfValidateWithVariableValueWhichIsZero() {
        PrizmaBotsLandingPage.verifyThatBotProfitOnPositionFromVariableValueIsCorrectIfValidateWithVariableValueWhichIsZero();
    }

    public void verifyThatBotProfitPercentageOnPositionIsCorrectOnBotsLandingPage(String profitPercentage, int position) {
        PrizmaBotsLandingPage.verifyThatBotProfitPercentageOnPositionIsCorrect(profitPercentage, position);
    }

    public void verifyThatBotProfitPercentageOnPositionFromVariableValueIsCorrectIfValidateWithVariableValue() {
        PrizmaBotsLandingPage.verifyThatBotProfitPercentageOnPositionFromVariableValueIsCorrectIfValidateWithVariableValue();
    }

    public void verifyThatBotNameOnPositionIsCorrectToValidateWithVariableValueOnBotsLandingPage(int position) {
        PrizmaBotsLandingPage.verifyThatBotNameOnPositionIsCorrectToValidateWithVariableValue(position);
    }

    public void verifyThatBotLastRunOnPositionIsCorrectToValidateWithVariableValueOnBotsLandingPage(int position) {
        PrizmaBotsLandingPage.verifyThatBotLastRunOnPositionIsCorrectToValidateWithVariableValue(position);
    }

    public void verifyThatBotAverangeProfitCurrencyOnPositionIsCorrectToValidateWithVariableValueOnBotsLandingPage(int position) {
        PrizmaBotsLandingPage.verifyThatBotAverangeProfitCurrencyOnPositionIsCorrectToValidateWithVariableValue(position);
    }

    public void verifyThatBotAverangeProfitPercentageOnPositionIsCorrectToValidateWithVariableValueOnBotsLandingPage(int position) {
        PrizmaBotsLandingPage.verifyThatBotAverangeProfitPercentageOnPositionIsCorrectToValidateWithVariableValue(position);
    }

    public void verifyThatBotProfitCurrencyOnPositionIsCorrectToValidateWithVariableValueOnBotsLandingPage(int position) {
        PrizmaBotsLandingPage.verifyThatBotProfitCurrencyOnPositionIsCorrectToValidateWithVariableValue(position);
    }

    public void verifyThatBotProfitPercentageOnPositionIsCorrectToValidateWithVariableValueOnBotsLandingPage(int position) {
        PrizmaBotsLandingPage.verifyThatBotProfitPercentageOnPositionIsCorrectToValidateWithVariableValue(position);
    }

    public void clickOnBotRunBtnOnBotDetailsPage() {
        PrizmaRunBotPage.clickOnBotRunBtnOnBotDetailsPage();
    }

    public void clickOnBotRunBtnOnBotRunDialog() {
        PrizmaRunBotPage.clickOnBotRunBtnOnBotRunPage();
    }

    public void clickOnBotRunBtnOnBotRunDialogWithoutWaitDriver() {
        PrizmaRunBotPage.clickOnBotRunBtnOnBotRunPageWithoutWaitDriver();
    }

    public void clickOnBotRunBtnOnBotRunPageWithoutAnyWaitDriver() {
        PrizmaRunBotPage.clickOnBotRunBtnOnBotRunPageWithoutAnyWaitDriver();
    }

    public void clickOnCloseBtnOnBotRunDialog() {
        PrizmaRunBotPage.clickOnCloseBtnOnBotRunPage();
    }

    public void clickOnStopBtnOnBotRunPage() {
        PrizmaRunBotPage.clickOnStopBtnOnBotRunPage();
    }

    public void setDataSourceOnBotRunDialog(String dataSource) {
        PrizmaRunBotPage.setDataSourceOnBotRunPage(dataSource);
    }

    public void setStartBalanceOnBotRunDialog(String startBalance) {
        PrizmaRunBotPage.setStartBalanceOnBotRunPage(startBalance);
    }

    public void setRunFromMonthOnBotRunPage(String runFromMonth) {
        PrizmaRunBotPage.setRunFromMonthOnBotRunPage(runFromMonth);
    }

    public void setRunFromDayOnBotRunPage(String runFromDay) {
        PrizmaRunBotPage.setRunFromDayOnBotRunPage(runFromDay);
    }

    public void setRunFromYearOnBotRunPage(String runFromMonth, String runFromDay, String runFromYear) {
        PrizmaRunBotPage.setRunFromYearOnBotRunPage(runFromMonth, runFromDay, runFromYear);
    }

    public void setRunFromOnBotRunPage(String runFrom) {
        PrizmaRunBotPage.setRunFromOnBotRunPage(runFrom);
    }

    public void setRunToMonthOnBotRunPage(String runToMonth) {
        PrizmaRunBotPage.setRunToMonthOnBotRunPage(runToMonth);
    }

    public void setRunToDayOnBotRunPage(String runToDay) {
        PrizmaRunBotPage.setRunToDayOnBotRunPage(runToDay);
    }

    public void setRunToYearOnBotRunPage(String runToMonth, String runToDay, String runToYear) {
        PrizmaRunBotPage.setRunToYearOnBotRunPage(runToMonth, runToDay, runToYear);
    }

    public void setRunToOnBotRunPage(String runTo) {
        PrizmaRunBotPage.setRunToOnBotRunPage(runTo);
    }

    public void verifyThatOnRunBotFormErrorMessageAppear(String errorMessage) {
        PrizmaRunBotPage.verifyThatOnRunBotFormErrorMessageAppear(errorMessage);
    }

    public void verifyThatOnRunBotFormErrorMessageNotAppear(String errorMessage) {
        PrizmaRunBotPage.verifyThatOnRunBotFormErrorMessageNotAppear(errorMessage);
    }

    public void setTradingFeeOnBotRunDialog(String tradingFee) {
        PrizmaRunBotPage.setTradingFeeOnBotRunPage(tradingFee);
    }

    public void setTradeLimitOnBotRunDialog(String tradeLimit) {
        PrizmaRunBotPage.setTradeLimitOnBotRunPage(tradeLimit);
    }

    public void verifyThatRunBotDialogCloseAfterClickOnCloseBtn() {
        PrizmaRunBotPage.verifyThatRunBotDialogCloseAfterClickOnCloseBtn();
    }

    public void verifyThatRunBotDialogCloseWhenUserTriedToRunBotWithCorrectDataBot() {
        PrizmaRunBotPage.verifyThatRunBotDialogCloseWhenUserTriedToRunBotWithCorrectDataBot();
    }

    public void verifyThatRunBotDialogNotCloseWhenUserTriedToRunBotWithIncorrectDataBot() {
        PrizmaRunBotPage.verifyThatRunBotDialogNotCloseWhenUserTriedToRunBotWithIncorrectDataBot();
    }

    public void verifyThatBotLastRunIsCorrectOnBotSummaryTable(String botRun) {
        PrizmaBotsDetailsPage.verifyThatBotLastRunIsCorrectOnBotSummaryTable(botRun);
    }

    public void verifyThatBotLastRunIsCorrectOnBotSummaryTableIfValidateWithVariableValue() {
        PrizmaBotsDetailsPage.verifyThatBotLastRunIsCorrectOnBotSummaryTableIfValidateWithVariableValue();
    }

    public void verifyThatBotFromToIsCorrectOnBotSummaryTableStartDate(String date) {
        PrizmaBotsDetailsPage.verifyThatBotFromToIsCorrectOnBotSummaryTableStartDate(date);
    }

    public void verifyThatBotFromToIsCorrectOnBotSummaryTableStartDateIfValidateWithVariableValue() {
        PrizmaBotsDetailsPage.verifyThatBotFromToIsCorrectOnBotSummaryTableStartDateIfValidateWithVariableValue();
    }

    public void verifyThatBotFromToIsCorrectOnBotSummaryTableEndDate(String date) {
        PrizmaBotsDetailsPage.verifyThatBotFromToIsCorrectOnBotSummaryTableEndDate(date);
    }

    public void verifyThatBotFromToIsCorrectOnBotSummaryTableEndDateIfValidateWithVariableValue() {
        PrizmaBotsDetailsPage.verifyThatBotFromToIsCorrectOnBotSummaryTableEndDateIfValidateWithVariableValue();
    }

    public void verifyThatBotClosedPositionsIsCorrectOnBotSummaryTable(String count) {
        PrizmaBotsDetailsPage.verifyThatBotClosedPositionsIsCorrectOnBotSummaryTable(count);
    }

    public void verifyThatBotClosedPositionsIsCorrectOnBotSummaryTableIfValidateWithVariableValue() {
        PrizmaBotsDetailsPage.verifyThatBotClosedPositionsIsCorrectOnBotSummaryTableIfValidateWithVariableValue();
    }

    public void verifyThatBotInitialBalanceIsCorrectOnBotSummaryTable(String initialBalance) {
        PrizmaBotsDetailsPage.verifyThatBotInitialBalanceIsCorrectOnBotSummaryTable(initialBalance);
    }

    public void verifyThatBotInitialBalanceIsCorrectOnBotSummaryTableIfValidateWithVariableValue() {
        PrizmaBotsDetailsPage.verifyThatBotInitialBalanceIsCorrectOnBotSummaryTableIfValidateWithVariableValue();
    }

    public void verifyThatBotAvailableBalanceIsCorrectOnBotSummaryTable(String availableBalance) {
        PrizmaBotsDetailsPage.verifyThatBotAvailableBalanceIsCorrectOnBotSummaryTable(availableBalance);
    }

    public void verifyThatBotInOpenPositionsBalanceIsCorrectOnBotSummaryTable(String inOpenPositionsBalance) {
        PrizmaBotsDetailsPage.verifyThatBotInOpenPositionsBalanceIsCorrectOnBotSummaryTable(inOpenPositionsBalance);
    }

    public void verifyThatBotInOpenPositionsBalanceIsCorrectOnBotSummaryTableWithIfValidateVariableValue() {
        PrizmaBotsDetailsPage.verifyThatBotInOpenPositionsBalanceIsCorrectOnBotSummaryTableIfValidateWithVariableValue();
    }

    public void verifyThatBotTotalBalanceIsCorrectOnBotSummaryTable(String endingBalance) {
        PrizmaBotsDetailsPage.verifyThatBotTotalBalanceIsCorrectOnBotSummaryTable(endingBalance);
    }

    public void verifyThatBotTotalBalanceIsCorrectOnBotSummaryTableIfValidateWithVariableValue() {
        PrizmaBotsDetailsPage.verifyThatBotTotalBalanceIsCorrectOnBotSummaryTableIfValidateWithVariableValue();
    }

    public void verifyThatBotAverangeProfitCurrencyIsCorrectOnBotSummaryTable(String averangeProfitCurrency) {
        PrizmaBotsDetailsPage.verifyThatBotAverangeProfitCurrencyIsCorrectOnBotSummaryTable(averangeProfitCurrency);
    }

    public void verifyThatBotAverangeProfitIsCorrectOnBotSummaryTableIfValidateWithVariableValueWhichIsZero() {
        PrizmaBotsDetailsPage.verifyThatBotAverangeProfitIsCorrectOnBotSummaryTableIfValidateWithVariableValueWhichIsZero();
    }

    public void verifyThatBotAverangeProfitCurrencyIsCorrectOnBotSummaryTableIfValidateWithVariableValue() {
        PrizmaBotsDetailsPage.verifyThatBotAverangeProfitCurrencyIsCorrectOnBotSummaryTableIfValidateWithVariableValue();
    }

    public void verifyThatBotAverangeProfitPercentageIsCorrectOnBotSummaryTable(String averangeProfitPercentage) {
        PrizmaBotsDetailsPage.verifyThatBotAverangeProfitPercentageIsCorrectOnBotSummaryTable(averangeProfitPercentage);
    }

    public void verifyThatBotAverangeProfitPercentageIsCorrectOnBotSummaryTableIfValidateWithVariableValue() {
        PrizmaBotsDetailsPage.verifyThatBotAverangeProfitPercentageIsCorrectOnBotSummaryTableIfValidateWithVariableValue();
    }

    public void verifyThatBotTotalProfitCurrencyIsCorrectOnBotSummaryTable(String totalProfitCurrency) {
        PrizmaBotsDetailsPage.verifyThatBotTotalProfitCurrencyIsCorrectOnBotSummaryTable(totalProfitCurrency);
    }

    public void verifyThatBotTotalProfitIsCorrectOnBotSummaryTableIfValidateWithVariableValueWhichIsZero() {
        PrizmaBotsDetailsPage.verifyThatBotTotalProfitIsCorrectOnBotSummaryTableIfValidateWithVariableValueWhichIsZero();
    }

    public void verifyThatBotTotalProfitCurrencyIsCorrectOnBotSummaryTableIfValidateWithVariableValue() {
        PrizmaBotsDetailsPage.verifyThatBotTotalProfitCurrencyIsCorrectOnBotSummaryTableIfValidateWithVariableValue();
    }

    public void verifyThatBotTotalProfitPercentageIsCorrectOnBotSummaryTable(String totalProfitPercentage) {
        PrizmaBotsDetailsPage.verifyThatBotTotalProfitPercentageIsCorrectOnBotSummaryTable(totalProfitPercentage);
    }

    public void verifyThatBotTotalProfitPercentageIsCorrectOnBotSummaryTableIfValidateWithVariableValue() {
        PrizmaBotsDetailsPage.verifyThatBotTotalProfitPercentageIsCorrectOnBotSummaryTableIfValidateWithVariableValue();
    }

    public void verifyThatBotEarnedIsCorrectOnBotSummaryTable(String count) {
        PrizmaBotsDetailsPage.verifyThatBotEarnedIsCorrectOnBotSummaryTable(count);
    }

    public void verifyThatBotEarnedIsCorrectOnBotSummaryTableIfValidateWithVariableValue() {
        PrizmaBotsDetailsPage.verifyThatBotEarnedIsCorrectOnBotSummaryTableIfValidateWithVariableValue();
    }

    public void verifyThatBotEarnedIsCorrectOnBotSummaryTableIfValidateWithVariableValueWhichIsZero() {
        PrizmaBotsDetailsPage.verifyThatBotEarnedIsCorrectOnBotSummaryTableIfValidateWithVariableValueWhichIsZero();
    }

    public void clickOnOneMinBtnOnChartingPage() {
        PrizmaChartingPage.clickOnOneMinBtnOnChartingPage();
    }

    public void clickOnFiveMinBtnOnChartingPage() {
        PrizmaChartingPage.clickOnFiveMinBtnOnChartingPage();
    }

    public void clickOnFifteenMinBtnOnChartingPage() {
        PrizmaChartingPage.clickOnFifteenMinBtnOnChartingPage();
    }

    public void clickOnSixtyMinBtnOnChartingPage() {
        PrizmaChartingPage.clickOnSixtyMinBtnOnChartingPage();
    }

    public void clickOnTwelveHoursBtnOnChartingPage() {
        PrizmaChartingPage.clickOnTwelveHoursBtnOnChartingPage();
    }

    public void clickOnOneDayBtnOnChartingPage() {
        PrizmaChartingPage.clickOnOneDayBtnOnChartingPage();
    }

    public void clickOnCurrencyDropDownAndSelectProperOption(String currency) {
        PrizmaChartingPage.clickOnCurrencyDropDownAndSelectProperOption(currency);
    }

    public void verifyThatStartDateInPositionsTableRowByPositionXIsCorrect(String date, int position) {
        PrizmaBotsDetailsPage.verifyThatStartDateInPositionsTableRowByPositionXIsCorrect(date, position);
    }

    public void verifyThatStartDateInPositionsTableRowByPositionFromVariableValueIsCorrect(String date) {
        PrizmaBotsDetailsPage.verifyThatStartDateInPositionsTableRowByPositionFromVariableValueIsCorrect(date);
    }

    public void verifyThatEndDateInPositionsTableRowByPositionXIsCorrect(String date, int position) {
        PrizmaBotsDetailsPage.verifyThatEndDateInPositionsTableRowByPositionXIsCorrect(date, position);
    }

    public void verifyThatEndDateInPositionsTableRowByPositionFromVariableValueIsCorrect(String date) {
        PrizmaBotsDetailsPage.verifyThatEndDateInPositionsTableRowByPositionFromVariableValueIsCorrect(date);
    }

    public void verifyThatCurrencyPairInPositionsTableRowByPositionXIsCorrect(String currencyPair, int position) {
        PrizmaBotsDetailsPage.verifyThatCurrencyPairInPositionsTableRowByPositionXIsCorrect(currencyPair, position);
    }

    public void verifyThatCurrencyPairInPositionsTableRowByPositionFromVariableValueIsCorrect(String currencyPair) {
        PrizmaBotsDetailsPage.verifyThatCurrencyPairInPositionsTableRowByPositionFromVariableValueIsCorrect(currencyPair);
    }

    public void verifyThatTypeInPositionsTableRowByPositionXIsCorrect(String type, int position) {
        PrizmaBotsDetailsPage.verifyThatTypeInPositionsTableRowByPositionXIsCorrect(type, position);
    }

    public void verifyThatTypeInPositionsTableRowByPositionFromVariableValueIsCorrect(String type) {
        PrizmaBotsDetailsPage.verifyThatTypeInPositionsTableRowByPositionFromVariableValueIsCorrect(type);
    }

    public void verifyThatSizeInPositionsTableRowByPositionXIsCorrect(String size, int position) {
        PrizmaBotsDetailsPage.verifyThatSizeInPositionsTableRowByPositionXIsCorrect(size, position);
    }

    public void verifyThatSizeInPositionsTableRowByPositionFromVariableValueIsCorrect(String size) {
        PrizmaBotsDetailsPage.verifyThatSizeInPositionsTableRowByPositionFromVariableValueIsCorrect(size);
    }

    public void verifyThatEntryPriceInPositionsTableRowByPositionXIsCorrect(String entryPrice, int position) {
        PrizmaBotsDetailsPage.verifyThatEntryPriceInPositionsTableRowByPositionXIsCorrect(entryPrice, position);
    }

    public void verifyThatEntryPriceInPositionsTableRowByPositionFromVariableValueIsCorrect(String entryPrice) {
        PrizmaBotsDetailsPage.verifyThatEntryPriceInPositionsTableRowByPositionFromVariableValueIsCorrect(entryPrice);
    }

    public void verifyThatExitPriceInPositionsTableRowByPositionXIsCorrect(String exitPrice, int position) {
        PrizmaBotsDetailsPage.verifyThatExitPriceInPositionsTableRowByPositionXIsCorrect(exitPrice, position);
    }

    public void verifyThatExitPriceInPositionsTableRowByPositionFromVariableValueIsCorrect(String exitPrice) {
        PrizmaBotsDetailsPage.verifyThatExitPriceInPositionsTableRowByPositionFromVariableValueIsCorrect(exitPrice);
    }

    public void verifyThatEntryStartValueInPositionsTableRowByPositionXIsCorrect(double entryStartValue, int position) {
        PrizmaBotsDetailsPage.verifyThatEntryStartValueInPositionsTableRowByPositionXIsCorrect(entryStartValue, position);
    }

    public void verifyThatEntryStartValueInPositionsTableRowByPositionFromVariableValueIsCorrect(String entryStartValue) {
        PrizmaBotsDetailsPage.verifyThatEntryStartValueInPositionsTableRowByPositionFromVariableValueIsCorrect(entryStartValue);
    }

    public void verifyThatEntryCommissionValueInPositionsTableRowByPositionXIsCorrect(double entryCommissionValue, int position) {
        PrizmaBotsDetailsPage.verifyThatEntryCommissionValueInPositionsTableRowByPositionXIsCorrect(entryCommissionValue, position);
    }

    public void verifyThatEntryCommissionValueInPositionsTableRowByPositionFromVariableValueIsCorrect(String entryCommissionValue) {
        PrizmaBotsDetailsPage.verifyThatEntryCommissionValueInPositionsTableRowByPositionFromVariableValueIsCorrect(entryCommissionValue);
    }

    public void verifyThatEntryEndValueInPositionsTableRowByPositionXIsCorrect(double entryEndValue, int position) {
        PrizmaBotsDetailsPage.verifyThatEntryEndValueInPositionsTableRowByPositionXIsCorrect(entryEndValue, position);
    }

    public void verifyThatEntryEndValueInPositionsTableRowByPositionFromVariableValueIsCorrect(String entryEndValue) {
        PrizmaBotsDetailsPage.verifyThatEntryEndValueInPositionsTableRowByPositionFromVariableValueIsCorrect(entryEndValue);
    }

    public void verifyThatExitStartValueInPositionsTableRowByPositionXIsCorrect(double exitStartValue, int position) {
        PrizmaBotsDetailsPage.verifyThatExitStartValueInPositionsTableRowByPositionXIsCorrect(exitStartValue, position);
    }

    public void verifyThatExitStartValueInPositionsTableRowByPositionFromVariableValueIsCorrect(String exitStartValue) {
        PrizmaBotsDetailsPage.verifyThatExitStartValueInPositionsTableRowByPositionFromVariableValueIsCorrect(exitStartValue);
    }

    public void verifyThatExitCommissionValueInPositionsTableRowByPositionXIsCorrect(double exitCommissionValue, int position) {
        PrizmaBotsDetailsPage.verifyThatExitCommissionValueInPositionsTableRowByPositionXIsCorrect(exitCommissionValue, position);
    }

    public void verifyThatExitCommissionValueInPositionsTableRowByPositionFromVariableValueIsCorrect(String exitCommissionValue) {
        PrizmaBotsDetailsPage.verifyThatExitCommissionValueInPositionsTableRowByPositionFromVariableValueIsCorrect(exitCommissionValue);
    }

    public void verifyThatExitEndValueInPositionsTableRowByPositionXIsCorrect(double exitEndValue, int position) {
        PrizmaBotsDetailsPage.verifyThatExitEndValueInPositionsTableRowByPositionXIsCorrect(exitEndValue, position);
    }

    public void verifyThatExitEndValueInPositionsTableRowByPositionFromVariableValueIsCorrect(String exitEndValue) {
        PrizmaBotsDetailsPage.verifyThatExitEndValueInPositionsTableRowByPositionFromVariableValueIsCorrect(exitEndValue);
    }

    public void verifyThatProfitCurrencyInPositionsTableRowByPositionXIsCorrect(double profitCurrency, int position) {
        PrizmaBotsDetailsPage.verifyThatProfitCurrencyInPositionsTableRowByPositionXIsCorrect(profitCurrency, position);
    }

    public void verifyThatProfitCurrencyInPositionsTableRowByPositionFromVariableValueIsCorrect(String profitCurrency) {
        PrizmaBotsDetailsPage.verifyThatProfitCurrencyInPositionsTableRowByPositionFromVariableValueIsCorrect(profitCurrency);
    }

    public void verifyThatProfitPercentageInPositionsTableRowByPositionXIsCorrect(double profitPercentage, int position) {
        PrizmaBotsDetailsPage.verifyThatProfitPercentageInPositionsTableRowByPositionXIsCorrect(profitPercentage, position);
    }

    public void verifyThatProfitPercentageInPositionsTableRowByPositionFromVariableValueIsCorrect(String profitPercentage) {
        PrizmaBotsDetailsPage.verifyThatProfitPercentageInPositionsTableRowByPositionFromVariableValueIsCorrect(profitPercentage);
    }

    public void clickOnViewOnChartingButtonInPositionsTableRowByPositionX(int position) {
        PrizmaBotsDetailsPage.clickOnViewOnChartingButtonInPositionsTableRowByPositionX(position);
    }

    public void clickOnViewOnViewOnChartingButtonInPositionsTableRowByPositionFromVariableValue() {
        PrizmaBotsDetailsPage.clickOnViewOnViewOnChartingButtonInPositionsTableRowByPositionFromVariableValue();
    }

    public void getAndStoreTotalCountOfAllTrendingPositions() {
        PrizmaBotsDetailsPage.getAndStoreTotalCountOfAllTrendingPositions();
    }

    public void getAndStoreTotalCountOfOpenAndCloseTrendingPositions() {
        PrizmaBotsDetailsPage.getAndStoreTotalCountOfOpenAndCloseTrendingPositions();
    }

    public void getAndStoreInOpenTrendingPositionsValue() {
        PrizmaBotsDetailsPage.getAndStoreInOpenTrendingPositionsValue();
    }

    public void getAndCalculateAllProfitCurrencyValues() {
        PrizmaBotsDetailsPage.getAndCalculateAllProfitCurrencyValues();
    }

    public void getAndCalculateAllProfitCurrencyValuesForClosedPositions() {
        PrizmaBotsDetailsPage.getAndCalculateAllProfitCurrencyValuesForClosedPositions();
    }

    public void getAndCalculateAllProfitCurrencyValuesForOpenPositions() {
        PrizmaBotsDetailsPage.getAndCalculateAllProfitCurrencyValuesForOpenPositions();
    }

    public void getAndCalculateAllProfitPercentageValues() {
        PrizmaBotsDetailsPage.getAndCalculateAllProfitPercentageValues();
    }

    public void getAndCalculateAllProfitPercentageValuesForClosedPositions() {
        PrizmaBotsDetailsPage.getAndCalculateAllProfitPercentageValuesForClosedPositions();
    }

    public void getAndCalculateAllProfitPercentageValuesForOpenPositions() {
        PrizmaBotsDetailsPage.getAndCalculateAllProfitPercentageValuesForOpenPositions();
    }

    public void getAndCalculateTotalProfitPercentageValues() {
        PrizmaBotsDetailsPage.getAndCalculateTotalProfitPercentageValues();
    }

    public void getAndCalculateTotalBalance() {
        PrizmaBotsDetailsPage.getAndCalculateTotalBalance();
    }

    public void getAndCalculateAvailableBalance() {
        PrizmaBotsDetailsPage.getAndCalculateAvailableBalance();
    }

    public void getAndCalculateAverageProfitCurrency() {
        PrizmaBotsDetailsPage.getAndCalculateAverageProfitCurrency();
    }

    public void getAndCalculateAverageProfitPercentage() {
        PrizmaBotsDetailsPage.getAndCalculateAverageProfitPercentage();
    }

    public void getAndCalculateBotEarnedValues() {
        PrizmaBotsDetailsPage.getAndCalculateBotEarnedValues();
    }

    public void getAndCalculateAndValidateAllValuesInPositionTable(String fileName) {
        PrizmaBotsDetailsPage.getAndCalculateAndValidateAllValuesInPositionTable(fileName);
    }

    public void getAndCalculateAndValidateAllValuesInPositionTableTalibBot(String fileName) {
        PrizmaBotsDetailsPage.getAndCalculateAndValidateAllValuesInPositionTableTalibBot(fileName);
    }

    public void getAndCalculateAndValidateAllValuesInPositionTableWhichShouldBeEmpty(String fileName) {
        PrizmaBotsDetailsPage.getAndCalculateAndValidateAllValuesInPositionTableWhichShouldBeEmpty(fileName);
    }

    public void getAndCalculateAndValidateAllValuesInPositionTableWhichShouldBeNotEmpty(String fileName) {
        PrizmaBotsDetailsPage.getAndCalculateAndValidateAllValuesInPositionTableWhichShouldBeNotEmpty(fileName);
    }

    public void clickOnPositionsTableDateSortingToggle() {
        PrizmaBotsDetailsPage.clickOnPositionsTableDateSortingToggle();
    }

    public void clickOnPositionsTablePairSortingToggle() {
        PrizmaBotsDetailsPage.clickOnPositionsTablePairSortingToggle();
    }

    public void clickOnPositionsTableTypeSortingToggle() {
        PrizmaBotsDetailsPage.clickOnPositionsTableTypeSortingToggle();
    }

    public void clickOnPositionsTableProfitSortingToggle() {
        PrizmaBotsDetailsPage.clickOnPositionsTableProfitSortingToggle();
    }

    public void clickOnIndicatorsBtn() {
        PrizmaChartingPage.clickOnIndicatorsBtn();
    }

    public void clickOnCloseIndicatorsBtn() {
        PrizmaChartingPage.clickOnCloseIndicatorsBtn();
    }

    public void verifyThatIndicatorsDialogCloseAfterClickOnCloseBtn() {
        PrizmaChartingPage.verifyThatIndicatorsDialogCloseAfterClickOnCloseBtn();
    }

    public void enableVolumeCheckbox() {
        PrizmaChartingPage.enableVolumeCheckbox();
    }

    public void disableVolumeCheckbox() {
        PrizmaChartingPage.disableVolumeCheckbox();
    }

    public void enableBollingerBandsCheckbox() {
        PrizmaChartingPage.enableBollingerBandsCheckbox();
    }

    public void disableBollingerBandsCheckbox() {
        PrizmaChartingPage.disableBollingerBandsCheckbox();
    }

    public void enableRelativeStrengthIndexCheckbox() {
        PrizmaChartingPage.enableRelativeStrengthIndexCheckbox();
    }

    public void disableRelativeStrengthIndexCheckbox() {
        PrizmaChartingPage.disableRelativeStrengthIndexCheckbox();
    }

    public void disableAllChartingIndicatorsBeforeTests() {
        PrizmaChartingPage.disableAllChartingIndicatorsBeforeTests();
    }

    public void enableAllChartingIndicators() {
        PrizmaChartingPage.enableAllChartingIndicators();
    }

    public void verifyThatVolumeCheckboxEnabled() {
        PrizmaChartingPage.verifyThatVolumeCheckboxEnabled();
    }

    public void verifyThatVolumeCheckboxDisabled() {
        PrizmaChartingPage.verifyThatVolumeCheckboxDisabled();
    }

    public void verifyThatBollingerBandsEnabled() {
        PrizmaChartingPage.verifyThatBollingerBandsEnabled();
    }

    public void verifyThatBollingerBandsDisabled() {
        PrizmaChartingPage.verifyThatBollingerBandsDisabled();
    }

    public void verifyThaRelativeStrengthIndexCheckboxEnabled() {
        PrizmaChartingPage.verifyThaRelativeStrengthIndexCheckboxEnabled();
    }

    public void verifyThatRelativeStrengthIndexCheckboxDisabled() {
        PrizmaChartingPage.verifyThatRelativeStrengthIndexCheckboxDisabled();
    }

    public void clickOnVolumeLabel() {
        PrizmaChartingPage.clickOnVolumeLabel();
    }

    public void clickOnBollingerBandsLabel() {
        PrizmaChartingPage.clickOnBollingerBandsLabel();
    }

    public void clickOnRelativeStrengthIndexLabel() {
        PrizmaChartingPage.clickOnRelativeStrengthIndexLabel();
    }

    public void setLength(String length) {
        PrizmaChartingPage.setLength(length);
    }

    public void setScalar(String scalar) {
        PrizmaChartingPage.setScalar(scalar);
    }

    public void setStandartDeviation(String standartDeviation) {
        PrizmaChartingPage.setStandartDeviation(standartDeviation);
    }

    public void setDifferencePeriod(String differencePeriod) {
        PrizmaChartingPage.setDifferencePeriod(differencePeriod);
    }

    public void setDegreesOfFreedom(String degreesOfFreedom) {
        PrizmaChartingPage.setDegreesOfFreedom(degreesOfFreedom);
    }

    public void setOffset(String offset) {
        PrizmaChartingPage.setOffset(offset);
    }

    public void clickOnIndicatorsResetBtn() {
        PrizmaChartingPage.clickOnIndicatorsResetBtn();
    }

    public void clickOnIndicatorsApplyBtn() {
        PrizmaChartingPage.clickOnIndicatorsApplyBtn();
    }

    public void verifyThatIndicatorNoSettingsMessageAppear(String message) {
        PrizmaChartingPage.verifyThatIndicatorNoSettingsMessageAppear(message);
    }

    public void verifyThatIndicatorNoSettingsMessageNotAppear(String message) {
        PrizmaChartingPage.verifyThatIndicatorNoSettingsMessageNotAppear(message);
    }

    public void verifyThatIndicatorsApplyBtnEnabled() {
        PrizmaChartingPage.verifyThatIndicatorsApplyBtnEnabled();
    }

    public void verifyThatIndicatorsApplyBtnDisabled() {
        PrizmaChartingPage.verifyThatIndicatorsApplyBtnDisabled();
    }

    public void verifyThatIndicatorsResetBtnEnabled() {
        PrizmaChartingPage.verifyThatIndicatorsResetBtnEnabled();
    }

    public void verifyThatIndicatorsResetBtnDisabled() {
        PrizmaChartingPage.verifyThatIndicatorsResetBtnDisabled();
    }

    public void saveForFutureTestsValuesFromAllBollingerBandsIndicatorConfigurationsFields() {
        PrizmaChartingPage.saveForFutureTestsValuesFromAllBollingerBandsIndicatorConfigurationsFields();
    }

    public void verifyThatAllBollingerBandsIndicatorConfigurationsFieldsRestoreBackAfterClickOnResetBtn() {
        PrizmaChartingPage.verifyThatAllBollingerBandsIndicatorConfigurationsFieldsRestoreBackAfterClickOnResetBtn();
    }

    public void saveForFutureTestsValuesFromAllRelativeStrengthIndexIndicatorConfigurationsFields() {
        PrizmaChartingPage.saveForFutureTestsValuesFromAllRelativeStrengthIndexIndicatorConfigurationsFields();
    }

    public void verifyThatAllRelativeStrengthIndexIndicatorConfigurationsFieldsRestoreBackAfterClickOnResetBtn() {
        PrizmaChartingPage.verifyThatAllRelativeStrengthIndexIndicatorConfigurationsFieldsRestoreBackAfterClickOnResetBtn();
    }
}
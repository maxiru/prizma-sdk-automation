package prizma.maxiru.tests;

import org.junit.FixMethodOrder;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import prizma.maxiru.framework.common.*;

import static prizma.maxiru.framework.common.PrizmaDriver.*;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)

public class WebSDKRunAllBotsAndValidateBotStopTests {
    @Rule
    public PrizmaScreenshotRule screenshotRule = new PrizmaScreenshotRule();

    public Actionwords actionwords = new Actionwords();

    public void testcase001RedirectToBotDetailsPageAndRunBotWithLongPositionTrendingDialogWithValidValuesAndValidateBotStopAbility(String link, String startBalance, String runFrom, String runTo, String tradingFee, String tradeLimit) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.clickOnBotsBtnOnHomePage();
        actionwords.getScreenshot("testcase001RedirectToBotDetailsPageAndRunBotWithLongPositionTrendingDialogWithValidValuesAndValidateBotStopAbility_1");
        actionwords.clickOnBotRowContainerToOpenBotDetailPageByBotNameFromVariableValue();
        actionwords.clickOnBotRunBtnOnBotDetailsPage();
        actionwords.getScreenshot("testcase001RedirectToBotDetailsPageAndRunBotWithLongPositionTrendingDialogWithValidValuesAndValidateBotStopAbility_2");
        actionwords.setStartBalanceOnBotRunDialog(startBalance);
        actionwords.setRunFromOnBotRunPage(runFrom);
        actionwords.setRunToOnBotRunPage(runTo);
        actionwords.setTradingFeeOnBotRunDialog(tradingFee);
        actionwords.setTradeLimitOnBotRunDialog(tradeLimit);
        actionwords.getScreenshot("testcase001RedirectToBotDetailsPageAndRunBotWithLongPositionTrendingDialogWithValidValuesAndValidateBotStopAbility_3");
        actionwords.clickOnBotRunBtnOnBotRunPageWithoutAnyWaitDriver();
        actionwords.clickOnStopBtnOnBotRunPage();
        actionwords.getScreenshot("testcase001RedirectToBotDetailsPageAndRunBotWithLongPositionTrendingDialogWithValidValuesAndValidateBotStopAbility_4");
        actionwords.verifyThatRunBotDialogCloseWhenUserTriedToRunBotWithCorrectDataBot();
        actionwords.getScreenshot("testcase001RedirectToBotDetailsPageAndRunBotWithLongPositionTrendingDialogWithValidValuesAndValidateBotStopAbility_5");
        actionwords.refreshPage();
        actionwords.openWebsite(link);
        actionwords.clickOnBotsBtnOnHomePage();
        actionwords.getScreenshot("testcase001RedirectToBotDetailsPageAndRunBotWithLongPositionTrendingDialogWithValidValuesAndValidateBotStopAbility_6");
        actionwords.clickOnBotRowContainerToOpenBotDetailPageByBotNameFromVariableValue();
        actionwords.getScreenshot("testcase001RedirectToBotDetailsPageAndRunBotWithLongPositionTrendingDialogWithValidValuesAndValidateBotStopAbility_7");
    }

    @Test
    public void testcase001RedirectToBotDetailsPageAndRunBotWithLongPositionTrendingDialogWithValidValuesAndValidateBotStopAbilityDataSet1() {
        testcase001RedirectToBotDetailsPageAndRunBotWithLongPositionTrendingDialogWithValidValuesAndValidateBotStopAbility(websitelink, "10000", "10/01/2021", "10/10/2021", "0.5", "10");
    }
}
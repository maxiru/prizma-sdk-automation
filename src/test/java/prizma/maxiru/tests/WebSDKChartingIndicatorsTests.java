package prizma.maxiru.tests;

import org.junit.FixMethodOrder;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import prizma.maxiru.framework.common.*;

import static prizma.maxiru.framework.common.PrizmaDriver.*;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)

public class WebSDKChartingIndicatorsTests {
    @Rule
    public PrizmaScreenshotRule screenshotRule = new PrizmaScreenshotRule();

    public Actionwords actionwords = new Actionwords();

    public void testcase001OpenChartingPageAndDisableAllChartingIndicatorsBeforeTests(String link) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.clickOnChartingBtnOnHomePage();
        actionwords.getScreenshot("testcase001OpenChartingPageAndDisableAllChartingIndicatorsBeforeTests_1");
        actionwords.clickOnIndicatorsBtn();
        actionwords.getScreenshot("testcase002OpenChartingPageAndOpenCloseIndicatorsDialog_2");
        actionwords.disableAllChartingIndicatorsBeforeTests();
        actionwords.verifyThatVolumeCheckboxDisabled();
        actionwords.verifyThatBollingerBandsDisabled();
        actionwords.verifyThatRelativeStrengthIndexCheckboxDisabled();
        actionwords.getScreenshot("testcase002OpenChartingPageAndOpenCloseIndicatorsDialog_3");
        actionwords.clickOnCloseIndicatorsBtn();
        actionwords.verifyThatIndicatorsDialogCloseAfterClickOnCloseBtn();
        actionwords.getScreenshot("testcase001OpenChartingPageAndDisableAllChartingIndicatorsBeforeTests_4");
    }

    @Test
    public void testcase001OpenChartingPageAndDisableAllChartingIndicatorsBeforeTestsDataSet1() {
        testcase001OpenChartingPageAndDisableAllChartingIndicatorsBeforeTests(websitelink);
    }

    public void testcase002OpenChartingPageAndOpenCloseIndicatorsDialog(String link) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.clickOnChartingBtnOnHomePage();
        actionwords.getScreenshot("testcase002OpenChartingPageAndOpenCloseIndicatorsDialog_1");
        actionwords.clickOnIndicatorsBtn();
        actionwords.getScreenshot("testcase002OpenChartingPageAndOpenCloseIndicatorsDialog_2");
        actionwords.clickOnCloseIndicatorsBtn();
        actionwords.verifyThatIndicatorsDialogCloseAfterClickOnCloseBtn();
        actionwords.getScreenshot("testcase002OpenChartingPageAndOpenCloseIndicatorsDialog_3");
    }

    @Test
    public void testcase002OpenChartingPageAndOpenCloseIndicatorsDialogDataSet1() {
        testcase002OpenChartingPageAndOpenCloseIndicatorsDialog(websitelink);
    }

    public void testcase003EnableDisableDisableVolumeIndicator(String link) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.clickOnChartingBtnOnHomePage();
        actionwords.getScreenshot("testcase003EnableDisableDisableVolumeIndicator_1");
        actionwords.clickOnIndicatorsBtn();
        actionwords.getScreenshot("testcase003EnableDisableDisableVolumeIndicator_2");
        actionwords.enableVolumeCheckbox();
        actionwords.verifyThatVolumeCheckboxEnabled();
        actionwords.getScreenshot("testcase003EnableDisableDisableVolumeIndicator_3");
        actionwords.clickOnCloseIndicatorsBtn();
        actionwords.verifyThatIndicatorsDialogCloseAfterClickOnCloseBtn();
        actionwords.getScreenshot("testcase003EnableDisableDisableVolumeIndicator_4");
        actionwords.clickOnIndicatorsBtn();
        actionwords.disableVolumeCheckbox();
        actionwords.verifyThatVolumeCheckboxDisabled();
        actionwords.getScreenshot("testcase003EnableDisableDisableVolumeIndicator_5");
        actionwords.clickOnCloseIndicatorsBtn();
        actionwords.verifyThatIndicatorsDialogCloseAfterClickOnCloseBtn();
        actionwords.getScreenshot("testcase003EnableDisableDisableVolumeIndicator_6");
    }

    @Test
    public void testcase003EnableDisableDisableVolumeIndicatorDataSet1() {
        testcase003EnableDisableDisableVolumeIndicator(websitelink);
    }

    public void testcase004EnableDisableBollingerBandsIndicator(String link) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.clickOnChartingBtnOnHomePage();
        actionwords.getScreenshot("testcase004EnableDisableBollingerBandsIndicator_1");
        actionwords.clickOnIndicatorsBtn();
        actionwords.getScreenshot("testcase004EnableDisableBollingerBandsIndicator_2");
        actionwords.enableBollingerBandsCheckbox();
        actionwords.verifyThatBollingerBandsEnabled();
        actionwords.getScreenshot("testcase004EnableDisableBollingerBandsIndicator_3");
        actionwords.clickOnCloseIndicatorsBtn();
        actionwords.verifyThatIndicatorsDialogCloseAfterClickOnCloseBtn();
        actionwords.getScreenshot("testcase004EnableDisableBollingerBandsIndicator_4");
        actionwords.clickOnIndicatorsBtn();
        actionwords.disableBollingerBandsCheckbox();
        actionwords.verifyThatBollingerBandsDisabled();
        actionwords.getScreenshot("testcase004EnableDisableBollingerBandsIndicator_5");
        actionwords.clickOnCloseIndicatorsBtn();
        actionwords.verifyThatIndicatorsDialogCloseAfterClickOnCloseBtn();
        actionwords.getScreenshot("testcase004EnableDisableBollingerBandsIndicator_6");
    }

    @Test
    public void testcase004EnableDisableBollingerBandsIndicatorDataSet1() {
        testcase004EnableDisableBollingerBandsIndicator(websitelink);
    }

    public void testcase005EnableDisableRelativeStrengthIndexIndicator(String link) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.clickOnChartingBtnOnHomePage();
        actionwords.getScreenshot("testcase005EnableDisableRelativeStrengthIndexIndicator_1");
        actionwords.clickOnIndicatorsBtn();
        actionwords.getScreenshot("testcase005EnableDisableRelativeStrengthIndexIndicator_2");
        actionwords.enableRelativeStrengthIndexCheckbox();
        actionwords.verifyThaRelativeStrengthIndexCheckboxEnabled();
        actionwords.getScreenshot("testcase005EnableDisableRelativeStrengthIndexIndicator_3");
        actionwords.clickOnCloseIndicatorsBtn();
        actionwords.verifyThatIndicatorsDialogCloseAfterClickOnCloseBtn();
        actionwords.getScreenshot("testcase005EnableDisableRelativeStrengthIndexIndicator_4");
        actionwords.clickOnIndicatorsBtn();
        actionwords.disableRelativeStrengthIndexCheckbox();
        actionwords.verifyThatRelativeStrengthIndexCheckboxDisabled();
        actionwords.getScreenshot("testcase005EnableDisableRelativeStrengthIndexIndicator_5");
        actionwords.clickOnCloseIndicatorsBtn();
        actionwords.verifyThatIndicatorsDialogCloseAfterClickOnCloseBtn();
        actionwords.getScreenshot("testcase005EnableDisableRelativeStrengthIndexIndicator_6");
    }

    @Test
    public void testcase005EnableDisableRelativeStrengthIndexIndicatorDataSet1() {
        testcase005EnableDisableRelativeStrengthIndexIndicator(websitelink);
    }

    public void testcase006EnableDisableAllIndicator(String link) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.clickOnChartingBtnOnHomePage();
        actionwords.getScreenshot("testcase006EnableDisableAllIndicator_1");
        actionwords.clickOnIndicatorsBtn();
        actionwords.getScreenshot("testcase006EnableDisableAllIndicator_2");
        actionwords.enableAllChartingIndicators();
        actionwords.verifyThatVolumeCheckboxEnabled();
        actionwords.verifyThatBollingerBandsEnabled();
        actionwords.verifyThaRelativeStrengthIndexCheckboxEnabled();
        actionwords.getScreenshot("testcase006EnableDisableAllIndicator_3");
        actionwords.clickOnCloseIndicatorsBtn();
        actionwords.verifyThatIndicatorsDialogCloseAfterClickOnCloseBtn();
        actionwords.getScreenshot("testcase006EnableDisableAllIndicator_4");
        actionwords.clickOnIndicatorsBtn();
        actionwords.disableAllChartingIndicatorsBeforeTests();
        actionwords.verifyThatVolumeCheckboxDisabled();
        actionwords.verifyThatBollingerBandsDisabled();
        actionwords.verifyThatRelativeStrengthIndexCheckboxDisabled();
        actionwords.getScreenshot("testcase006EnableDisableAllIndicator_5");
        actionwords.clickOnCloseIndicatorsBtn();
        actionwords.verifyThatIndicatorsDialogCloseAfterClickOnCloseBtn();
        actionwords.getScreenshot("testcase006EnableDisableAllIndicator_6");
    }

    @Test
    public void testcase006EnableDisableAllIndicatorDataSet1() {
        testcase006EnableDisableAllIndicator(websitelink);
    }
}
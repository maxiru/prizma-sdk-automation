package prizma.maxiru.tests;

import org.junit.FixMethodOrder;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import prizma.maxiru.framework.common.*;

import static prizma.maxiru.framework.common.PrizmaDriver.*;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)

public class WebSDKRunMultiTrendingBotAndValidateBotSummaryAndPositionsCalculationsTests {
    @Rule
    public PrizmaScreenshotRule screenshotRule = new PrizmaScreenshotRule();

    public Actionwords actionwords = new Actionwords();

    public void testcase001RedirectToBotDetailsPageAndRunBotWithMultiPositionTrendingDialogWithValidValuesAndValidateBotLandingPageAfter(String link, String fileName, String startBalance, String runFrom, String runTo, String tradingFee, String tradeLimit, String botName) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.deleteTheLogsFolder();
        actionwords.createTheLogsFolder();
        actionwords.createTheLogsFileForFailedTests(fileName);
        actionwords.clickOnBotsBtnOnHomePage();
        actionwords.getScreenshot("testcase001RedirectToBotDetailsPageAndRunBotWithMultiPositionTrendingDialogWithValidValuesAndValidateBotLandingPageAfter_1");
        actionwords.clickOnBotRowContainerToOpenBotDetailPageByBotName(botName);
        actionwords.clickOnBotRunBtnOnBotDetailsPage();
        actionwords.getScreenshot("testcase001RedirectToBotDetailsPageAndRunBotWithMultiPositionTrendingDialogWithValidValuesAndValidateBotLandingPageAfter_2");
        actionwords.setStartBalanceOnBotRunDialog(startBalance);
        actionwords.setRunFromOnBotRunPage(runFrom);
        actionwords.setRunToOnBotRunPage(runTo);
        actionwords.setTradingFeeOnBotRunDialog(tradingFee);
        actionwords.setTradeLimitOnBotRunDialog(tradeLimit);
        actionwords.getScreenshot("testcase001RedirectToBotDetailsPageAndRunBotWithMultiPositionTrendingDialogWithValidValuesAndValidateBotLandingPageAfter_3");
        actionwords.clickOnBotRunBtnOnBotRunDialog();
        actionwords.getScreenshot("testcase001RedirectToBotDetailsPageAndRunBotWithMultiPositionTrendingDialogWithValidValuesAndValidateBotLandingPageAfter_4");
        actionwords.verifyThatRunBotDialogCloseWhenUserTriedToRunBotWithCorrectDataBot();
        actionwords.refreshPage();
        actionwords.openWebsite(link);
        actionwords.clickOnBotsBtnOnHomePage();
        actionwords.getScreenshot("testcase001RedirectToBotDetailsPageAndRunBotWithMultiPositionTrendingDialogWithValidValuesAndValidateBotLandingPageAfter_5");
        actionwords.clickOnBotRowContainerToOpenBotDetailPageByBotName(botName);
        actionwords.getAndStoreTotalCountOfAllTrendingPositions();
        actionwords.getAndStoreTotalCountOfOpenAndCloseTrendingPositions();
        actionwords.getAndStoreInOpenTrendingPositionsValue();
        actionwords.getAndCalculateAllProfitCurrencyValues();
        actionwords.getAndCalculateAllProfitCurrencyValuesForClosedPositions();
        actionwords.getAndCalculateAllProfitCurrencyValuesForOpenPositions();
        actionwords.getAndCalculateAllProfitPercentageValues();
        actionwords.getAndCalculateAllProfitPercentageValuesForClosedPositions();
        actionwords.getAndCalculateAllProfitPercentageValuesForOpenPositions();
        actionwords.getAndCalculateTotalBalance();
        actionwords.getAndCalculateAvailableBalance();
        actionwords.getAndCalculateAverageProfitCurrency();
        actionwords.getAndCalculateAverageProfitPercentage();
        actionwords.getAndCalculateTotalProfitPercentageValues();
        actionwords.getAndCalculateBotEarnedValues();
        actionwords.getScreenshot("testcase001RedirectToBotDetailsPageAndRunBotWithMultiPositionTrendingDialogWithValidValuesAndValidateBotLandingPageAfter_6");
        actionwords.refreshPage();
        actionwords.openWebsite(link);
        actionwords.clickOnBotsBtnOnHomePage();
        actionwords.getScreenshot("testcase001RedirectToBotDetailsPageAndRunBotWithMultiPositionTrendingDialogWithValidValuesAndValidateBotLandingPageAfter_7");
        actionwords.verifyThatBotNameOnPositionFromVariableValueIsCorrect(botName);
        actionwords.verifyThatBotLastRunOnPositionFromVariableValueIsCorrectIfValidateWithVariableValue();
        actionwords.verifyThatBotAverangeProfitCurrencyOnPositionFromVariableValueIsCorrectIfValidateWithVariableValue();
        actionwords.verifyThatBotAverangeProfitPercentageOnPositionFromVariableValueIsCorrectIfValidateWithVariableValue();
        actionwords.verifyThatBotProfitCurrencyOnPositionFromVariableValueIsCorrectIfValidateWithVariableValue();
        actionwords.verifyThatBotProfitPercentageOnPositionFromVariableValueIsCorrectIfValidateWithVariableValue();
        actionwords.getScreenshot("testcase001RedirectToBotDetailsPageAndRunBotWithMultiPositionTrendingDialogWithValidValuesAndValidateBotLandingPageAfter_8");
    }

    @Test
    public void testcase001RedirectToBotDetailsPageAndRunBotWithMultiPositionTrendingDialogWithValidValuesAndValidateBotLandingPageAfterDataSet1() {
        testcase001RedirectToBotDetailsPageAndRunBotWithMultiPositionTrendingDialogWithValidValuesAndValidateBotLandingPageAfter(websitelink, "bot_multi_landing_page.txt", "1000", "10/01/2021", "10/10/2021", "0.5", "10", "test_multiorder");
    }

    public void testcase002RedirectToBotDetailsPageAndRunBotDialogWithMultiPositionTrendingValidValuesAndValidateBotSummaryPageAfter(String link, String fileName, String startBalance, String tradingFee, String tradeLimit, String botName) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.createTheLogsFileForFailedTests(fileName);
        actionwords.clickOnBotsBtnOnHomePage();
        actionwords.getScreenshot("testcase002RedirectToBotDetailsPageAndRunBotDialogWithMultiPositionTrendingValidValuesAndValidateBotSummaryPageAfter_1");
        actionwords.clickOnBotRowContainerToOpenBotDetailPageByBotName(botName);
        actionwords.clickOnBotRunBtnOnBotDetailsPage();
        actionwords.getScreenshot("testcase002RedirectToBotDetailsPageAndRunBotDialogWithMultiPositionTrendingValidValuesAndValidateBotSummaryPageAfter_2");
        actionwords.setStartBalanceOnBotRunDialog(startBalance);
        actionwords.setTradingFeeOnBotRunDialog(tradingFee);
        actionwords.setTradeLimitOnBotRunDialog(tradeLimit);
        actionwords.getScreenshot("testcase002RedirectToBotDetailsPageAndRunBotDialogWithMultiPositionTrendingValidValuesAndValidateBotSummaryPageAfter_3");
        actionwords.clickOnBotRunBtnOnBotRunDialog();
        actionwords.getScreenshot("testcase002RedirectToBotDetailsPageAndRunBotDialogWithMultiPositionTrendingValidValuesAndValidateBotSummaryPageAfter_4");
        actionwords.verifyThatRunBotDialogCloseWhenUserTriedToRunBotWithCorrectDataBot();
        actionwords.refreshPage();
        actionwords.openWebsite(link);
        actionwords.clickOnBotsBtnOnHomePage();
        actionwords.getScreenshot("testcase002RedirectToBotDetailsPageAndRunBotDialogWithMultiPositionTrendingValidValuesAndValidateBotSummaryPageAfter_5");
        actionwords.clickOnBotRowContainerToOpenBotDetailPageByBotName(botName);
        actionwords.getAndStoreTotalCountOfAllTrendingPositions();
        actionwords.getAndStoreTotalCountOfOpenAndCloseTrendingPositions();
        actionwords.getAndStoreInOpenTrendingPositionsValue();
        actionwords.getAndCalculateAllProfitCurrencyValues();
        actionwords.getAndCalculateAllProfitCurrencyValuesForClosedPositions();
        actionwords.getAndCalculateAllProfitCurrencyValuesForOpenPositions();
        actionwords.getAndCalculateAllProfitPercentageValues();
        actionwords.getAndCalculateAllProfitPercentageValuesForClosedPositions();
        actionwords.getAndCalculateAllProfitPercentageValuesForOpenPositions();
        actionwords.getAndCalculateTotalBalance();
        actionwords.getAndCalculateAvailableBalance();
        actionwords.getAndCalculateAverageProfitCurrency();
        actionwords.getAndCalculateAverageProfitPercentage();
        actionwords.getAndCalculateTotalProfitPercentageValues();
        actionwords.getAndCalculateBotEarnedValues();
        actionwords.getScreenshot("testcase002RedirectToBotDetailsPageAndRunBotDialogWithMultiPositionTrendingValidValuesAndValidateBotSummaryPageAfter_6");
        actionwords.refreshPage();
        actionwords.openWebsite(link);
        actionwords.clickOnBotsBtnOnHomePage();
        actionwords.getScreenshot("testcase002RedirectToBotDetailsPageAndRunBotDialogWithMultiPositionTrendingValidValuesAndValidateBotSummaryPageAfter_7");
        actionwords.clickOnBotRowContainerToOpenBotDetailPageByBotName(botName);
        actionwords.verifyThatBotLastRunIsCorrectOnBotSummaryTableIfValidateWithVariableValue();
        actionwords.verifyThatBotFromToIsCorrectOnBotSummaryTableStartDateIfValidateWithVariableValue();
        actionwords.verifyThatBotFromToIsCorrectOnBotSummaryTableEndDateIfValidateWithVariableValue();
        actionwords.verifyThatBotClosedPositionsIsCorrectOnBotSummaryTableIfValidateWithVariableValue();
        actionwords.verifyThatBotInitialBalanceIsCorrectOnBotSummaryTableIfValidateWithVariableValue();
        actionwords.verifyThatBotInOpenPositionsBalanceIsCorrectOnBotSummaryTableWithIfValidateVariableValue();
        actionwords.verifyThatBotTotalBalanceIsCorrectOnBotSummaryTableIfValidateWithVariableValue();
        actionwords.verifyThatBotAverangeProfitCurrencyIsCorrectOnBotSummaryTableIfValidateWithVariableValue();
        actionwords.verifyThatBotAverangeProfitPercentageIsCorrectOnBotSummaryTableIfValidateWithVariableValue();
        actionwords.verifyThatBotTotalProfitCurrencyIsCorrectOnBotSummaryTableIfValidateWithVariableValue();
        actionwords.verifyThatBotTotalProfitPercentageIsCorrectOnBotSummaryTableIfValidateWithVariableValue();
        actionwords.verifyThatBotEarnedIsCorrectOnBotSummaryTableIfValidateWithVariableValue();
        actionwords.getScreenshot("testcase002RedirectToBotDetailsPageAndRunBotDialogWithMultiPositionTrendingValidValuesAndValidateBotSummaryPageAfter_8");
    }

    @Test
    public void testcase002RedirectToBotDetailsPageAndRunBotDialogWithMultiPositionTrendingValidValuesAndValidateBotSummaryPageAfterDataSet1() {
        testcase002RedirectToBotDetailsPageAndRunBotDialogWithMultiPositionTrendingValidValuesAndValidateBotSummaryPageAfter(websitelink, "bot_multi_summary_page.txt", "1000", "0.5", "10", "test_multiorder");
    }

    public void testcase003RedirectToBotDetailsPageAndRunBotWithMultiPositionTrendingDialogWithValidValuesAndValidateBotPositionPageAfter(String link, String fileName, String startBalance, String tradingFee, String tradeLimit, String botName) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.createTheLogsFileForFailedTests(fileName);
        actionwords.clickOnBotsBtnOnHomePage();
        actionwords.getScreenshot("testcase003RedirectToBotDetailsPageAndRunBotWithMultiPositionTrendingDialogWithValidValuesAndValidateBotPositionPageAfter_1");
        actionwords.clickOnBotRowContainerToOpenBotDetailPageByBotName(botName);
        actionwords.clickOnBotRunBtnOnBotDetailsPage();
        actionwords.getScreenshot("testcase003RedirectToBotDetailsPageAndRunBotWithMultiPositionTrendingDialogWithValidValuesAndValidateBotPositionPageAfter_2");
        actionwords.setStartBalanceOnBotRunDialog(startBalance);
        actionwords.setTradingFeeOnBotRunDialog(tradingFee);
        actionwords.setTradeLimitOnBotRunDialog(tradeLimit);
        actionwords.getScreenshot("testcase003RedirectToBotDetailsPageAndRunBotWithMultiPositionTrendingDialogWithValidValuesAndValidateBotPositionPageAfter_3");
        actionwords.clickOnBotRunBtnOnBotRunDialog();
        actionwords.getScreenshot("testcase003RedirectToBotDetailsPageAndRunBotWithMultiPositionTrendingDialogWithValidValuesAndValidateBotPositionPageAfter_4");
        actionwords.verifyThatRunBotDialogCloseWhenUserTriedToRunBotWithCorrectDataBot();
        actionwords.refreshPage();
        actionwords.openWebsite(link);
        actionwords.clickOnBotsBtnOnHomePage();
        actionwords.getScreenshot("testcase003RedirectToBotDetailsPageAndRunBotWithMultiPositionTrendingDialogWithValidValuesAndValidateBotPositionPageAfter_5");
        actionwords.clickOnBotRowContainerToOpenBotDetailPageByBotName(botName);
        actionwords.getAndCalculateAndValidateAllValuesInPositionTable("testcase003RedirectToBotDetailsPageAndRunBotWithMultiPositionTrendingDialogWithValidValuesAndValidateBotPositionPageAfter_");
    }

    @Test
    @Ignore
    public void testcase003RedirectToBotDetailsPageAndRunBotWithMultiPositionTrendingDialogWithValidValuesAndValidateBotPositionPageAfterDataSet1() {
        testcase003RedirectToBotDetailsPageAndRunBotWithMultiPositionTrendingDialogWithValidValuesAndValidateBotPositionPageAfter(websitelink, "bot_multi_position_page.txt", "1000", "0.5", "10", "test_multiorder");
    }

    public void testcase004RedirectToBotDetailsPageAndRunBotWithMultiPositionTrendingDialogWithValidValuesAndValidateBotPositionPageAfterPlusRedirectToChartingAfterClickOnViewOnChartBtn(String link, String startBalance, String tradingFee, String tradeLimit, int position, String botName) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.clickOnBotsBtnOnHomePage();
        actionwords.getScreenshot("testcase004RedirectToBotDetailsPageAndRunBotWithMultiPositionTrendingDialogWithValidValuesAndValidateBotPositionPageAfterPlusRedirectToChartingAfterClickOnViewOnChartBtn_1");
        actionwords.clickOnBotRowContainerToOpenBotDetailPageByBotName(botName);
        actionwords.clickOnBotRunBtnOnBotDetailsPage();
        actionwords.getScreenshot("testcase004RedirectToBotDetailsPageAndRunBotWithMultiPositionTrendingDialogWithValidValuesAndValidateBotPositionPageAfterPlusRedirectToChartingAfterClickOnViewOnChartBtn_2");
        actionwords.setStartBalanceOnBotRunDialog(startBalance);
        actionwords.setTradingFeeOnBotRunDialog(tradingFee);
        actionwords.setTradeLimitOnBotRunDialog(tradeLimit);
        actionwords.getScreenshot("testcase004RedirectToBotDetailsPageAndRunBotWithMultiPositionTrendingDialogWithValidValuesAndValidateBotPositionPageAfterPlusRedirectToChartingAfterClickOnViewOnChartBtn_3");
        actionwords.clickOnBotRunBtnOnBotRunDialog();
        actionwords.getScreenshot("testcase004RedirectToBotDetailsPageAndRunBotWithMultiPositionTrendingDialogWithValidValuesAndValidateBotPositionPageAfterPlusRedirectToChartingAfterClickOnViewOnChartBtn_4");
        actionwords.verifyThatRunBotDialogCloseWhenUserTriedToRunBotWithCorrectDataBot();
        actionwords.refreshPage();
        actionwords.openWebsite(link);
        actionwords.clickOnBotsBtnOnHomePage();
        actionwords.getScreenshot("testcase004RedirectToBotDetailsPageAndRunBotWithMultiPositionTrendingDialogWithValidValuesAndValidateBotPositionPageAfterPlusRedirectToChartingAfterClickOnViewOnChartBtn_5");
        actionwords.clickOnBotRowContainerToOpenBotDetailPageByBotName(botName);
        actionwords.getScreenshot("testcase004RedirectToBotDetailsPageAndRunBotWithMultiPositionTrendingDialogWithValidValuesAndValidateBotPositionPageAfterPlusRedirectToChartingAfterClickOnViewOnChartBtn_6");
        actionwords.clickOnViewOnChartingButtonInPositionsTableRowByPositionX(position);
        actionwords.getScreenshot("testcase004RedirectToBotDetailsPageAndRunBotWithMultiPositionTrendingDialogWithValidValuesAndValidateBotPositionPageAfterPlusRedirectToChartingAfterClickOnViewOnChartBtn_7");
    }

    @Test
    public void testcase004RedirectToBotDetailsPageAndRunBotWithMultiPositionTrendingDialogWithValidValuesAndValidateBotPositionPageAfterPlusRedirectToChartingAfterClickOnViewOnChartBtnDataSet1() {
        testcase004RedirectToBotDetailsPageAndRunBotWithMultiPositionTrendingDialogWithValidValuesAndValidateBotPositionPageAfterPlusRedirectToChartingAfterClickOnViewOnChartBtn(websitelink, "1000", "0.5", "10", 1, "test_multiorder");
    }
}
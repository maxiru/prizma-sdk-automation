package prizma.maxiru.tests;

import org.junit.FixMethodOrder;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import prizma.maxiru.framework.common.*;

import static app.PrizmaRunBotPage.*;
import static prizma.maxiru.framework.common.PrizmaDriver.*;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)

public class WebSDKRunBotTests {
    @Rule
    public PrizmaScreenshotRule screenshotRule = new PrizmaScreenshotRule();

    public Actionwords actionwords = new Actionwords();

    public void testcase001RedirectToBotDetailsPageAndOpenCloseRunBotDialog(String link) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.clickOnBotsBtnOnHomePage();
        actionwords.getScreenshot("testcase001RedirectToBotDetailsPageAndOpenCloseRunBotDialog_1");
        actionwords.clickOnBotRowContainerToOpenBotDetailPageByBotNameFromVariableValue();
        actionwords.clickOnBotRunBtnOnBotDetailsPage();
        actionwords.getScreenshot("testcase001RedirectToBotDetailsPageAndOpenCloseRunBotDialog_2");
        actionwords.clickOnCloseBtnOnBotRunDialog();
        actionwords.verifyThatRunBotDialogCloseAfterClickOnCloseBtn();
        actionwords.getScreenshot("testcase001RedirectToBotDetailsPageAndOpenCloseRunBotDialog_3");
    }

    @Test
    public void testcase001RedirectToBotDetailsPageAndOpenCloseRunBotDialogDataSet1() {
        testcase001RedirectToBotDetailsPageAndOpenCloseRunBotDialog(websitelink);
    }

    public void testcase002RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithoutSetupFromToDate(String link) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.clickOnBotsBtnOnHomePage();
        actionwords.getScreenshot("testcase002RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithoutSetupFromToDate_1");
        actionwords.clickOnBotRowContainerToOpenBotDetailPageByBotNameFromVariableValue();
        actionwords.clickOnBotRunBtnOnBotDetailsPage();
        actionwords.getScreenshot("testcase002RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithoutSetupFromToDate_2");
        actionwords.clickOnBotRunBtnOnBotRunDialogWithoutWaitDriver();
        actionwords.getScreenshot("testcase002RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithoutSetupFromToDate_3");
        actionwords.verifyThatRunBotDialogNotCloseWhenUserTriedToRunBotWithIncorrectDataBot();
        actionwords.getScreenshot("testcase002RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithoutSetupFromToDate_4");
    }

    @Test
    public void testcase002RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithoutSetupFromToDateDataSet1() {
        testcase002RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithoutSetupFromToDate(websitelink);
    }

    public void testcase003RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithEmptyToDate(String link, String runFrom) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.clickOnBotsBtnOnHomePage();
        actionwords.getScreenshot("testcase003RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithEmptyToDate_1");
        actionwords.clickOnBotRowContainerToOpenBotDetailPageByBotNameFromVariableValue();
        actionwords.clickOnBotRunBtnOnBotDetailsPage();
        actionwords.getScreenshot("testcase003RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithEmptyToDate_2");
        actionwords.setRunFromOnBotRunPage(runFrom);
        actionwords.clickOnBotRunBtnOnBotRunDialogWithoutWaitDriver();
        actionwords.getScreenshot("testcase003RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithEmptyToDate_3");
        actionwords.verifyThatRunBotDialogNotCloseWhenUserTriedToRunBotWithIncorrectDataBot();
        actionwords.getScreenshot("testcase003RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithEmptyToDate_4");
    }

    @Test
    public void testcase003RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithEmptyToDateDataSet1() {
        testcase003RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithEmptyToDate(websitelink, "10/01/2021");
    }

    public void testcase004RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithEmptyFromDate(String link, String runTo) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.clickOnBotsBtnOnHomePage();
        actionwords.getScreenshot("testcase004RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithEmptyFromDate_1");
        actionwords.clickOnBotRowContainerToOpenBotDetailPageByBotNameFromVariableValue();
        actionwords.clickOnBotRunBtnOnBotDetailsPage();
        actionwords.getScreenshot("testcase004RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithEmptyFromDate_2");
        actionwords.setRunToOnBotRunPage(runTo);
        actionwords.clickOnBotRunBtnOnBotRunDialogWithoutWaitDriver();
        actionwords.getScreenshot("testcase004RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithEmptyFromDate_3");
        actionwords.verifyThatRunBotDialogNotCloseWhenUserTriedToRunBotWithIncorrectDataBot();
        actionwords.getScreenshot("testcase004RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithEmptyFromDate_4");
    }

    @Test
    public void testcase004RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithEmptyFromDateDataSet1() {
        testcase004RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithEmptyFromDate(websitelink, "10/10/2021");
    }

    public void testcase005RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWhenToDateIsLessThenFromDate(String link, String runFrom, String runTo, String errorMessage) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.clickOnBotsBtnOnHomePage();
        actionwords.getScreenshot("testcase005RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWhenToDateIsLessThenFromDate_1");
        actionwords.clickOnBotRowContainerToOpenBotDetailPageByBotNameFromVariableValue();
        actionwords.clickOnBotRunBtnOnBotDetailsPage();
        actionwords.getScreenshot("testcase005RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWhenToDateIsLessThenFromDate_2");
        actionwords.setRunFromOnBotRunPage(runFrom);
        actionwords.setRunToOnBotRunPage(runTo);
        actionwords.clickOnBotRunBtnOnBotRunDialogWithoutWaitDriver();
        actionwords.getScreenshot("testcase005RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWhenToDateIsLessThenFromDate_3");
        actionwords.verifyThatRunBotDialogNotCloseWhenUserTriedToRunBotWithIncorrectDataBot();
        actionwords.verifyThatOnRunBotFormErrorMessageAppear(errorMessage);
        actionwords.getScreenshot("testcase005RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWhenToDateIsLessThenFromDate_4");
    }

    @Test
    public void testcase005RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWhenToDateIsLessThenFromDateDataSet1() {
        testcase005RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWhenToDateIsLessThenFromDate(websitelink, "10/10/2021", "10/01/2021", TO_DATE_IS_LESS_THEN_FROM_DATE);
    }

    public void testcase006RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithInvalidFromAndToDate(String link, String startBalance, String runFrom, String runTo, String tradingFee, String tradeLimit, String fileName1, String fileName2, String fileName3, String fileName4) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.clickOnBotsBtnOnHomePage();
        actionwords.getScreenshot(fileName1);
        actionwords.clickOnBotRowContainerToOpenBotDetailPageByBotNameFromVariableValue();
        actionwords.clickOnBotRunBtnOnBotDetailsPage();
        actionwords.getScreenshot(fileName2);
        actionwords.setStartBalanceOnBotRunDialog(startBalance);
        actionwords.setRunFromOnBotRunPage(runFrom);
        actionwords.setRunToOnBotRunPage(runTo);
        actionwords.setTradingFeeOnBotRunDialog(tradingFee);
        actionwords.setTradeLimitOnBotRunDialog(tradeLimit);
        actionwords.clickOnBotRunBtnOnBotRunDialogWithoutWaitDriver();
        actionwords.getScreenshot(fileName3);
        actionwords.verifyThatRunBotDialogNotCloseWhenUserTriedToRunBotWithIncorrectDataBot();
        actionwords.getScreenshot(fileName4);
    }

    @Test
    @Ignore
    public void testcase006RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithInvalidFromAndToDateDataSet1() {
        testcase006RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithInvalidFromAndToDate(websitelink, "1000", "13/10/2020", "13/10/2021", "0.5", "10", "testcase006RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithInvalidFromAndToDateDataSet1_1", "testcase006RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithInvalidFromAndToDateDataSet1_2", "testcase006RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithInvalidFromAndToDateDataSet1_3", "testcase006RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithInvalidFromAndToDateDataSet1_4");
    }

    @Test
    @Ignore
    public void testcase006RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithInvalidFromAndToDateDataSet2() {
        testcase006RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithInvalidFromAndToDate(websitelink, "1000", "12/32/2020", "12/32/2020", "0.5", "10", "testcase006RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithInvalidFromAndToDateDataSet2_1", "testcase006RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithInvalidFromAndToDateDataSet2_2", "testcase006RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithInvalidFromAndToDateDataSet2_3", "testcase006RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithInvalidFromAndToDateDataSet2_4");
    }

    @Test
    @Ignore
    public void testcase006RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithInvalidFromAndToDateDataSet3() {
        testcase006RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithInvalidFromAndToDate(websitelink, "1000", "12/12/2023", "12/12/2023", "0.5", "10", "testcase006RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithInvalidFromAndToDateDataSet3_1", "testcase006RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithInvalidFromAndToDateDataSet3_2", "testcase006RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithInvalidFromAndToDateDataSet3_3", "testcase006RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithInvalidFromAndToDateDataSet3_4");
    }

    public void testcase007RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithEmptyStartBalance(String link, String startBalance) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.clickOnBotsBtnOnHomePage();
        actionwords.getScreenshot("testcase007RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithEmptyStartBalance_1");
        actionwords.clickOnBotRowContainerToOpenBotDetailPageByBotNameFromVariableValue();
        actionwords.clickOnBotRunBtnOnBotDetailsPage();
        actionwords.getScreenshot("testcase007RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithEmptyStartBalance_2");
        actionwords.setStartBalanceOnBotRunDialog(startBalance);
        actionwords.clickOnBotRunBtnOnBotRunDialogWithoutWaitDriver();
        actionwords.getScreenshot("testcase007RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithEmptyStartBalance_3");
        actionwords.verifyThatRunBotDialogNotCloseWhenUserTriedToRunBotWithIncorrectDataBot();
        actionwords.getScreenshot("testcase007RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithEmptyStartBalance_4");
    }

    @Test
    public void testcase007RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithEmptyStartBalanceDataSet1() {
        testcase007RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithEmptyStartBalance(websitelink, "");
    }

    public void testcase008RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithStartBalanceLessThenLimit(String link, String startBalance, String fileName1, String fileName2, String fileName3, String fileName4, String errorMessage) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.clickOnBotsBtnOnHomePage();
        actionwords.getScreenshot(fileName1);
        actionwords.clickOnBotRowContainerToOpenBotDetailPageByBotNameFromVariableValue();
        actionwords.clickOnBotRunBtnOnBotDetailsPage();
        actionwords.getScreenshot(fileName2);
        actionwords.setStartBalanceOnBotRunDialog(startBalance);
        actionwords.clickOnBotRunBtnOnBotRunDialogWithoutWaitDriver();
        actionwords.getScreenshot(fileName3);
        actionwords.verifyThatRunBotDialogNotCloseWhenUserTriedToRunBotWithIncorrectDataBot();
        actionwords.verifyThatOnRunBotFormErrorMessageAppear(errorMessage);
        actionwords.getScreenshot(fileName4);
    }

    @Test
    public void testcase008RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithStartBalanceLessThenLimitDataSet1() {
        testcase008RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithStartBalanceLessThenLimit(websitelink, "1", "testcase008RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithStartBalanceLessThenLimitDataSet1_1", "testcase008RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithStartBalanceLessThenLimitDataSet1_2", "testcase008RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithStartBalanceLessThenLimitDataSet1_3", "testcase008RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithStartBalanceLessThenLimitDataSet1_4", START_BALANCE_LESS_THEN_LIMIT);
    }

    @Test
    public void testcase008RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithStartBalanceLessThenLimitDataSet2() {
        testcase008RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithStartBalanceLessThenLimit(websitelink, "99", "testcase008RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithStartBalanceLessThenLimitDataSet2_1", "testcase008RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithStartBalanceLessThenLimitDataSet2_2", "testcase008RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithStartBalanceLessThenLimitDataSet2_3", "testcase008RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithStartBalanceLessThenLimitDataSet2_4", START_BALANCE_LESS_THEN_LIMIT);
    }

    public void testcase009RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithStartBalanceMoreThenLimit(String link, String startBalance, String fileName1, String fileName2, String fileName3, String fileName4, String errorMessage) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.clickOnBotsBtnOnHomePage();
        actionwords.getScreenshot(fileName1);
        actionwords.clickOnBotRowContainerToOpenBotDetailPageByBotNameFromVariableValue();
        actionwords.clickOnBotRunBtnOnBotDetailsPage();
        actionwords.getScreenshot(fileName2);
        actionwords.setStartBalanceOnBotRunDialog(startBalance);
        actionwords.clickOnBotRunBtnOnBotRunDialogWithoutWaitDriver();
        actionwords.getScreenshot(fileName3);
        actionwords.verifyThatRunBotDialogNotCloseWhenUserTriedToRunBotWithIncorrectDataBot();
        actionwords.verifyThatOnRunBotFormErrorMessageNotAppear(errorMessage);
        actionwords.getScreenshot(fileName4);
    }

    @Test
    public void testcase009RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithStartBalanceMoreThenLimitDataSet1() {
        testcase009RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithStartBalanceMoreThenLimit(websitelink, "100", "testcase009RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithStartBalanceMoreThenLimitDataSet1_1", "testcase009RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithStartBalanceMoreThenLimitDataSet1_2", "testcase009RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithStartBalanceMoreThenLimitDataSet1_3", "testcase009RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithStartBalanceMoreThenLimitDataSet1_4", START_BALANCE_LESS_THEN_LIMIT);
    }

    @Test
    public void testcase009RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithStartBalanceMoreThenLimitDataSet2() {
        testcase009RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithStartBalanceMoreThenLimit(websitelink, "101", "testcase009RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithStartBalanceMoreThenLimitDataSet2_1", "testcase009RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithStartBalanceMoreThenLimitDataSet2_2", "testcase009RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithStartBalanceMoreThenLimitDataSet2_3", "testcase009RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithStartBalanceMoreThenLimitDataSet2_4", START_BALANCE_LESS_THEN_LIMIT);
    }

    public void testcase010RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithEmptyStartBalance(String link, String startBalance, String fileName1, String fileName2, String fileName3, String fileName4) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.clickOnBotsBtnOnHomePage();
        actionwords.getScreenshot(fileName1);
        actionwords.clickOnBotRowContainerToOpenBotDetailPageByBotNameFromVariableValue();
        actionwords.clickOnBotRunBtnOnBotDetailsPage();
        actionwords.getScreenshot(fileName2);
        actionwords.setStartBalanceOnBotRunDialog(startBalance);
        actionwords.clickOnBotRunBtnOnBotRunDialogWithoutWaitDriver();
        actionwords.getScreenshot(fileName3);
        actionwords.verifyThatRunBotDialogNotCloseWhenUserTriedToRunBotWithIncorrectDataBot();
        actionwords.getScreenshot(fileName4);
    }

    @Test
    public void testcase010RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithEmptyStartBalanceDataSet1() {
        testcase010RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithEmptyStartBalance(websitelink, "", "testcase010RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithEmptyStartBalanceDataSet1_1", "testcase010RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithEmptyStartBalanceDataSet1_2", "testcase010RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithEmptyStartBalanceDataSet1_3", "testcase010RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithEmptyStartBalanceDataSet1_4");
    }

    public void testcase011RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithTrendingFeeMoreThenLimit(String link, String tradingFee, String fileName1, String fileName2, String fileName3, String fileName4, String errorMessage) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.clickOnBotsBtnOnHomePage();
        actionwords.getScreenshot(fileName1);
        actionwords.clickOnBotRowContainerToOpenBotDetailPageByBotNameFromVariableValue();
        actionwords.clickOnBotRunBtnOnBotDetailsPage();
        actionwords.getScreenshot(fileName2);
        actionwords.setTradingFeeOnBotRunDialog(tradingFee);
        actionwords.clickOnBotRunBtnOnBotRunDialogWithoutWaitDriver();
        actionwords.getScreenshot(fileName3);
        actionwords.verifyThatRunBotDialogNotCloseWhenUserTriedToRunBotWithIncorrectDataBot();
        actionwords.verifyThatOnRunBotFormErrorMessageAppear(errorMessage);
        actionwords.getScreenshot(fileName4);
    }

    @Test
    public void testcase011RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithTrendingFeeMoreThenLimitDataSet1() {
        testcase011RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithTrendingFeeMoreThenLimit(websitelink, "101", "testcase011RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithTrendingFeeMoreThenLimitDataSet1_1", "testcase011RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithTrendingFeeMoreThenLimitDataSet1_2", "testcase011RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithTrendingFeeMoreThenLimitDataSet1_3", "testcase011RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithTrendingFeeMoreThenLimitDataSet1_4", TRENDING_FEE_MORE_THEN_LIMIT);
    }

    @Test
    public void testcase011RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithTrendingFeeMoreThenLimitDataSet2() {
        testcase011RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithTrendingFeeMoreThenLimit(websitelink, "100.1", "testcase011RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithTrendingFeeMoreThenLimitDataSet2_1", "testcase011RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithTrendingFeeMoreThenLimitDataSet2_2", "testcase011RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithTrendingFeeMoreThenLimitDataSet2_3", "testcase011RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithTrendingFeeMoreThenLimitDataSet2_4", TRENDING_FEE_MORE_THEN_LIMIT);
    }

    public void testcase012RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithTrendingFeeLessThenLimit(String link, String tradingFee, String fileName1, String fileName2, String fileName3, String fileName4, String errorMessage) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.clickOnBotsBtnOnHomePage();
        actionwords.getScreenshot(fileName1);
        actionwords.clickOnBotRowContainerToOpenBotDetailPageByBotNameFromVariableValue();
        actionwords.clickOnBotRunBtnOnBotDetailsPage();
        actionwords.getScreenshot(fileName2);
        actionwords.setTradingFeeOnBotRunDialog(tradingFee);
        actionwords.clickOnBotRunBtnOnBotRunDialogWithoutWaitDriver();
        actionwords.getScreenshot(fileName3);
        actionwords.verifyThatRunBotDialogNotCloseWhenUserTriedToRunBotWithIncorrectDataBot();
        actionwords.verifyThatOnRunBotFormErrorMessageNotAppear(errorMessage);
        actionwords.getScreenshot(fileName4);
    }

    @Test
    public void testcase012RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithTrendingFeeLessThenLimitDataSet1() {
        testcase012RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithTrendingFeeLessThenLimit(websitelink, "0", "testcase012RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithTrendingFeeLessThenLimitDataSet1_1", "testcase012RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithTrendingFeeLessThenLimitDataSet1_2", "testcase012RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithTrendingFeeLessThenLimitDataSet1_3", "testcase012RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithTrendingFeeLessThenLimitDataSet1_4", TRENDING_FEE_MORE_THEN_LIMIT);
    }

    @Test
    public void testcase012RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithTrendingFeeLessThenLimitDataSet2() {
        testcase012RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithTrendingFeeLessThenLimit(websitelink, "0.1", "testcase012RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithTrendingFeeLessThenLimitDataSet2_1", "testcase012RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithTrendingFeeLessThenLimitDataSet2_2", "testcase012RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithTrendingFeeLessThenLimitDataSet2_3", "testcase012RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithTrendingFeeLessThenLimitDataSet2_4", TRENDING_FEE_MORE_THEN_LIMIT);
    }

    @Test
    public void testcase012RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithTrendingFeeLessThenLimitDataSet3() {
        testcase012RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithTrendingFeeLessThenLimit(websitelink, "100", "testcase012RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithTrendingFeeLessThenLimitDataSet3_1", "testcase012RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithTrendingFeeLessThenLimitDataSet3_2", "testcase012RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithTrendingFeeLessThenLimitDataSet3_3", "testcase012RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithTrendingFeeLessThenLimitDataSet3_4", TRENDING_FEE_MORE_THEN_LIMIT);
    }

    @Test
    public void testcase012RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithTrendingFeeLessThenLimitDataSet4() {
        testcase012RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithTrendingFeeLessThenLimit(websitelink, "99.9", "testcase012RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithTrendingFeeLessThenLimitDataSet4_1", "testcase012RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithTrendingFeeLessThenLimitDataSet4_2", "testcase012RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithTrendingFeeLessThenLimitDataSet4_3", "testcase012RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithTrendingFeeLessThenLimitDataSet4_4", TRENDING_FEE_MORE_THEN_LIMIT);
    }

    public void testcase013RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithEmptyTrendingFee(String link, String tradingFee, String fileName1, String fileName2, String fileName3, String fileName4) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.clickOnBotsBtnOnHomePage();
        actionwords.getScreenshot(fileName1);
        actionwords.clickOnBotRowContainerToOpenBotDetailPageByBotNameFromVariableValue();
        actionwords.clickOnBotRunBtnOnBotDetailsPage();
        actionwords.getScreenshot(fileName2);
        actionwords.setTradingFeeOnBotRunDialog(tradingFee);
        actionwords.clickOnBotRunBtnOnBotRunDialogWithoutWaitDriver();
        actionwords.getScreenshot(fileName3);
        actionwords.verifyThatRunBotDialogNotCloseWhenUserTriedToRunBotWithIncorrectDataBot();
        actionwords.getScreenshot(fileName4);
    }

    @Test
    public void testcase013RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithEmptyTrendingFeeDataSet1() {
        testcase013RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithEmptyTrendingFee(websitelink, "", "testcase013RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithEmptyTrendingFeeDataSet1_1", "testcase013RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithEmptyTrendingFeeDataSet1_2", "testcase013RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithEmptyTrendingFeeDataSet1_3", "testcase013RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithEmptyTrendingFeeDataSet1_4");
    }

    public void testcase014RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithTradeLimitLessThenLimit(String link, String tradeLimit, String fileName1, String fileName2, String fileName3, String fileName4, String errorMessage) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.clickOnBotsBtnOnHomePage();
        actionwords.getScreenshot(fileName1);
        actionwords.clickOnBotRowContainerToOpenBotDetailPageByBotNameFromVariableValue();
        actionwords.clickOnBotRunBtnOnBotDetailsPage();
        actionwords.getScreenshot(fileName2);
        actionwords.setTradeLimitOnBotRunDialog(tradeLimit);
        actionwords.clickOnBotRunBtnOnBotRunDialogWithoutWaitDriver();
        actionwords.getScreenshot(fileName3);
        actionwords.verifyThatRunBotDialogNotCloseWhenUserTriedToRunBotWithIncorrectDataBot();
        actionwords.verifyThatOnRunBotFormErrorMessageAppear(errorMessage);
        actionwords.getScreenshot(fileName4);
    }

    @Test
    public void testcase014RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithTradeLimitLessThenLimitDataSet1() {
        testcase014RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithTradeLimitLessThenLimit(websitelink, "1", "testcase014RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithTradeLimitLessThenLimitDataSet1_1", "testcase014RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithTradeLimitLessThenLimitDataSet1_2", "testcase014RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithTradeLimitLessThenLimitDataSet1_3", "testcase014RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithTradeLimitLessThenLimitDataSet1_4", TRADE_LIMIT_LESS_THEN_LIMIT);
    }

    @Test
    public void testcase014RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithTradeLimitLessThenLimitDataSet2() {
        testcase014RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithTradeLimitLessThenLimit(websitelink, "4", "testcase014RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithTradeLimitLessThenLimitDataSet2_1", "testcase014RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithTradeLimitLessThenLimitDataSet2_2", "testcase014RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithTradeLimitLessThenLimitDataSet2_3", "testcase014RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithTradeLimitLessThenLimitDataSet2_4", TRADE_LIMIT_LESS_THEN_LIMIT);
    }

    public void testcase015RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithTradeLimitMoreThenLimit(String link, String tradeLimit, String fileName1, String fileName2, String fileName3, String fileName4, String errorMessage) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.clickOnBotsBtnOnHomePage();
        actionwords.getScreenshot(fileName1);
        actionwords.clickOnBotRowContainerToOpenBotDetailPageByBotNameFromVariableValue();
        actionwords.clickOnBotRunBtnOnBotDetailsPage();
        actionwords.getScreenshot(fileName2);
        actionwords.setTradeLimitOnBotRunDialog(tradeLimit);
        actionwords.clickOnBotRunBtnOnBotRunDialogWithoutWaitDriver();
        actionwords.getScreenshot(fileName3);
        actionwords.verifyThatRunBotDialogNotCloseWhenUserTriedToRunBotWithIncorrectDataBot();
        actionwords.verifyThatOnRunBotFormErrorMessageNotAppear(errorMessage);
        actionwords.getScreenshot(fileName4);
    }

    @Test
    public void testcase015RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithTradeLimitMoreThenLimitDataSet1() {
        testcase015RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithTradeLimitMoreThenLimit(websitelink, "5", "testcase015RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithTradeLimitMoreThenLimitDataSet1_1", "testcase015RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithTradeLimitMoreThenLimitDataSet1_2", "testcase015RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithTradeLimitMoreThenLimitDataSet1_3", "testcase015RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithTradeLimitMoreThenLimitDataSet1_4", TRADE_LIMIT_LESS_THEN_LIMIT);
    }

    @Test
    public void testcase015RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithTradeLimitMoreThenLimitDataSet2() {
        testcase015RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithTradeLimitMoreThenLimit(websitelink, "100", "testcase015RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithTradeLimitMoreThenLimitDataSet2_1", "testcase015RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithTradeLimitMoreThenLimitDataSet2_2", "testcase015RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithTradeLimitMoreThenLimitDataSet2_3", "testcase015RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithTradeLimitMoreThenLimitDataSet2_4", TRADE_LIMIT_LESS_THEN_LIMIT);
    }

    @Test
    public void testcase015RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithTradeLimitMoreThenLimitDataSet3() {
        testcase015RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithTradeLimitMoreThenLimit(websitelink, "101", "testcase015RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithTradeLimitMoreThenLimitDataSet3_1", "testcase015RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithTradeLimitMoreThenLimitDataSet3_2", "testcase015RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithTradeLimitMoreThenLimitDataSet3_3", "testcase015RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithTradeLimitMoreThenLimitDataSet3_4", TRADE_LIMIT_LESS_THEN_LIMIT);
    }

    public void testcase016RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithEmptyTradeLimit(String link, String tradeLimit, String fileName1, String fileName2, String fileName3, String fileName4) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.clickOnBotsBtnOnHomePage();
        actionwords.getScreenshot(fileName1);
        actionwords.clickOnBotRowContainerToOpenBotDetailPageByBotNameFromVariableValue();
        actionwords.clickOnBotRunBtnOnBotDetailsPage();
        actionwords.getScreenshot(fileName2);
        actionwords.setTradeLimitOnBotRunDialog(tradeLimit);
        actionwords.clickOnBotRunBtnOnBotRunDialogWithoutWaitDriver();
        actionwords.getScreenshot(fileName3);
        actionwords.verifyThatRunBotDialogNotCloseWhenUserTriedToRunBotWithIncorrectDataBot();
        actionwords.getScreenshot(fileName4);
    }

    @Test
    public void testcase016RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithEmptyTradeLimitDataSet1() {
        testcase016RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithEmptyTradeLimit(websitelink, "", "testcase016RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithEmptyTradeLimitDataSet1_1", "testcase016RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithEmptyTradeLimitDataSet1_2", "testcase016RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithEmptyTradeLimitDataSet1_3", "testcase016RedirectToBotDetailsPageAndOpenRunBotDialogAndTryToRunBotWithEmptyTradeLimitDataSet1_4");
    }

    public void testcase017RedirectToBotDetailsPageAndOpenRunBotDialogAndRunBotWithTheSameFromAndToDate(String link, String startBalance, String runFrom, String runTo, String tradingFee, String tradeLimit) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.clickOnBotsBtnOnHomePage();
        actionwords.getScreenshot("testcase017RedirectToBotDetailsPageAndOpenRunBotDialogAndRunBotWithTheSameFromAndToDate_1");
        actionwords.clickOnBotRowContainerToOpenBotDetailPageByBotNameFromVariableValue();
        actionwords.clickOnBotRunBtnOnBotDetailsPage();
        actionwords.getScreenshot("testcase017RedirectToBotDetailsPageAndOpenRunBotDialogAndRunBotWithTheSameFromAndToDate_2");
        actionwords.setStartBalanceOnBotRunDialog(startBalance);
        actionwords.setRunFromOnBotRunPage(runFrom);
        actionwords.setRunToOnBotRunPage(runTo);
        actionwords.setTradingFeeOnBotRunDialog(tradingFee);
        actionwords.setTradeLimitOnBotRunDialog(tradeLimit);
        actionwords.clickOnBotRunBtnOnBotRunDialog();
        actionwords.getScreenshot("testcase017RedirectToBotDetailsPageAndOpenRunBotDialogAndRunBotWithTheSameFromAndToDate_3");
        actionwords.verifyThatRunBotDialogCloseWhenUserTriedToRunBotWithCorrectDataBot();
        actionwords.getScreenshot("testcase017RedirectToBotDetailsPageAndOpenRunBotDialogAndRunBotWithTheSameFromAndToDate_4");
        actionwords.refreshPage();
        actionwords.getScreenshot("testcase017RedirectToBotDetailsPageAndOpenRunBotDialogAndRunBotWithTheSameFromAndToDate_5");
    }

    @Test
    public void testcase017RedirectToBotDetailsPageAndOpenRunBotDialogAndRunBotWithTheSameFromAndToDateDataSet1() {
        testcase017RedirectToBotDetailsPageAndOpenRunBotDialogAndRunBotWithTheSameFromAndToDate(websitelink, "1000", "10/01/2021", "10/10/2021", "0.5", "10");
    }

    public void testcase018RedirectToBotDetailsPageAndOpenRunBotDialogAndRunBotWithDoubleTradingFee(String link, String startBalance, String tradingFee, String tradeLimit) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.clickOnBotsBtnOnHomePage();
        actionwords.getScreenshot("testcase018RedirectToBotDetailsPageAndOpenRunBotDialogAndRunBotWithDoubleTradingFee_1");
        actionwords.clickOnBotRowContainerToOpenBotDetailPageByBotNameFromVariableValue();
        actionwords.clickOnBotRunBtnOnBotDetailsPage();
        actionwords.getScreenshot("testcase018RedirectToBotDetailsPageAndOpenRunBotDialogAndRunBotWithDoubleTradingFee_2");
        actionwords.setStartBalanceOnBotRunDialog(startBalance);
        actionwords.setTradingFeeOnBotRunDialog(tradingFee);
        actionwords.setTradeLimitOnBotRunDialog(tradeLimit);
        actionwords.clickOnBotRunBtnOnBotRunDialog();
        actionwords.getScreenshot("testcase018RedirectToBotDetailsPageAndOpenRunBotDialogAndRunBotWithDoubleTradingFee_3");
        actionwords.verifyThatRunBotDialogCloseWhenUserTriedToRunBotWithCorrectDataBot();
        actionwords.getScreenshot("testcase018RedirectToBotDetailsPageAndOpenRunBotDialogAndRunBotWithDoubleTradingFee_4");
        actionwords.refreshPage();
        actionwords.getScreenshot("testcase018RedirectToBotDetailsPageAndOpenRunBotDialogAndRunBotWithDoubleTradingFee_5");
    }

    @Test
    public void testcase018RedirectToBotDetailsPageAndOpenRunBotDialogAndRunBotWithDoubleTradingFeeDataSet1() {
        testcase018RedirectToBotDetailsPageAndOpenRunBotDialogAndRunBotWithDoubleTradingFee(websitelink, "1000", "1.5", "10");
    }

    public void testcase019RedirectToBotDetailsPageAndOpenRunBotDialogAndRunBotWithIntTradingFee(String link, String startBalance, String tradingFee, String tradeLimit) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.clickOnBotsBtnOnHomePage();
        actionwords.getScreenshot("testcase019RedirectToBotDetailsPageAndOpenRunBotDialogAndRunBotWithIntTradingFee_1");
        actionwords.clickOnBotRowContainerToOpenBotDetailPageByBotNameFromVariableValue();
        actionwords.clickOnBotRunBtnOnBotDetailsPage();
        actionwords.getScreenshot("testcase019RedirectToBotDetailsPageAndOpenRunBotDialogAndRunBotWithIntTradingFee_2");
        actionwords.setStartBalanceOnBotRunDialog(startBalance);
        actionwords.setTradingFeeOnBotRunDialog(tradingFee);
        actionwords.setTradeLimitOnBotRunDialog(tradeLimit);
        actionwords.clickOnBotRunBtnOnBotRunDialog();
        actionwords.getScreenshot("testcase019RedirectToBotDetailsPageAndOpenRunBotDialogAndRunBotWithIntTradingFee_3");
        actionwords.verifyThatRunBotDialogCloseWhenUserTriedToRunBotWithCorrectDataBot();
        actionwords.getScreenshot("testcase019RedirectToBotDetailsPageAndOpenRunBotDialogAndRunBotWithIntTradingFee_4");
        actionwords.refreshPage();
        actionwords.getScreenshot("testcase019RedirectToBotDetailsPageAndOpenRunBotDialogAndRunBotWithIntTradingFee_5");
    }

    @Test
    public void testcase019RedirectToBotDetailsPageAndOpenRunBotDialogAndRunBotWithIntTradingFeeDataSet1() {
        testcase019RedirectToBotDetailsPageAndOpenRunBotDialogAndRunBotWithIntTradingFee(websitelink, "1000", "5", "5");
    }

    public void testcase020RedirectToBotDetailsPageAndOpenRunBotDialogAndRunBotWithZeroTradingFee(String link, String startBalance, String tradingFee, String tradeLimit) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.clickOnBotsBtnOnHomePage();
        actionwords.getScreenshot("testcase020RedirectToBotDetailsPageAndOpenRunBotDialogAndRunBotWithZeroTradingFee_1");
        actionwords.clickOnBotRowContainerToOpenBotDetailPageByBotNameFromVariableValue();
        actionwords.clickOnBotRunBtnOnBotDetailsPage();
        actionwords.getScreenshot("testcase020RedirectToBotDetailsPageAndOpenRunBotDialogAndRunBotWithZeroTradingFee_2");
        actionwords.setStartBalanceOnBotRunDialog(startBalance);
        actionwords.setTradingFeeOnBotRunDialog(tradingFee);
        actionwords.setTradeLimitOnBotRunDialog(tradeLimit);
        actionwords.clickOnBotRunBtnOnBotRunDialog();
        actionwords.getScreenshot("testcase020RedirectToBotDetailsPageAndOpenRunBotDialogAndRunBotWithZeroTradingFee_3");
        actionwords.verifyThatRunBotDialogCloseWhenUserTriedToRunBotWithCorrectDataBot();
        actionwords.getScreenshot("testcase020RedirectToBotDetailsPageAndOpenRunBotDialogAndRunBotWithZeroTradingFee_4");
        actionwords.refreshPage();
        actionwords.getScreenshot("testcase020RedirectToBotDetailsPageAndOpenRunBotDialogAndRunBotWithZeroTradingFee_5");
    }

    @Test
    public void testcase020RedirectToBotDetailsPageAndOpenRunBotDialogAndRunBotWithZeroTradingFeeDataSet1() {
        testcase020RedirectToBotDetailsPageAndOpenRunBotDialogAndRunBotWithZeroTradingFee(websitelink, "1000", "0", "10");
    }
}
package prizma.maxiru.tests;

import org.junit.FixMethodOrder;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import prizma.maxiru.framework.common.*;

import static prizma.maxiru.framework.common.PrizmaDriver.*;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)

public class WebSDKRunShortTrendingBotAndValidateBotPositionsTableSortingTests {
    @Rule
    public PrizmaScreenshotRule screenshotRule = new PrizmaScreenshotRule();

    public Actionwords actionwords = new Actionwords();

    public void testcase001RedirectToBotDetailsPageAndRunBotWithShortPositionTrendingDialogWithValidValuesAndValidateBotLandingPageAfter(String link, String fileName, String startBalance, String runFrom, String runTo, String tradingFee, String tradeLimit, String botName) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.deleteTheLogsFolder();
        actionwords.createTheLogsFolder();
        actionwords.createTheLogsFileForFailedTests(fileName);
        actionwords.clickOnBotsBtnOnHomePage();
        actionwords.getScreenshot("testcase001RedirectToBotDetailsPageAndRunBotWithShortPositionTrendingDialogWithValidValuesAndValidateBotLandingPageAfter_1");
        actionwords.clickOnBotRowContainerToOpenBotDetailPageByBotName(botName);
        actionwords.clickOnBotRunBtnOnBotDetailsPage();
        actionwords.getScreenshot("testcase001RedirectToBotDetailsPageAndRunBotWithShortPositionTrendingDialogWithValidValuesAndValidateBotLandingPageAfter_2");
        actionwords.setStartBalanceOnBotRunDialog(startBalance);
        actionwords.setRunFromOnBotRunPage(runFrom);
        actionwords.setRunToOnBotRunPage(runTo);
        actionwords.setTradingFeeOnBotRunDialog(tradingFee);
        actionwords.setTradeLimitOnBotRunDialog(tradeLimit);
        actionwords.getScreenshot("testcase001RedirectToBotDetailsPageAndRunBotWithShortPositionTrendingDialogWithValidValuesAndValidateBotLandingPageAfter_3");
        actionwords.clickOnBotRunBtnOnBotRunDialog();
        actionwords.getScreenshot("testcase001RedirectToBotDetailsPageAndRunBotWithShortPositionTrendingDialogWithValidValuesAndValidateBotLandingPageAfter_4");
        actionwords.verifyThatRunBotDialogCloseWhenUserTriedToRunBotWithCorrectDataBot();
        actionwords.refreshPage();
        actionwords.openWebsite(link);
        actionwords.clickOnBotsBtnOnHomePage();
        actionwords.getScreenshot("testcase001RedirectToBotDetailsPageAndRunBotWithShortPositionTrendingDialogWithValidValuesAndValidateBotLandingPageAfter_5");
        actionwords.clickOnBotRowContainerToOpenBotDetailPageByBotName(botName);
        actionwords.getAndStoreTotalCountOfAllTrendingPositions();
        actionwords.getAndStoreTotalCountOfOpenAndCloseTrendingPositions();
        actionwords.getAndStoreInOpenTrendingPositionsValue();
        actionwords.getAndCalculateAllProfitCurrencyValues();
        actionwords.getAndCalculateAllProfitCurrencyValuesForClosedPositions();
        actionwords.getAndCalculateAllProfitCurrencyValuesForOpenPositions();
        actionwords.getAndCalculateAllProfitPercentageValues();
        actionwords.getAndCalculateAllProfitPercentageValuesForClosedPositions();
        actionwords.getAndCalculateAllProfitPercentageValuesForOpenPositions();
        actionwords.getAndCalculateTotalBalance();
        actionwords.getAndCalculateAvailableBalance();
        actionwords.getAndCalculateAverageProfitCurrency();
        actionwords.getAndCalculateAverageProfitPercentage();
        actionwords.getAndCalculateTotalProfitPercentageValues();
        actionwords.getAndCalculateBotEarnedValues();
        actionwords.getScreenshot("testcase001RedirectToBotDetailsPageAndRunBotWithShortPositionTrendingDialogWithValidValuesAndValidateBotLandingPageAfter_6");
        actionwords.refreshPage();
        actionwords.openWebsite(link);
        actionwords.clickOnBotsBtnOnHomePage();
        actionwords.getScreenshot("testcase001RedirectToBotDetailsPageAndRunBotWithShortPositionTrendingDialogWithValidValuesAndValidateBotLandingPageAfter_7");
        actionwords.verifyThatBotNameOnPositionFromVariableValueIsCorrect(botName);
        actionwords.verifyThatBotLastRunOnPositionFromVariableValueIsCorrectIfValidateWithVariableValue();
        actionwords.verifyThatBotAverangeProfitCurrencyOnPositionFromVariableValueIsCorrectIfValidateWithVariableValue();
        actionwords.verifyThatBotAverangeProfitPercentageOnPositionFromVariableValueIsCorrectIfValidateWithVariableValue();
        actionwords.verifyThatBotProfitCurrencyOnPositionFromVariableValueIsCorrectIfValidateWithVariableValue();
        actionwords.verifyThatBotProfitPercentageOnPositionFromVariableValueIsCorrectIfValidateWithVariableValue();
        actionwords.getScreenshot("testcase001RedirectToBotDetailsPageAndRunBotWithShortPositionTrendingDialogWithValidValuesAndValidateBotLandingPageAfter_8");
    }

    @Test
    public void testcase001RedirectToBotDetailsPageAndRunBotWithShortPositionTrendingDialogWithValidValuesAndValidateBotLandingPageAfterDataSet1() {
        testcase001RedirectToBotDetailsPageAndRunBotWithShortPositionTrendingDialogWithValidValuesAndValidateBotLandingPageAfter(websitelink, "bot_short_landing_page.txt", "1000", "10/01/2021", "10/10/2021", "0.5", "10", "test_short");
    }

    public void testcase002RedirectToBotDetailsPageAndValidateBotPositionsTableSortingByDate(String link, String startBalance, String tradingFee, String tradeLimit, String botName) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.clickOnBotsBtnOnHomePage();
        actionwords.getScreenshot("testcase002RedirectToBotDetailsPageAndValidateBotPositionsTableSortingByDate_1");
        actionwords.clickOnBotRowContainerToOpenBotDetailPageByBotName(botName);
        actionwords.clickOnBotRunBtnOnBotDetailsPage();
        actionwords.getScreenshot("testcase002RedirectToBotDetailsPageAndValidateBotPositionsTableSortingByDate_2");
        actionwords.setStartBalanceOnBotRunDialog(startBalance);
        actionwords.setTradingFeeOnBotRunDialog(tradingFee);
        actionwords.setTradeLimitOnBotRunDialog(tradeLimit);
        actionwords.getScreenshot("testcase002RedirectToBotDetailsPageAndValidateBotPositionsTableSortingByDate_3");
        actionwords.clickOnBotRunBtnOnBotRunDialog();
        actionwords.getScreenshot("testcase002RedirectToBotDetailsPageAndValidateBotPositionsTableSortingByDate_4");
        actionwords.verifyThatRunBotDialogCloseWhenUserTriedToRunBotWithCorrectDataBot();
        actionwords.refreshPage();
        actionwords.openWebsite(link);
        actionwords.clickOnBotsBtnOnHomePage();
        actionwords.getScreenshot("testcase002RedirectToBotDetailsPageAndValidateBotPositionsTableSortingByDate_5");
        actionwords.clickOnBotRowContainerToOpenBotDetailPageByBotName(botName);
        actionwords.getScreenshot("testcase002RedirectToBotDetailsPageAndValidateBotPositionsTableSortingByDate_6");
        actionwords.clickOnPositionsTableDateSortingToggle();
        actionwords.getScreenshot("testcase002RedirectToBotDetailsPageAndValidateBotPositionsTableSortingByDate_7");
        actionwords.clickOnPositionsTableDateSortingToggle();
        actionwords.getScreenshot("testcase002RedirectToBotDetailsPageAndValidateBotPositionsTableSortingByDate_8");
        actionwords.clickOnPositionsTableDateSortingToggle();
        actionwords.getScreenshot("testcase002RedirectToBotDetailsPageAndValidateBotPositionsTableSortingByDate_9");
    }

    @Test
    public void testcase002RedirectToBotDetailsPageAndValidateBotPositionsTableSortingByDateDataSet1() {
        testcase002RedirectToBotDetailsPageAndValidateBotPositionsTableSortingByDate(websitelink, "1000", "0.5", "10", "test_short");
    }

    public void testcase003RedirectToBotDetailsPageAndValidateBotPositionsTableSortingByPair(String link, String startBalance, String tradingFee, String tradeLimit, String botName) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.clickOnBotsBtnOnHomePage();
        actionwords.getScreenshot("testcase003RedirectToBotDetailsPageAndValidateBotPositionsTableSortingByPair_1");
        actionwords.clickOnBotRowContainerToOpenBotDetailPageByBotName(botName);
        actionwords.clickOnBotRunBtnOnBotDetailsPage();
        actionwords.getScreenshot("testcase003RedirectToBotDetailsPageAndValidateBotPositionsTableSortingByPair_2");
        actionwords.setStartBalanceOnBotRunDialog(startBalance);
        actionwords.setTradingFeeOnBotRunDialog(tradingFee);
        actionwords.setTradeLimitOnBotRunDialog(tradeLimit);
        actionwords.getScreenshot("testcase003RedirectToBotDetailsPageAndValidateBotPositionsTableSortingByPair_3");
        actionwords.clickOnBotRunBtnOnBotRunDialog();
        actionwords.getScreenshot("testcase003RedirectToBotDetailsPageAndValidateBotPositionsTableSortingByPair_4");
        actionwords.verifyThatRunBotDialogCloseWhenUserTriedToRunBotWithCorrectDataBot();
        actionwords.refreshPage();
        actionwords.openWebsite(link);
        actionwords.clickOnBotsBtnOnHomePage();
        actionwords.getScreenshot("testcase003RedirectToBotDetailsPageAndValidateBotPositionsTableSortingByPair_5");
        actionwords.clickOnBotRowContainerToOpenBotDetailPageByBotName(botName);
        actionwords.getScreenshot("testcase003RedirectToBotDetailsPageAndValidateBotPositionsTableSortingByPair_6");
        actionwords.clickOnPositionsTablePairSortingToggle();
        actionwords.getScreenshot("testcase003RedirectToBotDetailsPageAndValidateBotPositionsTableSortingByPair_7");
        actionwords.clickOnPositionsTablePairSortingToggle();
        actionwords.getScreenshot("testcase003RedirectToBotDetailsPageAndValidateBotPositionsTableSortingByPair_8");
        actionwords.clickOnPositionsTablePairSortingToggle();
        actionwords.getScreenshot("testcase003RedirectToBotDetailsPageAndValidateBotPositionsTableSortingByPair_9");
    }

    @Test
    public void testcase003RedirectToBotDetailsPageAndValidateBotPositionsTableSortingByPairDataSet1() {
        testcase003RedirectToBotDetailsPageAndValidateBotPositionsTableSortingByPair(websitelink, "1000", "0.5", "10", "test_short");
    }

    public void testcase004RedirectToBotDetailsPageAndValidateBotPositionsTableSortingByType(String link, String startBalance, String tradingFee, String tradeLimit, String botName) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.clickOnBotsBtnOnHomePage();
        actionwords.getScreenshot("testcase004RedirectToBotDetailsPageAndValidateBotPositionsTableSortingByType_1");
        actionwords.clickOnBotRowContainerToOpenBotDetailPageByBotName(botName);
        actionwords.clickOnBotRunBtnOnBotDetailsPage();
        actionwords.getScreenshot("testcase004RedirectToBotDetailsPageAndValidateBotPositionsTableSortingByType_2");
        actionwords.setStartBalanceOnBotRunDialog(startBalance);
        actionwords.setTradingFeeOnBotRunDialog(tradingFee);
        actionwords.setTradeLimitOnBotRunDialog(tradeLimit);
        actionwords.getScreenshot("testcase004RedirectToBotDetailsPageAndValidateBotPositionsTableSortingByType_3");
        actionwords.clickOnBotRunBtnOnBotRunDialog();
        actionwords.getScreenshot("testcase004RedirectToBotDetailsPageAndValidateBotPositionsTableSortingByType_4");
        actionwords.verifyThatRunBotDialogCloseWhenUserTriedToRunBotWithCorrectDataBot();
        actionwords.refreshPage();
        actionwords.openWebsite(link);
        actionwords.clickOnBotsBtnOnHomePage();
        actionwords.getScreenshot("testcase004RedirectToBotDetailsPageAndValidateBotPositionsTableSortingByType_5");
        actionwords.clickOnBotRowContainerToOpenBotDetailPageByBotName(botName);
        actionwords.getScreenshot("testcase004RedirectToBotDetailsPageAndValidateBotPositionsTableSortingByType_6");
        actionwords.clickOnPositionsTableTypeSortingToggle();
        actionwords.getScreenshot("testcase004RedirectToBotDetailsPageAndValidateBotPositionsTableSortingByType_7");
        actionwords.clickOnPositionsTableTypeSortingToggle();
        actionwords.getScreenshot("testcase004RedirectToBotDetailsPageAndValidateBotPositionsTableSortingByType_8");
        actionwords.clickOnPositionsTableTypeSortingToggle();
        actionwords.getScreenshot("testcase004RedirectToBotDetailsPageAndValidateBotPositionsTableSortingByType_9");
    }

    @Test
    public void testcase004RedirectToBotDetailsPageAndValidateBotPositionsTableSortingByTypeDataSet1() {
        testcase004RedirectToBotDetailsPageAndValidateBotPositionsTableSortingByType(websitelink, "1000", "0.5", "10", "test_short");
    }

    public void testcase005RedirectToBotDetailsPageAndValidateBotPositionsTableSortingByProfit(String link, String startBalance, String tradingFee, String tradeLimit, String botName) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.clickOnBotsBtnOnHomePage();
        actionwords.getScreenshot("testcase005RedirectToBotDetailsPageAndValidateBotPositionsTableSortingByProfit_1");
        actionwords.clickOnBotRowContainerToOpenBotDetailPageByBotName(botName);
        actionwords.clickOnBotRunBtnOnBotDetailsPage();
        actionwords.getScreenshot("testcase005RedirectToBotDetailsPageAndValidateBotPositionsTableSortingByProfit_2");
        actionwords.setStartBalanceOnBotRunDialog(startBalance);
        actionwords.setTradingFeeOnBotRunDialog(tradingFee);
        actionwords.setTradeLimitOnBotRunDialog(tradeLimit);
        actionwords.getScreenshot("testcase005RedirectToBotDetailsPageAndValidateBotPositionsTableSortingByProfit_3");
        actionwords.clickOnBotRunBtnOnBotRunDialog();
        actionwords.getScreenshot("testcase005RedirectToBotDetailsPageAndValidateBotPositionsTableSortingByProfit_4");
        actionwords.verifyThatRunBotDialogCloseWhenUserTriedToRunBotWithCorrectDataBot();
        actionwords.refreshPage();
        actionwords.openWebsite(link);
        actionwords.clickOnBotsBtnOnHomePage();
        actionwords.getScreenshot("testcase005RedirectToBotDetailsPageAndValidateBotPositionsTableSortingByProfit_5");
        actionwords.clickOnBotRowContainerToOpenBotDetailPageByBotName(botName);
        actionwords.getScreenshot("testcase005RedirectToBotDetailsPageAndValidateBotPositionsTableSortingByProfit_6");
        actionwords.clickOnPositionsTableProfitSortingToggle();
        actionwords.getScreenshot("testcase005RedirectToBotDetailsPageAndValidateBotPositionsTableSortingByProfit_7");
        actionwords.clickOnPositionsTableProfitSortingToggle();
        actionwords.getScreenshot("testcase005RedirectToBotDetailsPageAndValidateBotPositionsTableSortingByProfit_8");
        actionwords.clickOnPositionsTableProfitSortingToggle();
        actionwords.getScreenshot("testcase005RedirectToBotDetailsPageAndValidateBotPositionsTableSortingByProfit_9");
    }

    @Test
    public void testcase005RedirectToBotDetailsPageAndValidateBotPositionsTableSortingByProfitDataSet1() {
        testcase005RedirectToBotDetailsPageAndValidateBotPositionsTableSortingByProfit(websitelink, "1000", "0.5", "10", "test_short");
    }
}
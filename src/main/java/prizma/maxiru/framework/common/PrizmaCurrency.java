package prizma.maxiru.framework.common;

public class PrizmaCurrency {
    public static String CURRENCY_1 = "ATOM";
    public static String CURRENCY_2 = "BAT";
    public static String CURRENCY_3 = "BCC";
    public static String CURRENCY_4 = "DASH";
    public static String CURRENCY_5 = "ADA";
    public static String CURRENCY_6 = "BTT";
    public static String CURRENCY_7 = "BNB";
    public static String CURRENCY_8 = "ENJ";
}
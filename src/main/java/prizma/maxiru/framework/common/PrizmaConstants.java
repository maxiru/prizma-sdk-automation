package prizma.maxiru.framework.common;

public class PrizmaConstants {
    public static final String PLATFORM_FIREFOX = "Firefox";
    public static final String PLATFORM_CHROME = "Chrome";
    public static final String PLATFORM_SAFARI = "Safari";
    public static final String SELENOID = "selenoid";
    public static final String DOWNLOAD_FILES_FOLDER_PATH = System.getProperty("user.dir") + "/downloads/";
    public static final String LOGS_FOLDER_PATH = System.getProperty("user.dir") + "/logs/";
    public static String FILE_NAME_FOR_FUTURE_TESTS = "";
    public static final Integer WAIT_TIME = 1500;
    public static final Integer LESS_WAIT_TIME = 2500;
    public static final Integer SHORT_WAIT_TIME = 4000;
    public static final Integer LONG_WAIT_TIME = 10000;
    public static final Integer HOLD_WAIT_TIME = 10000;
    public static final Integer CHARTING_WAIT_TIME = 40000;
    public static final Integer DOWNLOAD_WAIT_TIME = 80000;
}
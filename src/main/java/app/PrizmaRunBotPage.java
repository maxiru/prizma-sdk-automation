package app;

import org.junit.*;
import org.openqa.selenium.*;

import java.util.*;

import prizma.maxiru.framework.common.*;

import static app.PrizmaBotsDetailsPage.*;

public class PrizmaRunBotPage {

    //Elements with XPath, Tag and CCS selector
    public static final String BOT_RUN_BTN = ".bot-detail-page__run-button";
    public static final String DATA_SOURCE_INPUT = "run-form__dataSource";
    public static final String START_BALANCE_INPUT = "run-form__startBalance";
    public static final String RUN_FROM_MONTH_INPUT = "month";
    public static final String RUN_FROM_DAY_INPUT = "day";
    public static final String RUN_FROM_YEAR_INPUT = "year";
    public static final String RUN_FROM_INPUT = "run-form__from";
    public static final String RUN_TO_MONTH_INPUT = "month";
    public static final String RUN_TO_DAY_INPUT = "day";
    public static final String RUN_TO_YEAR_INPUT = "year";
    public static final String RUN_TO_INPUT = "run-form__to";
    public static final String TRADING_FEE_INPUT = "run-form__fee";
    public static final String TRADE_LIMIT_INPUT = "run-form__tradeLimit";
    public static final String BOT_RUN_BTN_ON_BOT_RUN_PAGE = "//button[text()='Run']";
    public static final String CLOSE_BTN_ON_BOT_RUN_PAGE = "span[class='icon modal__close-button']";
    public static final String STOP_BTN_ON_BOT_RUN_PAGE = "//span[contains(@class,'exit')]";

    //Other message
    public static String TO_DATE_IS_LESS_THEN_FROM_DATE = "To: Date cannot be before 10/10/2021";
    public static String START_BALANCE_LESS_THEN_LIMIT = "Start balance: The value cannot be less than 100";
    public static String TRENDING_FEE_MORE_THEN_LIMIT = "Trading fee: The value cannot be more than 100";
    public static String TRADE_LIMIT_LESS_THEN_LIMIT = "Trade limit: The value cannot be less than 5";

    /***
     * Methods to get the elements
     *
     * @return WebElement
     */
    public static WebElement getBotRunBtn() {
        WebElement webElement;

        if (PrizmaDriver.isSelenoid()) {
            webElement = PrizmaDriver.getCurrentDriver().findElement(By.cssSelector(BOT_RUN_BTN));
            if (webElement.isDisplayed() == false) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + BOT_RUN_BTN + " is empty. Check video and logs, or something was changed on DD side.");
            }
            return webElement;
        } else {
            webElement = PrizmaDriver.getCurrentRemoveWebDriver().findElement(By.cssSelector(BOT_RUN_BTN));
            if (webElement.isDisplayed() == false) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + BOT_RUN_BTN + " is empty. Check video and logs, or something was changed on DD side.");
            }
            return webElement;
        }
    }

    public static WebElement getDataSourceInput() {
        List<WebElement> list;
        if (PrizmaDriver.isSelenoid()) {
            list = PrizmaDriver.getCurrentDriver().findElements(By.id(DATA_SOURCE_INPUT));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + DATA_SOURCE_INPUT + " is empty. Check video and logs, or something was changed on SDK side.");
            }
            return list.get(0);
        } else {
            list = PrizmaDriver.getCurrentRemoveWebDriver().findElements(By.id(DATA_SOURCE_INPUT));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + DATA_SOURCE_INPUT + " is empty. Check video and logs, or something was changed on SDK side.");
            }
            return list.get(0);
        }
    }

    public static WebElement getStartBalanceInput() {
        List<WebElement> list;
        if (PrizmaDriver.isSelenoid()) {
            list = PrizmaDriver.getCurrentDriver().findElements(By.id(START_BALANCE_INPUT));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + START_BALANCE_INPUT + " is empty. Check video and logs, or something was changed on SDK side.");
            }
            return list.get(0);
        } else {
            list = PrizmaDriver.getCurrentRemoveWebDriver().findElements(By.id(START_BALANCE_INPUT));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + START_BALANCE_INPUT + " is empty. Check video and logs, or something was changed on SDK side.");
            }
            return list.get(0);
        }
    }

    public static WebElement getRunFromMonthInput() {
        List<WebElement> list;
        if (PrizmaDriver.isSelenoid()) {
            list = PrizmaDriver.getCurrentDriver().findElements(By.name(RUN_FROM_MONTH_INPUT));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + RUN_FROM_MONTH_INPUT + " is empty. Check video and logs, or something was changed on SDK side.");
            }
            return list.get(0);
        } else {
            list = PrizmaDriver.getCurrentRemoveWebDriver().findElements(By.name(RUN_FROM_MONTH_INPUT));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + RUN_FROM_MONTH_INPUT + " is empty. Check video and logs, or something was changed on SDK side.");
            }
            return list.get(0);
        }
    }

    public static WebElement getRunFromDayInput() {
        List<WebElement> list;
        if (PrizmaDriver.isSelenoid()) {
            list = PrizmaDriver.getCurrentDriver().findElements(By.name(RUN_FROM_DAY_INPUT));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + RUN_FROM_DAY_INPUT + " is empty. Check video and logs, or something was changed on SDK side.");
            }
            return list.get(0);
        } else {
            list = PrizmaDriver.getCurrentRemoveWebDriver().findElements(By.name(RUN_FROM_DAY_INPUT));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + RUN_FROM_DAY_INPUT + " is empty. Check video and logs, or something was changed on SDK side.");
            }
            return list.get(0);
        }
    }

    public static WebElement getRunFromYearInput() {
        List<WebElement> list;
        if (PrizmaDriver.isSelenoid()) {
            list = PrizmaDriver.getCurrentDriver().findElements(By.name(RUN_FROM_YEAR_INPUT));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + RUN_FROM_YEAR_INPUT + " is empty. Check video and logs, or something was changed on SDK side.");
            }
            return list.get(0);
        } else {
            list = PrizmaDriver.getCurrentRemoveWebDriver().findElements(By.name(RUN_FROM_YEAR_INPUT));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + RUN_FROM_DAY_INPUT + " is empty. Check video and logs, or something was changed on SDK side.");
            }
            return list.get(0);
        }
    }

    public static WebElement getRunFromInput() {
        List<WebElement> list;
        if (PrizmaDriver.isSelenoid()) {
            list = PrizmaDriver.getCurrentDriver().findElements(By.id(RUN_FROM_INPUT));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + RUN_FROM_INPUT + " is empty. Check video and logs, or something was changed on SDK side.");
            }
            return list.get(0);
        } else {
            list = PrizmaDriver.getCurrentRemoveWebDriver().findElements(By.id(RUN_FROM_INPUT));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + RUN_FROM_INPUT + " is empty. Check video and logs, or something was changed on SDK side.");
            }
            return list.get(0);
        }
    }

    public static WebElement getRunToMonthInput() {
        List<WebElement> list;
        if (PrizmaDriver.isSelenoid()) {
            list = PrizmaDriver.getCurrentDriver().findElements(By.name(RUN_TO_MONTH_INPUT));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + RUN_TO_MONTH_INPUT + " is empty. Check video and logs, or something was changed on SDK side.");
            }
            return list.get(list.size() - 1);
        } else {
            list = PrizmaDriver.getCurrentRemoveWebDriver().findElements(By.name(RUN_TO_MONTH_INPUT));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + RUN_TO_MONTH_INPUT + " is empty. Check video and logs, or something was changed on SDK side.");
            }
            return list.get(list.size() - 1);
        }
    }

    public static WebElement getRunToDayInput() {
        List<WebElement> list;
        if (PrizmaDriver.isSelenoid()) {
            list = PrizmaDriver.getCurrentDriver().findElements(By.name(RUN_TO_DAY_INPUT));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + RUN_TO_DAY_INPUT + " is empty. Check video and logs, or something was changed on SDK side.");
            }
            return list.get(list.size() - 1);
        } else {
            list = PrizmaDriver.getCurrentRemoveWebDriver().findElements(By.name(RUN_TO_DAY_INPUT));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + RUN_TO_DAY_INPUT + " is empty. Check video and logs, or something was changed on SDK side.");
            }
            return list.get(list.size() - 1);
        }
    }

    public static WebElement getRunToYearInput() {
        List<WebElement> list;
        if (PrizmaDriver.isSelenoid()) {
            list = PrizmaDriver.getCurrentDriver().findElements(By.name(RUN_TO_YEAR_INPUT));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + RUN_TO_YEAR_INPUT + " is empty. Check video and logs, or something was changed on SDK side.");
            }
            return list.get(list.size() - 1);
        } else {
            list = PrizmaDriver.getCurrentRemoveWebDriver().findElements(By.name(RUN_TO_YEAR_INPUT));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + RUN_TO_YEAR_INPUT + " is empty. Check video and logs, or something was changed on SDK side.");
            }
            return list.get(list.size() - 1);
        }
    }

    public static WebElement getRunToInput() {
        List<WebElement> list;
        if (PrizmaDriver.isSelenoid()) {
            list = PrizmaDriver.getCurrentDriver().findElements(By.id(RUN_TO_INPUT));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + RUN_TO_INPUT + " is empty. Check video and logs, or something was changed on SDK side.");
            }
            return list.get(list.size() - 1);
        } else {
            list = PrizmaDriver.getCurrentRemoveWebDriver().findElements(By.id(RUN_TO_INPUT));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + RUN_TO_INPUT + " is empty. Check video and logs, or something was changed on SDK side.");
            }
            return list.get(list.size() - 1);
        }
    }

    public static WebElement getTradingFeeInput() {
        List<WebElement> list;
        if (PrizmaDriver.isSelenoid()) {
            list = PrizmaDriver.getCurrentDriver().findElements(By.id(TRADING_FEE_INPUT));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + TRADING_FEE_INPUT + " is empty. Check video and logs, or something was changed on SDK side.");
            }
            return list.get(0);
        } else {
            list = PrizmaDriver.getCurrentRemoveWebDriver().findElements(By.id(TRADING_FEE_INPUT));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + TRADING_FEE_INPUT + " is empty. Check video and logs, or something was changed on SDK side.");
            }
            return list.get(0);
        }
    }

    public static WebElement getTradeLimitInput() {
        List<WebElement> list;
        if (PrizmaDriver.isSelenoid()) {
            list = PrizmaDriver.getCurrentDriver().findElements(By.id(TRADE_LIMIT_INPUT));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + TRADE_LIMIT_INPUT + " is empty. Check video and logs, or something was changed on SDK side.");
            }
            return list.get(0);
        } else {
            list = PrizmaDriver.getCurrentRemoveWebDriver().findElements(By.id(TRADE_LIMIT_INPUT));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + TRADE_LIMIT_INPUT + " is empty. Check video and logs, or something was changed on DD side.");
            }
            return list.get(0);
        }
    }

    public static WebElement getBotRunBtnOnBotRunPage() {
        List<WebElement> list;
        if (PrizmaDriver.isSelenoid()) {
            list = PrizmaDriver.getCurrentDriver().findElements(By.xpath(BOT_RUN_BTN_ON_BOT_RUN_PAGE));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + BOT_RUN_BTN_ON_BOT_RUN_PAGE + " is empty. Check video and logs, or something was changed on SDK side.");
            }
            return list.get(list.size() - 1);
        } else {
            list = PrizmaDriver.getCurrentRemoveWebDriver().findElements(By.xpath(BOT_RUN_BTN_ON_BOT_RUN_PAGE));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + BOT_RUN_BTN_ON_BOT_RUN_PAGE + " is empty. Check video and logs, or something was changed on SDK side.");
            }
            return list.get(list.size() - 1);
        }
    }

    public static WebElement getCloseBtnOnBotRunPage() {
        List<WebElement> list;
        if (PrizmaDriver.isSelenoid()) {
            list = PrizmaDriver.getCurrentDriver().findElements(By.cssSelector(CLOSE_BTN_ON_BOT_RUN_PAGE));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + CLOSE_BTN_ON_BOT_RUN_PAGE + " is empty. Check video and logs, or something was changed on SDK side.");
            }
            return list.get(0);
        } else {
            list = PrizmaDriver.getCurrentRemoveWebDriver().findElements(By.cssSelector(CLOSE_BTN_ON_BOT_RUN_PAGE));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + CLOSE_BTN_ON_BOT_RUN_PAGE + " is empty. Check video and logs, or something was changed on SDK side.");
            }
            return list.get(0);
        }
    }

    public static WebElement getStopBtnOnBotRunPage() {
        List<WebElement> list;
        if (PrizmaDriver.isSelenoid()) {
            list = PrizmaDriver.getCurrentDriver().findElements(By.cssSelector(STOP_BTN_ON_BOT_RUN_PAGE));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + STOP_BTN_ON_BOT_RUN_PAGE + " is empty. Check video and logs, or something was changed on SDK side.");
            }
            return list.get(0);
        } else {
            list = PrizmaDriver.getCurrentRemoveWebDriver().findElements(By.cssSelector(STOP_BTN_ON_BOT_RUN_PAGE));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + STOP_BTN_ON_BOT_RUN_PAGE + " is empty. Check video and logs, or something was changed on SDK side.");
            }
            return list.get(0);
        }
    }

    /***
     * Methods to click on the elements
     *
     */
    public static void clickOnBotRunBtnOnBotDetailsPage() {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
        getBotRunBtn().click();
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
    }

    public static void clickOnDataSourceInput() {
        PrizmaUtils.scrollToElementOnPage(getDataSourceInput());
        PrizmaDriver.waitToBeClickable(getDataSourceInput());
        getDataSourceInput().click();
    }

    public static void clickOnStartBalanceInput() {
        PrizmaUtils.scrollToElementOnPage(getStartBalanceInput());
        PrizmaDriver.waitToBeClickable(getStartBalanceInput());
        getStartBalanceInput().click();
    }

    public static void clickOnRunFromMonthInput() {
        PrizmaUtils.scrollToElementOnPage(getRunFromMonthInput());
        PrizmaDriver.waitToBeClickable(getRunFromMonthInput());
        getRunFromMonthInput().click();
    }

    public static void clickOnRunFromDayInput() {
        PrizmaUtils.scrollToElementOnPage(getRunFromDayInput());
        PrizmaDriver.waitToBeClickable(getRunFromDayInput());
        getRunFromDayInput().click();
    }

    public static void clickOnRunFromYearInput() {
        PrizmaUtils.scrollToElementOnPage(getRunFromYearInput());
        PrizmaDriver.waitToBeClickable(getRunFromYearInput());
        getRunFromYearInput().click();
    }

    public static void clickOnRunFromInput() {
        PrizmaUtils.scrollToElementOnPage(getRunFromInput());
        PrizmaDriver.waitToBeClickable(getRunFromInput());
        getRunFromInput().click();
    }

    public static void clickOnRunToMonthInput() {
        PrizmaUtils.scrollToElementOnPage(getRunToMonthInput());
        PrizmaDriver.waitToBeClickable(getRunToMonthInput());
        getRunToMonthInput().click();
    }

    public static void clickOnRunToDayInput() {
        PrizmaUtils.scrollToElementOnPage(getRunToDayInput());
        PrizmaDriver.waitToBeClickable(getRunToDayInput());
        getRunToDayInput().click();
    }

    public static void clickOnRunToYearInput() {
        PrizmaUtils.scrollToElementOnPage(getRunToYearInput());
        PrizmaDriver.waitToBeClickable(getRunToYearInput());
        getRunToYearInput().click();
    }

    public static void clickOnRunToInput() {
        PrizmaUtils.scrollToElementOnPage(getRunToInput());
        PrizmaDriver.waitToBeClickable(getRunToInput());
        getRunToInput().click();
    }

    public static void clickOnTradingFeeInput() {
        PrizmaUtils.scrollToElementOnPage(getTradingFeeInput());
        PrizmaDriver.waitToBeClickable(getTradingFeeInput());
        getTradingFeeInput().click();
    }

    public static void clickOnTradeLimitInput() {
        PrizmaUtils.scrollToElementOnPage(getTradeLimitInput());
        PrizmaDriver.waitToBeClickable(getTradeLimitInput());
        getTradeLimitInput().click();
    }

    public static void clickOnBotRunBtnOnBotRunPage() {
        RUN_BOT_TIME_FOR_FUTURE_TESTS = "";
        RUN_BOT_TIME_FOR_FUTURE_TESTS = "Today at " + PrizmaUtils.getCurrentTime();

        try {
            PrizmaUtils.scrollToElementOnPage(getBotRunBtnOnBotRunPage());
            PrizmaDriver.waitToBeClickable(getBotRunBtnOnBotRunPage());
            getBotRunBtnOnBotRunPage().click();

            PrizmaDriver.waitFor(PrizmaConstants.DOWNLOAD_WAIT_TIME);
        } catch (Throwable ex) {
            PrizmaDriver.waitFor(PrizmaConstants.DOWNLOAD_WAIT_TIME);
        }
    }

    public static void clickOnBotRunBtnOnBotRunPageWithoutWaitDriver() {
        RUN_BOT_TIME_FOR_FUTURE_TESTS = "";
        RUN_BOT_TIME_FOR_FUTURE_TESTS = "Today at " + PrizmaUtils.getCurrentTime();

        try {
            PrizmaUtils.scrollToElementOnPage(getBotRunBtnOnBotRunPage());
            getBotRunBtnOnBotRunPage().click();

            PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
        } catch (Throwable ex) {
            PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
        }
    }

    public static void clickOnBotRunBtnOnBotRunPageWithoutAnyWaitDriver() {
        RUN_BOT_TIME_FOR_FUTURE_TESTS = "";
        RUN_BOT_TIME_FOR_FUTURE_TESTS = "Today at " + PrizmaUtils.getCurrentTime();

        try {
            PrizmaUtils.scrollToElementOnPage(getBotRunBtnOnBotRunPage());
            getBotRunBtnOnBotRunPage().click();
        } catch (Throwable ex) {
            //ignore
        }
    }

    public static void clickOnCloseBtnOnBotRunPage() {
        try {
            PrizmaUtils.scrollToElementOnPage(getCloseBtnOnBotRunPage());
            PrizmaDriver.waitToBeClickable(getCloseBtnOnBotRunPage());
            getCloseBtnOnBotRunPage().click();

            PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
        } catch (Throwable ex) {
            PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
        }
    }

    public static void clickOnStopBtnOnBotRunPage() {
        try {
            PrizmaDriver.waitDriver(By.cssSelector(STOP_BTN_ON_BOT_RUN_PAGE), 30);
            getStopBtnOnBotRunPage().click();

            PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
        } catch (Throwable ex) {
            PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
        }
    }

    /***
     * Methods to clear the elements
     *
     */
    public static void clearDataSourceInput() {
        PrizmaUtils.scrollToElementOnPage(getDataSourceInput());
        PrizmaDriver.waitToBeClickable(getDataSourceInput());
        getDataSourceInput().clear();

        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
    }

    public static void clearStartBalanceInput() {
        PrizmaUtils.scrollToElementOnPage(getStartBalanceInput());
        PrizmaDriver.waitToBeClickable(getStartBalanceInput());
        getStartBalanceInput().clear();

        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
    }

    public static void clearRunFromMonthInput() {
        PrizmaUtils.scrollToElementOnPage(getRunFromMonthInput());
        PrizmaDriver.waitToBeClickable(getRunFromMonthInput());
        getRunFromMonthInput().clear();

        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
    }

    public static void clearRunFromDayInput() {
        PrizmaUtils.scrollToElementOnPage(getRunFromDayInput());
        PrizmaDriver.waitToBeClickable(getRunFromDayInput());
        getRunFromDayInput().clear();

        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
    }

    public static void clearRunFromYearInput() {
        PrizmaUtils.scrollToElementOnPage(getRunFromYearInput());
        PrizmaDriver.waitToBeClickable(getRunFromYearInput());
        getRunFromDayInput().clear();

        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
    }

    public static void clearRunFromInput() {
        PrizmaUtils.scrollToElementOnPage(getRunFromInput());
        PrizmaDriver.waitToBeClickable(getRunFromInput());
        getRunFromInput().clear();

        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
    }

    public static void clearRunToMonthInput() {
        PrizmaUtils.scrollToElementOnPage(getRunToMonthInput());
        PrizmaDriver.waitToBeClickable(getRunToMonthInput());
        getRunToMonthInput().clear();

        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
    }

    public static void clearRunToDayInput() {
        PrizmaUtils.scrollToElementOnPage(getRunToDayInput());
        PrizmaDriver.waitToBeClickable(getRunToDayInput());
        getRunToDayInput().clear();

        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
    }

    public static void clearRunToYearInput() {
        PrizmaUtils.scrollToElementOnPage(getRunToYearInput());
        PrizmaDriver.waitToBeClickable(getRunToYearInput());
        getRunToYearInput().clear();

        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
    }

    public static void clearRunToInput() {
        PrizmaUtils.scrollToElementOnPage(getRunToInput());
        PrizmaDriver.waitToBeClickable(getRunToInput());
        getRunToInput().clear();

        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
    }

    public static void clearTradingFeeInput() {
        PrizmaUtils.scrollToElementOnPage(getTradingFeeInput());
        PrizmaDriver.waitToBeClickable(getTradingFeeInput());
        getTradingFeeInput().clear();

        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
    }

    public static void clearTradeLimitInput() {
        PrizmaUtils.scrollToElementOnPage(getTradeLimitInput());
        PrizmaDriver.waitToBeClickable(getTradeLimitInput());
        getTradeLimitInput().clear();

        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
    }

    /***
     * Methods to set the value into element
     */
    public static void setDataSourceOnBotRunPage(String dataSource) {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        if (dataSource.isEmpty()) {
            clickOnDataSourceInput();
            clearDataSourceInput();
        } else {
            clickOnDataSourceInput();
            clearDataSourceInput();
            getDataSourceInput().sendKeys(dataSource);

            PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
        }
    }

    public static void setStartBalanceOnBotRunPage(String startBalance) {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        if (startBalance.isEmpty()) {
            clickOnStartBalanceInput();
            clearStartBalanceInput();
        } else {
            clickOnStartBalanceInput();
            clearStartBalanceInput();
            getStartBalanceInput().sendKeys(startBalance);

            PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
        }

        if (!startBalance.isEmpty()) {
            INITIAL_BALANCE_VALUE_FOR_FUTURE_TESTS = 0;
            INITIAL_BALANCE_VALUE_FOR_CALCULATION_FOR_FUTURE_TESTS = 0;
            INITIAL_BALANCE_VALUE_FOR_FUTURE_TESTS = Double.valueOf(startBalance);
            INITIAL_BALANCE_VALUE_FOR_CALCULATION_FOR_FUTURE_TESTS = Double.valueOf(startBalance);
        }
    }

    public static void setRunFromMonthOnBotRunPage(String runFromMonth) {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        if (runFromMonth.isEmpty()) {
            clickOnRunFromMonthInput();
            clearRunFromMonthInput();
        } else {
            clickOnRunFromMonthInput();
            clearRunFromMonthInput();
            getRunFromMonthInput().sendKeys(runFromMonth);

            PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
        }
    }

    public static void setRunFromDayOnBotRunPage(String runFromDay) {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        if (runFromDay.isEmpty()) {
            clickOnRunFromDayInput();
            clearRunFromDayInput();
        } else {
            clickOnRunFromDayInput();
            clearRunFromDayInput();
            getRunFromDayInput().sendKeys(runFromDay);

            PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
        }
    }

    public static void setRunFromYearOnBotRunPage(String runFromMonth, String runFromDay, String runFromYear) {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        if (runFromYear.isEmpty()) {
            clickOnRunFromYearInput();
            clearRunFromYearInput();
        } else {
            clickOnRunFromYearInput();
            clearRunFromYearInput();
            getRunFromYearInput().sendKeys(runFromYear);

            PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
        }

        FROM_DATE_FOR_FUTURE_TESTS = "";
        FROM_DATE_FOR_FUTURE_TESTS = runFromMonth + "/" + runFromDay + "/" + runFromYear;
    }

    public static void setRunFromOnBotRunPage(String runFrom) {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        if (!runFrom.isEmpty()) {
            clickOnRunFromInput();
            clearRunFromInput();
            getRunFromInput().sendKeys(runFrom);
            getRunFromInput().sendKeys(Keys.ENTER);

            PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
        } else {
            clearRunFromInput();
            getRunFromInput().sendKeys(Keys.ENTER);
        }

        FROM_DATE_FOR_FUTURE_TESTS = "";
        FROM_DATE_FOR_FUTURE_TESTS = runFrom;
    }

    public static void setRunToMonthOnBotRunPage(String runToMonth) {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        if (runToMonth.isEmpty()) {
            clickOnRunToMonthInput();
            clearRunToMonthInput();
        } else {
            clickOnRunToMonthInput();
            clearRunToMonthInput();
            getRunToMonthInput().sendKeys(runToMonth);

            PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
        }
    }

    public static void setRunToDayOnBotRunPage(String runToDay) {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        if (runToDay.isEmpty()) {
            clickOnRunToDayInput();
            clearRunToDayInput();
        } else {
            clickOnRunToDayInput();
            clearRunToDayInput();
            getRunToDayInput().sendKeys(runToDay);

            PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
        }
    }

    public static void setRunToYearOnBotRunPage(String runToMonth, String runToDay, String runToYear) {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        if (runToYear.isEmpty()) {
            clickOnRunToYearInput();
            clearRunToYearInput();
        } else {
            clickOnRunToYearInput();
            clearRunToYearInput();
            getRunToYearInput().sendKeys(runToYear);

            PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
        }

        TO_DATE_FOR_FUTURE_TESTS = "";
        TO_DATE_FOR_FUTURE_TESTS = runToMonth + "/" + runToDay + "/" + runToYear;
    }

    public static void setRunToOnBotRunPage(String runTo) {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        if (runTo.isEmpty()) {
            clickOnRunToInput();
            clearRunToInput();
        } else {
            clickOnRunToInput();
            clearRunToInput();
            getRunToInput().sendKeys(runTo);
            getRunToInput().sendKeys(Keys.ENTER);

            PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
        }

        TO_DATE_FOR_FUTURE_TESTS = "";
        TO_DATE_FOR_FUTURE_TESTS = runTo;
    }

    public static void setTradingFeeOnBotRunPage(String tradingFee) {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        if (tradingFee.isEmpty()) {
            clickOnTradingFeeInput();
            clearTradingFeeInput();
        } else {
            clickOnTradingFeeInput();
            clearTradingFeeInput();
            getTradingFeeInput().sendKeys(tradingFee);

            PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
        }

        if (!tradingFee.isEmpty()) {
            TRADING_FEE_FOR_FUTURE_TESTS = 0;
            TRADING_FEE_FOR_FUTURE_TESTS = Double.valueOf(tradingFee) / 100;
        }
    }

    public static void setTradeLimitOnBotRunPage(String tradeLimit) {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        if (tradeLimit.isEmpty()) {
            clickOnTradeLimitInput();
            clearTradeLimitInput();
        } else {
            clickOnTradeLimitInput();
            clearTradeLimitInput();
            getTradeLimitInput().sendKeys(tradeLimit);

            PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
        }
    }

    /***
     * Asserts methods
     */
    public static void verifyThatRunBotDialogCloseAfterClickOnCloseBtn() {
        boolean flag;
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        try {
            PrizmaDriver.waitDriver(By.id(START_BALANCE_INPUT), 10);
            flag = true;
        } catch (Throwable ex) {
            flag = false;
        }

        if (flag) {
            Assert.fail("Looks like that something when wrong as after click on close btn on Run bot dialog -> dialog not close. Check video and logs, or something was changed on Prizma DD side.");
        }
    }

    public static void verifyThatRunBotDialogCloseWhenUserTriedToRunBotWithCorrectDataBot() {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        try {
            PrizmaDriver.waitDriver(By.id(START_BALANCE_INPUT), 10);
            Assert.fail("Looks like that something when wrong as after click Run bot dialog not close when user tried to run bot with correct data bot but run bot dialog should close. Check video and logs, or something was changed on Prizma DD side.");
        } catch (Throwable ex) {
            //ignore
        }
    }

    public static void verifyThatRunBotDialogNotCloseWhenUserTriedToRunBotWithIncorrectDataBot() {
        boolean flag;
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        try {
            PrizmaDriver.waitDriver(By.id(START_BALANCE_INPUT), 10);
            flag = true;
        } catch (Throwable ex) {
            flag = false;
        }

        if (!flag) {
            Assert.fail("Looks like that something when wrong as after click Run bot dialog close when user tried to run bot with incorrect data bot run bot dialog should be still open and present inline error. Check video and logs, or something was changed on Prizma DD side.");
        }
    }

    //Method to check error message
    public static void verifyThatOnRunBotFormErrorMessageAppear(String errorMessage) {
        boolean flag;
        String element = "//*[contains(text(),'" + errorMessage + "')]";
        try {
            PrizmaDriver.waitDriver(By.xpath(element), 5);
            flag = true;
        } catch (Throwable ex) {
            flag = false;
        }

        if (!flag) {
            Assert.fail("Error message/label with text: " + errorMessage + " not appear!");
        }
    }

    //Method to check error message
    public static void verifyThatOnRunBotFormErrorMessageNotAppear(String errorMessage) {
        boolean flag;
        String element = "//*[contains(text(),'" + errorMessage + "')]";
        try {
            PrizmaDriver.waitDriver(By.xpath(element), 5);
            flag = false;
        } catch (Throwable ex) {
            flag = true;
        }

        if (!flag) {
            Assert.fail("Error message/label with text: " + errorMessage + " appear, but shouldn't!");
        }
    }
}

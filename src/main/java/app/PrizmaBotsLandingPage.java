package app;

import org.junit.*;
import org.openqa.selenium.*;

import java.util.*;

import prizma.maxiru.framework.common.*;

import static app.PrizmaBotsDetailsPage.*;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;

public class PrizmaBotsLandingPage {

    //Elements with XPath, Tag and CCS selector
    public static final String BOTS_LIST = "bots-list";
    public static final String BOT_ROW_CONTAINER = "div[class='data-row']";
    public static final String BOT_ROW_NAME_LABEL = "a[class='data-row__link data-row__value']";
    public static final String BOT_LAST_RUN_LABEL = "span[class='data-row__value']";
    public static final String BOT_AVERANGE_PROFIT_LABEL_CONTAINER = "span[class='data-row__value']";
    public static final String BOT_AVERANGE_PROFIT_LABEL_CONTAINER_CURRENCY = "span[class='data-row__value__item']";
    public static final String BOT_AVERANGE_PROFIT_LABEL_CONTAINER_CURRENCY_LABEL = "span";
    public static final String BOT_AVERANGE_PROFIT_LABEL_CONTAINER_PERCENTAGE = "span[class='data-row__value__item data-row__value__item_additional']";
    public static final String BOT_AVERANGE_PROFIT_LABEL_CONTAINER_PERCENTAGE_LABEL = "span";
    public static final String BOT_PROFIT_LABEL_CONTAINER = "span[class='data-row__value']";
    public static final String BOT_PROFIT_LABEL_CONTAINER_CURRENCY = "span[class='data-row__value__item']";
    public static final String BOT_PROFIT_LABEL_CONTAINER_CURRENCY_LABEL = "span";
    public static final String BOT__PROFIT_LABEL_CONTAINER_PERCENTAGE = "span[class='data-row__value__item data-row__value__item_additional']";
    public static final String BOT__PROFIT_LABEL_CONTAINER_PERCENTAGE_LABEL = "span";
    public static String BOT_NAME_FOR_FUTURE_TESTS = "bot_sample";

    //Other elements
    public static String BOT_NAME_TEXT_FOR_FUTURE_TESTS = "";
    public static String BOT_LAST_RUN_TIME_TEXT_FOR_FUTURE_TESTS = "";
    public static String AVERAGE_PROFIT_CURRENCY_TEXT_FOR_FUTURE_TESTS = "";
    public static String AVERAGE_PROFIT_PERCENTAGE_TEXT_FOR_FUTURE_TESTS = "";
    public static String PROFIT_CURRENCY_TEXT_FOR_FUTURE_TESTS = "";
    public static String PROFIT_PERCENTAGE_TEXT_FOR_FUTURE_TESTS = "";
    public static int BOT_POSITION_FOR_FUTURE_TESTS = 0;

    /***
     * Methods to get the elements
     *
     * @return WebElement
     */
    public static WebElement getBotsList() {
        List<WebElement> list;
        if (PrizmaDriver.isSelenoid()) {
            list = PrizmaDriver.getCurrentDriver().findElements(By.id(BOTS_LIST));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + BOTS_LIST + " is empty. Check video and logs, or something was changed on SDK side.");
            }
            return list.get(0);
        } else {
            list = PrizmaDriver.getCurrentRemoveWebDriver().findElements(By.id(BOTS_LIST));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + BOTS_LIST + " is empty. Check video and logs, or something was changed on SDK side.");
            }
            return list.get(0);
        }
    }

    public static WebElement getBotRowContainer(int position) {
        List<WebElement> list;
        if (PrizmaDriver.isSelenoid()) {
            list = getBotsList().findElements(By.cssSelector(BOT_ROW_CONTAINER));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + BOT_ROW_CONTAINER + " is empty. Check video and logs, or something was changed on SDK side.");
            }
            return list.get(position - 1);
        } else {
            list = getBotsList().findElements(By.cssSelector(BOT_ROW_CONTAINER));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + BOT_ROW_CONTAINER + " is empty. Check video and logs, or something was changed on SDK side.");
            }
            return list.get(position - 1);
        }
    }

    public static int getBotRowCount() {
        List<WebElement> list;
        if (PrizmaDriver.isSelenoid()) {
            list = getBotsList().findElements(By.cssSelector(BOT_ROW_CONTAINER));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + BOT_ROW_CONTAINER + " is empty. Check video and logs, or something was changed on SDK side.");
            }
            return list.size();
        } else {
            list = getBotsList().findElements(By.cssSelector(BOT_ROW_CONTAINER));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + BOT_ROW_CONTAINER + " is empty. Check video and logs, or something was changed on SDK side.");
            }
            return list.size();
        }
    }

    public static WebElement getBotNameLabel(int position) {
        if (PrizmaDriver.isSelenoid()) {
            return getBotRowContainer(position).findElement(By.cssSelector(BOT_ROW_NAME_LABEL));
        } else {
            return getBotRowContainer(position).findElement(By.cssSelector(BOT_ROW_NAME_LABEL));
        }
    }

    public static WebElement getBotLastRunLabel(int position) {
        List<WebElement> list;
        list = getBotRowContainer(position).findElements(By.cssSelector(BOT_LAST_RUN_LABEL));
        return list.get(0);
    }

    public static WebElement getBotAverangeProfitLabelContainer(int position) {
        List<WebElement> list;
        list = getBotRowContainer(position).findElements(By.cssSelector(BOT_AVERANGE_PROFIT_LABEL_CONTAINER));
        return list.get(1);
    }

    public static WebElement getBotAverangeProfitLabel(int position) {
        return getBotAverangeProfitLabelContainer(position);
    }

    public static WebElement getBotAverangeProfitLabelCurrency(int position) {
        List<WebElement> list;
        list = getBotAverangeProfitLabelContainer(position).findElements(By.cssSelector(BOT_AVERANGE_PROFIT_LABEL_CONTAINER_CURRENCY));
        return list.get(0).findElement(By.tagName(BOT_AVERANGE_PROFIT_LABEL_CONTAINER_CURRENCY_LABEL));
    }

    public static WebElement getBotAverangeProfitLabelPercentage(int position) {
        List<WebElement> list;
        list = getBotAverangeProfitLabelContainer(position).findElements(By.cssSelector(BOT_AVERANGE_PROFIT_LABEL_CONTAINER_PERCENTAGE));
        return list.get(0).findElement(By.tagName(BOT_AVERANGE_PROFIT_LABEL_CONTAINER_PERCENTAGE_LABEL));
    }

    public static WebElement getBotProfitLabelContainer(int position) {
        List<WebElement> list;
        list = getBotRowContainer(position).findElements(By.cssSelector(BOT_PROFIT_LABEL_CONTAINER));
        return list.get(2);
    }

    public static WebElement getBotProfitLabel(int position) {
        return getBotProfitLabelContainer(position);
    }

    public static WebElement getBotProfitLabelCurrency(int position) {
        List<WebElement> list;
        list = getBotProfitLabelContainer(position).findElements(By.cssSelector(BOT_PROFIT_LABEL_CONTAINER_CURRENCY));
        return list.get(0).findElement(By.tagName(BOT_PROFIT_LABEL_CONTAINER_CURRENCY_LABEL));
    }

    public static WebElement getBotProfitLabelPercentage(int position) {
        List<WebElement> list;
        list = getBotProfitLabelContainer(position).findElements(By.cssSelector(BOT__PROFIT_LABEL_CONTAINER_PERCENTAGE));
        return list.get(0).findElement(By.tagName(BOT__PROFIT_LABEL_CONTAINER_PERCENTAGE_LABEL));
    }

    /***
     * Methods to click on the elements
     *
     */
    public static void clickOnBotRowContainerToOpenBotDetailPageByPositionX(int position) {
        try {
            PrizmaUtils.scrollToElementOnPage(getBotRowContainer(position));
            PrizmaDriver.waitToBeClickable(getBotRowContainer(position));
            getBotRowContainer(position).click();

            PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
        } catch (Throwable ex) {
            PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
        }
    }

    public static void clickOnBotRowContainerToOpenBotDetailPageByPositionFromVariableValue() {
        try {
            PrizmaUtils.scrollToElementOnPage(getBotRowContainer(BOT_POSITION_FOR_FUTURE_TESTS));
            PrizmaDriver.waitToBeClickable(getBotRowContainer(BOT_POSITION_FOR_FUTURE_TESTS));
            getBotRowContainer(BOT_POSITION_FOR_FUTURE_TESTS).click();

            PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
        } catch (Throwable ex) {
            PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
        }
    }

    public static void clickOnBotRowContainerToOpenBotDetailPageByBotName(String botName) {
        PrizmaDriver.waitFor(PrizmaConstants.CHARTING_WAIT_TIME);


        int count = getBotRowCount();
        for (int i = 1; i <= count; i++) {
            String temp = getTextFromBotNameLabel(i);
            if (temp.contains(botName)) {
                BOT_POSITION_FOR_FUTURE_TESTS = i;

//                PrizmaUtils.scrollToElementOnPage(getBotRowContainer(BOT_POSITION_FOR_FUTURE_TESTS));
//                PrizmaDriver.waitToBeClickable(getBotRowContainer(BOT_POSITION_FOR_FUTURE_TESTS));
                getBotRowContainer(BOT_POSITION_FOR_FUTURE_TESTS).click();

                PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

                break;
            }
        }
    }

    public static void clickOnBotRowContainerToOpenBotDetailPageByBotNameFromVariableValue() {
        PrizmaDriver.waitFor(PrizmaConstants.CHARTING_WAIT_TIME);

        int count = getBotRowCount();
        for (int i = 1; i <= count; i++) {
            String temp = getTextFromBotNameLabel(i);
            if (temp.contains(BOT_NAME_FOR_FUTURE_TESTS)) {
                BOT_POSITION_FOR_FUTURE_TESTS = i;

                PrizmaUtils.scrollToElementOnPage(getBotRowContainer(BOT_POSITION_FOR_FUTURE_TESTS));
                PrizmaDriver.waitToBeClickable(getBotRowContainer(BOT_POSITION_FOR_FUTURE_TESTS));
                getBotRowContainer(BOT_POSITION_FOR_FUTURE_TESTS).click();

                PrizmaDriver.waitFor(PrizmaConstants.HOLD_WAIT_TIME);

                break;
            }
        }

        PrizmaDriver.waitFor(PrizmaConstants.HOLD_WAIT_TIME);
    }

    /***
     * Methods to set the value into element
     */

    /***
     * Methods to get the texts from element
     *
     * @return String
     */
    public static String getTextFromBotNameLabel(int position) {
        try {
            return getBotNameLabel(position).getText().toString();
        } catch (Throwable ex) {
            return "";
        }
    }

    public static String getTextFromBotLastRunLabel(int position) {
        try {
            return getBotLastRunLabel(position).getText().toString();
        } catch (Throwable ex) {
            return "";
        }
    }

    public static String getTextFromBotAverangeProfitLabel(int position) {
        try {
            return getBotAverangeProfitLabel(position).getText().toString();
        } catch (Throwable ex) {
            return "";
        }
    }

    public static String getTextFromBotAverangeProfitLabelCurrency(int position) {
        try {
            return getBotAverangeProfitLabelCurrency(position).getText().toString();
        } catch (Throwable ex) {
            return "";
        }
    }

    public static String getTextFromBotAverangeProfitLabelPercentage(int position) {
        try {
            return getBotAverangeProfitLabelPercentage(position).getText().toString();
        } catch (Throwable ex) {
            return "";
        }
    }

    public static String getTextFromBotProfitLabel(int position) {
        try {
            return getBotProfitLabel(position).getText().toString();
        } catch (Throwable ex) {
            return "";
        }
    }

    public static String getTextFromBotProfitLabelCurrency(int position) {
        try {
            return getBotProfitLabelCurrency(position).getText().toString();
        } catch (Throwable ex) {
            return "";
        }
    }

    public static String getTextFromBotProfitLabelPercentage(int position) {
        try {
            return getBotProfitLabelPercentage(position).getText().toString();
        } catch (Throwable ex) {
            return "";
        }
    }

    /***
     * Asserts methods
     */
    public static void verifyThatBotNameOnPositionIsCorrect(String botName, int position) {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        try {
            assertThat(getTextFromBotNameLabel(position), containsString(botName));
        } catch (Throwable ex) {
            PrizmaUtils.logFailedTestResultsToTheLogsFile("Looks like that something when wrong as bot name on position: " + position + " was: " + getTextFromBotNameLabel(position) + " when tests expect: " + botName + ". Check video and logs, or something was changed on Prizma SDK side.");
        }
    }

    public static void verifyThatBotNameOnPositionFromVariableValueIsCorrectIfValidateWithVariableValue() {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        try {
            assertThat(getTextFromBotNameLabel(BOT_POSITION_FOR_FUTURE_TESTS), containsString(BOT_NAME_FOR_FUTURE_TESTS));
        } catch (Throwable ex) {
            PrizmaUtils.logFailedTestResultsToTheLogsFile("Looks like that something when wrong as bot name on position: " + BOT_POSITION_FOR_FUTURE_TESTS + " was: " + getTextFromBotNameLabel(BOT_POSITION_FOR_FUTURE_TESTS) + " when tests expect: " + BOT_NAME_FOR_FUTURE_TESTS + ". Check video and logs, or something was changed on Prizma SDK side.");
        }
    }

    public static void verifyThatBotNameOnPositionFromVariableValueIsCorrect(String botName) {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        try {
            assertThat(getTextFromBotNameLabel(BOT_POSITION_FOR_FUTURE_TESTS), containsString(botName));
        } catch (Throwable ex) {
            PrizmaUtils.logFailedTestResultsToTheLogsFile("Looks like that something when wrong as bot name on position: " + BOT_POSITION_FOR_FUTURE_TESTS + " was: " + getTextFromBotNameLabel(BOT_POSITION_FOR_FUTURE_TESTS) + " when tests expect: " + botName + ". Check video and logs, or something was changed on Prizma SDK side.");
        }
    }

    public static void verifyThatBotLastRunOnPositionIsCorrect(String botRun, int position) {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        try {
            assertThat(botRun, containsString(getTextFromBotLastRunLabel(position)));
        } catch (Throwable ex) {
            PrizmaUtils.logFailedTestResultsToTheLogsFile("Looks like that something when wrong as bot last run on position: " + position + " was: " + getTextFromBotLastRunLabel(position) + " when tests expect: " + botRun + ". Check video and logs, or something was changed on Prizma SDK side.");
        }
    }

    public static void verifyThatBotLastRunOnPositionFromVariableValueIsCorrectIfValidateWithVariableValue() {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        try {
            assertThat(RUN_BOT_TIME_FOR_FUTURE_TESTS, containsString(getTextFromBotLastRunLabel(BOT_POSITION_FOR_FUTURE_TESTS)));
        } catch (Throwable ex) {
            PrizmaUtils.logFailedTestResultsToTheLogsFile("Looks like that something when wrong as bot last run on position: " + BOT_POSITION_FOR_FUTURE_TESTS + " was: " + getTextFromBotLastRunLabel(BOT_POSITION_FOR_FUTURE_TESTS) + " when tests expect: " + RUN_BOT_TIME_FOR_FUTURE_TESTS + ". Check video and logs, or something was changed on Prizma SDK side.");
        }
    }

    public static void verifyThatBotAverangeProfitCurrencyOnPositionIsCorrect(String averangeProfitCurrency, int position) {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        try {
            assertThat(getTextFromBotAverangeProfitLabelCurrency(position), containsString(averangeProfitCurrency));
        } catch (Throwable ex) {
            PrizmaUtils.logFailedTestResultsToTheLogsFile("Looks like that something when wrong as bot averange profit currency on position: " + position + " was: " + getTextFromBotAverangeProfitLabelCurrency(position) + " when tests expect: " + averangeProfitCurrency + ". Check video and logs, or something was changed on Prizma SDK side.");
        }
    }

    public static void verifyThatBotAverangeProfitCurrencyOnPositionFromVariableValueIsCorrectIfValidateWithVariableValue() {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        double tempValue = PrizmaUtils.showOnlyTwoDigitAfterDecimal(AVERAGE_PROFIT_CURRENCY_VALUE_FOR_FUTURE_TESTS);
        String temp = PrizmaUtils.addCommaForThousandNumber(Double.toString(tempValue));
        temp = PrizmaUtils.addDollarSignInTheBegging(temp);

        try {
            assertThat(getTextFromBotAverangeProfitLabelCurrency(BOT_POSITION_FOR_FUTURE_TESTS), containsString(temp));
        } catch (Throwable ex) {
            PrizmaUtils.logFailedTestResultsToTheLogsFile("Looks like that something when wrong as bot averange profit currency on position: " + BOT_POSITION_FOR_FUTURE_TESTS + " was: " + getTextFromBotAverangeProfitLabelCurrency(BOT_POSITION_FOR_FUTURE_TESTS) + " when tests expect: " + temp + ". Check video and logs, or something was changed on Prizma SDK side.");
        }
    }

    public static void verifyThatBotAverangeProfitOnPositionFromVariableValueIsCorrectIfValidateWithVariableValueWhichIsZero() {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        String temp = "";

        if (AVERAGE_PROFIT_CURRENCY_VALUE_FOR_FUTURE_TESTS == 0) {
            temp = "–";
        }

        try {
            assertThat(getTextFromBotAverangeProfitLabel(BOT_POSITION_FOR_FUTURE_TESTS), containsString(temp));
        } catch (Throwable ex) {
            PrizmaUtils.logFailedTestResultsToTheLogsFile("Looks like that something when wrong as bot averange profit on position: " + BOT_POSITION_FOR_FUTURE_TESTS + " was: " + getTextFromBotAverangeProfitLabel(BOT_POSITION_FOR_FUTURE_TESTS) + " when tests expect: " + temp + ". Check video and logs, or something was changed on Prizma SDK side.");
        }
    }

    public static void verifyThatBotAverangeProfitPercentageOnPositionIsCorrect(String averangeProfitPercentage, int position) {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        try {
            assertThat(getTextFromBotAverangeProfitLabelPercentage(position), containsString(averangeProfitPercentage));
        } catch (Throwable ex) {
            PrizmaUtils.logFailedTestResultsToTheLogsFile("Looks like that something when wrong as bot averange profit percentage on position: " + position + " was: " + getTextFromBotAverangeProfitLabelPercentage(position) + " when tests expect: " + averangeProfitPercentage + ". Check video and logs, or something was changed on Prizma SDK side.");
        }
    }

    public static void verifyThatBotAverangeProfitPercentageOnPositionFromVariableValueIsCorrectIfValidateWithVariableValue() {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        double tempValue = PrizmaUtils.showOnlyTwoDigitAfterDecimal(AVERAGE_PROFIT_PERCENTAGE_VALUE_FOR_FUTURE_TESTS);
        String temp = PrizmaUtils.addCommaForThousandNumber(Double.toString(tempValue));
        temp = PrizmaUtils.addPercentageSignInTheEnd(temp);

        try {
            assertThat(getTextFromBotAverangeProfitLabelPercentage(BOT_POSITION_FOR_FUTURE_TESTS), containsString(temp));
        } catch (Throwable ex) {
            PrizmaUtils.logFailedTestResultsToTheLogsFile("Looks like that something when wrong as bot averange profit percentage on position: " + BOT_POSITION_FOR_FUTURE_TESTS + " was: " + getTextFromBotAverangeProfitLabelPercentage(BOT_POSITION_FOR_FUTURE_TESTS) + " when tests expect: " + temp + ". Check video and logs, or something was changed on Prizma SDK side.");
        }
    }

    public static void verifyThatBotProfitCurrencyOnPositionIsCorrect(String profitCurrency, int position) {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        try {
            assertThat(getTextFromBotProfitLabelCurrency(position), containsString(profitCurrency));
        } catch (Throwable ex) {
            PrizmaUtils.logFailedTestResultsToTheLogsFile("Looks like that something when wrong as bot profit currency on position: " + position + " was: " + getTextFromBotProfitLabelCurrency(position) + " when tests expect: " + profitCurrency + ". Check video and logs, or something was changed on Prizma SDK side.");
        }
    }

    public static void verifyThatBotProfitOnPositionFromVariableValueIsCorrectIfValidateWithVariableValueWhichIsZero() {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        String temp = "";

        if (TOTAL_PROFITS_CURRENCY_VALUE_FROM_CLOSED_POSITIONS_FOR_FUTURE_TESTS == 0) {
            temp = "–";
        }

        try {
            assertThat(getTextFromBotProfitLabel(BOT_POSITION_FOR_FUTURE_TESTS), containsString(temp));
        } catch (Throwable ex) {
            PrizmaUtils.logFailedTestResultsToTheLogsFile("Looks like that something when wrong as bot profit currency on position: " + BOT_POSITION_FOR_FUTURE_TESTS + " was: " + getTextFromBotProfitLabel(BOT_POSITION_FOR_FUTURE_TESTS) + " when tests expect: " + temp + ". Check video and logs, or something was changed on Prizma SDK side.");
        }
    }

    public static void verifyThatBotProfitCurrencyOnPositionFromVariableValueIsCorrectIfValidateWithVariableValue() {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        double tempValue = PrizmaUtils.showOnlyTwoDigitAfterDecimal(TOTAL_PROFITS_CURRENCY_VALUE_FROM_CLOSED_POSITIONS_FOR_FUTURE_TESTS);
        String temp = PrizmaUtils.addCommaForThousandNumber(Double.toString(tempValue));
        temp = PrizmaUtils.addDollarSignInTheBegging(temp);

        try {
            assertThat(getTextFromBotProfitLabelCurrency(BOT_POSITION_FOR_FUTURE_TESTS), containsString(temp));
        } catch (Throwable ex) {
            PrizmaUtils.logFailedTestResultsToTheLogsFile("Looks like that something when wrong as bot profit currency on position: " + BOT_POSITION_FOR_FUTURE_TESTS + " was: " + getTextFromBotProfitLabelCurrency(BOT_POSITION_FOR_FUTURE_TESTS) + " when tests expect: " + temp + ". Check video and logs, or something was changed on Prizma SDK side.");
        }
    }

    public static void verifyThatBotProfitPercentageOnPositionIsCorrect(String profitPercentage, int position) {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        try {
            assertThat(getTextFromBotProfitLabelPercentage(position), containsString(profitPercentage));
        } catch (Throwable ex) {
            PrizmaUtils.logFailedTestResultsToTheLogsFile("Looks like that something when wrong as bot profit currency on position: " + position + " was: " + getTextFromBotProfitLabelPercentage(position) + " when tests expect: " + profitPercentage + ". Check video and logs, or something was changed on Prizma SDK side.");
        }
    }

    public static void verifyThatBotProfitPercentageOnPositionFromVariableValueIsCorrectIfValidateWithVariableValue() {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        double tempValue = PrizmaUtils.showOnlyTwoDigitAfterDecimal(TOTAL_PROFITS_PERCENTAGE_VALUE_FOR_FUTURE_TESTS);
        String temp = PrizmaUtils.addCommaForThousandNumber(Double.toString(tempValue));
        temp = PrizmaUtils.addPercentageSignInTheEnd(temp);

        try {
            assertThat(getTextFromBotProfitLabelPercentage(BOT_POSITION_FOR_FUTURE_TESTS), containsString(temp));
        } catch (Throwable ex) {
            PrizmaUtils.logFailedTestResultsToTheLogsFile("Looks like that something when wrong as bot profit currency on position: " + BOT_POSITION_FOR_FUTURE_TESTS + " was: " + getTextFromBotProfitLabelPercentage(BOT_POSITION_FOR_FUTURE_TESTS) + " when tests expect: " + temp + ". Check video and logs, or something was changed on Prizma SDK side.");
        }
    }

    public static void verifyThatBotNameOnPositionIsCorrectToValidateWithVariableValue(int position) {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        try {
            assertThat(getTextFromBotNameLabel(position), containsString(BOT_NAME_TEXT_FOR_FUTURE_TESTS));
        } catch (Throwable ex) {
            PrizmaUtils.logFailedTestResultsToTheLogsFile("Looks like that something when wrong as bot name on position: " + position + " was: " + getTextFromBotNameLabel(position) + " when tests expect: " + BOT_NAME_TEXT_FOR_FUTURE_TESTS + ". Check video and logs, or something was changed on Prizma SDK side.");
        }
    }

    public static void verifyThatBotLastRunOnPositionIsCorrectToValidateWithVariableValue(int position) {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        try {
            assertThat(getTextFromBotLastRunLabel(position), containsString(BOT_LAST_RUN_TIME_TEXT_FOR_FUTURE_TESTS));
        } catch (Throwable ex) {
            PrizmaUtils.logFailedTestResultsToTheLogsFile("Looks like that something when wrong as bot last run on position: " + position + " was: " + getTextFromBotLastRunLabel(position) + " when tests expect: " + BOT_LAST_RUN_TIME_TEXT_FOR_FUTURE_TESTS + ". Check video and logs, or something was changed on Prizma SDK side.");
        }
    }

    public static void verifyThatBotAverangeProfitCurrencyOnPositionIsCorrectToValidateWithVariableValue(int position) {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        try {
            assertThat(getTextFromBotAverangeProfitLabelCurrency(position), containsString(AVERAGE_PROFIT_CURRENCY_TEXT_FOR_FUTURE_TESTS));
        } catch (Throwable ex) {
            PrizmaUtils.logFailedTestResultsToTheLogsFile("Looks like that something when wrong as bot averange profit currency on position: " + position + " was: " + getTextFromBotAverangeProfitLabelCurrency(position) + " when tests expect: " + AVERAGE_PROFIT_CURRENCY_TEXT_FOR_FUTURE_TESTS + ". Check video and logs, or something was changed on Prizma SDK side.");
        }
    }

    public static void verifyThatBotAverangeProfitPercentageOnPositionIsCorrectToValidateWithVariableValue(int position) {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        try {
            assertThat(getTextFromBotAverangeProfitLabelPercentage(position), containsString(AVERAGE_PROFIT_PERCENTAGE_TEXT_FOR_FUTURE_TESTS));
        } catch (Throwable ex) {
            PrizmaUtils.logFailedTestResultsToTheLogsFile("Looks like that something when wrong as bot averange profit percentage on position: " + position + " was: " + getTextFromBotAverangeProfitLabelPercentage(position) + " when tests expect: " + AVERAGE_PROFIT_PERCENTAGE_TEXT_FOR_FUTURE_TESTS + ". Check video and logs, or something was changed on Prizma SDK side.");
        }
    }

    public static void verifyThatBotProfitCurrencyOnPositionIsCorrectToValidateWithVariableValue(int position) {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        try {
            assertThat(getTextFromBotProfitLabelCurrency(position), containsString(PROFIT_CURRENCY_TEXT_FOR_FUTURE_TESTS));
        } catch (Throwable ex) {
            PrizmaUtils.logFailedTestResultsToTheLogsFile("Looks like that something when wrong as bot profit currency on position: " + position + " was: " + getTextFromBotProfitLabelCurrency(position) + " when tests expect: " + PROFIT_CURRENCY_TEXT_FOR_FUTURE_TESTS + ". Check video and logs, or something was changed on Prizma SDK side.");
        }
    }

    public static void verifyThatBotProfitPercentageOnPositionIsCorrectToValidateWithVariableValue(int position) {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        try {
            assertThat(getTextFromBotProfitLabelPercentage(position), containsString(PROFIT_PERCENTAGE_TEXT_FOR_FUTURE_TESTS));
        } catch (Throwable ex) {
            PrizmaUtils.logFailedTestResultsToTheLogsFile("Looks like that something when wrong as bot profit currency on position: " + position + " was: " + getTextFromBotProfitLabelPercentage(position) + " when tests expect: " + PROFIT_PERCENTAGE_TEXT_FOR_FUTURE_TESTS + ". Check video and logs, or something was changed on Prizma SDK side.");
        }
    }
}

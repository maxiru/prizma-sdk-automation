package app;

import org.junit.*;
import org.openqa.selenium.*;

import java.util.*;

import prizma.maxiru.framework.common.*;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.assertThat;

public class PrizmaChartingPage {

    //Elements with XPath, Tag and CCS selector
    public static final String ONE_MIN_BTN = "//span[text()='M1']";
    public static final String FIVE_MIN_BTN = "//span[text()='M5']";
    public static final String FIFTEEN_MIN_BTN = "//span[text()='M15']";
    public static final String SIXTY_MIN_BTN = "//span[text()='H1']";
    public static final String TWELVE_HOURS_BTN = "//span[text()='H12']";
    public static final String ONE_DAY_BTN = "//span[text()='D1']";
    public static final String CURRENCY_DROPDOWN = "span[class='select__current charting-header__symbols__select__current']";
    public static final String INDICATORS_BTN = ".charting-page__header__buttons button";
    public static final String VOLUME_CHECKBOX = "//input[@class='checkbox__input']";
    public static final String VOLUME_LABEL = "//span[@class='indicators-modal__aside__item__text']";
    public static final String BOLLINGER_BANDS_CHECKBOX = "//input[@class='checkbox__input']";
    public static final String BOLLINGER_BANDS_LABEL = "//span[@class='indicators-modal__aside__item__text']";
    public static final String RELATIVE_STRENGTH_INDEX_CHECKBOX = "//input[@class='checkbox__input']";
    public static final String RELATIVE_STRENGTH_INDEX_LABEL = "//span[@class='indicators-modal__aside__item__text']";
    public static final String VOLUME_CHECKBOX_CONTAINER = "//aside/ul/li/label[@class='checkbox indicators-modal__aside__item__checkbox']";
    public static final String BOLLINGER_BANDS_CHECKBOX_CONTAINER = "//label[@class='checkbox indicators-modal__aside__item__checkbox']";
    public static final String RELATIVE_STRENGTH_INDEX_CHECKBOX_CONTAINER = "//label[@class='checkbox indicators-modal__aside__item__checkbox']";
    public static final String INDICATORS_CLOSE_BTN = ".modal__close-button";
    public static final String LENGTH_INPUT = "indicators-settings-form__length";
    public static final String SCALAR_INPUT = "indicators-settings-form__scalar";
    public static final String STANDART_DEVIATION_INPUT = "indicators-settings-form__sdt";
    public static final String DIFFERENCE_PERIOD_INPUT = "indicators-settings-form__drift";
    public static final String DEGREES_OF_FREEDOM_INPUT = "indicators-settings-form__ddof";
    public static final String OFFSET_INPUT = "indicators-settings-form__offset";
    public static final String INDICATORS_RESET_BTN = "button[class='button indicators-settings-form__reset-button']";
    public static final String INDICATORS_APPLY_BTN = "button[class='button indicators-settings-form__submit-button button_theme_primary']";

    //Other message
    public static String INDICATOR_NO_SETTINGS_MESSAGE = "Indicator has no settings";
    public static String LENGTH_VALUE_FOR_FUTURE_TESTS = "";
    public static String SCALAR_VALUE_FOR_FUTURE_TESTS = "";
    public static String STANDART_DEVIATION_VALUE_FOR_FUTURE_TESTS = "";
    public static String DIFFERENCE_PERIOD_VALUE_FOR_FUTURE_TESTS = "";
    public static String DEGREES_OF_FREEDOM_VALUE_FOR_FUTURE_TESTS = "";
    public static String OFFSET_VALUE_FOR_FUTURE_TESTS = "";

    /***
     * Methods to get the elements
     *
     * @return WebElement
     */
    public static WebElement getOneMinBtnOnChartingPage() {
        List<WebElement> list;
        if (PrizmaDriver.isSelenoid()) {
            list = PrizmaDriver.getCurrentDriver().findElements(By.xpath(ONE_MIN_BTN));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + ONE_MIN_BTN + " is empty. Check video and logs, or something was changed on SDK side.");
            }
            return list.get(0);
        } else {
            list = PrizmaDriver.getCurrentRemoveWebDriver().findElements(By.xpath(ONE_MIN_BTN));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + ONE_MIN_BTN + " is empty. Check video and logs, or something was changed on SDK side.");
            }
            return list.get(0);
        }
    }

    public static WebElement getFiveMinBtnOnChartingPage() {
        List<WebElement> list;
        if (PrizmaDriver.isSelenoid()) {
            list = PrizmaDriver.getCurrentDriver().findElements(By.xpath(FIVE_MIN_BTN));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + FIVE_MIN_BTN + " is empty. Check video and logs, or something was changed on SDK side.");
            }
            return list.get(0);
        } else {
            list = PrizmaDriver.getCurrentRemoveWebDriver().findElements(By.xpath(FIVE_MIN_BTN));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + FIVE_MIN_BTN + " is empty. Check video and logs, or something was changed on SDK side.");
            }
            return list.get(0);
        }
    }

    public static WebElement getFifteenMinBtnOnChartingPage() {
        List<WebElement> list;
        if (PrizmaDriver.isSelenoid()) {
            list = PrizmaDriver.getCurrentDriver().findElements(By.xpath(FIFTEEN_MIN_BTN));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + FIFTEEN_MIN_BTN + " is empty. Check video and logs, or something was changed on SDK side.");
            }
            return list.get(0);
        } else {
            list = PrizmaDriver.getCurrentRemoveWebDriver().findElements(By.xpath(FIFTEEN_MIN_BTN));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + FIFTEEN_MIN_BTN + " is empty. Check video and logs, or something was changed on SDK side.");
            }
            return list.get(0);
        }
    }

    public static WebElement getSixtyMinBtnOnChartingPage() {
        List<WebElement> list;
        if (PrizmaDriver.isSelenoid()) {
            list = PrizmaDriver.getCurrentDriver().findElements(By.xpath(SIXTY_MIN_BTN));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + SIXTY_MIN_BTN + " is empty. Check video and logs, or something was changed on SDK side.");
            }
            return list.get(0);
        } else {
            list = PrizmaDriver.getCurrentRemoveWebDriver().findElements(By.xpath(SIXTY_MIN_BTN));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + SIXTY_MIN_BTN + " is empty. Check video and logs, or something was changed on SDK side.");
            }
            return list.get(0);
        }
    }

    public static WebElement getTwelveHoursBtnOnChartingPage() {
        List<WebElement> list;
        if (PrizmaDriver.isSelenoid()) {
            list = PrizmaDriver.getCurrentDriver().findElements(By.xpath(TWELVE_HOURS_BTN));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + TWELVE_HOURS_BTN + " is empty. Check video and logs, or something was changed on SDK side.");
            }
            return list.get(0);
        } else {
            list = PrizmaDriver.getCurrentRemoveWebDriver().findElements(By.xpath(TWELVE_HOURS_BTN));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + TWELVE_HOURS_BTN + " is empty. Check video and logs, or something was changed on SDK side.");
            }
            return list.get(0);
        }
    }

    public static WebElement getOneDayBtnOnChartingPage() {
        List<WebElement> list;
        if (PrizmaDriver.isSelenoid()) {
            list = PrizmaDriver.getCurrentDriver().findElements(By.xpath(ONE_DAY_BTN));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + ONE_DAY_BTN + " is empty. Check video and logs, or something was changed on SDK side.");
            }
            return list.get(0);
        } else {
            list = PrizmaDriver.getCurrentRemoveWebDriver().findElements(By.xpath(ONE_DAY_BTN));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + ONE_DAY_BTN + " is empty. Check video and logs, or something was changed on SDK side.");
            }
            return list.get(0);
        }
    }

    public static WebElement getCurrencyDropdown() {
        List<WebElement> list;
        if (PrizmaDriver.isSelenoid()) {
            list = PrizmaDriver.getCurrentDriver().findElements(By.cssSelector(CURRENCY_DROPDOWN));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + CURRENCY_DROPDOWN + " is empty. Check video and logs, or something was changed on SDK side.");
            }
            return list.get(0);
        } else {
            list = PrizmaDriver.getCurrentRemoveWebDriver().findElements(By.cssSelector(CURRENCY_DROPDOWN));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + CURRENCY_DROPDOWN + " is empty. Check video and logs, or something was changed on SDK side.");
            }
            return list.get(0);
        }
    }

    public static WebElement getIndicatorsBtn() {
        WebElement webElement;

        if (PrizmaDriver.isSelenoid()) {
            webElement = PrizmaDriver.getCurrentDriver().findElement(By.cssSelector(INDICATORS_BTN));
            if (webElement.isDisplayed() == false) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + INDICATORS_BTN + " is empty. Check video and logs, or something was changed on DD side.");
            }
            return webElement;
        } else {
            webElement = PrizmaDriver.getCurrentRemoveWebDriver().findElement(By.cssSelector(INDICATORS_BTN));
            if (webElement.isDisplayed() == false) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + INDICATORS_BTN + " is empty. Check video and logs, or something was changed on DD side.");
            }
            return webElement;
        }
    }

    public static WebElement getIndicatorsCloseBtn() {
        WebElement webElement;

        if (PrizmaDriver.isSelenoid()) {
            webElement = PrizmaDriver.getCurrentDriver().findElement(By.cssSelector(INDICATORS_CLOSE_BTN));
            if (webElement.isDisplayed() == false) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + INDICATORS_CLOSE_BTN + " is empty. Check video and logs, or something was changed on DD side.");
            }
            return webElement;
        } else {
            webElement = PrizmaDriver.getCurrentRemoveWebDriver().findElement(By.cssSelector(INDICATORS_CLOSE_BTN));
            if (webElement.isDisplayed() == false) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + INDICATORS_CLOSE_BTN + " is empty. Check video and logs, or something was changed on DD side.");
            }
            return webElement;
        }
    }

    public static WebElement getIndicatorsResetBtn() {
        List<WebElement> list;
        if (PrizmaDriver.isSelenoid()) {
            list = PrizmaDriver.getCurrentDriver().findElements(By.cssSelector(INDICATORS_RESET_BTN));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + INDICATORS_RESET_BTN + " is empty. Check video and logs, or something was changed on SDK side.");
            }
            return list.get(0);
        } else {
            list = PrizmaDriver.getCurrentRemoveWebDriver().findElements(By.cssSelector(INDICATORS_RESET_BTN));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + INDICATORS_RESET_BTN + " is empty. Check video and logs, or something was changed on SDK side.");
            }
            return list.get(0);
        }
    }

    public static WebElement getIndicatorsApplyBtn() {
        List<WebElement> list;
        if (PrizmaDriver.isSelenoid()) {
            list = PrizmaDriver.getCurrentDriver().findElements(By.cssSelector(INDICATORS_APPLY_BTN));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + INDICATORS_APPLY_BTN + " is empty. Check video and logs, or something was changed on SDK side.");
            }
            return list.get(0);
        } else {
            list = PrizmaDriver.getCurrentRemoveWebDriver().findElements(By.cssSelector(INDICATORS_APPLY_BTN));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + INDICATORS_APPLY_BTN + " is empty. Check video and logs, or something was changed on SDK side.");
            }
            return list.get(0);
        }
    }

    public static WebElement getVolumeCheckbox() {
        List<WebElement> list;
        if (PrizmaDriver.isSelenoid()) {
            list = PrizmaDriver.getCurrentDriver().findElements(By.xpath(VOLUME_CHECKBOX));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + VOLUME_CHECKBOX + " is empty. Check video and logs, or something was changed on SDK side.");
            }
            return list.get(1);
        } else {
            list = PrizmaDriver.getCurrentRemoveWebDriver().findElements(By.xpath(VOLUME_CHECKBOX));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + VOLUME_CHECKBOX + " is empty. Check video and logs, or something was changed on SDK side.");
            }
            return list.get(1);
        }
    }

    public static WebElement getVolumeLabel() {
        List<WebElement> list;
        if (PrizmaDriver.isSelenoid()) {
            list = PrizmaDriver.getCurrentDriver().findElements(By.xpath(VOLUME_LABEL));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + VOLUME_LABEL + " is empty. Check video and logs, or something was changed on SDK side.");
            }
            return list.get(1);
        } else {
            list = PrizmaDriver.getCurrentRemoveWebDriver().findElements(By.xpath(VOLUME_LABEL));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + VOLUME_LABEL + " is empty. Check video and logs, or something was changed on SDK side.");
            }
            return list.get(1);
        }
    }

    public static WebElement getBollingerBandsCheckbox() {
        List<WebElement> list;
        if (PrizmaDriver.isSelenoid()) {
            list = PrizmaDriver.getCurrentDriver().findElements(By.xpath(BOLLINGER_BANDS_CHECKBOX));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + BOLLINGER_BANDS_CHECKBOX + " is empty. Check video and logs, or something was changed on SDK side.");
            }
            return list.get(2);
        } else {
            list = PrizmaDriver.getCurrentRemoveWebDriver().findElements(By.xpath(BOLLINGER_BANDS_CHECKBOX));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + BOLLINGER_BANDS_CHECKBOX + " is empty. Check video and logs, or something was changed on SDK side.");
            }
            return list.get(2);
        }
    }

    public static WebElement getBollingerBandsLabel() {
        List<WebElement> list;
        if (PrizmaDriver.isSelenoid()) {
            list = PrizmaDriver.getCurrentDriver().findElements(By.xpath(BOLLINGER_BANDS_LABEL));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + BOLLINGER_BANDS_LABEL + " is empty. Check video and logs, or something was changed on SDK side.");
            }
            return list.get(2);
        } else {
            list = PrizmaDriver.getCurrentRemoveWebDriver().findElements(By.xpath(BOLLINGER_BANDS_LABEL));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + BOLLINGER_BANDS_LABEL + " is empty. Check video and logs, or something was changed on SDK side.");
            }
            return list.get(2);
        }
    }

    public static WebElement getRelativeStrengthIndexCheckbox() {
        List<WebElement> list;
        if (PrizmaDriver.isSelenoid()) {
            list = PrizmaDriver.getCurrentDriver().findElements(By.xpath(RELATIVE_STRENGTH_INDEX_CHECKBOX));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + RELATIVE_STRENGTH_INDEX_CHECKBOX + " is empty. Check video and logs, or something was changed on SDK side.");
            }
            return list.get(3);
        } else {
            list = PrizmaDriver.getCurrentRemoveWebDriver().findElements(By.xpath(RELATIVE_STRENGTH_INDEX_CHECKBOX));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + RELATIVE_STRENGTH_INDEX_CHECKBOX + " is empty. Check video and logs, or something was changed on SDK side.");
            }
            return list.get(3);
        }
    }

    public static WebElement getRelativeStrengthIndexLabel() {
        List<WebElement> list;
        if (PrizmaDriver.isSelenoid()) {
            list = PrizmaDriver.getCurrentDriver().findElements(By.xpath(RELATIVE_STRENGTH_INDEX_LABEL));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + RELATIVE_STRENGTH_INDEX_LABEL + " is empty. Check video and logs, or something was changed on SDK side.");
            }
            return list.get(3);
        } else {
            list = PrizmaDriver.getCurrentRemoveWebDriver().findElements(By.xpath(RELATIVE_STRENGTH_INDEX_LABEL));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + RELATIVE_STRENGTH_INDEX_LABEL + " is empty. Check video and logs, or something was changed on SDK side.");
            }
            return list.get(3);
        }
    }

    public static WebElement getVolumeCheckboxContainer() {
        List<WebElement> list;
        if (PrizmaDriver.isSelenoid()) {
            list = PrizmaDriver.getCurrentDriver().findElements(By.xpath(VOLUME_CHECKBOX_CONTAINER));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + VOLUME_CHECKBOX_CONTAINER + " is empty. Check video and logs, or something was changed on SDK side.");
            }
            return list.get(1);
        } else {
            list = PrizmaDriver.getCurrentRemoveWebDriver().findElements(By.xpath(VOLUME_CHECKBOX_CONTAINER));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + VOLUME_CHECKBOX_CONTAINER + " is empty. Check video and logs, or something was changed on SDK side.");
            }
            return list.get(1);
        }
    }

    public static WebElement getBollingerBandsCheckboxContainer() {
        List<WebElement> list;
        if (PrizmaDriver.isSelenoid()) {
            list = PrizmaDriver.getCurrentDriver().findElements(By.xpath(BOLLINGER_BANDS_CHECKBOX_CONTAINER));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + BOLLINGER_BANDS_CHECKBOX_CONTAINER + " is empty. Check video and logs, or something was changed on SDK side.");
            }
            return list.get(2);
        } else {
            list = PrizmaDriver.getCurrentRemoveWebDriver().findElements(By.xpath(BOLLINGER_BANDS_CHECKBOX_CONTAINER));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + BOLLINGER_BANDS_CHECKBOX_CONTAINER + " is empty. Check video and logs, or something was changed on SDK side.");
            }
            return list.get(2);
        }
    }

    public static WebElement getRelativeStrengthIndexCheckboxContainer() {
        List<WebElement> list;
        if (PrizmaDriver.isSelenoid()) {
            list = PrizmaDriver.getCurrentDriver().findElements(By.xpath(RELATIVE_STRENGTH_INDEX_CHECKBOX_CONTAINER));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + RELATIVE_STRENGTH_INDEX_CHECKBOX_CONTAINER + " is empty. Check video and logs, or something was changed on SDK side.");
            }
            return list.get(3);
        } else {
            list = PrizmaDriver.getCurrentRemoveWebDriver().findElements(By.xpath(RELATIVE_STRENGTH_INDEX_CHECKBOX_CONTAINER));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + RELATIVE_STRENGTH_INDEX_CHECKBOX_CONTAINER + " is empty. Check video and logs, or something was changed on SDK side.");
            }
            return list.get(3);
        }
    }

    public static WebElement getLengthInput() {
        List<WebElement> list;
        if (PrizmaDriver.isSelenoid()) {
            list = PrizmaDriver.getCurrentDriver().findElements(By.id(LENGTH_INPUT));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + LENGTH_INPUT + " is empty. Check video and logs, or something was changed on SDK side.");
            }
            return list.get(0);
        } else {
            list = PrizmaDriver.getCurrentRemoveWebDriver().findElements(By.id(LENGTH_INPUT));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + LENGTH_INPUT + " is empty. Check video and logs, or something was changed on SDK side.");
            }
            return list.get(0);
        }
    }

    public static WebElement getScalarInput() {
        List<WebElement> list;
        if (PrizmaDriver.isSelenoid()) {
            list = PrizmaDriver.getCurrentDriver().findElements(By.id(SCALAR_INPUT));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + SCALAR_INPUT + " is empty. Check video and logs, or something was changed on SDK side.");
            }
            return list.get(0);
        } else {
            list = PrizmaDriver.getCurrentRemoveWebDriver().findElements(By.id(SCALAR_INPUT));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + SCALAR_INPUT + " is empty. Check video and logs, or something was changed on SDK side.");
            }
            return list.get(0);
        }
    }

    public static WebElement getStandartDeviationInput() {
        List<WebElement> list;
        if (PrizmaDriver.isSelenoid()) {
            list = PrizmaDriver.getCurrentDriver().findElements(By.id(STANDART_DEVIATION_INPUT));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + STANDART_DEVIATION_INPUT + " is empty. Check video and logs, or something was changed on SDK side.");
            }
            return list.get(0);
        } else {
            list = PrizmaDriver.getCurrentRemoveWebDriver().findElements(By.id(STANDART_DEVIATION_INPUT));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + STANDART_DEVIATION_INPUT + " is empty. Check video and logs, or something was changed on SDK side.");
            }
            return list.get(0);
        }
    }

    public static WebElement getDifferencePeriodInput() {
        List<WebElement> list;
        if (PrizmaDriver.isSelenoid()) {
            list = PrizmaDriver.getCurrentDriver().findElements(By.id(DIFFERENCE_PERIOD_INPUT));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + DIFFERENCE_PERIOD_INPUT + " is empty. Check video and logs, or something was changed on SDK side.");
            }
            return list.get(0);
        } else {
            list = PrizmaDriver.getCurrentRemoveWebDriver().findElements(By.id(DIFFERENCE_PERIOD_INPUT));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + DIFFERENCE_PERIOD_INPUT + " is empty. Check video and logs, or something was changed on SDK side.");
            }
            return list.get(0);
        }
    }

    public static WebElement getDegreesOfFreedomInput() {
        List<WebElement> list;
        if (PrizmaDriver.isSelenoid()) {
            list = PrizmaDriver.getCurrentDriver().findElements(By.id(DEGREES_OF_FREEDOM_INPUT));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + DEGREES_OF_FREEDOM_INPUT + " is empty. Check video and logs, or something was changed on SDK side.");
            }
            return list.get(0);
        } else {
            list = PrizmaDriver.getCurrentRemoveWebDriver().findElements(By.id(DEGREES_OF_FREEDOM_INPUT));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + DEGREES_OF_FREEDOM_INPUT + " is empty. Check video and logs, or something was changed on SDK side.");
            }
            return list.get(0);
        }
    }

    public static WebElement getOffsetInput() {
        List<WebElement> list;
        if (PrizmaDriver.isSelenoid()) {
            list = PrizmaDriver.getCurrentDriver().findElements(By.id(OFFSET_INPUT));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + OFFSET_INPUT + " is empty. Check video and logs, or something was changed on SDK side.");
            }
            return list.get(0);
        } else {
            list = PrizmaDriver.getCurrentRemoveWebDriver().findElements(By.id(OFFSET_INPUT));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + OFFSET_INPUT + " is empty. Check video and logs, or something was changed on SDK side.");
            }
            return list.get(0);
        }
    }

    /***
     * Methods to click on the elements
     *
     */
    public static void clickOnOneMinBtnOnChartingPage() {
        try {
            PrizmaUtils.scrollToElementOnPage(getOneMinBtnOnChartingPage());
            PrizmaDriver.waitToBeClickable(getOneMinBtnOnChartingPage());
            getOneMinBtnOnChartingPage().click();

            PrizmaDriver.waitFor(PrizmaConstants.DOWNLOAD_WAIT_TIME);
            PrizmaDriver.waitFor(PrizmaConstants.DOWNLOAD_WAIT_TIME);
            PrizmaDriver.waitFor(PrizmaConstants.DOWNLOAD_WAIT_TIME);
            PrizmaDriver.waitFor(PrizmaConstants.DOWNLOAD_WAIT_TIME);
            PrizmaDriver.waitFor(PrizmaConstants.DOWNLOAD_WAIT_TIME);
        } catch (Throwable ex) {
            PrizmaDriver.waitFor(PrizmaConstants.DOWNLOAD_WAIT_TIME);
        }
    }

    public static void clickOnFiveMinBtnOnChartingPage() {
        try {
            PrizmaUtils.scrollToElementOnPage(getFiveMinBtnOnChartingPage());
            PrizmaDriver.waitToBeClickable(getFiveMinBtnOnChartingPage());
            getFiveMinBtnOnChartingPage().click();

            PrizmaDriver.waitFor(PrizmaConstants.DOWNLOAD_WAIT_TIME);
            PrizmaDriver.waitFor(PrizmaConstants.DOWNLOAD_WAIT_TIME);
            PrizmaDriver.waitFor(PrizmaConstants.DOWNLOAD_WAIT_TIME);
            PrizmaDriver.waitFor(PrizmaConstants.DOWNLOAD_WAIT_TIME);
            PrizmaDriver.waitFor(PrizmaConstants.DOWNLOAD_WAIT_TIME);
        } catch (Throwable ex) {
            PrizmaDriver.waitFor(PrizmaConstants.DOWNLOAD_WAIT_TIME);
        }
    }

    public static void clickOnFifteenMinBtnOnChartingPage() {
        try {
            PrizmaUtils.scrollToElementOnPage(getFifteenMinBtnOnChartingPage());
            PrizmaDriver.waitToBeClickable(getFifteenMinBtnOnChartingPage());
            getFifteenMinBtnOnChartingPage().click();

            PrizmaDriver.waitFor(PrizmaConstants.DOWNLOAD_WAIT_TIME);
            PrizmaDriver.waitFor(PrizmaConstants.DOWNLOAD_WAIT_TIME);
            PrizmaDriver.waitFor(PrizmaConstants.DOWNLOAD_WAIT_TIME);
            PrizmaDriver.waitFor(PrizmaConstants.DOWNLOAD_WAIT_TIME);
            PrizmaDriver.waitFor(PrizmaConstants.DOWNLOAD_WAIT_TIME);
        } catch (Throwable ex) {
            PrizmaDriver.waitFor(PrizmaConstants.DOWNLOAD_WAIT_TIME);
        }
    }

    public static void clickOnSixtyMinBtnOnChartingPage() {
        try {
            PrizmaUtils.scrollToElementOnPage(getSixtyMinBtnOnChartingPage());
            PrizmaDriver.waitToBeClickable(getSixtyMinBtnOnChartingPage());
            getSixtyMinBtnOnChartingPage().click();

            PrizmaDriver.waitFor(PrizmaConstants.DOWNLOAD_WAIT_TIME);
            PrizmaDriver.waitFor(PrizmaConstants.DOWNLOAD_WAIT_TIME);
            PrizmaDriver.waitFor(PrizmaConstants.DOWNLOAD_WAIT_TIME);
            PrizmaDriver.waitFor(PrizmaConstants.DOWNLOAD_WAIT_TIME);
            PrizmaDriver.waitFor(PrizmaConstants.DOWNLOAD_WAIT_TIME);
        } catch (Throwable ex) {
            PrizmaDriver.waitFor(PrizmaConstants.DOWNLOAD_WAIT_TIME);
        }
    }

    public static void clickOnTwelveHoursBtnOnChartingPage() {
        try {
            PrizmaUtils.scrollToElementOnPage(getTwelveHoursBtnOnChartingPage());
            PrizmaDriver.waitToBeClickable(getTwelveHoursBtnOnChartingPage());
            getTwelveHoursBtnOnChartingPage().click();

            PrizmaDriver.waitFor(PrizmaConstants.DOWNLOAD_WAIT_TIME);
            PrizmaDriver.waitFor(PrizmaConstants.DOWNLOAD_WAIT_TIME);
            PrizmaDriver.waitFor(PrizmaConstants.DOWNLOAD_WAIT_TIME);
            PrizmaDriver.waitFor(PrizmaConstants.DOWNLOAD_WAIT_TIME);
            PrizmaDriver.waitFor(PrizmaConstants.DOWNLOAD_WAIT_TIME);
        } catch (Throwable ex) {
            PrizmaDriver.waitFor(PrizmaConstants.DOWNLOAD_WAIT_TIME);
        }
    }

    public static void clickOnOneDayBtnOnChartingPage() {
        try {
            PrizmaUtils.scrollToElementOnPage(getOneDayBtnOnChartingPage());
            PrizmaDriver.waitToBeClickable(getOneDayBtnOnChartingPage());
            getOneDayBtnOnChartingPage().click();

            PrizmaDriver.waitFor(PrizmaConstants.DOWNLOAD_WAIT_TIME);
            PrizmaDriver.waitFor(PrizmaConstants.DOWNLOAD_WAIT_TIME);
            PrizmaDriver.waitFor(PrizmaConstants.DOWNLOAD_WAIT_TIME);
            PrizmaDriver.waitFor(PrizmaConstants.DOWNLOAD_WAIT_TIME);
            PrizmaDriver.waitFor(PrizmaConstants.DOWNLOAD_WAIT_TIME);
        } catch (Throwable ex) {
            PrizmaDriver.waitFor(PrizmaConstants.DOWNLOAD_WAIT_TIME);
        }
    }

    public static void clickOnCurrencyDropDownAndSelectProperOption(String currency) {
        try {
            String temp = getTextFromCurrencyDropdown();

            if (!temp.contains(currency)) {
                getCurrencyDropdown().click();

                List<WebElement> list;
                if (PrizmaDriver.isSelenoid()) {
                    list = PrizmaDriver.getCurrentDriver().findElements(By.xpath("//li[text()='" + currency + "']"));
                    if (list.size() == 0) {
                        Assert.fail("Looks like that something when wrong as List of elements with locator: " + "//li[text()='" + currency + "']" + " is empty. Check video and logs, or something was changed on SDK side.");
                    }
                    list.get(0).click();
                } else {
                    list = PrizmaDriver.getCurrentRemoveWebDriver().findElements(By.xpath("//li[text()='" + currency + "']"));
                    if (list.size() == 0) {
                        Assert.fail("Looks like that something when wrong as List of elements with locator: " + "//li[text()='" + currency + "']" + " is empty. Check video and logs, or something was changed on SDK side.");
                    }
                    list.get(0).click();
                }
            }

            PrizmaDriver.waitFor(PrizmaConstants.DOWNLOAD_WAIT_TIME);
        } catch (Throwable ex) {
            //ignore
        }
    }

    public static void clickOnIndicatorsBtn() {

        getIndicatorsBtn().click();

        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
    }

    public static void clickOnCloseIndicatorsBtn() {
        getIndicatorsCloseBtn().click();
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
    }

    public static void clickOnIndicatorsResetBtn() {
        try {
            PrizmaUtils.scrollToElementOnPage(getIndicatorsResetBtn());
            PrizmaDriver.waitToBeClickable(getIndicatorsResetBtn());
            getIndicatorsResetBtn().click();

            PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
        } catch (Throwable ex) {
            PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
        }
    }

    public static void clickOnIndicatorsApplyBtn() {
        try {
            PrizmaUtils.scrollToElementOnPage(getIndicatorsApplyBtn());
            PrizmaDriver.waitToBeClickable(getIndicatorsApplyBtn());
            getIndicatorsApplyBtn().click();

            PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
        } catch (Throwable ex) {
            PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
        }
    }

    public static void enableVolumeCheckbox() {
        String temp = getCheckedStatusFromVolumeCheckbox();
        if (temp.contains("false")) {
            getVolumeCheckboxContainer().click();
        }
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
    }

    public static void disableVolumeCheckbox() {
        String temp = getCheckedStatusFromVolumeCheckbox();
        if (temp.contains("true")) {
            getVolumeCheckboxContainer().click();
        }
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
    }

    public static void clickOnVolumeLabel() {
        try {
            PrizmaUtils.scrollToElementOnPage(getVolumeLabel());
            PrizmaDriver.waitToBeClickable(getVolumeLabel());
            getVolumeLabel().click();

            PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
        } catch (Throwable ex) {
            PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
        }
    }

    public static void enableBollingerBandsCheckbox() {
        String temp = getCheckedStatusFromBollingerBandsCheckbox();
        if (temp.contains("false")) {
            getBollingerBandsCheckboxContainer().click();
        }
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
    }

    public static void disableBollingerBandsCheckbox() {
        String temp = getCheckedStatusFromBollingerBandsCheckbox();
        if (temp.contains("true")) {
            getBollingerBandsCheckboxContainer().click();
        }
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
    }

    public static void clickOnBollingerBandsLabel() {
        try {
            PrizmaUtils.scrollToElementOnPage(getBollingerBandsLabel());
            PrizmaDriver.waitToBeClickable(getBollingerBandsLabel());
            getBollingerBandsLabel().click();

            PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
        } catch (Throwable ex) {
            PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
        }
    }

    public static void enableRelativeStrengthIndexCheckbox() {
        String temp = getCheckedStatusFromRelativeStrengthIndexCheckbox();
        if (temp.contains("false")) {
            getRelativeStrengthIndexCheckboxContainer().click();
        }
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
    }

    public static void disableRelativeStrengthIndexCheckbox() {
        String temp = getCheckedStatusFromRelativeStrengthIndexCheckbox();
        if (temp.contains("true")) {
            getRelativeStrengthIndexCheckboxContainer().click();
        }
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
    }

    public static void clickOnRelativeStrengthIndexLabel() {
        try {
            PrizmaUtils.scrollToElementOnPage(getRelativeStrengthIndexLabel());
            PrizmaDriver.waitToBeClickable(getRelativeStrengthIndexLabel());
            getRelativeStrengthIndexLabel().click();

            PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
        } catch (Throwable ex) {
            PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
        }
    }

    public static void disableAllChartingIndicatorsBeforeTests() {
        String temp = getCheckedStatusFromVolumeCheckbox();
        if (temp.contains("true")) {
            getVolumeCheckboxContainer().click();
        }
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        String temp2 = getCheckedStatusFromBollingerBandsCheckbox();
        if (temp2.contains("true")) {
            getBollingerBandsCheckboxContainer().click();
        }
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        String temp3 = getCheckedStatusFromRelativeStrengthIndexCheckbox();
        if (temp3.contains("true")) {
            getRelativeStrengthIndexCheckboxContainer().click();
        }
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
    }

    public static void enableAllChartingIndicators() {
        String temp = getCheckedStatusFromVolumeCheckbox();
        if (temp.contains("false")) {
            getVolumeCheckboxContainer().click();
        }
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        String temp2 = getCheckedStatusFromBollingerBandsCheckbox();
        if (temp2.contains("false")) {
            getBollingerBandsCheckboxContainer().click();
        }
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        String temp3 = getCheckedStatusFromRelativeStrengthIndexCheckbox();
        if (temp3.contains("false")) {
            getRelativeStrengthIndexCheckboxContainer().click();
        }
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
    }

    public static void clickOnLengthInput() {
        PrizmaUtils.scrollToElementOnPage(getLengthInput());
        PrizmaDriver.waitToBeClickable(getLengthInput());
        getLengthInput().click();
    }

    public static void clickOnScalarInput() {
        PrizmaUtils.scrollToElementOnPage(getScalarInput());
        PrizmaDriver.waitToBeClickable(getScalarInput());
        getScalarInput().click();
    }

    public static void clickOnStandartDeviationInput() {
        PrizmaUtils.scrollToElementOnPage(getStandartDeviationInput());
        PrizmaDriver.waitToBeClickable(getStandartDeviationInput());
        getStandartDeviationInput().click();
    }

    public static void clickOnDifferencePeriodInput() {
        PrizmaUtils.scrollToElementOnPage(getDifferencePeriodInput());
        PrizmaDriver.waitToBeClickable(getDifferencePeriodInput());
        getDifferencePeriodInput().click();
    }

    public static void clickOnDegreesOfFreedomInput() {
        PrizmaUtils.scrollToElementOnPage(getDegreesOfFreedomInput());
        PrizmaDriver.waitToBeClickable(getDegreesOfFreedomInput());
        getDegreesOfFreedomInput().click();
    }

    public static void clickOnOffsetInput() {
        PrizmaUtils.scrollToElementOnPage(getOffsetInput());
        PrizmaDriver.waitToBeClickable(getOffsetInput());
        getOffsetInput().click();
    }

    /***
     * Methods to clear the elements
     *
     */
    public static void clearLengthInput() {
        PrizmaUtils.scrollToElementOnPage(getLengthInput());
        PrizmaDriver.waitToBeClickable(getLengthInput());
        getLengthInput().clear();

        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
    }

    public static void clearScalarInput() {
        PrizmaUtils.scrollToElementOnPage(getScalarInput());
        PrizmaDriver.waitToBeClickable(getScalarInput());
        getScalarInput().clear();

        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
    }

    public static void clearStandartDeviationInput() {
        PrizmaUtils.scrollToElementOnPage(getStandartDeviationInput());
        PrizmaDriver.waitToBeClickable(getStandartDeviationInput());
        getStandartDeviationInput().clear();

        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
    }

    public static void clearDifferencePeriodInput() {
        PrizmaUtils.scrollToElementOnPage(getDifferencePeriodInput());
        PrizmaDriver.waitToBeClickable(getDifferencePeriodInput());
        getDifferencePeriodInput().clear();

        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
    }

    public static void clearDegreesOfFreedomInput() {
        PrizmaUtils.scrollToElementOnPage(getDegreesOfFreedomInput());
        PrizmaDriver.waitToBeClickable(getDegreesOfFreedomInput());
        getDegreesOfFreedomInput().clear();

        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
    }

    public static void clearOffsetInput() {
        PrizmaUtils.scrollToElementOnPage(getOffsetInput());
        PrizmaDriver.waitToBeClickable(getOffsetInput());
        getOffsetInput().clear();

        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
    }

    /***
     * Methods to set the value into element
     */
    public static void setLength(String length) {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        if (length.isEmpty()) {
            clickOnLengthInput();
            clearLengthInput();
        } else {
            clickOnLengthInput();
            clearLengthInput();
            getLengthInput().sendKeys(length);

            PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
        }
    }

    public static void setScalar(String scalar) {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        if (scalar.isEmpty()) {
            clickOnScalarInput();
            clearScalarInput();
        } else {
            clickOnScalarInput();
            clearScalarInput();
            getScalarInput().sendKeys(scalar);

            PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
        }
    }

    public static void setStandartDeviation(String standartDeviation) {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        if (standartDeviation.isEmpty()) {
            clickOnStandartDeviationInput();
            clearStandartDeviationInput();
        } else {
            clickOnStandartDeviationInput();
            clearStandartDeviationInput();
            getStandartDeviationInput().sendKeys(standartDeviation);

            PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
        }
    }

    public static void setDifferencePeriod(String differencePeriod) {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        if (differencePeriod.isEmpty()) {
            clickOnDifferencePeriodInput();
            clearDifferencePeriodInput();
        } else {
            clickOnDifferencePeriodInput();
            clearDifferencePeriodInput();
            getDifferencePeriodInput().sendKeys(differencePeriod);

            PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
        }
    }

    public static void setDegreesOfFreedom(String degreesOfFreedom) {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        if (degreesOfFreedom.isEmpty()) {
            clickOnDegreesOfFreedomInput();
            clearDegreesOfFreedomInput();
        } else {
            clickOnDegreesOfFreedomInput();
            clearDegreesOfFreedomInput();
            getDegreesOfFreedomInput().sendKeys(degreesOfFreedom);

            PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
        }
    }

    public static void setOffset(String offset) {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        if (offset.isEmpty()) {
            clickOnOffsetInput();
            clearOffsetInput();
        } else {
            clickOnOffsetInput();
            clearOffsetInput();
            getOffsetInput().sendKeys(offset);

            PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
        }
    }

    /***
     * Methods to get the texts from element
     *
     * @return String
     */
    public static String getTextFromCurrencyDropdown() {
        try {
            return getCurrencyDropdown().getText().toString();
        } catch (Throwable ex) {
            return "";
        }
    }

    public static String getCheckedStatusFromVolumeCheckbox() {
        try {
            return getVolumeCheckbox().getAttribute("checked").toString();
        } catch (Throwable ex) {
            return "false";
        }
    }

    public static String getCheckedStatusFromBollingerBandsCheckbox() {
        try {
            return getBollingerBandsCheckbox().getAttribute("checked").toString();
        } catch (Throwable ex) {
            return "false";
        }
    }

    public static String getCheckedStatusFromRelativeStrengthIndexCheckbox() {
        try {
            return getRelativeStrengthIndexCheckbox().getAttribute("checked").toString();
        } catch (Throwable ex) {
            return "false";
        }
    }

    public static String getEnableDisableStatusFromIndicatorsApplyBtn() {
        try {
            return getIndicatorsApplyBtn().getAttribute("disabled").toString();
        } catch (Throwable ex) {
            return "false";
        }
    }

    public static String getEnableDisableStatusFromIndicatorsResetBtn() {
        try {
            return getIndicatorsResetBtn().getAttribute("disabled").toString();
        } catch (Throwable ex) {
            return "false";
        }
    }

    public static void getValueFromLengthInputAndStoreToVariableForFutureTests() {
        try {
            LENGTH_VALUE_FOR_FUTURE_TESTS = getLengthInput().getAttribute("value").toString();
        } catch (Throwable ex) {
            //ignore
        }
    }

    public static void getValueFromScalarInputAndStoreToVariableForFutureTests() {
        try {
            SCALAR_VALUE_FOR_FUTURE_TESTS = getScalarInput().getAttribute("value").toString();
        } catch (Throwable ex) {
            //ignore
        }
    }

    public static void getValueFromStandartDeviationInputAndStoreToVariableForFutureTests() {
        try {
            STANDART_DEVIATION_VALUE_FOR_FUTURE_TESTS = getStandartDeviationInput().getAttribute("value").toString();
        } catch (Throwable ex) {
            //ignore
        }
    }

    public static void getValueFromDifferencePeriodInputAndStoreToVariableForFutureTests() {
        try {
            DIFFERENCE_PERIOD_VALUE_FOR_FUTURE_TESTS = getDifferencePeriodInput().getAttribute("value").toString();
        } catch (Throwable ex) {
            //ignore
        }
    }

    public static void getValueFromDegreesOfFreedomInputAndStoreToVariableForFutureTests() {
        try {
            DEGREES_OF_FREEDOM_VALUE_FOR_FUTURE_TESTS = getDegreesOfFreedomInput().getAttribute("value").toString();
        } catch (Throwable ex) {
            //ignore
        }
    }

    public static void getValueFromOffsetInputAndStoreToVariableForFutureTests() {
        try {
            OFFSET_VALUE_FOR_FUTURE_TESTS = getOffsetInput().getAttribute("value").toString();
        } catch (Throwable ex) {
            //ignore
        }
    }

    public static String getValueFromLengthInput() {
        try {
            return getLengthInput().getAttribute("value").toString();
        } catch (Throwable ex) {
            return "no value";
        }
    }

    public static String getValueFromScalarInput() {
        try {
            return getScalarInput().getAttribute("value").toString();
        } catch (Throwable ex) {
            return "no value";
        }
    }

    public static String getValueFromStandartDeviationInput() {
        try {
            return getStandartDeviationInput().getAttribute("value").toString();
        } catch (Throwable ex) {
            return "no value";
        }
    }

    public static String getValueFromDifferencePeriodInput() {
        try {
            return getDifferencePeriodInput().getAttribute("value").toString();
        } catch (Throwable ex) {
            return "no value";
        }
    }

    public static String getValueFromDegreesOfFreedomInput() {
        try {
            return getDegreesOfFreedomInput().getAttribute("value").toString();
        } catch (Throwable ex) {
            return "no value";
        }
    }

    public static String getValueFromOffsetInput() {
        try {
            return getOffsetInput().getAttribute("value").toString();
        } catch (Throwable ex) {
            return "no value";
        }
    }

    /***
     * Asserts methods
     */
    public static void saveForFutureTestsValuesFromAllBollingerBandsIndicatorConfigurationsFields() {
        getValueFromLengthInputAndStoreToVariableForFutureTests();
        getValueFromStandartDeviationInputAndStoreToVariableForFutureTests();
        getValueFromDegreesOfFreedomInputAndStoreToVariableForFutureTests();
        getValueFromOffsetInputAndStoreToVariableForFutureTests();
    }

    public static void verifyThatAllBollingerBandsIndicatorConfigurationsFieldsRestoreBackAfterClickOnResetBtn() {
        try {
            assertThat(getValueFromLengthInput(), containsString(LENGTH_VALUE_FOR_FUTURE_TESTS));
        } catch (Throwable ex) {
            Assert.fail("Looks like that something when wrong as Length value was: " + getValueFromLengthInput() + " when tests expect: " + LENGTH_VALUE_FOR_FUTURE_TESTS + ". Check video and logs, or something was changed on Prizma DD side.");
        }

        try {
            assertThat(getValueFromStandartDeviationInput(), containsString(STANDART_DEVIATION_VALUE_FOR_FUTURE_TESTS));
        } catch (Throwable ex) {
            Assert.fail("Looks like that something when wrong as Standart Deviation value was: " + getValueFromStandartDeviationInput() + " when tests expect: " + STANDART_DEVIATION_VALUE_FOR_FUTURE_TESTS + ". Check video and logs, or something was changed on Prizma DD side.");
        }

        try {
            assertThat(getValueFromDegreesOfFreedomInput(), containsString(DEGREES_OF_FREEDOM_VALUE_FOR_FUTURE_TESTS));
        } catch (Throwable ex) {
            Assert.fail("Looks like that something when wrong as Degrees Of Freedom value was: " + getValueFromDegreesOfFreedomInput() + " when tests expect: " + DEGREES_OF_FREEDOM_VALUE_FOR_FUTURE_TESTS + ". Check video and logs, or something was changed on Prizma DD side.");
        }

        try {
            assertThat(getValueFromOffsetInput(), containsString(OFFSET_VALUE_FOR_FUTURE_TESTS));
        } catch (Throwable ex) {
            Assert.fail("Looks like that something when wrong as Offset value was: " + getValueFromOffsetInput() + " when tests expect: " + OFFSET_VALUE_FOR_FUTURE_TESTS + ". Check video and logs, or something was changed on Prizma DD side.");
        }
    }

    public static void saveForFutureTestsValuesFromAllRelativeStrengthIndexIndicatorConfigurationsFields() {
        getValueFromLengthInputAndStoreToVariableForFutureTests();
        getValueFromScalarInputAndStoreToVariableForFutureTests();
        getValueFromDifferencePeriodInputAndStoreToVariableForFutureTests();
        getValueFromOffsetInputAndStoreToVariableForFutureTests();
    }

    public static void verifyThatAllRelativeStrengthIndexIndicatorConfigurationsFieldsRestoreBackAfterClickOnResetBtn() {
        try {
            assertThat(getValueFromLengthInput(), containsString(LENGTH_VALUE_FOR_FUTURE_TESTS));
        } catch (Throwable ex) {
            Assert.fail("Looks like that something when wrong as Length value was: " + getValueFromLengthInput() + " when tests expect: " + LENGTH_VALUE_FOR_FUTURE_TESTS + ". Check video and logs, or something was changed on Prizma DD side.");
        }

        try {
            assertThat(getValueFromScalarInput(), containsString(SCALAR_VALUE_FOR_FUTURE_TESTS));
        } catch (Throwable ex) {
            Assert.fail("Looks like that something when wrong as Scalar value was: " + getValueFromScalarInput() + " when tests expect: " + SCALAR_VALUE_FOR_FUTURE_TESTS + ". Check video and logs, or something was changed on Prizma DD side.");
        }

        try {
            assertThat(getValueFromDifferencePeriodInput(), containsString(DIFFERENCE_PERIOD_VALUE_FOR_FUTURE_TESTS));
        } catch (Throwable ex) {
            Assert.fail("Looks like that something when wrong as Difference Period value was: " + getValueFromDifferencePeriodInput() + " when tests expect: " + DIFFERENCE_PERIOD_VALUE_FOR_FUTURE_TESTS + ". Check video and logs, or something was changed on Prizma DD side.");
        }

        try {
            assertThat(getValueFromOffsetInput(), containsString(OFFSET_VALUE_FOR_FUTURE_TESTS));
        } catch (Throwable ex) {
            Assert.fail("Looks like that something when wrong as Offset value was: " + getValueFromOffsetInput() + " when tests expect: " + OFFSET_VALUE_FOR_FUTURE_TESTS + ". Check video and logs, or something was changed on Prizma DD side.");
        }
    }

    public static void verifyThatIndicatorsDialogCloseAfterClickOnCloseBtn() {
        boolean flag;
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        try {
            PrizmaDriver.waitDriver(By.xpath(RELATIVE_STRENGTH_INDEX_CHECKBOX_CONTAINER), 10);
            flag = true;
        } catch (Throwable ex) {
            flag = false;
        }

        if (flag) {
            Assert.fail("Looks like that something when wrong as after click on close btn on Indicator dialog -> dialog not close. Check video and logs, or something was changed on Prizma DD side.");
        }
    }

    public static void verifyThatVolumeCheckboxEnabled() {
        String temp = getCheckedStatusFromVolumeCheckbox();

        if (temp.contains("false")) {
            Assert.fail("Looks like that something when wrong as Volume Checkbox is not enabled when should be. Check video and logs, or something was changed on Prizma DD side.");
        }
    }

    public static void verifyThatVolumeCheckboxDisabled() {
        String temp = getCheckedStatusFromVolumeCheckbox();

        if (temp.contains("true")) {
            Assert.fail("Looks like that something when wrong as Volume Checkbox is not disabled when should be. Check video and logs, or something was changed on Prizma DD side.");
        }
    }

    public static void verifyThatBollingerBandsEnabled() {
        String temp = getCheckedStatusFromBollingerBandsCheckbox();

        if (temp.contains("false")) {
            Assert.fail("Looks like that something when wrong as Bollinger Bands Checkbox is not enabled when should be. Check video and logs, or something was changed on Prizma DD side.");
        }
    }

    public static void verifyThatBollingerBandsDisabled() {
        String temp = getCheckedStatusFromBollingerBandsCheckbox();

        if (temp.contains("true")) {
            Assert.fail("Looks like that something when wrong as Bollinger Bands Checkbox is not disabled when should be. Check video and logs, or something was changed on Prizma DD side.");
        }
    }

    public static void verifyThaRelativeStrengthIndexCheckboxEnabled() {
        String temp = getCheckedStatusFromRelativeStrengthIndexCheckbox();

        if (temp.contains("false")) {
            Assert.fail("Looks like that something when wrong as Relative Strength Index Checkbox is not enabled when should be. Check video and logs, or something was changed on Prizma DD side.");
        }
    }

    public static void verifyThatRelativeStrengthIndexCheckboxDisabled() {
        String temp = getCheckedStatusFromRelativeStrengthIndexCheckbox();

        if (temp.contains("true")) {
            Assert.fail("Looks like that something when wrong as Relative Strength Index Checkbox is not disabled when should be. Check video and logs, or something was changed on Prizma DD side.");
        }
    }

    //Method to check error message
    public static void verifyThatIndicatorNoSettingsMessageAppear(String message) {
        boolean flag;
        String element = "//*[contains(text(),'" + message + "')]";
        try {
            PrizmaDriver.waitDriver(By.xpath(element), 5);
            flag = true;
        } catch (Throwable ex) {
            flag = false;
        }

        if (!flag) {
            Assert.fail("Indicator No Settings Message with text: " + message + " not appear!");
        }
    }

    //Method to check error message
    public static void verifyThatIndicatorNoSettingsMessageNotAppear(String message) {
        boolean flag;
        String element = "//*[contains(text(),'" + message + "')]";
        try {
            PrizmaDriver.waitDriver(By.xpath(element), 5);
            flag = true;
        } catch (Throwable ex) {
            flag = false;
        }

        if (flag) {
            Assert.fail("Indicator No Settings Message with text: " + message + " appear when shouldn't!");
        }
    }

    public static void verifyThatIndicatorsApplyBtnEnabled() {
        String temp = getEnableDisableStatusFromIndicatorsApplyBtn();

        if (temp.contains("true")) {
            Assert.fail("Looks like that something when wrong as Indicators Apply Btn is not enabled when should be. Check video and logs, or something was changed on Prizma DD side.");
        }
    }

    public static void verifyThatIndicatorsApplyBtnDisabled() {
        String temp = getEnableDisableStatusFromIndicatorsApplyBtn();

        if (temp.contains("false")) {
            Assert.fail("Looks like that something when wrong as Indicators Apply Btn is not disabled when should be. Check video and logs, or something was changed on Prizma DD side.");
        }
    }

    public static void verifyThatIndicatorsResetBtnEnabled() {
        String temp = getEnableDisableStatusFromIndicatorsResetBtn();

        if (temp.contains("true")) {
            Assert.fail("Looks like that something when wrong as Indicators Reset Btn is not enabled when should be. Check video and logs, or something was changed on Prizma DD side.");
        }
    }

    public static void verifyThatIndicatorsResetBtnDisabled() {
        String temp = getEnableDisableStatusFromIndicatorsResetBtn();

        if (temp.contains("false")) {
            Assert.fail("Looks like that something when wrong as Indicators Reset Btn is not disabled when should be. Check video and logs, or something was changed on Prizma DD side.");
        }
    }
}

package app;

import org.junit.*;
import org.openqa.selenium.*;

import java.util.*;

import prizma.maxiru.framework.common.*;

import static app.PrizmaChartingPage.*;

public class PrizmaHomePage {

    //Elements with XPath, Tag and CCS selector
    public static final String BOTS_BTN = "a[href='#/bots']";
    public static final String CHARTING_BTN = "a[href='#/charting']";

    /***
     * Methods to get the elements
     *
     * @return WebElement
     */
    public static WebElement getBotsBtnOnHomePage() {
        WebElement webElement;

        if (PrizmaDriver.isSelenoid()) {
            webElement = PrizmaDriver.getCurrentDriver().findElement(By.cssSelector(BOTS_BTN));
            if (webElement.isDisplayed() == false) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + BOTS_BTN + " is empty. Check video and logs, or something was changed on DD side.");
            }
            return webElement;
        } else {
            webElement = PrizmaDriver.getCurrentRemoveWebDriver().findElement(By.cssSelector(BOTS_BTN));
            if (webElement.isDisplayed() == false) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + BOTS_BTN + " is empty. Check video and logs, or something was changed on DD side.");
            }
            return webElement;
        }
    }

    public static WebElement getChartingBtnOnHomePage() {
        WebElement webElement;

        if (PrizmaDriver.isSelenoid()) {
            webElement = PrizmaDriver.getCurrentDriver().findElement(By.cssSelector(CHARTING_BTN));
            if (webElement.isDisplayed() == false) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + CHARTING_BTN + " is empty. Check video and logs, or something was changed on DD side.");
            }
            return webElement;
        } else {
            webElement = PrizmaDriver.getCurrentRemoveWebDriver().findElement(By.cssSelector(CHARTING_BTN));
            if (webElement.isDisplayed() == false) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + CHARTING_BTN + " is empty. Check video and logs, or something was changed on DD side.");
            }
            return webElement;
        }
    }

    /***
     * Methods to click on the elements
     *
     */
    public static void clickOnBotsBtnOnHomePage() {
        try {
            PrizmaUtils.scrollToElementOnPage(getBotsBtnOnHomePage());
            PrizmaDriver.waitToBeClickable(getBotsBtnOnHomePage());
            getBotsBtnOnHomePage().click();

            PrizmaDriver.waitFor(PrizmaConstants.HOLD_WAIT_TIME);
        } catch (Throwable ex) {
            PrizmaDriver.waitFor(PrizmaConstants.HOLD_WAIT_TIME);
        }
    }

    public static void clickOnChartingBtnOnHomePage() {
        try {
            PrizmaUtils.scrollToElementOnPage(getChartingBtnOnHomePage());
            PrizmaDriver.waitToBeClickable(getChartingBtnOnHomePage());
            getChartingBtnOnHomePage().click();

            PrizmaDriver.waitFor(PrizmaConstants.HOLD_WAIT_TIME);
        } catch (Throwable ex) {
            PrizmaDriver.waitFor(PrizmaConstants.HOLD_WAIT_TIME);
            Assert.fail("Looks like that something when wrong as wait driver with locator for chart button fail. Check video and logs, or something was changed on Prizma SDK side.");
        }

        try {
            PrizmaDriver.waitDriver(By.xpath(ONE_MIN_BTN), 60);
        } catch (Throwable ex) {
            try {
                Assert.fail("Looks like that something when wrong as wait driver with locator: " + ONE_MIN_BTN + " fail. Check video and logs, or something was changed on Prizma SDK side.");
            } catch (Throwable ex2) {
                //ignore
            }
        }
    }
}

package app;

import org.junit.Assert;
import org.openqa.selenium.*;

import java.util.*;

import prizma.maxiru.framework.common.*;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;

public class PrizmaBotsDetailsPage {

    //Elements with XPath, Tag and CCS selector
    public static final String BOT_SUMMARY_TABLE = "bot-summary";
    public static final String BOT_LAST_RUN_LABEL_ON_BOT_SUMMARY_TABLE = "span[class='data-row__value']";
    public static final String BOT_FROM_TO_LABEL_ON_BOT_SUMMARY_TABLE_CONTAINER = "span[class='data-row__value']";
    public static final String BOT_FROM_TO_LABEL_ON_BOT_SUMMARY_TABLE_START_DATE = "span[class='data-row__value__item']";
    public static final String BOT_FROM_TO_LABEL_ON_BOT_SUMMARY_TABLE_END_DATE = "span[class='data-row__value__item']";
    public static final String BOT_CLOSED_POSITIONS_LABEL_ON_BOT_SUMMARY_TABLE = "span[class='data-row__value']";
    public static final String BOT_INITIAL_BALANCE_LABEL_ON_BOT_SUMMARY_TABLE = "span[class='data-row__value']";
    public static final String BOT_AVAILABLE_BALANCE_LABEL_ON_BOT_SUMMARY_TABLE = "span[class='data-row__value']";
    public static final String BOT_IN_OPEN_POSITIONS_LABEL_ON_BOT_SUMMARY_TABLE = "span[class='data-row__value']";
    public static final String BOT_TOTAL_BALANCE_LABEL_ON_BOT_SUMMARY_TABLE = "span[class='data-row__value']";
    public static final String BOT_AVERANGE_PROFIT_LABEL_ON_BOT_SUMMARY_TABLE_CONTAINER = "span[class='data-row__value']";
    public static final String BOT_AVERANGE_PROFIT_LABEL_ON_BOT_SUMMARY_TABLE_CONTAINER_CURRENCY = "span[class='data-row__value__item']";
    public static final String BOT_AVERANGE_PROFIT_LABEL_ON_BOT_SUMMARY_TABLE_CONTAINER_CURRENCY_LABEL = "span";
    public static final String BOT_AVERANGE_PROFIT_LABEL_ON_BOT_SUMMARY_TABLE_CONTAINER_PERCENTAGE = "span[class='data-row__value__item data-row__value__item_additional']";
    public static final String BOT_AVERANGE_PROFIT_LABEL_ON_BOT_SUMMARY_TABLE_CONTAINER_PERCENTAGE_LABEL = "span";
    public static final String BOT_TOTAL_PROFIT_LABEL_ON_BOT_SUMMARY_TABLE_CONTAINER = "span[class='data-row__value']";
    public static final String BOT_TOTAL_PROFIT_LABEL_ON_BOT_SUMMARY_TABLE_CONTAINER_CURRENCY = "span[class='data-row__value__item']";
    public static final String BOT_TOTAL_PROFIT_LABEL_ON_BOT_SUMMARY_TABLE_CONTAINER_CURRENCY_LABEL = "span";
    public static final String BOT_TOTAL_PROFIT_LABEL_ON_BOT_SUMMARY_TABLE_CONTAINER_PERCENTAGE = "span[class='data-row__value__item data-row__value__item_additional']";
    public static final String BOT_TOTAL_PROFIT_LABEL_ON_BOT_SUMMARY_TABLE_CONTAINER_PERCENTAGE_LABEL = "span";
    public static final String BOT_BOT_EARNED_LABEL_ON_BOT_SUMMARY_TABLE = "span[class='data-row__value']";
    public static final String POSITIONS_TABLE = "//tbody[contains(@class,'data-table__body')]";
    public static final String POSITIONS_TABLE_ROW = "tr";
    public static final String START_DATE_POSITIONS_TABLE = "td:nth-child(1) span:nth-child(1)";
    public static final String END_DATE_POSITIONS_TABLE = "td:nth-child(1) span:nth-child(2)";
    public static final String CURRENCY_PAIR_POSITIONS_TABLE = "td:nth-child(2)";
    public static final String TYPE_POSITIONS_TABLE = "td:nth-child(3)";
    public static final String SIZE_POSITIONS_TABLE = "td:nth-child(4)";
    public static final String ENTRY_PRICE_POSITIONS_TABLE = "td:nth-child(5)";
    public static final String EXIT_PRICE_POSITIONS_TABLE = "td:nth-child(6)";
    public static final String ENTRY_AMOUNT_START_PRICE_POSITIONS_TABLE = "td:nth-child(7) span:nth-child(1)";
    public static final String ENTRY_AMOUNT_COMMISSION_POSITIONS_TABLE = "td:nth-child(7) span:nth-child(2)";
    public static final String ENTRY_AMOUNT_END_PRICE_POSITIONS_TABLE = "td:nth-child(7) span:nth-child(2)";
    public static final String EXIT_AMOUNT_START_PRICE_POSITIONS_TABLE = "td:nth-child(8) span:nth-child(1)";
    public static final String EXIT_AMOUNT_COMMISSION_POSITIONS_TABLE = "td:nth-child(8) span:nth-child(2)";
    public static final String EXIT_AMOUNT_END_PRICE_POSITIONS_TABLE = "td:nth-child(8) span:nth-child(2)";

    public static final String PROFIT_POSITIONS_TABLE = "td:nth-child(7) span:nth-child(1)";
    public static final String PROFIT_PERCENTAGE_POSITIONS_TABLE = "td:nth-child(7)  span:nth-child(2)";
    public static final String VIEW_LINK_POSITIONS_TABLE = "td:nth-child(10) a";
    public static final String DATE_SORTING_TOGGLE = "//th[contains(@title, 'Toggle SortBy')]";
    public static final String PAIR_SORTING_TOGGLE = "//th[contains(@title, 'Toggle SortBy')]";
    public static final String TYPE_SORTING_TOGGLE = "//th[contains(@title, 'Toggle SortBy')]";
    public static final String PROFIT_SORTING_TOGGLE = "//th[contains(@title, 'Toggle SortBy')]";
    public static double BOT_COMMISSION_VALUE_FOR_FUTURE_TESTS = 0.01;
    public static double PERCENTAGE_FROM_INITIAL_BALANCE_WITH_WHICH_BOT_COULD_TRADE_FOR_FUTURE_TESTS = 0.1;
    public static double PERCENTAGE_FROM_INITIAL_BALANCE_WITH_WHICH_BOT_COULD_TRADE_FOR_FUTURE_TESTS_TALIB_BOT = 0.5;

    //Other elements
    public static String RUN_BOT_TIME_FOR_FUTURE_TESTS;
    public static String FROM_DATE_FOR_FUTURE_TESTS;
    public static String TO_DATE_FOR_FUTURE_TESTS;
    public static double INITIAL_BALANCE_VALUE_FOR_FUTURE_TESTS;
    public static double INITIAL_BALANCE_VALUE_FOR_CALCULATION_FOR_FUTURE_TESTS;
    public static int POSITIONS_TABLE_ROW_VALUE_FOR_FUTURE_TESTS;
    public static int TOTAL_POSITIONS_VALUE_FOR_FUTURE_TESTS;
    public static int OPEN_POSITIONS_VALUE_FOR_FUTURE_TESTS;
    public static int CLOSE_POSITIONS_VALUE_FOR_FUTURE_TESTS;
    public static double IN_OPEN_POSITIONS_CURRENCY_VALUE_FOR_FUTURE_TESTS;
    public static double TOTAL_PROFITS_CURRENCY_VALUE_FROM_ALL_POSITIONS_FOR_FUTURE_TESTS;
    public static double TOTAL_PROFITS_CURRENCY_VALUE_FROM_CLOSED_POSITIONS_FOR_FUTURE_TESTS;
    public static double TOTAL_PROFITS_CURRENCY_VALUE_FROM_OPEN_POSITIONS_FOR_FUTURE_TESTS;
    public static double TOTAL_PROFITS_PERCENTAGE_VALUE_FROM_ALL_POSITIONS_FOR_FUTURE_TESTS;
    public static double TOTAL_PROFITS_PERCENTAGE_VALUE_FROM_CLOSED_POSITIONS_FOR_FUTURE_TESTS;
    public static double TOTAL_PROFITS_PERCENTAGE_VALUE_FROM_OPEN_POSITIONS_FOR_FUTURE_TESTS;
    public static double TOTAL_PROFITS_PERCENTAGE_VALUE_FOR_FUTURE_TESTS;
    public static double TOTAL_BALANCE_VALUE_FOR_FUTURE_TESTS;
    public static double AVAILABLE_BALANCE_VALUE_FOR_FUTURE_TESTS;
    public static double AVERAGE_PROFIT_CURRENCY_VALUE_FOR_FUTURE_TESTS;
    public static double AVERAGE_PROFIT_PERCENTAGE_VALUE_FOR_FUTURE_TESTS;
    public static int CLOSE_POSITIVE_POSITIONS_VALUE_FOR_FUTURE_TESTS;
    public static double BOT_EARNED_VALUE_FOR_FUTURE_TESTS;
    public static double TRADING_FEE_FOR_FUTURE_TESTS;
    public static double POSITION_SIZE_FOR_FUTURE_TESTS;

    /***
     * Methods to get the elements
     *
     * @return WebElement
     */
    public static WebElement getBotSummaryTable() {
        List<WebElement> list;
        if (PrizmaDriver.isSelenoid()) {
            list = PrizmaDriver.getCurrentDriver().findElements(By.id(BOT_SUMMARY_TABLE));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + BOT_SUMMARY_TABLE + " is empty. Check video and logs, or something was changed on SDK side.");
            }
            return list.get(0);
        } else {
            list = PrizmaDriver.getCurrentRemoveWebDriver().findElements(By.id(BOT_SUMMARY_TABLE));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + BOT_SUMMARY_TABLE + " is empty. Check video and logs, or something was changed on SDK side.");
            }
            return list.get(0);
        }
    }

    public static WebElement getBotLastRunLabelOnBotSummaryTable() {
        List<WebElement> list;
        list = getBotSummaryTable().findElements(By.cssSelector(BOT_LAST_RUN_LABEL_ON_BOT_SUMMARY_TABLE));
        return list.get(0);
    }

    public static WebElement getBotFromToLabelOnBotSummaryTableContainer() {
        List<WebElement> list;
        list = getBotSummaryTable().findElements(By.cssSelector(BOT_FROM_TO_LABEL_ON_BOT_SUMMARY_TABLE_CONTAINER));
        return list.get(1);
    }

    public static WebElement getBotFromToLabelOnBotSummaryTableStartDate() {
        List<WebElement> list;
        list = getBotFromToLabelOnBotSummaryTableContainer().findElements(By.cssSelector(BOT_FROM_TO_LABEL_ON_BOT_SUMMARY_TABLE_START_DATE));
        return list.get(0);
    }

    public static WebElement getBotFromToLabelOnBotSummaryTableEndDate() {
        List<WebElement> list;
        list = getBotFromToLabelOnBotSummaryTableContainer().findElements(By.cssSelector(BOT_FROM_TO_LABEL_ON_BOT_SUMMARY_TABLE_END_DATE));
        return list.get(1);
    }

    public static WebElement getBotClosedPositionsLabelOnBotSummaryTable() {
        List<WebElement> list;
        list = getBotSummaryTable().findElements(By.cssSelector(BOT_CLOSED_POSITIONS_LABEL_ON_BOT_SUMMARY_TABLE));
        return list.get(2);
    }

    public static WebElement geBotInitialBalanceLabelOnBotSummaryTable() {
        List<WebElement> list;
        list = getBotSummaryTable().findElements(By.cssSelector(BOT_INITIAL_BALANCE_LABEL_ON_BOT_SUMMARY_TABLE));
        return list.get(3);
    }

    public static WebElement geBotAvailableBalanceLabelOnBotSummaryTable() {
        List<WebElement> list;
        list = getBotSummaryTable().findElements(By.cssSelector(BOT_AVAILABLE_BALANCE_LABEL_ON_BOT_SUMMARY_TABLE));
        return list.get(4);
    }

    public static WebElement geBotInOpenPositionsLabelOnBotSummaryTable() {
        List<WebElement> list;
        list = getBotSummaryTable().findElements(By.cssSelector(BOT_IN_OPEN_POSITIONS_LABEL_ON_BOT_SUMMARY_TABLE));
        return list.get(5);
    }

    public static WebElement geBotTotalBalanceLabelOnBotSummaryTable() {
        List<WebElement> list;
        list = getBotSummaryTable().findElements(By.cssSelector(BOT_TOTAL_BALANCE_LABEL_ON_BOT_SUMMARY_TABLE));
        return list.get(6);
    }

    public static WebElement geBotAverangeProfitLabelOnBotSummaryTableContainer() {
        List<WebElement> list;
        list = getBotSummaryTable().findElements(By.cssSelector(BOT_AVERANGE_PROFIT_LABEL_ON_BOT_SUMMARY_TABLE_CONTAINER));
        return list.get(7);
    }

    public static WebElement geBotAverangeProfitLabelOnBotSummaryTable() {
        return geBotAverangeProfitLabelOnBotSummaryTableContainer();
    }

    public static WebElement geBotAverangeProfitLabelCurrencyOnBotSummaryTable() {
        List<WebElement> list;
        list = geBotAverangeProfitLabelOnBotSummaryTableContainer().findElements(By.cssSelector(BOT_AVERANGE_PROFIT_LABEL_ON_BOT_SUMMARY_TABLE_CONTAINER_CURRENCY));
        return list.get(0).findElement(By.tagName(BOT_AVERANGE_PROFIT_LABEL_ON_BOT_SUMMARY_TABLE_CONTAINER_CURRENCY_LABEL));
    }

    public static WebElement geBotAverangeProfitLabelPercentageOnBotSummaryTable() {
        List<WebElement> list;
        list = geBotAverangeProfitLabelOnBotSummaryTableContainer().findElements(By.cssSelector(BOT_AVERANGE_PROFIT_LABEL_ON_BOT_SUMMARY_TABLE_CONTAINER_PERCENTAGE));
        return list.get(0).findElement(By.tagName(BOT_AVERANGE_PROFIT_LABEL_ON_BOT_SUMMARY_TABLE_CONTAINER_PERCENTAGE_LABEL));
    }

    public static WebElement geBotTotalProfitLabelOnBotSummaryTableContainer() {
        List<WebElement> list;
        list = getBotSummaryTable().findElements(By.cssSelector(BOT_TOTAL_PROFIT_LABEL_ON_BOT_SUMMARY_TABLE_CONTAINER));
        return list.get(8);
    }

    public static WebElement geBotTotalProfitLabelOnBotSummaryTable() {
        return geBotTotalProfitLabelOnBotSummaryTableContainer();
    }

    public static WebElement geBotTotalProfitLabelCurrencyOnBotSummaryTable() {
        List<WebElement> list;
        list = geBotTotalProfitLabelOnBotSummaryTableContainer().findElements(By.cssSelector(BOT_TOTAL_PROFIT_LABEL_ON_BOT_SUMMARY_TABLE_CONTAINER_CURRENCY));
        return list.get(0).findElement(By.tagName(BOT_TOTAL_PROFIT_LABEL_ON_BOT_SUMMARY_TABLE_CONTAINER_CURRENCY_LABEL));
    }

    public static WebElement geBotTotalProfitLabelPercentageOnBotSummaryTable() {
        List<WebElement> list;
        list = geBotTotalProfitLabelOnBotSummaryTableContainer().findElements(By.cssSelector(BOT_TOTAL_PROFIT_LABEL_ON_BOT_SUMMARY_TABLE_CONTAINER_PERCENTAGE));
        return list.get(0).findElement(By.tagName(BOT_TOTAL_PROFIT_LABEL_ON_BOT_SUMMARY_TABLE_CONTAINER_PERCENTAGE_LABEL));
    }

    public static WebElement geBotEarnedLabelZeroOnBotSummaryTable() {
        List<WebElement> list;
        list = getBotSummaryTable().findElements(By.cssSelector(BOT_BOT_EARNED_LABEL_ON_BOT_SUMMARY_TABLE));
        return list.get(9);
    }

    public static WebElement geBotEarnedLabelOnBotSummaryTable() {
        List<WebElement> list;
        list = getBotSummaryTable().findElements(By.cssSelector(BOT_BOT_EARNED_LABEL_ON_BOT_SUMMARY_TABLE));
        return list.get(9);
    }

    public static WebElement getPositionsTable() {
        List<WebElement> list;
        if (PrizmaDriver.isSelenoid()) {
            list = PrizmaDriver.getCurrentDriver().findElements(By.xpath(POSITIONS_TABLE));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + POSITIONS_TABLE + " is empty. Check video and logs, or something was changed on SDK side.");
            }
            return list.get(0);
        } else {
            list = PrizmaDriver.getCurrentRemoveWebDriver().findElements(By.xpath(POSITIONS_TABLE));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + POSITIONS_TABLE + " is empty. Check video and logs, or something was changed on SDK side.");
            }
            return list.get(0);
        }
    }

    public static WebElement getPositionsTableRowByPositionX(int position) {
        List<WebElement> list;
        list = getPositionsTable().findElements(By.tagName(POSITIONS_TABLE_ROW));
        return list.get(position - 1);
    }

    public static WebElement getPositionsTableRowByPositionFromVariableValue() {
        List<WebElement> list;
        list = getPositionsTable().findElements(By.tagName(POSITIONS_TABLE_ROW));
        return list.get(POSITIONS_TABLE_ROW_VALUE_FOR_FUTURE_TESTS - 1);
    }

    public static int getPositionsTableCount() {
        try {
            List<WebElement> list;
            list = getPositionsTable().findElements(By.tagName(POSITIONS_TABLE_ROW));
            return list.size();
        } catch (Throwable ex) {
            return 0;
        }
    }

    public static WebElement getStartDateInPositionsTableRowByPositionX(int position) {
//        List<WebElement> list1;
//        List<WebElement> list2;
//        list1 = getPositionsTableRowByPositionX(position).findElements(By.cssSelector(DATE_CONTAINER_ON_POSITIONS_TABLE));
//        list2 = list1.get(0).findElements(By.cssSelector(DATE_START_DATE_LABEL_ON_POSITIONS_TABLE));
//        return list2.get(0);
        return getPositionsTableRowByPositionX(position).findElement(By.cssSelector(START_DATE_POSITIONS_TABLE));
    }

    public static WebElement getStartDateInPositionsTableRowByPositionFromVariableValue() {
//        List<WebElement> list1;
//        List<WebElement> list2;
//        list1 = getPositionsTableRowByPositionFromVariableValue().findElements(By.cssSelector(DATE_CONTAINER_ON_POSITIONS_TABLE));
//        list2 = list1.get(0).findElements(By.cssSelector(DATE_START_DATE_LABEL_ON_POSITIONS_TABLE));
//        return list2.get(0);
        return getPositionsTableRowByPositionFromVariableValue().findElement(By.cssSelector(START_DATE_POSITIONS_TABLE));
    }

    public static WebElement getEndDateInPositionsTableRowByPositionX(int position) {
//        List<WebElement> list1;
//        List<WebElement> list2;
//        list1 = getPositionsTableRowByPositionX(position).findElements(By.cssSelector(DATE_CONTAINER_ON_POSITIONS_TABLE));
//        list2 = list1.get(0).findElements(By.cssSelector(DATE_END_DATE_LABEL_ON_POSITIONS_TABLE));
//        return list2.get(1);
        return getPositionsTableRowByPositionX(position).findElement(By.cssSelector(END_DATE_POSITIONS_TABLE));
    }

    public static WebElement getEndDateInPositionsTableRowByPositionFromVariableValue() {
//        List<WebElement> list1;
//        List<WebElement> list2;
//        list1 = getPositionsTableRowByPositionFromVariableValue().findElements(By.cssSelector(DATE_CONTAINER_ON_POSITIONS_TABLE));
//        list2 = list1.get(0).findElements(By.cssSelector(DATE_END_DATE_LABEL_ON_POSITIONS_TABLE));
//        return list2.get(1);
        return getPositionsTableRowByPositionFromVariableValue().findElement(By.cssSelector(END_DATE_POSITIONS_TABLE));
    }

    public static WebElement getCurrencyPairInPositionsTableRowByPositionX(int position) {
//        List<WebElement> list;
//        list = getPositionsTableRowByPositionX(position).findElements(By.cssSelector(CURRENCY_PAIR_LABEL_ON_POSITIONS_TABLE));
//        return list.get(1);
        return getPositionsTableRowByPositionX(position).findElement(By.cssSelector(CURRENCY_PAIR_POSITIONS_TABLE));
    }

    public static WebElement getCurrencyPairInPositionsTableRowByFromVariableValue() {
//        List<WebElement> list;
//        list = getPositionsTableRowByPositionFromVariableValue().findElements(By.cssSelector(CURRENCY_PAIR_LABEL_ON_POSITIONS_TABLE));
//        return list.get(1);
        return getPositionsTableRowByPositionFromVariableValue().findElement(By.cssSelector(CURRENCY_PAIR_POSITIONS_TABLE));
    }

    public static WebElement getTypeInPositionsTableRowByPositionX(int position) {
//        List<WebElement> list;
//        list = getPositionsTableRowByPositionX(position).findElements(By.cssSelector(TYPE_LABEL_ON_POSITIONS_TABLE));
//        return list.get(2);
        return getPositionsTableRowByPositionX(position).findElement(By.cssSelector(TYPE_POSITIONS_TABLE));
    }

    public static WebElement getTypeInPositionsTableRowByFromVariableValue() {
//        List<WebElement> list;
//        list = getPositionsTableRowByPositionFromVariableValue().findElements(By.cssSelector(TYPE_LABEL_ON_POSITIONS_TABLE));
//        return list.get(2);
        return getPositionsTableRowByPositionFromVariableValue().findElement(By.cssSelector(TYPE_POSITIONS_TABLE));
    }

    public static WebElement getSizeInPositionsTableRowByPositionX(int position) {
//        List<WebElement> list;
//        list = getPositionsTableRowByPositionX(position).findElements(By.cssSelector(SIZE_CONTAINER_ON_POSITIONS_TABLE));
//        return list.get(3);
        return getPositionsTableRowByPositionX(position).findElement(By.cssSelector(SIZE_POSITIONS_TABLE));
    }

    public static WebElement getSizeInPositionsTableRowByPositionFromVariableValue() {
//        List<WebElement> list;
//        list = getPositionsTableRowByPositionFromVariableValue().findElements(By.cssSelector(SIZE_CONTAINER_ON_POSITIONS_TABLE));
//        return list.get(3);
        return getPositionsTableRowByPositionFromVariableValue().findElement(By.cssSelector(SIZE_POSITIONS_TABLE));
    }

    public static WebElement getEntryPriceInPositionsTableRowByPositionX(int position) {
//        List<WebElement> list1;
//        List<WebElement> list2;
//        list1 = getPositionsTableRowByPositionX(position).findElements(By.cssSelector(ENTRY_PRICE_CONTAINER_ON_POSITIONS_TABLE));
//        list2 = list1.get(4).findElements(By.xpath(ENTRY_PRICE_LABEL_ON_POSITIONS_TABLE));
//        return list2.get(list2.size() - 1);
        return getPositionsTableRowByPositionX(position).findElement(By.cssSelector(ENTRY_PRICE_POSITIONS_TABLE));
    }

    public static WebElement getEntryPriceInPositionsTableRowByPositionFromVariableValue() {
//        List<WebElement> list1;
//        List<WebElement> list2;
//        list1 = getPositionsTableRowByPositionFromVariableValue().findElements(By.cssSelector(ENTRY_PRICE_CONTAINER_ON_POSITIONS_TABLE));
//        list2 = list1.get(4).findElements(By.xpath(ENTRY_PRICE_LABEL_ON_POSITIONS_TABLE));
//        return list2.get(list2.size() - 1);
        return getPositionsTableRowByPositionFromVariableValue().findElement(By.cssSelector(ENTRY_PRICE_POSITIONS_TABLE));
    }

    public static WebElement getExitPriceInPositionsTableRowByPositionX(int position) {
//        List<WebElement> list1;
//        List<WebElement> list2;
//        list1 = getPositionsTableRowByPositionX(position).findElements(By.cssSelector(EXIT_PRICE_CONTAINER_ON_POSITIONS_TABLE));
//        list2 = list1.get(5).findElements(By.xpath(EXIT_PRICE_LABEL_ON_POSITIONS_TABLE));
//        return list2.get(list2.size() - 1);
        return getPositionsTableRowByPositionX(position).findElement(By.cssSelector(EXIT_PRICE_POSITIONS_TABLE));
    }

    public static WebElement getExitPriceInPositionsTableRowByPositionFromVariableValue() {
//        List<WebElement> list1;
//        List<WebElement> list2;
//        list1 = getPositionsTableRowByPositionFromVariableValue().findElements(By.cssSelector(EXIT_PRICE_CONTAINER_ON_POSITIONS_TABLE));
//        list2 = list1.get(5).findElements(By.xpath(EXIT_PRICE_LABEL_ON_POSITIONS_TABLE));
//        return list2.get(list2.size() - 1);
        return getPositionsTableRowByPositionFromVariableValue().findElement(By.cssSelector(EXIT_PRICE_POSITIONS_TABLE));
    }

    public static WebElement getEntryStartValueInPositionsTableRowByPositionX(int position) {
//        List<WebElement> list1;
//        List<WebElement> list2;
//        list1 = getPositionsTableRowByPositionX(position).findElements(By.cssSelector(ENTRY_AMOUNT_CONTAINER_ON_POSITIONS_TABLE));
//        list2 = list1.get(6).findElements(By.cssSelector(ENTRY_START_VALUE_CONTAINER_ON_POSITIONS_TABLE));
//        return list2.get(0).findElement(By.tagName(ENTRY_START_VALUE_LABEL_ON_POSITIONS_TABLE));
        return getPositionsTableRowByPositionX(position).findElement(By.cssSelector(ENTRY_AMOUNT_START_PRICE_POSITIONS_TABLE));
    }

    public static WebElement getEntryStartValueInPositionsTableRowByPositionFromVariableValue() {
//        List<WebElement> list1;
//        List<WebElement> list2;
//        list1 = getPositionsTableRowByPositionFromVariableValue().findElements(By.cssSelector(ENTRY_AMOUNT_CONTAINER_ON_POSITIONS_TABLE));
//        list2 = list1.get(6).findElements(By.cssSelector(ENTRY_START_VALUE_CONTAINER_ON_POSITIONS_TABLE));
//        return list2.get(0).findElement(By.tagName(ENTRY_START_VALUE_LABEL_ON_POSITIONS_TABLE));
        return getPositionsTableRowByPositionFromVariableValue().findElement(By.cssSelector(ENTRY_AMOUNT_START_PRICE_POSITIONS_TABLE));
    }

    public static WebElement getEntryCommissionValueInPositionsTableRowByPositionX(int position) {
//        List<WebElement> list1;
//        List<WebElement> list2;
//        list1 = getPositionsTableRowByPositionX(position).findElements(By.cssSelector(ENTRY_AMOUNT_CONTAINER_ON_POSITIONS_TABLE));
//        list2 = list1.get(6).findElements(By.cssSelector(ENTRY_COMMISSION_VALUE_CONTAINER_ON_POSITIONS_TABLE));
//        return list2.get(1).findElement(By.tagName(ENTRY_COMMISSION_VALUE_LABEL_ON_POSITIONS_TABLE));
        return getPositionsTableRowByPositionX(position).findElement(By.cssSelector(ENTRY_AMOUNT_COMMISSION_POSITIONS_TABLE));
    }

    public static WebElement getEntryCommissionValueInPositionsTableRowByPositionFromVariableValue() {
//        List<WebElement> list1;
//        List<WebElement> list2;
//        list1 = getPositionsTableRowByPositionFromVariableValue().findElements(By.cssSelector(ENTRY_AMOUNT_CONTAINER_ON_POSITIONS_TABLE));
//        list2 = list1.get(6).findElements(By.cssSelector(ENTRY_COMMISSION_VALUE_CONTAINER_ON_POSITIONS_TABLE));
//        return list2.get(1).findElement(By.tagName(ENTRY_COMMISSION_VALUE_LABEL_ON_POSITIONS_TABLE));
        return getPositionsTableRowByPositionFromVariableValue().findElement(By.cssSelector(ENTRY_AMOUNT_COMMISSION_POSITIONS_TABLE));
    }

    public static WebElement getEntryEndValueInPositionsTableRowByPositionX(int position) {
//        List<WebElement> list1;
//        List<WebElement> list2;
//        list1 = getPositionsTableRowByPositionX(position).findElements(By.cssSelector(ENTRY_AMOUNT_CONTAINER_ON_POSITIONS_TABLE));
//        list2 = list1.get(6).findElements(By.cssSelector(ENTRY_END_VALUE_CONTAINER_ON_POSITIONS_TABLE));
//        return list2.get(2).findElement(By.tagName(ENTRY_END_VALUE_LABEL_ON_POSITIONS_TABLE));
        return getPositionsTableRowByPositionX(position).findElement(By.cssSelector(ENTRY_AMOUNT_END_PRICE_POSITIONS_TABLE));
    }

    public static WebElement getEntryEndValueInPositionsTableRowByPositionFromVariableValue() {
//        List<WebElement> list1;
//        List<WebElement> list2;
//        list1 = getPositionsTableRowByPositionFromVariableValue().findElements(By.cssSelector(ENTRY_AMOUNT_CONTAINER_ON_POSITIONS_TABLE));
//        list2 = list1.get(6).findElements(By.cssSelector(ENTRY_END_VALUE_CONTAINER_ON_POSITIONS_TABLE));
//        return list2.get(2).findElement(By.tagName(ENTRY_END_VALUE_LABEL_ON_POSITIONS_TABLE));
        return getPositionsTableRowByPositionFromVariableValue().findElement(By.cssSelector(ENTRY_AMOUNT_END_PRICE_POSITIONS_TABLE));
    }

    public static WebElement getExitStartValueInPositionsTableRowByPositionX(int position) {
//        List<WebElement> list1;
//        List<WebElement> list2;
//        list1 = getPositionsTableRowByPositionX(position).findElements(By.cssSelector(EXIT_AMOUNT_CONTAINER_ON_POSITIONS_TABLE));
//        list2 = list1.get(7).findElements(By.cssSelector(EXIT_START_VALUE_CONTAINER_ON_POSITIONS_TABLE));
//        return list2.get(0).findElement(By.tagName(EXIT_START_VALUE_LABEL_ON_POSITIONS_TABLE));
        return getPositionsTableRowByPositionX(position).findElement(By.cssSelector(EXIT_AMOUNT_START_PRICE_POSITIONS_TABLE));
    }

    public static WebElement getExitStartValueInPositionsTableRowByPositionFromVariableValue() {
//        List<WebElement> list1;
//        List<WebElement> list2;
//        list1 = getPositionsTableRowByPositionFromVariableValue().findElements(By.cssSelector(EXIT_AMOUNT_CONTAINER_ON_POSITIONS_TABLE));
//        list2 = list1.get(7).findElements(By.cssSelector(EXIT_START_VALUE_CONTAINER_ON_POSITIONS_TABLE));
//        return list2.get(0).findElement(By.tagName(EXIT_START_VALUE_LABEL_ON_POSITIONS_TABLE));
        return getPositionsTableRowByPositionFromVariableValue().findElement(By.cssSelector(EXIT_AMOUNT_START_PRICE_POSITIONS_TABLE));
    }

    public static WebElement getExitCommissionValueInPositionsTableRowByPositionX(int position) {
//        List<WebElement> list1;
//        List<WebElement> list2;
//        list1 = getPositionsTableRowByPositionX(position).findElements(By.cssSelector(EXIT_AMOUNT_CONTAINER_ON_POSITIONS_TABLE));
//        list2 = list1.get(7).findElements(By.cssSelector(EXIT_COMMISSION_VALUE_CONTAINER_ON_POSITIONS_TABLE));
//        return list2.get(1).findElement(By.tagName(EXIT_COMMISSION_VALUE_LABEL_ON_POSITIONS_TABLE));
        return getPositionsTableRowByPositionX(position).findElement(By.cssSelector(EXIT_AMOUNT_COMMISSION_POSITIONS_TABLE));
    }

    public static WebElement getExitCommissionValueInPositionsTableRowByPositionFromVariableValue() {
//        List<WebElement> list1;
//        List<WebElement> list2;
//        list1 = getPositionsTableRowByPositionFromVariableValue().findElements(By.cssSelector(EXIT_AMOUNT_CONTAINER_ON_POSITIONS_TABLE));
//        list2 = list1.get(7).findElements(By.cssSelector(EXIT_COMMISSION_VALUE_CONTAINER_ON_POSITIONS_TABLE));
//        return list2.get(1).findElement(By.tagName(EXIT_COMMISSION_VALUE_LABEL_ON_POSITIONS_TABLE));
        return getPositionsTableRowByPositionFromVariableValue().findElement(By.cssSelector(EXIT_AMOUNT_COMMISSION_POSITIONS_TABLE));
    }

    public static WebElement getExitEndValueInPositionsTableRowByPositionX(int position) {
//        List<WebElement> list1;
//        List<WebElement> list2;
//        list1 = getPositionsTableRowByPositionX(position).findElements(By.cssSelector(EXIT_AMOUNT_CONTAINER_ON_POSITIONS_TABLE));
//        list2 = list1.get(7).findElements(By.cssSelector(EXIT_END_VALUE_CONTAINER_ON_POSITIONS_TABLE));
//        return list2.get(2).findElement(By.tagName(EXIT_END_VALUE_LABEL_ON_POSITIONS_TABLE));
        return getPositionsTableRowByPositionX(position).findElement(By.cssSelector(EXIT_AMOUNT_END_PRICE_POSITIONS_TABLE));
    }

    public static WebElement getExitEndValueInPositionsTableRowByPositionFromVariableValue() {
//        List<WebElement> list1;
//        List<WebElement> list2;
//        list1 = getPositionsTableRowByPositionFromVariableValue().findElements(By.cssSelector(EXIT_AMOUNT_CONTAINER_ON_POSITIONS_TABLE));
//        list2 = list1.get(7).findElements(By.cssSelector(EXIT_END_VALUE_CONTAINER_ON_POSITIONS_TABLE));
//        return list2.get(2).findElement(By.tagName(EXIT_END_VALUE_LABEL_ON_POSITIONS_TABLE));
        return getPositionsTableRowByPositionFromVariableValue().findElement(By.cssSelector(EXIT_AMOUNT_END_PRICE_POSITIONS_TABLE));
    }

    public static WebElement getProfitCurrencyInPositionsTableRowByPositionX(int position) {
//        List<WebElement> list1;
//        List<WebElement> list2;
//        list1 = getPositionsTableRowByPositionX(position).findElements(By.cssSelector(PROFIT_CONTAINER_ON_POSITIONS_TABLE));
//        list2 = list1.get(8).findElements(By.cssSelector(PROFIT_CURRENCY_CONTAINER_ON_POSITIONS_TABLE));
//        return list2.get(0).findElement(By.tagName(PROFIT_CURRENCY_LABEL_ON_POSITIONS_TABLE));
        return getPositionsTableRowByPositionX(position).findElement(By.cssSelector(PROFIT_POSITIONS_TABLE));
    }

    public static WebElement getProfitCurrencyInPositionsTableRowByPositionFromVariableValue() {
//        List<WebElement> list1;
//        List<WebElement> list2;
//        list1 = getPositionsTableRowByPositionFromVariableValue().findElements(By.cssSelector(PROFIT_CONTAINER_ON_POSITIONS_TABLE));
//        list2 = list1.get(8).findElements(By.cssSelector(PROFIT_CURRENCY_CONTAINER_ON_POSITIONS_TABLE));
//        return list2.get(0).findElement(By.tagName(PROFIT_CURRENCY_LABEL_ON_POSITIONS_TABLE));
        return getPositionsTableRowByPositionFromVariableValue().findElement(By.cssSelector(PROFIT_POSITIONS_TABLE));
    }

    public static WebElement getProfitPercentageInPositionsTableRowByPositionX(int position) {
//        List<WebElement> list1;
//        List<WebElement> list2;
//        list1 = getPositionsTableRowByPositionX(position).findElements(By.cssSelector(PROFIT_CONTAINER_ON_POSITIONS_TABLE));
//        list2 = list1.get(8).findElements(By.cssSelector(PROFIT_PERCENTAGE_CONTAINER_ON_POSITIONS_TABLE));
//        return list2.get(0).findElement(By.tagName(PROFIT_PERCENTAGE_LABEL_ON_POSITIONS_TABLE));
        return getPositionsTableRowByPositionX(position).findElement(By.cssSelector(PROFIT_PERCENTAGE_POSITIONS_TABLE));
    }

    public static WebElement getProfitPercentageInPositionsTableRowByPositionFromVariableValue() {
//        List<WebElement> list1;
//        List<WebElement> list2;
//        list1 = getPositionsTableRowByPositionFromVariableValue().findElements(By.cssSelector(PROFIT_CONTAINER_ON_POSITIONS_TABLE));
//        list2 = list1.get(8).findElements(By.cssSelector(PROFIT_PERCENTAGE_CONTAINER_ON_POSITIONS_TABLE));
//        return list2.get(0).findElement(By.tagName(PROFIT_PERCENTAGE_LABEL_ON_POSITIONS_TABLE));
        return getPositionsTableRowByPositionFromVariableValue().findElement(By.cssSelector(PROFIT_PERCENTAGE_POSITIONS_TABLE));
    }

    public static WebElement getViewOnChartingButtonInPositionsTableRowByPositionX(int position) {
//        List<WebElement> list;
//        list = getPositionsTableRowByPositionX(position).findElements(By.cssSelector(VIEW_ON_CHARTING_BUTTON_ON_POSITIONS_TABLE_CONTAINER));
//        return list.get(9);
        return getPositionsTableRowByPositionX(position).findElement(By.cssSelector(VIEW_LINK_POSITIONS_TABLE));
    }

    public static WebElement getViewOnChartingButtonInPositionsTableRowByPositionFromVariableValue() {
//        List<WebElement> list;
//        list = getPositionsTableRowByPositionFromVariableValue().findElements(By.cssSelector(VIEW_ON_CHARTING_BUTTON_ON_POSITIONS_TABLE_CONTAINER));
//        return list.get(9);
        return getPositionsTableRowByPositionFromVariableValue().findElement(By.cssSelector(VIEW_LINK_POSITIONS_TABLE));
    }

    public static WebElement getPositionsTableDateSortingToggle() {
        List<WebElement> list;
        list = getPositionsTable().findElements(By.xpath(DATE_SORTING_TOGGLE));
        return list.get(0);
    }

    public static WebElement getPositionsTablePairSortingToggle() {
        List<WebElement> list;
        list = getPositionsTable().findElements(By.xpath(PAIR_SORTING_TOGGLE));
        return list.get(1);
    }

    public static WebElement getPositionsTableTypeSortingToggle() {
        List<WebElement> list;
        list = getPositionsTable().findElements(By.xpath(TYPE_SORTING_TOGGLE));
        return list.get(2);
    }

    public static WebElement getPositionsTableProfitSortingToggle() {
        List<WebElement> list;
        list = getPositionsTable().findElements(By.xpath(PROFIT_SORTING_TOGGLE));
        return list.get(3);
    }

    /***
     * Methods to click on the elements
     */
    public static void clickOnViewOnChartingButtonInPositionsTableRowByPositionX(int position) {
        try {
            PrizmaUtils.scrollToElementOnPage(getViewOnChartingButtonInPositionsTableRowByPositionX(position));
            getViewOnChartingButtonInPositionsTableRowByPositionX(position).click();

            PrizmaDriver.waitFor(PrizmaConstants.CHARTING_WAIT_TIME);
        } catch (Throwable ex) {
            PrizmaDriver.waitFor(PrizmaConstants.CHARTING_WAIT_TIME);
        }
    }

    public static void clickOnViewOnViewOnChartingButtonInPositionsTableRowByPositionFromVariableValue() {
        try {
            PrizmaUtils.scrollToElementOnPage(getViewOnChartingButtonInPositionsTableRowByPositionFromVariableValue());
            getViewOnChartingButtonInPositionsTableRowByPositionFromVariableValue().click();

            PrizmaDriver.waitFor(PrizmaConstants.CHARTING_WAIT_TIME);
        } catch (Throwable ex) {
            PrizmaDriver.waitFor(PrizmaConstants.CHARTING_WAIT_TIME);
        }
    }

    public static void clickOnPositionsTableDateSortingToggle() {
        try {
            PrizmaUtils.scrollToElementOnPage(getPositionsTableDateSortingToggle());
            getPositionsTableDateSortingToggle().click();

            PrizmaDriver.waitFor(PrizmaConstants.LONG_WAIT_TIME);
        } catch (Throwable ex) {
            PrizmaDriver.waitFor(PrizmaConstants.LONG_WAIT_TIME);
        }
    }

    public static void clickOnPositionsTablePairSortingToggle() {
        try {
            PrizmaUtils.scrollToElementOnPage(getPositionsTablePairSortingToggle());
            getPositionsTablePairSortingToggle().click();

            PrizmaDriver.waitFor(PrizmaConstants.LONG_WAIT_TIME);
        } catch (Throwable ex) {
            PrizmaDriver.waitFor(PrizmaConstants.LONG_WAIT_TIME);
        }
    }

    public static void clickOnPositionsTableTypeSortingToggle() {
        try {
            PrizmaUtils.scrollToElementOnPage(getPositionsTableTypeSortingToggle());
            getPositionsTableTypeSortingToggle().click();

            PrizmaDriver.waitFor(PrizmaConstants.LONG_WAIT_TIME);
        } catch (Throwable ex) {
            PrizmaDriver.waitFor(PrizmaConstants.LONG_WAIT_TIME);
        }
    }

    public static void clickOnPositionsTableProfitSortingToggle() {
        try {
            PrizmaUtils.scrollToElementOnPage(getPositionsTableProfitSortingToggle());
            getPositionsTableProfitSortingToggle().click();

            PrizmaDriver.waitFor(PrizmaConstants.LONG_WAIT_TIME);
        } catch (Throwable ex) {
            PrizmaDriver.waitFor(PrizmaConstants.LONG_WAIT_TIME);
        }
    }

    /***
     * Methods to get the texts from element
     *
     * @return String
     */
    public static String getTextFromBotLastRunLabelOnBotSummaryTable() {
        try {
            return getBotLastRunLabelOnBotSummaryTable().getText().toString();
        } catch (Throwable ex) {
            return "";
        }
    }

    public static String getTextFromToBotFromLabelOnBotSummaryTableStartDate() {
        try {
            return getBotFromToLabelOnBotSummaryTableStartDate().getText().toString();
        } catch (Throwable ex) {
            return "";
        }
    }

    public static String getTextFromToBotFromLabelOnBotSummaryTableEndDate() {
        try {
            return getBotFromToLabelOnBotSummaryTableEndDate().getText().toString();
        } catch (Throwable ex) {
            return "";
        }
    }

    public static String getTextFromClosedPositionsLabelOnBotSummaryTable() {
        try {
            return getBotClosedPositionsLabelOnBotSummaryTable().getText().toString();
        } catch (Throwable ex) {
            return "";
        }
    }

    public static String getTextFromBotInitialBalanceLabelOnBotSummaryTable() {
        try {
            return geBotInitialBalanceLabelOnBotSummaryTable().getText().toString();
        } catch (Throwable ex) {
            return "";
        }
    }

    public static String getTextFromBotAvailableBalanceLabelOnBotSummaryTable() {
        try {
            return geBotAvailableBalanceLabelOnBotSummaryTable().getText().toString();
        } catch (Throwable ex) {
            return "";
        }
    }

    public static String getTextFromBotInOpenPositionsLabelOnBotSummaryTable() {
        try {
            return geBotInOpenPositionsLabelOnBotSummaryTable().getText().toString();
        } catch (Throwable ex) {
            return "";
        }
    }

    public static String getTextFromBotTotalBalanceLabelOnBotSummaryTable() {
        try {
            return geBotTotalBalanceLabelOnBotSummaryTable().getText().toString();
        } catch (Throwable ex) {
            return "";
        }
    }

    public static String getTextFromBotAverangeProfitLabelOnBotSummaryTable() {
        try {
            return geBotAverangeProfitLabelOnBotSummaryTable().getText().toString();
        } catch (Throwable ex) {
            return "";
        }
    }

    public static String getTextFromBotAverangeProfitLabelCurrencyOnBotSummaryTable() {
        try {
            return geBotAverangeProfitLabelCurrencyOnBotSummaryTable().getText().toString();
        } catch (Throwable ex) {
            return "";
        }
    }

    public static String getTextFromBotAverangeProfitLabelPercentageOnBotSummaryTable() {
        try {
            return geBotAverangeProfitLabelPercentageOnBotSummaryTable().getText().toString();
        } catch (Throwable ex) {
            return "";
        }
    }

    public static String getTextFromBotTotalProfitLabelOnBotSummaryTable() {
        try {
            return geBotTotalProfitLabelOnBotSummaryTable().getText().toString();
        } catch (Throwable ex) {
            return "";
        }
    }

    public static String getTextFromBotTotalProfitLabelCurrencyOnBotSummaryTable() {
        try {
            return geBotTotalProfitLabelCurrencyOnBotSummaryTable().getText().toString();
        } catch (Throwable ex) {
            return "";
        }
    }

    public static String getTextFromBotTotalProfitLabelPercentageOnBotSummaryTable() {
        try {
            return geBotTotalProfitLabelPercentageOnBotSummaryTable().getText().toString();
        } catch (Throwable ex) {
            return "";
        }
    }

    public static String getTextFromBotEarnedLabelZeroOnBotSummaryTable() {
        try {
            return geBotEarnedLabelZeroOnBotSummaryTable().getText().toString();
        } catch (Throwable ex) {
            return "";
        }
    }

    public static String getTextFromBotEarnedLabelOnBotSummaryTable() {
        try {
            return geBotEarnedLabelOnBotSummaryTable().getText().toString();
        } catch (Throwable ex) {
            return "";
        }
    }

    public static String getTextFromStartDateInPositionsTableRowByPositionX(int position) {
        try {
            return getStartDateInPositionsTableRowByPositionX(position).getText().toString();
        } catch (Throwable ex) {
            return "";
        }
    }

    public static String getTextFromStartDateInPositionsTableRowByPositionFromVariableValue() {
        try {
            return getStartDateInPositionsTableRowByPositionFromVariableValue().getText().toString();
        } catch (Throwable ex) {
            return "";
        }
    }

    public static String getTextFromEndDateInPositionsTableRowByPositionX(int position) {
        try {
            return getEndDateInPositionsTableRowByPositionX(position).getText().toString();
        } catch (Throwable ex) {
            return "";
        }
    }

    public static String getTextFromEndDateInPositionsTableRowByPositionFromVariableValue() {
        try {
            return getEndDateInPositionsTableRowByPositionFromVariableValue().getText().toString();
        } catch (Throwable ex) {
            return "";
        }
    }

    public static String getTextFromCurrencyPairInPositionsTableRowByPositionX(int position) {
        try {
            return getCurrencyPairInPositionsTableRowByPositionX(position).getText().toString();
        } catch (Throwable ex) {
            return "";
        }
    }

    public static String getTextFromCurrencyPairInPositionsTableRowByFromVariableValue() {
        try {
            return getCurrencyPairInPositionsTableRowByFromVariableValue().getText().toString();
        } catch (Throwable ex) {
            return "";
        }
    }

    public static String getTextFromTypeInPositionsTableRowByPositionX(int position) {
        try {
            return getTypeInPositionsTableRowByPositionX(position).getText().toString();
        } catch (Throwable ex) {
            return "";
        }
    }

    public static String getTextFromTypeInPositionsTableRowByFromVariableValue() {
        try {
            return getTypeInPositionsTableRowByFromVariableValue().getText().toString();
        } catch (Throwable ex) {
            return "";
        }
    }

    public static String getTextFromSizeInPositionsTableRowByPositionX(int position) {
        try {
            return getSizeInPositionsTableRowByPositionX(position).getText().toString();
        } catch (Throwable ex) {
            return "";
        }
    }

    public static String getTextFromSizeInPositionsTableRowByFromVariableValue() {
        try {
            return getSizeInPositionsTableRowByPositionFromVariableValue().getText().toString();
        } catch (Throwable ex) {
            return "";
        }
    }

    public static String getTextFromEntryPriceInPositionsTableRowByPositionX(int position) {
        try {
            return getEntryPriceInPositionsTableRowByPositionX(position).getText().toString();
        } catch (Throwable ex) {
            return "";
        }
    }

    public static String getTextFromEntryPriceInPositionsTableRowByFromVariableValue() {
        try {
            return getEntryPriceInPositionsTableRowByPositionFromVariableValue().getText().toString();
        } catch (Throwable ex) {
            return "";
        }
    }

    public static String getTextFromExitPriceInPositionsTableRowByPositionX(int position) {
        try {
            return getExitPriceInPositionsTableRowByPositionX(position).getText().toString();
        } catch (Throwable ex) {
            return "";
        }
    }

    public static String getTextFromExitPriceInPositionsTableRowByFromVariableValue() {
        try {
            return getExitPriceInPositionsTableRowByPositionFromVariableValue().getText().toString();
        } catch (Throwable ex) {
            return "";
        }
    }

    public static String getTextFromEntryStartValueInPositionsTableRowByPositionX(int position) {
        try {
            return getEntryStartValueInPositionsTableRowByPositionX(position).getText().toString();
        } catch (Throwable ex) {
            return "";
        }
    }

    public static String getTextFromEntryStartValueInPositionsTableRowByPositionFromVariableValue() {
        try {
            return getEntryStartValueInPositionsTableRowByPositionFromVariableValue().getText().toString();
        } catch (Throwable ex) {
            return "";
        }
    }

    public static String getTextFromEntryCommissionValueInPositionsTableRowByPositionX(int position) {
        try {
            return getEntryCommissionValueInPositionsTableRowByPositionX(position).getText().toString();
        } catch (Throwable ex) {
            return "";
        }
    }

    public static String getTextFromEntryCommissionValueInPositionsTableRowByPositionFromVariableValue() {
        try {
            return getEntryCommissionValueInPositionsTableRowByPositionFromVariableValue().getText().toString();
        } catch (Throwable ex) {
            return "";
        }
    }

    public static String getTextFromEntryEndValueInPositionsTableRowByPositionX(int position) {
        try {
            return getEntryEndValueInPositionsTableRowByPositionX(position).getText().toString();
        } catch (Throwable ex) {
            return "";
        }
    }

    public static String getTextFromEntryEndValueInPositionsTableRowByPositionFromVariableValue() {
        try {
            return getEntryEndValueInPositionsTableRowByPositionFromVariableValue().getText().toString();
        } catch (Throwable ex) {
            return "";
        }
    }

    public static String getTextFromExitStartValueInPositionsTableRowByPositionX(int position) {
        try {
            return getExitStartValueInPositionsTableRowByPositionX(position).getText().toString();
        } catch (Throwable ex) {
            return "";
        }
    }

    public static String getTextFromExitStartValueInPositionsTableRowByPositionFromVariableValue() {
        try {
            return getExitStartValueInPositionsTableRowByPositionFromVariableValue().getText().toString();
        } catch (Throwable ex) {
            return "";
        }
    }

    public static String getTextFromExitCommissionValueInPositionsTableRowByPositionX(int position) {
        try {
            return getExitCommissionValueInPositionsTableRowByPositionX(position).getText().toString();
        } catch (Throwable ex) {
            return "";
        }
    }

    public static String getTextFromExitCommissionValueInPositionsTableRowByPositionFromVariableValue() {
        try {
            return getExitCommissionValueInPositionsTableRowByPositionFromVariableValue().getText().toString();
        } catch (Throwable ex) {
            return "";
        }
    }

    public static String getTextFromExitEndValueInPositionsTableRowByPositionX(int position) {
        try {
            return getExitEndValueInPositionsTableRowByPositionX(position).getText().toString();
        } catch (Throwable ex) {
            return "";
        }
    }

    public static String getTextFromExitEndValueInPositionsTableRowByPositionFromVariableValue() {
        try {
            return getExitEndValueInPositionsTableRowByPositionFromVariableValue().getText().toString();
        } catch (Throwable ex) {
            return "";
        }
    }

    public static String getTextFromProfitCurrencyInPositionsTableRowByPositionX(int position) {
        try {
            return getProfitCurrencyInPositionsTableRowByPositionX(position).getText().toString();
        } catch (Throwable ex) {
            return "";
        }
    }

    public static String getTextFromProfitCurrencyInPositionsTableRowByPositionFromVariableValue() {
        try {
            return getProfitCurrencyInPositionsTableRowByPositionFromVariableValue().getText().toString();
        } catch (Throwable ex) {
            return "";
        }
    }

    public static String getTextFromProfitPercentageInPositionsTableRowByPositionX(int position) {
        try {
            return getProfitPercentageInPositionsTableRowByPositionX(position).getText().toString();
        } catch (Throwable ex) {
            return "";
        }
    }

    public static String getTextFromProfitPercentageInPositionsTableRowByPositionFromVariableValue() {
        try {
            return getProfitPercentageInPositionsTableRowByPositionFromVariableValue().getText().toString();
        } catch (Throwable ex) {
            return "";
        }
    }

    /***
     * Asserts methods
     */
    public static void verifyThatBotLastRunIsCorrectOnBotSummaryTable(String botRun) {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        try {
            assertThat(botRun, containsString(getTextFromBotLastRunLabelOnBotSummaryTable()));
        } catch (Throwable ex) {
            PrizmaUtils.logFailedTestResultsToTheLogsFile("Looks like that something when wrong as bot last run was: " + getTextFromBotLastRunLabelOnBotSummaryTable() + " when tests expect: " + botRun + ". Check video and logs, or something was changed on Prizma SDK side.");
        }
    }

    public static void verifyThatBotLastRunIsCorrectOnBotSummaryTableIfValidateWithVariableValue() {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        try {
            assertThat(RUN_BOT_TIME_FOR_FUTURE_TESTS, containsString(getTextFromBotLastRunLabelOnBotSummaryTable()));
        } catch (Throwable ex) {
            PrizmaUtils.logFailedTestResultsToTheLogsFile("Looks like that something when wrong as bot last run was: " + getTextFromBotLastRunLabelOnBotSummaryTable() + " when tests expect: " + RUN_BOT_TIME_FOR_FUTURE_TESTS + ". Check video and logs, or something was changed on Prizma SDK side.");
        }
    }

    public static void verifyThatBotFromToIsCorrectOnBotSummaryTableStartDate(String date) {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        try {
            assertThat(getTextFromToBotFromLabelOnBotSummaryTableStartDate(), containsString(date));
        } catch (Throwable ex) {
            PrizmaUtils.logFailedTestResultsToTheLogsFile("Looks like that something when wrong as bot from/to start date from date was: " + getTextFromToBotFromLabelOnBotSummaryTableStartDate() + " when tests expect: " + date + ". Check video and logs, or something was changed on Prizma SDK side.");
        }
    }

    public static void verifyThatBotFromToIsCorrectOnBotSummaryTableStartDateIfValidateWithVariableValue() {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        try {
            assertThat(getTextFromToBotFromLabelOnBotSummaryTableStartDate(), containsString(FROM_DATE_FOR_FUTURE_TESTS));
        } catch (Throwable ex) {
            PrizmaUtils.logFailedTestResultsToTheLogsFile("Looks like that something when wrong as bot from/to start date from date was: " + getTextFromToBotFromLabelOnBotSummaryTableStartDate() + " when tests expect: " + FROM_DATE_FOR_FUTURE_TESTS + ". Check video and logs, or something was changed on Prizma SDK side.");
        }
    }

    public static void verifyThatBotFromToIsCorrectOnBotSummaryTableEndDate(String date) {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        try {
            assertThat(getTextFromToBotFromLabelOnBotSummaryTableEndDate(), containsString(date));
        } catch (Throwable ex) {
            PrizmaUtils.logFailedTestResultsToTheLogsFile("Looks like that something when wrong as bot from/to end date from date was: " + getTextFromToBotFromLabelOnBotSummaryTableEndDate() + " when tests expect: " + date + ". Check video and logs, or something was changed on Prizma SDK side.");
        }
    }

    public static void verifyThatBotFromToIsCorrectOnBotSummaryTableEndDateIfValidateWithVariableValue() {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        try {
            assertThat(getTextFromToBotFromLabelOnBotSummaryTableEndDate(), containsString(TO_DATE_FOR_FUTURE_TESTS));
        } catch (Throwable ex) {
            PrizmaUtils.logFailedTestResultsToTheLogsFile("Looks like that something when wrong as bot from/to end date from date was: " + getTextFromToBotFromLabelOnBotSummaryTableEndDate() + " when tests expect: " + TO_DATE_FOR_FUTURE_TESTS + ". Check video and logs, or something was changed on Prizma SDK side.");
        }
    }

    public static void verifyThatBotClosedPositionsIsCorrectOnBotSummaryTable(String count) {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        try {
            assertThat(getTextFromClosedPositionsLabelOnBotSummaryTable(), containsString(count));
        } catch (Throwable ex) {
            PrizmaUtils.logFailedTestResultsToTheLogsFile("Looks like that something when wrong as bot closed positions was: " + getTextFromClosedPositionsLabelOnBotSummaryTable() + " when tests expect: " + count + ". Check video and logs, or something was changed on Prizma SDK side.");
        }
    }

    public static void verifyThatBotClosedPositionsIsCorrectOnBotSummaryTableIfValidateWithVariableValue() {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        try {
            assertThat(getTextFromClosedPositionsLabelOnBotSummaryTable(), containsString(String.valueOf(CLOSE_POSITIONS_VALUE_FOR_FUTURE_TESTS)));
        } catch (Throwable ex) {
            PrizmaUtils.logFailedTestResultsToTheLogsFile("Looks like that something when wrong as bot closed positions was: " + getTextFromClosedPositionsLabelOnBotSummaryTable() + " when tests expect: " + CLOSE_POSITIONS_VALUE_FOR_FUTURE_TESTS + ". Check video and logs, or something was changed on Prizma SDK side.");
        }
    }

    public static void verifyThatBotInitialBalanceIsCorrectOnBotSummaryTable(String initialBalance) {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        try {
            assertThat(getTextFromBotInitialBalanceLabelOnBotSummaryTable(), containsString(initialBalance));
        } catch (Throwable ex) {
            PrizmaUtils.logFailedTestResultsToTheLogsFile("Looks like that something when wrong as bot initial balance was: " + getTextFromBotInitialBalanceLabelOnBotSummaryTable() + " when tests expect: " + initialBalance + ". Check video and logs, or something was changed on Prizma SDK side.");
        }
    }

    public static void verifyThatBotInitialBalanceIsCorrectOnBotSummaryTableIfValidateWithVariableValue() {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        double tempValue = PrizmaUtils.showOnlyTwoDigitAfterDecimal(INITIAL_BALANCE_VALUE_FOR_FUTURE_TESTS);
        String temp = PrizmaUtils.addCommaForThousandNumber(Double.toString(tempValue));
        temp = PrizmaUtils.addDollarSignInTheBegging(temp);

        try {
            assertThat(getTextFromBotInitialBalanceLabelOnBotSummaryTable(), containsString(temp));
        } catch (Throwable ex) {
            PrizmaUtils.logFailedTestResultsToTheLogsFile("Looks like that something when wrong as bot initial balance was: " + getTextFromBotInitialBalanceLabelOnBotSummaryTable() + " when tests expect: " + temp + ". Check video and logs, or something was changed on Prizma SDK side.");
        }
    }

    public static void verifyThatBotAvailableBalanceIsCorrectOnBotSummaryTable(String availableBalance) {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        try {
            assertThat(getTextFromBotAvailableBalanceLabelOnBotSummaryTable(), containsString(availableBalance));
        } catch (Throwable ex) {
            PrizmaUtils.logFailedTestResultsToTheLogsFile("Looks like that something when wrong as bot initial balance was: " + getTextFromBotAvailableBalanceLabelOnBotSummaryTable() + " when tests expect: " + availableBalance + ". Check video and logs, or something was changed on Prizma SDK side.");
        }
    }

    public static void verifyThatBotInOpenPositionsBalanceIsCorrectOnBotSummaryTable(String inOpenPositionsBalance) {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        try {
            assertThat(getTextFromBotInOpenPositionsLabelOnBotSummaryTable(), containsString(inOpenPositionsBalance));
        } catch (Throwable ex) {
            PrizmaUtils.logFailedTestResultsToTheLogsFile("Looks like that something when wrong as bot in open positions balance was: " + getTextFromBotInOpenPositionsLabelOnBotSummaryTable() + " when tests expect: " + inOpenPositionsBalance + ". Check video and logs, or something was changed on Prizma SDK side.");
        }
    }

    public static void verifyThatBotInOpenPositionsBalanceIsCorrectOnBotSummaryTableIfValidateWithVariableValue() {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        double tempValue = PrizmaUtils.showOnlyTwoDigitAfterDecimal(IN_OPEN_POSITIONS_CURRENCY_VALUE_FOR_FUTURE_TESTS);
        String temp = PrizmaUtils.addCommaForThousandNumber(Double.toString(tempValue));
        temp = PrizmaUtils.addDollarSignInTheBegging(temp);

        try {
            assertThat(getTextFromBotInOpenPositionsLabelOnBotSummaryTable(), containsString(temp));
        } catch (Throwable ex) {
            PrizmaUtils.logFailedTestResultsToTheLogsFile("Looks like that something when wrong as bot in open positions balance was: " + getTextFromBotInOpenPositionsLabelOnBotSummaryTable() + " when tests expect: " + temp + ". Check video and logs, or something was changed on Prizma SDK side.");
        }
    }

    public static void verifyThatBotTotalBalanceIsCorrectOnBotSummaryTable(String endingBalance) {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        try {
            assertThat(getTextFromBotTotalBalanceLabelOnBotSummaryTable(), containsString(endingBalance));
        } catch (Throwable ex) {
            PrizmaUtils.logFailedTestResultsToTheLogsFile("Looks like that something when wrong as bot total balance was: " + getTextFromBotTotalBalanceLabelOnBotSummaryTable() + " when tests expect: " + endingBalance + ". Check video and logs, or something was changed on Prizma SDK side.");
        }
    }

    public static void verifyThatBotTotalBalanceIsCorrectOnBotSummaryTableIfValidateWithVariableValue() {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        double tempValue = PrizmaUtils.showOnlyTwoDigitAfterDecimal(TOTAL_BALANCE_VALUE_FOR_FUTURE_TESTS);
        String temp = PrizmaUtils.addCommaForThousandNumber(Double.toString(tempValue));
        temp = PrizmaUtils.addDollarSignInTheBegging(temp);

        try {
            assertThat(getTextFromBotTotalBalanceLabelOnBotSummaryTable(), containsString(temp));
        } catch (Throwable ex) {
            PrizmaUtils.logFailedTestResultsToTheLogsFile("Looks like that something when wrong as bot total balance was: " + getTextFromBotTotalBalanceLabelOnBotSummaryTable() + " when tests expect: " + temp + ". Check video and logs, or something was changed on Prizma SDK side.");
        }
    }

    public static void verifyThatBotAverangeProfitCurrencyIsCorrectOnBotSummaryTable(String averangeProfitCurrency) {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        try {
            assertThat(getTextFromBotAverangeProfitLabelCurrencyOnBotSummaryTable(), containsString(averangeProfitCurrency));
        } catch (Throwable ex) {
            PrizmaUtils.logFailedTestResultsToTheLogsFile("Looks like that something when wrong as bot averange profit currency was: " + getTextFromBotAverangeProfitLabelCurrencyOnBotSummaryTable() + " when tests expect: " + averangeProfitCurrency + ". Check video and logs, or something was changed on Prizma SDK side.");
        }
    }

    public static void verifyThatBotAverangeProfitIsCorrectOnBotSummaryTableIfValidateWithVariableValueWhichIsZero() {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        String temp = "";

        if (AVERAGE_PROFIT_CURRENCY_VALUE_FOR_FUTURE_TESTS == 0) {
            temp = "–";
        }

        try {
            assertThat(getTextFromBotAverangeProfitLabelOnBotSummaryTable(), containsString(temp));
        } catch (Throwable ex) {
            PrizmaUtils.logFailedTestResultsToTheLogsFile("Looks like that something when wrong as bot averange profit currency was: " + getTextFromBotAverangeProfitLabelOnBotSummaryTable() + " when tests expect: " + temp + ". Check video and logs, or something was changed on Prizma SDK side.");
        }
    }

    public static void verifyThatBotAverangeProfitCurrencyIsCorrectOnBotSummaryTableIfValidateWithVariableValue() {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        double tempValue = PrizmaUtils.showOnlyTwoDigitAfterDecimal(AVERAGE_PROFIT_CURRENCY_VALUE_FOR_FUTURE_TESTS);
        String temp = PrizmaUtils.addCommaForThousandNumber(Double.toString(tempValue));
        temp = PrizmaUtils.addDollarSignInTheBegging(temp);

        try {
            assertThat(getTextFromBotAverangeProfitLabelCurrencyOnBotSummaryTable(), containsString(temp));
        } catch (Throwable ex) {
            PrizmaUtils.logFailedTestResultsToTheLogsFile("Looks like that something when wrong as bot averange profit currency was: " + getTextFromBotAverangeProfitLabelCurrencyOnBotSummaryTable() + " when tests expect: " + temp + ". Check video and logs, or something was changed on Prizma SDK side.");
        }
    }

    public static void verifyThatBotAverangeProfitPercentageIsCorrectOnBotSummaryTable(String averangeProfitPercentage) {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        try {
            assertThat(getTextFromBotAverangeProfitLabelPercentageOnBotSummaryTable(), containsString(averangeProfitPercentage));
        } catch (Throwable ex) {
            PrizmaUtils.logFailedTestResultsToTheLogsFile("Looks like that something when wrong as bot averange profit percentage was: " + getTextFromBotAverangeProfitLabelPercentageOnBotSummaryTable() + " when tests expect: " + averangeProfitPercentage + ". Check video and logs, or something was changed on Prizma SDK side.");
        }
    }

    public static void verifyThatBotAverangeProfitPercentageIsCorrectOnBotSummaryTableIfValidateWithVariableValue() {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        double tempValue = PrizmaUtils.showOnlyTwoDigitAfterDecimal(AVERAGE_PROFIT_PERCENTAGE_VALUE_FOR_FUTURE_TESTS);
        String temp = PrizmaUtils.addCommaForThousandNumber(Double.toString(tempValue));
        temp = PrizmaUtils.addPercentageSignInTheEnd(temp);

        try {
            assertThat(getTextFromBotAverangeProfitLabelPercentageOnBotSummaryTable(), containsString(temp));
        } catch (Throwable ex) {
            PrizmaUtils.logFailedTestResultsToTheLogsFile("Looks like that something when wrong as bot averange profit percentage was: " + getTextFromBotAverangeProfitLabelPercentageOnBotSummaryTable() + " when tests expect: " + temp + ". Check video and logs, or something was changed on Prizma SDK side.");
        }
    }

    public static void verifyThatBotTotalProfitCurrencyIsCorrectOnBotSummaryTable(String totalProfitCurrency) {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        try {
            assertThat(getTextFromBotTotalProfitLabelCurrencyOnBotSummaryTable(), containsString(totalProfitCurrency));
        } catch (Throwable ex) {
            PrizmaUtils.logFailedTestResultsToTheLogsFile("Looks like that something when wrong as bot total profit currency was: " + getTextFromBotTotalProfitLabelCurrencyOnBotSummaryTable() + " when tests expect: " + totalProfitCurrency + ". Check video and logs, or something was changed on Prizma SDK side.");
        }
    }

    public static void verifyThatBotTotalProfitIsCorrectOnBotSummaryTableIfValidateWithVariableValueWhichIsZero() {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        String temp = "";

        if (TOTAL_PROFITS_CURRENCY_VALUE_FROM_ALL_POSITIONS_FOR_FUTURE_TESTS == 0) {
            temp = "–";
        }

        try {
            assertThat(getTextFromBotTotalProfitLabelOnBotSummaryTable(), containsString(temp));
        } catch (Throwable ex) {
            PrizmaUtils.logFailedTestResultsToTheLogsFile("Looks like that something when wrong as bot total profit currency was: " + getTextFromBotTotalProfitLabelOnBotSummaryTable() + " when tests expect: " + temp + ". Check video and logs, or something was changed on Prizma SDK side.");
        }
    }

    public static void verifyThatBotTotalProfitCurrencyIsCorrectOnBotSummaryTableIfValidateWithVariableValue() {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        double tempValue = PrizmaUtils.showOnlyTwoDigitAfterDecimal(TOTAL_PROFITS_CURRENCY_VALUE_FROM_ALL_POSITIONS_FOR_FUTURE_TESTS);
        String temp = PrizmaUtils.addCommaForThousandNumber(Double.toString(tempValue));
        temp = PrizmaUtils.addDollarSignInTheBegging(temp);

        try {
            assertThat(getTextFromBotTotalProfitLabelCurrencyOnBotSummaryTable(), containsString(temp));
        } catch (Throwable ex) {
            PrizmaUtils.logFailedTestResultsToTheLogsFile("Looks like that something when wrong as bot total profit currency was: " + getTextFromBotTotalProfitLabelCurrencyOnBotSummaryTable() + " when tests expect: " + temp + ". Check video and logs, or something was changed on Prizma SDK side.");
        }
    }

    public static void verifyThatBotTotalProfitPercentageIsCorrectOnBotSummaryTable(String totalProfitPercentage) {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        try {
            assertThat(getTextFromBotTotalProfitLabelPercentageOnBotSummaryTable(), containsString(totalProfitPercentage));
        } catch (Throwable ex) {
            PrizmaUtils.logFailedTestResultsToTheLogsFile("Looks like that something when wrong as bot total profit percentage was: " + getTextFromBotTotalProfitLabelPercentageOnBotSummaryTable() + " when tests expect: " + totalProfitPercentage + ". Check video and logs, or something was changed on Prizma SDK side.");
        }
    }

    public static void verifyThatBotTotalProfitPercentageIsCorrectOnBotSummaryTableIfValidateWithVariableValue() {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        double tempValue = PrizmaUtils.showOnlyTwoDigitAfterDecimal(TOTAL_PROFITS_PERCENTAGE_VALUE_FOR_FUTURE_TESTS);
        String temp = PrizmaUtils.addCommaForThousandNumber(Double.toString(tempValue));
        temp = PrizmaUtils.addPercentageSignInTheEnd(temp);

        try {
            assertThat(getTextFromBotTotalProfitLabelPercentageOnBotSummaryTable(), containsString(temp));
        } catch (Throwable ex) {
            PrizmaUtils.logFailedTestResultsToTheLogsFile("Looks like that something when wrong as bot total profit percentage was: " + getTextFromBotTotalProfitLabelPercentageOnBotSummaryTable() + " when tests expect: " + temp + ". Check video and logs, or something was changed on Prizma SDK side.");
        }
    }

    public static void verifyThatBotEarnedIsCorrectOnBotSummaryTable(String count) {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        try {
            assertThat(getTextFromBotEarnedLabelOnBotSummaryTable(), containsString(count));
        } catch (Throwable ex) {
            PrizmaUtils.logFailedTestResultsToTheLogsFile("Looks like that something when wrong as bot earned was: " + getTextFromBotEarnedLabelOnBotSummaryTable() + " when tests expect: " + count + ". Check video and logs, or something was changed on Prizma SDK side.");
        }
    }

    public static void verifyThatBotEarnedIsCorrectOnBotSummaryTableIfValidateWithVariableValue() {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        double tempValue = PrizmaUtils.showOnlyTwoDigitAfterDecimal(BOT_EARNED_VALUE_FOR_FUTURE_TESTS);
        String temp = PrizmaUtils.addCommaForThousandNumber(Double.toString(tempValue));
        temp = PrizmaUtils.addDollarSignInTheBegging(temp);

        try {
            assertThat(getTextFromBotEarnedLabelOnBotSummaryTable(), containsString(temp));
        } catch (Throwable ex) {
            PrizmaUtils.logFailedTestResultsToTheLogsFile("Looks like that something when wrong as bot earned was: " + getTextFromBotEarnedLabelOnBotSummaryTable() + " when tests expect: " + temp + ". Check video and logs, or something was changed on Prizma SDK side.");
        }
    }

    public static void verifyThatBotEarnedIsCorrectOnBotSummaryTableIfValidateWithVariableValueWhichIsZero() {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        String temp = "";

        if (BOT_EARNED_VALUE_FOR_FUTURE_TESTS == 0) {
            temp = "–";
        }

        try {
            assertThat(getTextFromBotEarnedLabelZeroOnBotSummaryTable(), containsString(temp));
        } catch (Throwable ex) {
            PrizmaUtils.logFailedTestResultsToTheLogsFile("Looks like that something when wrong as bot earned was: " + getTextFromBotEarnedLabelZeroOnBotSummaryTable() + " when tests expect: " + temp + ". Check video and logs, or something was changed on Prizma SDK side.");
        }
    }

    public static void verifyThatStartDateInPositionsTableRowByPositionXIsCorrect(String date, int position) {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        try {
            assertThat(getTextFromStartDateInPositionsTableRowByPositionX(position), containsString(date));
        } catch (Throwable ex) {
            PrizmaUtils.logFailedTestResultsToTheLogsFile("Looks like that something when wrong as start date was: " + getTextFromStartDateInPositionsTableRowByPositionX(position) + " in position " + position + " when tests expect: " + date + ". Check video and logs, or something was changed on Prizma SDK side.");
        }
    }

    public static void verifyThatStartDateInPositionsTableRowByPositionFromVariableValueIsCorrect(String date) {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        try {
            assertThat(getTextFromStartDateInPositionsTableRowByPositionFromVariableValue(), containsString(date));
        } catch (Throwable ex) {
            PrizmaUtils.logFailedTestResultsToTheLogsFile("Looks like that something when wrong as start date was: " + getTextFromStartDateInPositionsTableRowByPositionFromVariableValue() + " in position " + POSITIONS_TABLE_ROW_VALUE_FOR_FUTURE_TESTS + " when tests expect: " + date + ". Check video and logs, or something was changed on Prizma SDK side.");
        }
    }

    public static void verifyThatEndDateInPositionsTableRowByPositionXIsCorrect(String date, int position) {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        try {
            assertThat(getTextFromEndDateInPositionsTableRowByPositionX(position), containsString(date));
        } catch (Throwable ex) {
            PrizmaUtils.logFailedTestResultsToTheLogsFile("Looks like that something when wrong as end date was: " + getTextFromEndDateInPositionsTableRowByPositionX(position) + " in position " + position + " when tests expect: " + date + ". Check video and logs, or something was changed on Prizma SDK side.");
        }
    }

    public static void verifyThatEndDateInPositionsTableRowByPositionFromVariableValueIsCorrect(String date) {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        try {
            assertThat(getTextFromEndDateInPositionsTableRowByPositionFromVariableValue(), containsString(date));
        } catch (Throwable ex) {
            PrizmaUtils.logFailedTestResultsToTheLogsFile("Looks like that something when wrong as end date was: " + getTextFromEndDateInPositionsTableRowByPositionFromVariableValue() + " in position " + POSITIONS_TABLE_ROW_VALUE_FOR_FUTURE_TESTS + " when tests expect: " + date + ". Check video and logs, or something was changed on Prizma SDK side.");
        }
    }

    public static void verifyThatCurrencyPairInPositionsTableRowByPositionXIsCorrect(String currencyPair, int position) {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        try {
            assertThat(getTextFromCurrencyPairInPositionsTableRowByPositionX(position), containsString(currencyPair));
        } catch (Throwable ex) {
            PrizmaUtils.logFailedTestResultsToTheLogsFile("Looks like that something when wrong as currency pair was: " + getTextFromCurrencyPairInPositionsTableRowByPositionX(position) + " in position " + position + " when tests expect: " + currencyPair + ". Check video and logs, or something was changed on Prizma SDK side.");
        }
    }

    public static void verifyThatCurrencyPairInPositionsTableRowByPositionFromVariableValueIsCorrect(String currencyPair) {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        try {
            assertThat(getTextFromCurrencyPairInPositionsTableRowByFromVariableValue(), containsString(currencyPair));
        } catch (Throwable ex) {
            PrizmaUtils.logFailedTestResultsToTheLogsFile("Looks like that something when wrong as currency pair was: " + getTextFromCurrencyPairInPositionsTableRowByFromVariableValue() + " in position " + POSITIONS_TABLE_ROW_VALUE_FOR_FUTURE_TESTS + " when tests expect: " + currencyPair + ". Check video and logs, or something was changed on Prizma SDK side.");
        }
    }

    public static void verifyThatTypeInPositionsTableRowByPositionXIsCorrect(String type, int position) {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        try {
            assertThat(getTextFromTypeInPositionsTableRowByPositionX(position), containsString(type));
        } catch (Throwable ex) {
            PrizmaUtils.logFailedTestResultsToTheLogsFile("Looks like that something when wrong as type was: " + getTextFromTypeInPositionsTableRowByPositionX(position) + " in position " + position + " when tests expect: " + type + ". Check video and logs, or something was changed on Prizma SDK side.");
        }
    }

    public static void verifyThatTypeInPositionsTableRowByPositionFromVariableValueIsCorrect(String type) {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        try {
            assertThat(getTextFromTypeInPositionsTableRowByFromVariableValue(), containsString(type));
        } catch (Throwable ex) {
            PrizmaUtils.logFailedTestResultsToTheLogsFile("Looks like that something when wrong as type was: " + getTextFromTypeInPositionsTableRowByFromVariableValue() + " in position " + POSITIONS_TABLE_ROW_VALUE_FOR_FUTURE_TESTS + " when tests expect: " + type + ". Check video and logs, or something was changed on Prizma SDK side.");
        }
    }

    public static void verifyThatSizeInPositionsTableRowByPositionXIsCorrect(String size, int position) {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        try {
            assertThat(getTextFromSizeInPositionsTableRowByPositionX(position), containsString(size));
        } catch (Throwable ex) {
            PrizmaUtils.logFailedTestResultsToTheLogsFile("Looks like that something when wrong as size was: " + getTextFromSizeInPositionsTableRowByPositionX(position) + " in position " + position + " when tests expect: " + size + ". Check video and logs, or something was changed on Prizma SDK side.");
        }
    }

    public static void verifyThatSizeInPositionsTableRowByPositionFromVariableValueIsCorrect(String size) {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        try {
            assertThat(getTextFromSizeInPositionsTableRowByFromVariableValue(), containsString(size));
        } catch (Throwable ex) {
            PrizmaUtils.logFailedTestResultsToTheLogsFile("Looks like that something when wrong as size was: " + getTextFromSizeInPositionsTableRowByFromVariableValue() + " in position " + POSITIONS_TABLE_ROW_VALUE_FOR_FUTURE_TESTS + " when tests expect: " + size + ". Check video and logs, or something was changed on Prizma SDK side.");
        }
    }

    public static void verifyThatSizeInPositionsTableRowByPositionXIsCorrectIfValidateWithVariableValue(double value, int position) {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        String temp = PrizmaUtils.addCommaForThousandNumber(Double.toString(value));

        try {
            assertThat(getTextFromSizeInPositionsTableRowByPositionX(position), containsString(temp));
        } catch (Throwable ex) {
            PrizmaUtils.logFailedTestResultsToTheLogsFile("Looks like that something when wrong as size was: " + getTextFromSizeInPositionsTableRowByPositionX(position) + " in position " + position + " when tests expect: " + temp + ". Check video and logs, or something was changed on Prizma SDK side.");
        }
    }

    public static void verifyThatEntryPriceInPositionsTableRowByPositionXIsCorrect(String entryPrice, int position) {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        try {
            assertThat(getTextFromEntryPriceInPositionsTableRowByPositionX(position), containsString(entryPrice));
        } catch (Throwable ex) {
            PrizmaUtils.logFailedTestResultsToTheLogsFile("Looks like that something when wrong as entry prise was: " + getTextFromEntryPriceInPositionsTableRowByPositionX(position) + " in position " + position + " when tests expect: " + entryPrice + ". Check video and logs, or something was changed on Prizma SDK side.");
        }
    }

    public static void verifyThatEntryPriceInPositionsTableRowByPositionFromVariableValueIsCorrect(String entryPrice) {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        try {
            assertThat(getTextFromEntryPriceInPositionsTableRowByFromVariableValue(), containsString(entryPrice));
        } catch (Throwable ex) {
            PrizmaUtils.logFailedTestResultsToTheLogsFile("Looks like that something when wrong as entry prise was: " + getTextFromEntryPriceInPositionsTableRowByFromVariableValue() + " in position " + POSITIONS_TABLE_ROW_VALUE_FOR_FUTURE_TESTS + " when tests expect: " + entryPrice + ". Check video and logs, or something was changed on Prizma SDK side.");
        }
    }

    public static void verifyThatExitPriceInPositionsTableRowByPositionXIsCorrect(String exitPrice, int position) {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        try {
            assertThat(getTextFromExitPriceInPositionsTableRowByPositionX(position), containsString(exitPrice));
        } catch (Throwable ex) {
            PrizmaUtils.logFailedTestResultsToTheLogsFile("Looks like that something when wrong as exit prise was: " + getTextFromExitPriceInPositionsTableRowByPositionX(position) + " in position " + position + " when tests expect: " + exitPrice + ". Check video and logs, or something was changed on Prizma SDK side.");
        }
    }

    public static void verifyThatExitPriceInPositionsTableRowByPositionFromVariableValueIsCorrect(String exitPrice) {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        try {
            assertThat(getTextFromExitPriceInPositionsTableRowByFromVariableValue(), containsString(exitPrice));
        } catch (Throwable ex) {
            PrizmaUtils.logFailedTestResultsToTheLogsFile("Looks like that something when wrong as exit prise was: " + getTextFromExitPriceInPositionsTableRowByFromVariableValue() + " in position " + POSITIONS_TABLE_ROW_VALUE_FOR_FUTURE_TESTS + " when tests expect: " + exitPrice + ". Check video and logs, or something was changed on Prizma SDK side.");
        }
    }

    public static void verifyThatEntryStartValueInPositionsTableRowByPositionXIsCorrect(double entryStartValue, int position) {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        String temp = PrizmaUtils.addCommaForThousandNumber(Double.toString(entryStartValue));
        temp = PrizmaUtils.addDollarSignInTheBegging(temp);

        try {
            assertThat(getTextFromEntryStartValueInPositionsTableRowByPositionX(position), containsString(temp));
        } catch (Throwable ex) {
            PrizmaUtils.logFailedTestResultsToTheLogsFile("Looks like that something when wrong as entry start value was: " + getTextFromEntryStartValueInPositionsTableRowByPositionX(position) + " in position " + position + " when tests expect: " + temp + ". Check video and logs, or something was changed on Prizma SDK side.");
        }
    }

    public static void verifyThatEntryStartValueInPositionsTableRowByPositionFromVariableValueIsCorrect(String entryStartValue) {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        try {
            assertThat(getTextFromEntryStartValueInPositionsTableRowByPositionFromVariableValue(), containsString(entryStartValue));
        } catch (Throwable ex) {
            PrizmaUtils.logFailedTestResultsToTheLogsFile("Looks like that something when wrong as entry start value was: " + getTextFromEntryStartValueInPositionsTableRowByPositionFromVariableValue() + " in position " + POSITIONS_TABLE_ROW_VALUE_FOR_FUTURE_TESTS + " when tests expect: " + entryStartValue + ". Check video and logs, or something was changed on Prizma SDK side.");
        }
    }

    public static void verifyThatEntryCommissionValueInPositionsTableRowByPositionXIsCorrect(double entryCommissionValue, int position) {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        String temp = PrizmaUtils.addCommaForThousandNumber(Double.toString(entryCommissionValue));
        temp = PrizmaUtils.addDollarAndMinusSignInTheBegging(temp);

        try {
            assertThat(getTextFromEntryCommissionValueInPositionsTableRowByPositionX(position), containsString(temp));
        } catch (Throwable ex) {
            PrizmaUtils.logFailedTestResultsToTheLogsFile("Looks like that something when wrong as entry entry commission value was: " + getTextFromEntryCommissionValueInPositionsTableRowByPositionX(position) + " in position " + position + " when tests expect: " + temp + ". Check video and logs, or something was changed on Prizma SDK side.");
        }
    }

    public static void verifyThatEntryCommissionValueInPositionsTableRowByPositionFromVariableValueIsCorrect(String entryCommissionValue) {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        try {
            assertThat(getTextFromEntryCommissionValueInPositionsTableRowByPositionFromVariableValue(), containsString(entryCommissionValue));
        } catch (Throwable ex) {
            PrizmaUtils.logFailedTestResultsToTheLogsFile("Looks like that something when wrong as entry entry commission value was: " + getTextFromEntryCommissionValueInPositionsTableRowByPositionFromVariableValue() + " in position " + POSITIONS_TABLE_ROW_VALUE_FOR_FUTURE_TESTS + " when tests expect: " + entryCommissionValue + ". Check video and logs, or something was changed on Prizma SDK side.");
        }
    }

    public static void verifyThatEntryEndValueInPositionsTableRowByPositionXIsCorrect(double entryEndValue, int position) {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        String temp = PrizmaUtils.addCommaForThousandNumber(Double.toString(entryEndValue));
        temp = PrizmaUtils.addDollarSignInTheBegging(temp);

        try {
            assertThat(getTextFromEntryEndValueInPositionsTableRowByPositionX(position), containsString(temp));
        } catch (Throwable ex) {
            PrizmaUtils.logFailedTestResultsToTheLogsFile("Looks like that something when wrong as entry end value was: " + getTextFromEntryEndValueInPositionsTableRowByPositionX(position) + " in position " + position + " when tests expect: " + temp + ". Check video and logs, or something was changed on Prizma SDK side.");
        }
    }

    public static void verifyThatEntryEndValueInPositionsTableRowByPositionFromVariableValueIsCorrect(String entryEndValue) {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        try {
            assertThat(getTextFromEntryEndValueInPositionsTableRowByPositionFromVariableValue(), containsString(entryEndValue));
        } catch (Throwable ex) {
            PrizmaUtils.logFailedTestResultsToTheLogsFile("Looks like that something when wrong as entry end value was: " + getTextFromEntryEndValueInPositionsTableRowByPositionFromVariableValue() + " in position " + POSITIONS_TABLE_ROW_VALUE_FOR_FUTURE_TESTS + " when tests expect: " + entryEndValue + ". Check video and logs, or something was changed on Prizma SDK side.");
        }
    }

    public static void verifyThatExitStartValueInPositionsTableRowByPositionXIsCorrect(double exitStartValue, int position) {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        String temp = PrizmaUtils.addCommaForThousandNumber(Double.toString(exitStartValue));
        temp = PrizmaUtils.addDollarSignInTheBegging(temp);

        try {
            assertThat(getTextFromExitStartValueInPositionsTableRowByPositionX(position), containsString(temp));
        } catch (Throwable ex) {
            PrizmaUtils.logFailedTestResultsToTheLogsFile("Looks like that something when wrong as exit start value was: " + getTextFromExitStartValueInPositionsTableRowByPositionX(position) + " in position " + position + " when tests expect: " + temp + ". Check video and logs, or something was changed on Prizma SDK side.");
        }
    }

    public static void verifyThatExitStartValueInPositionsTableRowByPositionFromVariableValueIsCorrect(String exitStartValue) {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        try {
            assertThat(getTextFromExitStartValueInPositionsTableRowByPositionFromVariableValue(), containsString(exitStartValue));
        } catch (Throwable ex) {
            PrizmaUtils.logFailedTestResultsToTheLogsFile("Looks like that something when wrong as exit start value was: " + getTextFromExitStartValueInPositionsTableRowByPositionFromVariableValue() + " in position " + POSITIONS_TABLE_ROW_VALUE_FOR_FUTURE_TESTS + " when tests expect: " + exitStartValue + ". Check video and logs, or something was changed on Prizma SDK side.");
        }
    }

    public static void verifyThatExitCommissionValueInPositionsTableRowByPositionXIsCorrect(double exitCommissionValue, int position) {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        String temp = PrizmaUtils.addCommaForThousandNumber(Double.toString(exitCommissionValue));
        temp = PrizmaUtils.addDollarAndMinusSignInTheBegging(temp);

        try {
            assertThat(getTextFromExitCommissionValueInPositionsTableRowByPositionX(position), containsString(temp));
        } catch (Throwable ex) {
            PrizmaUtils.logFailedTestResultsToTheLogsFile("Looks like that something when wrong as exit commission value was: " + getTextFromExitCommissionValueInPositionsTableRowByPositionX(position) + " in position " + position + " when tests expect: " + temp + ". Check video and logs, or something was changed on Prizma SDK side.");
        }
    }

    public static void verifyThatExitCommissionValueInPositionsTableRowByPositionFromVariableValueIsCorrect(String exitCommissionValue) {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        try {
            assertThat(getTextFromExitCommissionValueInPositionsTableRowByPositionFromVariableValue(), containsString(exitCommissionValue));
        } catch (Throwable ex) {
            PrizmaUtils.logFailedTestResultsToTheLogsFile("Looks like that something when wrong as exit commission value was: " + getTextFromExitCommissionValueInPositionsTableRowByPositionFromVariableValue() + " in position " + POSITIONS_TABLE_ROW_VALUE_FOR_FUTURE_TESTS + " when tests expect: " + exitCommissionValue + ". Check video and logs, or something was changed on Prizma SDK side.");
        }
    }

    public static void verifyThatExitEndValueInPositionsTableRowByPositionXIsCorrect(double exitEndValue, int position) {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        String temp = PrizmaUtils.addCommaForThousandNumber(Double.toString(exitEndValue));
        temp = PrizmaUtils.addDollarSignInTheBegging(temp);

        try {
            assertThat(getTextFromExitEndValueInPositionsTableRowByPositionX(position), containsString(temp));
        } catch (Throwable ex) {
            PrizmaUtils.logFailedTestResultsToTheLogsFile("Looks like that something when wrong as exit end value was: " + getTextFromExitEndValueInPositionsTableRowByPositionX(position) + " in position " + position + " when tests expect: " + temp + ". Check video and logs, or something was changed on Prizma SDK side.");
        }
    }

    public static void verifyThatExitEndValueInPositionsTableRowByPositionFromVariableValueIsCorrect(String exitEndValue) {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        try {
            assertThat(getTextFromExitEndValueInPositionsTableRowByPositionFromVariableValue(), containsString(exitEndValue));
        } catch (Throwable ex) {
            PrizmaUtils.logFailedTestResultsToTheLogsFile("Looks like that something when wrong as exit end value was: " + getTextFromExitEndValueInPositionsTableRowByPositionFromVariableValue() + " in position " + POSITIONS_TABLE_ROW_VALUE_FOR_FUTURE_TESTS + " when tests expect: " + exitEndValue + ". Check video and logs, or something was changed on Prizma SDK side.");
        }
    }

    public static void verifyThatProfitCurrencyInPositionsTableRowByPositionXIsCorrect(double profitCurrency, int position) {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        String temp = PrizmaUtils.addCommaForThousandNumber(Double.toString(profitCurrency));
        temp = PrizmaUtils.addDollarSignInTheBegging(temp);

        try {
            assertThat(getTextFromProfitCurrencyInPositionsTableRowByPositionX(position), containsString(temp));
        } catch (Throwable ex) {
            PrizmaUtils.logFailedTestResultsToTheLogsFile("Looks like that something when wrong as profit currency was: " + getTextFromProfitCurrencyInPositionsTableRowByPositionX(position) + " in position " + position + " when tests expect: " + temp + ". Check video and logs, or something was changed on Prizma SDK side.");
        }
    }

    public static void verifyThatProfitCurrencyInPositionsTableRowByPositionFromVariableValueIsCorrect(String profitCurrency) {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        try {
            assertThat(getTextFromProfitCurrencyInPositionsTableRowByPositionFromVariableValue(), containsString(profitCurrency));
        } catch (Throwable ex) {
            PrizmaUtils.logFailedTestResultsToTheLogsFile("Looks like that something when wrong as profit currency was: " + getTextFromProfitCurrencyInPositionsTableRowByPositionFromVariableValue() + " in position " + POSITIONS_TABLE_ROW_VALUE_FOR_FUTURE_TESTS + " when tests expect: " + profitCurrency + ". Check video and logs, or something was changed on Prizma SDK side.");
        }
    }

    public static void verifyThatProfitPercentageInPositionsTableRowByPositionXIsCorrect(double profitPercentage, int position) {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        String temp = PrizmaUtils.addCommaForThousandNumber(Double.toString(profitPercentage));
        temp = PrizmaUtils.addPercentageSignInTheEnd(temp);

        try {
            assertThat(getTextFromProfitPercentageInPositionsTableRowByPositionX(position), containsString(temp));
        } catch (Throwable ex) {
            PrizmaUtils.logFailedTestResultsToTheLogsFile("Looks like that something when wrong as profit percentage was: " + getTextFromProfitPercentageInPositionsTableRowByPositionX(position) + " in position " + position + " when tests expect: " + temp + ". Check video and logs, or something was changed on Prizma SDK side.");
        }
    }

    public static void verifyThatProfitPercentageInPositionsTableRowByPositionFromVariableValueIsCorrect(String profitPercentage) {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        try {
            assertThat(getTextFromProfitPercentageInPositionsTableRowByPositionFromVariableValue(), containsString(profitPercentage));
        } catch (Throwable ex) {
            PrizmaUtils.logFailedTestResultsToTheLogsFile("Looks like that something when wrong as profit percentage was: " + getTextFromProfitPercentageInPositionsTableRowByPositionFromVariableValue() + " in position " + POSITIONS_TABLE_ROW_VALUE_FOR_FUTURE_TESTS + " when tests expect: " + profitPercentage + ". Check video and logs, or something was changed on Prizma SDK side.");
        }
    }

    /***
     * Calculation methods
     */
    public static void getAndStoreTotalCountOfAllTrendingPositions() {
        TOTAL_POSITIONS_VALUE_FOR_FUTURE_TESTS = 0;
        TOTAL_POSITIONS_VALUE_FOR_FUTURE_TESTS = getPositionsTableCount();
    }

    public static void getAndStoreTotalCountOfOpenAndCloseTrendingPositions() {
        OPEN_POSITIONS_VALUE_FOR_FUTURE_TESTS = 0;
        CLOSE_POSITIONS_VALUE_FOR_FUTURE_TESTS = 0;

        int count = getPositionsTableCount();

        for (int i = 1; i <= count; i++) {
            String temp = getTextFromEndDateInPositionsTableRowByPositionX(i);
            if (temp.contains("–")) {
                OPEN_POSITIONS_VALUE_FOR_FUTURE_TESTS++;
            } else {
                CLOSE_POSITIONS_VALUE_FOR_FUTURE_TESTS++;
            }
        }
    }

    public static void getAndStoreInOpenTrendingPositionsValue() {
        IN_OPEN_POSITIONS_CURRENCY_VALUE_FOR_FUTURE_TESTS = 0;
        double total_count = 0;

        int count = getPositionsTableCount();

        for (int i = 1; i <= count; i++) {
            String temp = getTextFromEndDateInPositionsTableRowByPositionX(i);
            if (temp.contains("/")) {
                String temp2 = getTextFromEntryStartValueInPositionsTableRowByPositionX(i);
                String temp3 = PrizmaUtils.removeSpecificSymbols(temp2);

                total_count = total_count + Double.valueOf(temp3);
            }
        }

        IN_OPEN_POSITIONS_CURRENCY_VALUE_FOR_FUTURE_TESTS = total_count;
    }

    public static void getAndCalculateAllProfitCurrencyValues() {
        TOTAL_PROFITS_CURRENCY_VALUE_FROM_ALL_POSITIONS_FOR_FUTURE_TESTS = 0;
        double total_count = 0;

        int count = getPositionsTableCount();

        for (int i = 1; i <= count; i++) {
            String temp = getTextFromProfitCurrencyInPositionsTableRowByPositionX(i);
            String temp2 = PrizmaUtils.removeSpecificSymbols(temp);
            total_count = total_count + Double.valueOf(temp2);
        }

        TOTAL_PROFITS_CURRENCY_VALUE_FROM_ALL_POSITIONS_FOR_FUTURE_TESTS = total_count;
    }

    public static void getAndCalculateAllProfitCurrencyValuesForClosedPositions() {
        TOTAL_PROFITS_CURRENCY_VALUE_FROM_CLOSED_POSITIONS_FOR_FUTURE_TESTS = 0;
        double total_count = 0;

        int count = getPositionsTableCount();

        for (int i = 1; i <= count; i++) {
            String temp = getTextFromEndDateInPositionsTableRowByPositionX(i);
            if (!temp.contains("–")) {
                String temp2 = getTextFromProfitCurrencyInPositionsTableRowByPositionX(i);
                String temp3 = PrizmaUtils.removeSpecificSymbols(temp2);
                total_count = total_count + Double.valueOf(temp3);
            }
        }

        TOTAL_PROFITS_CURRENCY_VALUE_FROM_CLOSED_POSITIONS_FOR_FUTURE_TESTS = total_count;
    }

    public static void getAndCalculateAllProfitCurrencyValuesForOpenPositions() {
        TOTAL_PROFITS_CURRENCY_VALUE_FROM_OPEN_POSITIONS_FOR_FUTURE_TESTS = 0;
        double total_count = 0;

        int count = getPositionsTableCount();

        for (int i = 1; i <= count; i++) {
            String temp = getTextFromEndDateInPositionsTableRowByPositionX(i);
            if (temp.contains("–")) {
                String temp2 = getTextFromProfitCurrencyInPositionsTableRowByPositionX(i);
                String temp3 = PrizmaUtils.removeSpecificSymbols(temp2);
                total_count = total_count + Double.valueOf(temp3);
            }
        }

        TOTAL_PROFITS_CURRENCY_VALUE_FROM_OPEN_POSITIONS_FOR_FUTURE_TESTS = total_count;
    }

    public static void getAndCalculateAllProfitPercentageValues() {
        TOTAL_PROFITS_PERCENTAGE_VALUE_FROM_ALL_POSITIONS_FOR_FUTURE_TESTS = 0;
        double total_count = 0;

        int count = getPositionsTableCount();

        for (int i = 1; i <= count; i++) {
            String temp = getTextFromProfitPercentageInPositionsTableRowByPositionX(i);
            String temp2 = PrizmaUtils.removeSpecificSymbols(temp);
            total_count = total_count + Double.valueOf(temp2);
        }

        TOTAL_PROFITS_PERCENTAGE_VALUE_FROM_ALL_POSITIONS_FOR_FUTURE_TESTS = total_count;
    }

    public static void getAndCalculateAllProfitPercentageValuesForClosedPositions() {
        TOTAL_PROFITS_PERCENTAGE_VALUE_FROM_CLOSED_POSITIONS_FOR_FUTURE_TESTS = 0;
        double total_count = 0;

        int count = getPositionsTableCount();

        for (int i = 1; i <= count; i++) {
            String temp = getTextFromEndDateInPositionsTableRowByPositionX(i);
            if (!temp.contains("–")) {
                String temp2 = getTextFromProfitPercentageInPositionsTableRowByPositionX(i);
                String temp3 = PrizmaUtils.removeSpecificSymbols(temp2);
                total_count = total_count + Double.valueOf(temp3);
            }
        }

        TOTAL_PROFITS_PERCENTAGE_VALUE_FROM_CLOSED_POSITIONS_FOR_FUTURE_TESTS = total_count;
    }

    public static void getAndCalculateAllProfitPercentageValuesForOpenPositions() {
        TOTAL_PROFITS_PERCENTAGE_VALUE_FROM_OPEN_POSITIONS_FOR_FUTURE_TESTS = 0;
        double total_count = 0;

        int count = getPositionsTableCount();

        for (int i = 1; i <= count; i++) {
            String temp = getTextFromEndDateInPositionsTableRowByPositionX(i);
            if (temp.contains("–")) {
                String temp2 = getTextFromProfitPercentageInPositionsTableRowByPositionX(i);
                String temp3 = PrizmaUtils.removeSpecificSymbols(temp2);
                total_count = total_count + Double.valueOf(temp3);
            }
        }

        TOTAL_PROFITS_PERCENTAGE_VALUE_FROM_OPEN_POSITIONS_FOR_FUTURE_TESTS = total_count;
    }

    public static void getAndCalculateTotalProfitPercentageValues() {
        TOTAL_PROFITS_PERCENTAGE_VALUE_FOR_FUTURE_TESTS = 0;
        TOTAL_PROFITS_PERCENTAGE_VALUE_FOR_FUTURE_TESTS = TOTAL_PROFITS_CURRENCY_VALUE_FROM_ALL_POSITIONS_FOR_FUTURE_TESTS * 100 / INITIAL_BALANCE_VALUE_FOR_FUTURE_TESTS;
    }

    public static void getAndCalculateTotalBalance() {
        TOTAL_BALANCE_VALUE_FOR_FUTURE_TESTS = INITIAL_BALANCE_VALUE_FOR_FUTURE_TESTS + TOTAL_PROFITS_CURRENCY_VALUE_FROM_CLOSED_POSITIONS_FOR_FUTURE_TESTS;
    }

    public static void getAndCalculateAvailableBalance() {
        AVAILABLE_BALANCE_VALUE_FOR_FUTURE_TESTS = TOTAL_BALANCE_VALUE_FOR_FUTURE_TESTS - IN_OPEN_POSITIONS_CURRENCY_VALUE_FOR_FUTURE_TESTS;
    }

    public static void getAndCalculateAverageProfitCurrency() {
        if (CLOSE_POSITIONS_VALUE_FOR_FUTURE_TESTS != 0) {
            AVERAGE_PROFIT_CURRENCY_VALUE_FOR_FUTURE_TESTS = TOTAL_PROFITS_CURRENCY_VALUE_FROM_ALL_POSITIONS_FOR_FUTURE_TESTS / CLOSE_POSITIONS_VALUE_FOR_FUTURE_TESTS;
        } else {
            AVERAGE_PROFIT_CURRENCY_VALUE_FOR_FUTURE_TESTS = 0;
        }
    }

    public static void getAndCalculateAverageProfitPercentage() {
        if (CLOSE_POSITIONS_VALUE_FOR_FUTURE_TESTS != 0) {
            AVERAGE_PROFIT_PERCENTAGE_VALUE_FOR_FUTURE_TESTS = TOTAL_PROFITS_PERCENTAGE_VALUE_FROM_ALL_POSITIONS_FOR_FUTURE_TESTS / CLOSE_POSITIONS_VALUE_FOR_FUTURE_TESTS;
        } else {
            AVERAGE_PROFIT_PERCENTAGE_VALUE_FOR_FUTURE_TESTS = 0;
        }
    }

    public static void getAndCalculateBotEarnedValues() {
        BOT_EARNED_VALUE_FOR_FUTURE_TESTS = 0;
        CLOSE_POSITIVE_POSITIONS_VALUE_FOR_FUTURE_TESTS = 0;

        double total_count = 0;
        int count = getPositionsTableCount();

        for (int i = 1; i <= count; i++) {
            String temp = getTextFromEndDateInPositionsTableRowByPositionX(i);
            if (!temp.contains("–")) {
                String temp2 = getTextFromProfitCurrencyInPositionsTableRowByPositionX(i);
                String temp3 = PrizmaUtils.removeSpecificSymbols(temp2);
                double tempValue = Double.valueOf(temp3);
                if (tempValue > 0) {
                    CLOSE_POSITIVE_POSITIONS_VALUE_FOR_FUTURE_TESTS++;
                    total_count = total_count + (tempValue * BOT_COMMISSION_VALUE_FOR_FUTURE_TESTS);
                }
            }
        }

        BOT_EARNED_VALUE_FOR_FUTURE_TESTS = total_count;
    }

    public static void getAndCalculateAndValidateAllValuesInPositionTable(String fileName) {
        int count = getPositionsTableCount();

        for (int i = 1; i <= count; i++) {
            PrizmaUtils.logFailedTestResultsToTheLogsFile("----------------");
            PrizmaUtils.logFailedTestResultsToTheLogsFile("Position index = " + i);
            PrizmaUtils.scrollToElementOnPage(getEntryPriceInPositionsTableRowByPositionX(i));

            if (getTextFromTypeInPositionsTableRowByPositionX(i).contains("Long")) {
                String closedPositions = getTextFromEndDateInPositionsTableRowByPositionX(i);
                if (!closedPositions.contains("–")) {
                    String temp = getTextFromEntryPriceInPositionsTableRowByPositionX(i);
                    String temp2 = PrizmaUtils.removeSpecificSymbols(temp);

                    double entryStartValue = INITIAL_BALANCE_VALUE_FOR_CALCULATION_FOR_FUTURE_TESTS * PERCENTAGE_FROM_INITIAL_BALANCE_WITH_WHICH_BOT_COULD_TRADE_FOR_FUTURE_TESTS;
                    PrizmaUtils.logFailedTestResultsToTheLogsFile("entryStartValue = " + entryStartValue);
                    double entryStartValue_temp = PrizmaUtils.showOnlyTwoDigitAfterDecimal(entryStartValue);
                    PrizmaUtils.logFailedTestResultsToTheLogsFile("entryStartValue_temp = " + entryStartValue_temp);

                    double entryCommission = entryStartValue_temp * TRADING_FEE_FOR_FUTURE_TESTS;
                    PrizmaUtils.logFailedTestResultsToTheLogsFile("entryCommission = " + entryCommission);
                    double entryCommission_temp = PrizmaUtils.showOnlyTwoDigitAfterDecimal(entryCommission);
                    PrizmaUtils.logFailedTestResultsToTheLogsFile("entryCommission_temp = " + entryCommission_temp);

                    double entryEndValue = entryStartValue_temp - entryCommission_temp;
                    PrizmaUtils.logFailedTestResultsToTheLogsFile("entryEndValue = " + entryEndValue);
                    double entryEndValue_temp = PrizmaUtils.showOnlyTwoDigitAfterDecimal(entryEndValue);
                    PrizmaUtils.logFailedTestResultsToTheLogsFile("entryEndValue_temp = " + entryEndValue_temp);

                    verifyThatEntryStartValueInPositionsTableRowByPositionXIsCorrect(entryStartValue_temp, i);
                    verifyThatEntryCommissionValueInPositionsTableRowByPositionXIsCorrect(entryCommission_temp, i);
                    verifyThatEntryEndValueInPositionsTableRowByPositionXIsCorrect(entryEndValue_temp, i);

                    POSITION_SIZE_FOR_FUTURE_TESTS = entryEndValue_temp / Double.valueOf(temp2);
                    PrizmaUtils.logFailedTestResultsToTheLogsFile("size = " + POSITION_SIZE_FOR_FUTURE_TESTS);
                    double POSITION_SIZE_FOR_FUTURE_TESTS_temp = Double.valueOf(PrizmaUtils.cutSevenDigitAfterDecimal(POSITION_SIZE_FOR_FUTURE_TESTS));
                    PrizmaUtils.logFailedTestResultsToTheLogsFile("size_temp = " + POSITION_SIZE_FOR_FUTURE_TESTS_temp);

                    verifyThatSizeInPositionsTableRowByPositionXIsCorrectIfValidateWithVariableValue(POSITION_SIZE_FOR_FUTURE_TESTS_temp, i);

                    String temp4 = getTextFromExitPriceInPositionsTableRowByPositionX(i);
                    String temp5 = PrizmaUtils.removeSpecificSymbols(temp4);

                    double exitStartValue = POSITION_SIZE_FOR_FUTURE_TESTS_temp * Double.valueOf(temp5);
                    PrizmaUtils.logFailedTestResultsToTheLogsFile("exitStartValue = " + exitStartValue);
                    double exitStartValue_temp = PrizmaUtils.showOnlyTwoDigitAfterDecimal(exitStartValue);
                    PrizmaUtils.logFailedTestResultsToTheLogsFile("exitStartValue_temp = " + exitStartValue_temp);

                    double exitCommission = exitStartValue_temp * TRADING_FEE_FOR_FUTURE_TESTS;
                    PrizmaUtils.logFailedTestResultsToTheLogsFile("exitCommission = " + exitCommission);
                    double exitCommission_temp = PrizmaUtils.showOnlyTwoDigitAfterDecimal(exitCommission);
                    PrizmaUtils.logFailedTestResultsToTheLogsFile("exitCommission_temp = " + exitCommission_temp);

                    double exitEndValue = exitStartValue_temp - exitCommission_temp;
                    PrizmaUtils.logFailedTestResultsToTheLogsFile("exitEndValue = " + exitEndValue);
                    double exitEndValue_temp = PrizmaUtils.showOnlyTwoDigitAfterDecimal(exitEndValue);
                    PrizmaUtils.logFailedTestResultsToTheLogsFile("exitEndValue_temp = " + exitEndValue_temp);

                    verifyThatExitStartValueInPositionsTableRowByPositionXIsCorrect(exitStartValue_temp, i);
                    verifyThatExitCommissionValueInPositionsTableRowByPositionXIsCorrect(exitCommission_temp, i);
                    verifyThatExitEndValueInPositionsTableRowByPositionXIsCorrect(exitEndValue_temp, i);

                    double profitCurrencyValue = exitEndValue_temp - entryStartValue_temp;
                    PrizmaUtils.logFailedTestResultsToTheLogsFile("profitCurrencyValue = " + profitCurrencyValue);
                    double profitCurrencyValue_temp = PrizmaUtils.showOnlyTwoDigitAfterDecimal(profitCurrencyValue);
                    PrizmaUtils.logFailedTestResultsToTheLogsFile("profitCurrencyValue_temp = " + profitCurrencyValue_temp);

                    verifyThatProfitCurrencyInPositionsTableRowByPositionXIsCorrect(profitCurrencyValue_temp, i);

                    double profitCurrencyPercentage = profitCurrencyValue_temp * 100 / entryStartValue_temp;
                    PrizmaUtils.logFailedTestResultsToTheLogsFile("profitCurrencyPercentage = " + profitCurrencyPercentage);
                    double profitCurrencyPercentage_temp = PrizmaUtils.showOnlyTwoDigitAfterDecimal(profitCurrencyPercentage);
                    PrizmaUtils.logFailedTestResultsToTheLogsFile("profitCurrencyPercentage_temp = " + profitCurrencyPercentage_temp);

                    verifyThatProfitPercentageInPositionsTableRowByPositionXIsCorrect(profitCurrencyPercentage_temp, i);

                    INITIAL_BALANCE_VALUE_FOR_CALCULATION_FOR_FUTURE_TESTS = INITIAL_BALANCE_VALUE_FOR_CALCULATION_FOR_FUTURE_TESTS - entryStartValue_temp + exitEndValue;
                    PrizmaUtils.logFailedTestResultsToTheLogsFile("new_initial_balance = " + INITIAL_BALANCE_VALUE_FOR_CALCULATION_FOR_FUTURE_TESTS);
                } else {
                    String temp = getTextFromEntryPriceInPositionsTableRowByPositionX(i);
                    String temp2 = PrizmaUtils.removeSpecificSymbols(temp);

                    double entryStartValue = INITIAL_BALANCE_VALUE_FOR_CALCULATION_FOR_FUTURE_TESTS * PERCENTAGE_FROM_INITIAL_BALANCE_WITH_WHICH_BOT_COULD_TRADE_FOR_FUTURE_TESTS;
                    PrizmaUtils.logFailedTestResultsToTheLogsFile("entryStartValue = " + entryStartValue);
                    double entryStartValue_temp = PrizmaUtils.showOnlyTwoDigitAfterDecimal(entryStartValue);
                    PrizmaUtils.logFailedTestResultsToTheLogsFile("entryStartValue_temp = " + entryStartValue_temp);

                    double entryCommission = entryStartValue * TRADING_FEE_FOR_FUTURE_TESTS / (1 + TRADING_FEE_FOR_FUTURE_TESTS);
                    PrizmaUtils.logFailedTestResultsToTheLogsFile("entryCommission = " + entryCommission);
                    double entryCommission_temp = PrizmaUtils.showOnlyTwoDigitAfterDecimal(entryCommission);
                    PrizmaUtils.logFailedTestResultsToTheLogsFile("entryCommission_temp = " + entryCommission_temp);

                    double entryEndValue = entryStartValue_temp - entryCommission_temp;
                    PrizmaUtils.logFailedTestResultsToTheLogsFile("entryEndValue = " + entryEndValue);
                    double entryEndValue_temp = PrizmaUtils.showOnlyTwoDigitAfterDecimal(entryEndValue);
                    PrizmaUtils.logFailedTestResultsToTheLogsFile("entryEndValue_temp = " + entryEndValue_temp);

                    verifyThatEntryStartValueInPositionsTableRowByPositionXIsCorrect(entryStartValue_temp, i);
                    verifyThatEntryCommissionValueInPositionsTableRowByPositionXIsCorrect(entryCommission_temp, i);
                    verifyThatEntryEndValueInPositionsTableRowByPositionXIsCorrect(entryEndValue_temp, i);

                    POSITION_SIZE_FOR_FUTURE_TESTS = entryEndValue_temp / Double.valueOf(temp2);
                    PrizmaUtils.logFailedTestResultsToTheLogsFile("size = " + POSITION_SIZE_FOR_FUTURE_TESTS);

                    double POSITION_SIZE_FOR_FUTURE_TESTS_temp = Double.valueOf(PrizmaUtils.cutSevenDigitAfterDecimal(POSITION_SIZE_FOR_FUTURE_TESTS));
                    PrizmaUtils.logFailedTestResultsToTheLogsFile("size = " + POSITION_SIZE_FOR_FUTURE_TESTS_temp);

                    verifyThatSizeInPositionsTableRowByPositionXIsCorrectIfValidateWithVariableValue(POSITION_SIZE_FOR_FUTURE_TESTS_temp, i);

                    double profitCurrencyValue = 0;
                    verifyThatProfitCurrencyInPositionsTableRowByPositionXIsCorrect(profitCurrencyValue, i);

                    double profitCurrencyPercentage = 0;
                    verifyThatProfitPercentageInPositionsTableRowByPositionXIsCorrect(profitCurrencyPercentage, i);
                }
            } else {
                String closedPositions = getTextFromEndDateInPositionsTableRowByPositionX(i);
                if (!closedPositions.contains("–")) {
                    String temp = getTextFromEntryPriceInPositionsTableRowByPositionX(i);
                    String temp2 = PrizmaUtils.removeSpecificSymbols(temp);

                    double entryStartValue = INITIAL_BALANCE_VALUE_FOR_CALCULATION_FOR_FUTURE_TESTS * PERCENTAGE_FROM_INITIAL_BALANCE_WITH_WHICH_BOT_COULD_TRADE_FOR_FUTURE_TESTS;
                    double entryEndValue = entryStartValue / (1 + TRADING_FEE_FOR_FUTURE_TESTS);
                    double entryCommission = entryStartValue * TRADING_FEE_FOR_FUTURE_TESTS / (1 + TRADING_FEE_FOR_FUTURE_TESTS);

                    verifyThatEntryStartValueInPositionsTableRowByPositionXIsCorrect(entryStartValue, i);
                    verifyThatEntryCommissionValueInPositionsTableRowByPositionXIsCorrect(entryCommission, i);
                    verifyThatEntryEndValueInPositionsTableRowByPositionXIsCorrect(entryEndValue, i);

                    double tempValue = PrizmaUtils.showOnlyTwoDigitAfterDecimal(entryEndValue);
                    POSITION_SIZE_FOR_FUTURE_TESTS = tempValue / Double.valueOf(temp2);
                    verifyThatSizeInPositionsTableRowByPositionXIsCorrectIfValidateWithVariableValue(POSITION_SIZE_FOR_FUTURE_TESTS, i);

                    String temp4 = getTextFromExitPriceInPositionsTableRowByPositionX(i);
                    String temp5 = PrizmaUtils.removeSpecificSymbols(temp4);
                    double exitStartValue = POSITION_SIZE_FOR_FUTURE_TESTS * Double.valueOf(temp5);
                    double exitEndValue = exitStartValue / (1 + TRADING_FEE_FOR_FUTURE_TESTS);
                    double exitCommission = exitStartValue * TRADING_FEE_FOR_FUTURE_TESTS / (1 + TRADING_FEE_FOR_FUTURE_TESTS);

                    verifyThatExitStartValueInPositionsTableRowByPositionXIsCorrect(exitStartValue, i);
                    verifyThatExitCommissionValueInPositionsTableRowByPositionXIsCorrect(exitCommission, i);
                    verifyThatExitEndValueInPositionsTableRowByPositionXIsCorrect(exitEndValue, i);

                    double profitCurrencyValue = entryEndValue - exitEndValue;
                    verifyThatProfitCurrencyInPositionsTableRowByPositionXIsCorrect(profitCurrencyValue, i);

                    double profitCurrencyPercentage = profitCurrencyValue * 100 / entryStartValue;
                    verifyThatProfitPercentageInPositionsTableRowByPositionXIsCorrect(profitCurrencyPercentage, i);

                    INITIAL_BALANCE_VALUE_FOR_CALCULATION_FOR_FUTURE_TESTS = INITIAL_BALANCE_VALUE_FOR_CALCULATION_FOR_FUTURE_TESTS + entryEndValue - exitStartValue;
                } else {
                    String temp = getTextFromEntryPriceInPositionsTableRowByPositionX(i);
                    String temp2 = PrizmaUtils.removeSpecificSymbols(temp);

                    double entryStartValue = INITIAL_BALANCE_VALUE_FOR_CALCULATION_FOR_FUTURE_TESTS * PERCENTAGE_FROM_INITIAL_BALANCE_WITH_WHICH_BOT_COULD_TRADE_FOR_FUTURE_TESTS;
                    double entryEndValue = entryStartValue / (1 + TRADING_FEE_FOR_FUTURE_TESTS);
                    double entryCommission = entryStartValue * TRADING_FEE_FOR_FUTURE_TESTS / (1 + TRADING_FEE_FOR_FUTURE_TESTS);

                    verifyThatEntryStartValueInPositionsTableRowByPositionXIsCorrect(entryStartValue, i);
                    verifyThatEntryCommissionValueInPositionsTableRowByPositionXIsCorrect(entryCommission, i);
                    verifyThatEntryEndValueInPositionsTableRowByPositionXIsCorrect(entryEndValue, i);


                    POSITION_SIZE_FOR_FUTURE_TESTS = entryEndValue / Double.valueOf(temp2);
                    verifyThatSizeInPositionsTableRowByPositionXIsCorrectIfValidateWithVariableValue(POSITION_SIZE_FOR_FUTURE_TESTS, i);

                    double profitCurrencyValue = 0;
                    verifyThatProfitCurrencyInPositionsTableRowByPositionXIsCorrect(profitCurrencyValue, i);

                    double profitCurrencyPercentage = 0;
                    verifyThatProfitPercentageInPositionsTableRowByPositionXIsCorrect(profitCurrencyPercentage, i);
                }
            }

            PrizmaUtils.logFailedTestResultsToTheLogsFile("----------------");
            PrizmaDriver.getScreenshot(fileName + "_" + i);
        }
    }

    public static void getAndCalculateAndValidateAllValuesInPositionTableTalibBot(String fileName) {
        int count = getPositionsTableCount();

        for (int i = 1; i <= count; i++) {
            PrizmaUtils.logFailedTestResultsToTheLogsFile("----------------");
            PrizmaUtils.logFailedTestResultsToTheLogsFile("Position index = " + i);
            PrizmaUtils.scrollToElementOnPage(getEntryPriceInPositionsTableRowByPositionX(i));

            if (getTextFromTypeInPositionsTableRowByPositionX(i).contains("Long")) {
                String closedPositions = getTextFromEndDateInPositionsTableRowByPositionX(i);
                if (!closedPositions.contains("–")) {
                    String temp = getTextFromEntryPriceInPositionsTableRowByPositionX(i);
                    String temp2 = PrizmaUtils.removeSpecificSymbols(temp);

                    double entryStartValue = INITIAL_BALANCE_VALUE_FOR_CALCULATION_FOR_FUTURE_TESTS * PERCENTAGE_FROM_INITIAL_BALANCE_WITH_WHICH_BOT_COULD_TRADE_FOR_FUTURE_TESTS_TALIB_BOT;
                    PrizmaUtils.logFailedTestResultsToTheLogsFile("entryStartValue = " + entryStartValue);
                    double entryStartValue_temp = PrizmaUtils.showOnlyTwoDigitAfterDecimal(entryStartValue);
                    PrizmaUtils.logFailedTestResultsToTheLogsFile("entryStartValue_temp = " + entryStartValue_temp);

                    double entryCommission = entryStartValue_temp * TRADING_FEE_FOR_FUTURE_TESTS;
                    PrizmaUtils.logFailedTestResultsToTheLogsFile("entryCommission = " + entryCommission);
                    double entryCommission_temp = PrizmaUtils.showOnlyTwoDigitAfterDecimal(entryCommission);
                    PrizmaUtils.logFailedTestResultsToTheLogsFile("entryCommission_temp = " + entryCommission_temp);

                    double entryEndValue = entryStartValue_temp - entryCommission_temp;
                    PrizmaUtils.logFailedTestResultsToTheLogsFile("entryEndValue = " + entryEndValue);
                    double entryEndValue_temp = PrizmaUtils.showOnlyTwoDigitAfterDecimal(entryEndValue);
                    PrizmaUtils.logFailedTestResultsToTheLogsFile("entryEndValue_temp = " + entryEndValue_temp);

                    verifyThatEntryStartValueInPositionsTableRowByPositionXIsCorrect(entryStartValue_temp, i);
                    verifyThatEntryCommissionValueInPositionsTableRowByPositionXIsCorrect(entryCommission_temp, i);
                    verifyThatEntryEndValueInPositionsTableRowByPositionXIsCorrect(entryEndValue_temp, i);

                    POSITION_SIZE_FOR_FUTURE_TESTS = entryEndValue_temp / Double.valueOf(temp2);
                    PrizmaUtils.logFailedTestResultsToTheLogsFile("size = " + POSITION_SIZE_FOR_FUTURE_TESTS);
                    double POSITION_SIZE_FOR_FUTURE_TESTS_temp = Double.valueOf(PrizmaUtils.cutSevenDigitAfterDecimal(POSITION_SIZE_FOR_FUTURE_TESTS));
                    PrizmaUtils.logFailedTestResultsToTheLogsFile("size_temp = " + POSITION_SIZE_FOR_FUTURE_TESTS_temp);

                    verifyThatSizeInPositionsTableRowByPositionXIsCorrectIfValidateWithVariableValue(POSITION_SIZE_FOR_FUTURE_TESTS_temp, i);

                    String temp4 = getTextFromExitPriceInPositionsTableRowByPositionX(i);
                    String temp5 = PrizmaUtils.removeSpecificSymbols(temp4);

                    double exitStartValue = POSITION_SIZE_FOR_FUTURE_TESTS_temp * Double.valueOf(temp5);
                    PrizmaUtils.logFailedTestResultsToTheLogsFile("exitStartValue = " + exitStartValue);
                    double exitStartValue_temp = PrizmaUtils.showOnlyTwoDigitAfterDecimal(exitStartValue);
                    PrizmaUtils.logFailedTestResultsToTheLogsFile("exitStartValue_temp = " + exitStartValue_temp);

                    double exitCommission = exitStartValue_temp * TRADING_FEE_FOR_FUTURE_TESTS;
                    PrizmaUtils.logFailedTestResultsToTheLogsFile("exitCommission = " + exitCommission);
                    double exitCommission_temp = PrizmaUtils.showOnlyTwoDigitAfterDecimal(exitCommission);
                    PrizmaUtils.logFailedTestResultsToTheLogsFile("exitCommission_temp = " + exitCommission_temp);

                    double exitEndValue = exitStartValue_temp - exitCommission_temp;
                    PrizmaUtils.logFailedTestResultsToTheLogsFile("exitEndValue = " + exitEndValue);
                    double exitEndValue_temp = PrizmaUtils.showOnlyTwoDigitAfterDecimal(exitEndValue);
                    PrizmaUtils.logFailedTestResultsToTheLogsFile("exitEndValue_temp = " + exitEndValue_temp);

                    verifyThatExitStartValueInPositionsTableRowByPositionXIsCorrect(exitStartValue_temp, i);
                    verifyThatExitCommissionValueInPositionsTableRowByPositionXIsCorrect(exitCommission_temp, i);
                    verifyThatExitEndValueInPositionsTableRowByPositionXIsCorrect(exitEndValue_temp, i);

                    double profitCurrencyValue = exitEndValue_temp - entryStartValue_temp;
                    PrizmaUtils.logFailedTestResultsToTheLogsFile("profitCurrencyValue = " + profitCurrencyValue);
                    double profitCurrencyValue_temp = PrizmaUtils.showOnlyTwoDigitAfterDecimal(profitCurrencyValue);
                    PrizmaUtils.logFailedTestResultsToTheLogsFile("profitCurrencyValue_temp = " + profitCurrencyValue_temp);

                    verifyThatProfitCurrencyInPositionsTableRowByPositionXIsCorrect(profitCurrencyValue_temp, i);

                    double profitCurrencyPercentage = profitCurrencyValue_temp * 100 / entryStartValue_temp;
                    PrizmaUtils.logFailedTestResultsToTheLogsFile("profitCurrencyPercentage = " + profitCurrencyPercentage);
                    double profitCurrencyPercentage_temp = PrizmaUtils.showOnlyTwoDigitAfterDecimal(profitCurrencyPercentage);
                    PrizmaUtils.logFailedTestResultsToTheLogsFile("profitCurrencyPercentage_temp = " + profitCurrencyPercentage_temp);

                    verifyThatProfitPercentageInPositionsTableRowByPositionXIsCorrect(profitCurrencyPercentage_temp, i);

                    INITIAL_BALANCE_VALUE_FOR_CALCULATION_FOR_FUTURE_TESTS = INITIAL_BALANCE_VALUE_FOR_CALCULATION_FOR_FUTURE_TESTS - entryStartValue_temp + exitEndValue;
                    PrizmaUtils.logFailedTestResultsToTheLogsFile("new_initial_balance = " + INITIAL_BALANCE_VALUE_FOR_CALCULATION_FOR_FUTURE_TESTS);
                } else {
                    String temp = getTextFromEntryPriceInPositionsTableRowByPositionX(i);
                    String temp2 = PrizmaUtils.removeSpecificSymbols(temp);

                    double entryStartValue = INITIAL_BALANCE_VALUE_FOR_CALCULATION_FOR_FUTURE_TESTS * PERCENTAGE_FROM_INITIAL_BALANCE_WITH_WHICH_BOT_COULD_TRADE_FOR_FUTURE_TESTS_TALIB_BOT;
                    PrizmaUtils.logFailedTestResultsToTheLogsFile("entryStartValue = " + entryStartValue);
                    double entryStartValue_temp = PrizmaUtils.showOnlyTwoDigitAfterDecimal(entryStartValue);
                    PrizmaUtils.logFailedTestResultsToTheLogsFile("entryStartValue_temp = " + entryStartValue_temp);

                    double entryCommission = entryStartValue * TRADING_FEE_FOR_FUTURE_TESTS / (1 + TRADING_FEE_FOR_FUTURE_TESTS);
                    PrizmaUtils.logFailedTestResultsToTheLogsFile("entryCommission = " + entryCommission);
                    double entryCommission_temp = PrizmaUtils.showOnlyTwoDigitAfterDecimal(entryCommission);
                    PrizmaUtils.logFailedTestResultsToTheLogsFile("entryCommission_temp = " + entryCommission_temp);

                    double entryEndValue = entryStartValue_temp - entryCommission_temp;
                    PrizmaUtils.logFailedTestResultsToTheLogsFile("entryEndValue = " + entryEndValue);
                    double entryEndValue_temp = PrizmaUtils.showOnlyTwoDigitAfterDecimal(entryEndValue);
                    PrizmaUtils.logFailedTestResultsToTheLogsFile("entryEndValue_temp = " + entryEndValue_temp);

                    verifyThatEntryStartValueInPositionsTableRowByPositionXIsCorrect(entryStartValue_temp, i);
                    verifyThatEntryCommissionValueInPositionsTableRowByPositionXIsCorrect(entryCommission_temp, i);
                    verifyThatEntryEndValueInPositionsTableRowByPositionXIsCorrect(entryEndValue_temp, i);

                    POSITION_SIZE_FOR_FUTURE_TESTS = entryEndValue_temp / Double.valueOf(temp2);
                    PrizmaUtils.logFailedTestResultsToTheLogsFile("size = " + POSITION_SIZE_FOR_FUTURE_TESTS);

                    double POSITION_SIZE_FOR_FUTURE_TESTS_temp = Double.valueOf(PrizmaUtils.cutSevenDigitAfterDecimal(POSITION_SIZE_FOR_FUTURE_TESTS));
                    PrizmaUtils.logFailedTestResultsToTheLogsFile("size = " + POSITION_SIZE_FOR_FUTURE_TESTS_temp);

                    verifyThatSizeInPositionsTableRowByPositionXIsCorrectIfValidateWithVariableValue(POSITION_SIZE_FOR_FUTURE_TESTS_temp, i);

                    double profitCurrencyValue = 0;
                    verifyThatProfitCurrencyInPositionsTableRowByPositionXIsCorrect(profitCurrencyValue, i);

                    double profitCurrencyPercentage = 0;
                    verifyThatProfitPercentageInPositionsTableRowByPositionXIsCorrect(profitCurrencyPercentage, i);
                }
            } else {
                String closedPositions = getTextFromEndDateInPositionsTableRowByPositionX(i);
                if (!closedPositions.contains("–")) {
                    String temp = getTextFromEntryPriceInPositionsTableRowByPositionX(i);
                    String temp2 = PrizmaUtils.removeSpecificSymbols(temp);

                    double entryStartValue = INITIAL_BALANCE_VALUE_FOR_CALCULATION_FOR_FUTURE_TESTS * PERCENTAGE_FROM_INITIAL_BALANCE_WITH_WHICH_BOT_COULD_TRADE_FOR_FUTURE_TESTS_TALIB_BOT;
                    double entryEndValue = entryStartValue / (1 + TRADING_FEE_FOR_FUTURE_TESTS);
                    double entryCommission = entryStartValue * TRADING_FEE_FOR_FUTURE_TESTS / (1 + TRADING_FEE_FOR_FUTURE_TESTS);

                    verifyThatEntryStartValueInPositionsTableRowByPositionXIsCorrect(entryStartValue, i);
                    verifyThatEntryCommissionValueInPositionsTableRowByPositionXIsCorrect(entryCommission, i);
                    verifyThatEntryEndValueInPositionsTableRowByPositionXIsCorrect(entryEndValue, i);

                    double tempValue = PrizmaUtils.showOnlyTwoDigitAfterDecimal(entryEndValue);
                    POSITION_SIZE_FOR_FUTURE_TESTS = tempValue / Double.valueOf(temp2);
                    verifyThatSizeInPositionsTableRowByPositionXIsCorrectIfValidateWithVariableValue(POSITION_SIZE_FOR_FUTURE_TESTS, i);

                    String temp4 = getTextFromExitPriceInPositionsTableRowByPositionX(i);
                    String temp5 = PrizmaUtils.removeSpecificSymbols(temp4);
                    double exitStartValue = POSITION_SIZE_FOR_FUTURE_TESTS * Double.valueOf(temp5);
                    double exitEndValue = exitStartValue / (1 + TRADING_FEE_FOR_FUTURE_TESTS);
                    double exitCommission = exitStartValue * TRADING_FEE_FOR_FUTURE_TESTS / (1 + TRADING_FEE_FOR_FUTURE_TESTS);

                    verifyThatExitStartValueInPositionsTableRowByPositionXIsCorrect(exitStartValue, i);
                    verifyThatExitCommissionValueInPositionsTableRowByPositionXIsCorrect(exitCommission, i);
                    verifyThatExitEndValueInPositionsTableRowByPositionXIsCorrect(exitEndValue, i);

                    double profitCurrencyValue = entryEndValue - exitEndValue;
                    verifyThatProfitCurrencyInPositionsTableRowByPositionXIsCorrect(profitCurrencyValue, i);

                    double profitCurrencyPercentage = profitCurrencyValue * 100 / entryStartValue;
                    verifyThatProfitPercentageInPositionsTableRowByPositionXIsCorrect(profitCurrencyPercentage, i);

                    INITIAL_BALANCE_VALUE_FOR_CALCULATION_FOR_FUTURE_TESTS = INITIAL_BALANCE_VALUE_FOR_CALCULATION_FOR_FUTURE_TESTS + entryEndValue - exitStartValue;
                } else {
                    String temp = getTextFromEntryPriceInPositionsTableRowByPositionX(i);
                    String temp2 = PrizmaUtils.removeSpecificSymbols(temp);

                    double entryStartValue = INITIAL_BALANCE_VALUE_FOR_CALCULATION_FOR_FUTURE_TESTS * PERCENTAGE_FROM_INITIAL_BALANCE_WITH_WHICH_BOT_COULD_TRADE_FOR_FUTURE_TESTS_TALIB_BOT;
                    double entryEndValue = entryStartValue / (1 + TRADING_FEE_FOR_FUTURE_TESTS);
                    double entryCommission = entryStartValue * TRADING_FEE_FOR_FUTURE_TESTS / (1 + TRADING_FEE_FOR_FUTURE_TESTS);

                    verifyThatEntryStartValueInPositionsTableRowByPositionXIsCorrect(entryStartValue, i);
                    verifyThatEntryCommissionValueInPositionsTableRowByPositionXIsCorrect(entryCommission, i);
                    verifyThatEntryEndValueInPositionsTableRowByPositionXIsCorrect(entryEndValue, i);


                    POSITION_SIZE_FOR_FUTURE_TESTS = entryEndValue / Double.valueOf(temp2);
                    verifyThatSizeInPositionsTableRowByPositionXIsCorrectIfValidateWithVariableValue(POSITION_SIZE_FOR_FUTURE_TESTS, i);

                    double profitCurrencyValue = 0;
                    verifyThatProfitCurrencyInPositionsTableRowByPositionXIsCorrect(profitCurrencyValue, i);

                    double profitCurrencyPercentage = 0;
                    verifyThatProfitPercentageInPositionsTableRowByPositionXIsCorrect(profitCurrencyPercentage, i);
                }
            }

            PrizmaUtils.logFailedTestResultsToTheLogsFile("----------------");
            PrizmaDriver.getScreenshot(fileName + "_" + i);
        }
    }

    public static void getAndCalculateAndValidateAllValuesInPositionTableWhichShouldBeEmpty(String fileName) {
        int count = getPositionsTableCount();

        if (count == 0) {
            PrizmaUtils.logFailedTestResultsToTheLogsFile("----------------");
            PrizmaUtils.logFailedTestResultsToTheLogsFile("Position count = " + count);
            PrizmaUtils.logFailedTestResultsToTheLogsFile("----------------");
            PrizmaDriver.getScreenshot(fileName);
        } else {
            PrizmaUtils.logFailedTestResultsToTheLogsFile("Looks like that something when wrong Position table was no empty when should be empty. Check video and logs, or something was changed on Prizma SDK side.");
        }
    }

    public static void getAndCalculateAndValidateAllValuesInPositionTableWhichShouldBeNotEmpty(String fileName) {
        int count = getPositionsTableCount();

        if (count > 0) {
            PrizmaUtils.logFailedTestResultsToTheLogsFile("----------------");
            PrizmaUtils.logFailedTestResultsToTheLogsFile("Position count = " + count);
            PrizmaUtils.logFailedTestResultsToTheLogsFile("----------------");
            PrizmaDriver.getScreenshot(fileName);
        } else {
            PrizmaUtils.logFailedTestResultsToTheLogsFile("Looks like that something when wrong Position table was empty when should be not empty. Check video and logs, or something was changed on Prizma SDK side.");
        }
    }
}
